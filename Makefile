# Project Directories
SRC 		:= src
BLD 		:= bld
TST			:= test
DEPDIR 		:= $(BLD)/deps
DOC_BLD     := $(BLD)/docs
PRJ_NAME 	:= $(shell basename $(CURDIR))

# Application Files
C_SOURCE_FILES 			:= $(sort $(shell find ./$(SRC) -type f -name *.c))
C_SOURCE_FILES_BASE 	:= $(notdir $(C_SOURCE_FILES))
C_OBJECT_FILES 			:= $(addprefix $(BLD)/, $(patsubst %.c, %.o, $(C_SOURCE_FILES_BASE)))
C_DEP_FILES 			:= $(addprefix $(DEPDIR)/, $(patsubst %.c, %.d, $(C_SOURCE_FILES_BASE)))

CXX_SOURCE_FILES 		:= $(sort $(shell find ./$(SRC) -type f -name *.cpp))
CXX_SOURCE_FILES_BASE 	:= $(notdir $(CXX_SOURCE_FILES))
CXX_OBJECT_FILES 		:= $(addprefix $(BLD)/, $(patsubst %.cpp, %.o, $(CXX_SOURCE_FILES_BASE)))
CXX_DEP_FILES 			:= $(addprefix $(DEPDIR)/, $(patsubst %.cpp, %.d, $(CXX_SOURCE_FILES_BASE)))

SOURCE_FILES			:= $(C_SOURCE_FILES) $(CXX_SOURCE_FILES)
SOURCE_FILES_BASE		:= $(C_SOURCE_FILES_BASE) $(CXX_SOURCE_FILES_BASE)
OBJECT_FILES			:= $(sort $(C_OBJECT_FILES) $(CXX_OBJECT_FILES))
SOURCE_DIRS				:= $(sort $(shell find ./$(SRC)/ -type d))
DEP_FILES				:= $(C_DEP_FILES) $(CXX_DEP_FILES)

# Test Files
C_TEST_SOURCE_FILES 		:= $(sort $(shell find ./$(TST) -type f -name *.c) $(filter-out */main.c, $(C_SOURCE_FILES)))
C_TEST_SOURCE_FILES_BASE 	:= $(notdir $(C_TEST_SOURCE_FILES))
C_TEST_OBJECT_FILES 		:= $(sort $(addprefix $(BLD)/, $(patsubst %.c, %.o, $(C_TEST_SOURCE_FILES_BASE))))
C_TEST_DEP_FILES 			:= $(addprefix $(DEPDIR)/, $(patsubst %.c, %.d, $(C_TEST_SOURCE_FILES_BASE)))

CXX_TEST_SOURCE_FILES 		:= $(sort $(shell find ./$(TST) -type f -name *.cpp) $(filter-out */main.cpp, $(CXX_SOURCE_FILES)))
CXX_TEST_SOURCE_FILES_BASE 	:= $(notdir $(CXX_TEST_SOURCE_FILES))
CXX_TEST_OBJECT_FILES 		:= $(sort $(addprefix $(BLD)/, $(patsubst %.cpp, %.o, $(CXX_TEST_SOURCE_FILES_BASE))))
CXX_TEST_DEP_FILES 			:= $(addprefix $(DEPDIR)/, $(patsubst %.cpp, %.d, $(CXX_TEST_SOURCE_FILES_BASE)))

TEST_OBJECT_FILES			:= $(filter-out $(BLD)/main.o ,$(C_TEST_OBJECT_FILES) $(CXX_TEST_OBJECT_FILES) $(OBJECT_FILES))
TEST_SOURCE_DIRS			:= $(sort $(shell find ./$(TST)/ -type d))
DEP_FILES					+= $(C_TEST_DEP_FILES) $(CXX_TEST_DEP_FILES) 

INCLUDE_DIRS 	:= $(addprefix -I, $(SOURCE_DIRS)) $(addprefix -I, $(TEST_SOURCE_DIRS))
VPATH = $(SOURCE_DIRS) $(TEST_SOURCE_DIRS)

CC 		    := gcc
CXX		    := g++
ASM_CMD     := objdump
IMG_CMD     := objcopy
SPHINXBUILD := sphinx-build

ifdef COV
COV_FLAGS := -fprofile-arcs -ftest-coverage -fPIC
OPT       := -O0 -g
else
COV_FLAGS :=
OPT       := -O3 -g
endif

WARN			:= -Wall
LINK			:= -lglfw -lGL -lpthread -lasound -lssl -lcrypto
CFLAGS 			:= $(INCLUDE_DIRS) $(OPT) $(WARN) $(COV_FLAGS)
DEPFLAGS		= -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d

# Output Files
EXE := $(PRJ_NAME)
IMG := $(PRJ_NAME).img
GTS := $(PRJ_NAME).test

# Surpress stdout
M	:= @

all: $(EXE) $(GTS)

exe: $(EXE)

img: $(IMG)

buildtest: $(GTS)

test: $(GTS)
	./$(GTS)

coverage: $(GTS)
	gcovr -r . --html-details -o $(BLD)/report.html

doc:
	make -C docs/ latexpdf
	cp docs/build/latex/PowerEmulator.pdf ./

stat:
	$(M)echo Main Source Files
	$(M)echo $(SOURCE_FILES)
	$(M)echo Test Source Files
	$(M)echo $(TEST_SOURCE_FILES)
	$(M)echo Main OBJECT Files
	$(M)echo $(OBJECT_FILES)
	$(M)echo Test OBJECT Files
	$(M)echo $(TEST_OBJECT_FILES)

clean:
	$(M)echo Cleaning
	$(M)rm -rf $(BLD)/*.o $(EXE) $(GTS)

cleanall:
	$(M)echo Cleaning all
	$(M)rm -rf $(BLD)/* $(EXE) $(GTS)	

remake: clean all

format:
	$(M)clang-format -i ./$(SRC)/**/* ./$(TST)/**/*

$(EXE): $(OBJECT_FILES)
	$(M)echo Linking
	$(M)$(CXX) $(CFLAGS) -o $@ $^ $(LINK)

$(IMG): $(EXE)
	$(M)echo Building Image
	$(M)$(IMG_CMD) -Obinary $^ $@

$(GTS): $(TEST_OBJECT_FILES)
	$(M)echo Linking Test 
	$(M)$(CXX) $(CFLAGS) -o $@ $^ $(LINK) -lgtest -lgmock 

# Generic Rules
$(BLD)/%.o: %.c %.h $(DEPDIR)/%.d | $(DEPDIR)
	$(M)echo Compiling $(@F)
	$(M)$(CC) $(DEPFLAGS) $(CFLAGS) -o $@ -c $<

$(BLD)/%.o: %.c $(DEPDIR)/%.d | $(DEPDIR)
	$(M)echo Compiling $(@F)
	$(M)$(CC) $(DEPFLAGS) $(CFLAGS) -o $@ -c $<

$(BLD)/%.o: %.cpp %.h $(DEPDIR)/%.d | $(DEPDIR)
	$(M)echo Compiling $(@F)
	$(M)$(CXX) $(DEPFLAGS) $(CFLAGS) -o $@ -c $<

$(BLD)/%.o: %.cpp $(DEPDIR)/%.d | $(DEPDIR)
	$(M)echo Compiling $(@F)
	$(M)$(CXX) $(DEPFLAGS) $(CFLAGS) -o $@ -c $<

.PHONY: all exe img clean remake test

$(DEP_FILES):

$(TEST_DEP_FILES):

$(SRC):
	mkdir $@

$(BLD):
	mkdir $@

$(DEPDIR): | $(BLD)
	mkdir $@


remake:
	make clean; make

include $(wildcard $(DEP_FILES))
include $(wildcard $(TEST_DEP_FILES))
