#!/usr/bin/python3

import sys
import os

if(len(sys.argv) != 3):
    print("Usage: {0} module namespace".format(sys.argv[0]))
    exit(-1)

module_name = sys.argv[1].lower()
class_name = module_name.capitalize()
namespace = sys.argv[2]

main_dir = "./src/{0}/".format(module_name)
test_dir = "./test/{0}/".format(module_name)

# Create the main source directory
os.system("mkdir " + main_dir)
os.system("mkdir " + test_dir)

# Create the main header
main_header = """#ifndef {0}_H
#define {0}_H
#include <{3}_types_data.h>
#include <{3}_const.h>

namespace {1} {{
    
class {2} {{
    public:
    {2}(void);
    
    private:

}};

}}

#endif
""".format(module_name.upper(), namespace, class_name, module_name)

types_data_header = """#ifndef {0}_TYPES_DATA_H
#define {0}_TYPES_DATA_H
#include <{2}_const.h>

namespace {1} {{

}}

#endif
""".format(module_name.upper(), namespace, module_name)

const_header = """#ifndef {0}_CONST_H
#define {0}_CONST_H

namespace {1} {{

}}

#endif
""".format(module_name.upper(), namespace)

main_source = """#include <{0}.h>

namespace {1} {{

{2}::{2}(void){{

}}

}}
""".format(module_name, namespace, class_name)

test_mock_header = """#ifndef {0}_MOCK_H
#define {0}_MOCK_H
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <{3}.h>

namespace Mocks {{
    
class {2} : public {1}::{2}{{
    public:
    {2}(void){{}}
    
    private:

}};

}}

#endif
""".format(module_name.upper(), namespace, class_name, module_name)


with open(main_dir + "{0}.h".format(module_name), 'w') as fh:
    fh.write(main_header)

with open(main_dir + "{0}_types_data.h".format(module_name), 'w') as fh:
    fh.write(types_data_header)

with open(main_dir + "{0}_const.h".format(module_name), 'w') as fh:
    fh.write(const_header)

with open(main_dir + "{0}.cpp".format(module_name), 'w') as fh:
    fh.write(main_source)

with open(test_dir + "{0}_mock.h".format(module_name), 'w') as fh:
    fh.write(test_mock_header)


