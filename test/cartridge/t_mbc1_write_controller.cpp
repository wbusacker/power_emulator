#include <gtest/gtest.h>
#include <mbc1.h>

TEST(MBC1, NoSwapRam_SelectAllROMBanks) {

    Cart::MBC1 controller;

    /* Cycle through all 128 possible banks */

    for (uint16_t i = 0; i < 128; i++) {

        /* The lower 5 bits of the bank number go into the 0x2000 range */
        controller.write_controller(0x2000, i & 0b11111);

        /* Upper 2 bits, shifted down, go into 4000 */
        controller.write_controller(0x4000, i >> 5);

        /* Special case for bank 0 */
        if (i == 0) {
            EXPECT_EQ(controller.get_active_rom_bank(), 1);
        } else {
            EXPECT_EQ(controller.get_active_rom_bank(), i);
        }
    }
}

TEST(MBC1, NoSwapRam_BoundedROMBankNumbers) {
    Cart::MBC1 controller;

    for (uint16_t i = 0; i < 256; i++) {
        for (uint16_t j = 0; j < 256; j++) {
            controller.write_controller(0x2000, i);
            controller.write_controller(0x4000, j);

            EXPECT_GT(controller.get_active_rom_bank(), 0);
            EXPECT_LE(controller.get_active_rom_bank(), 128);
        }
    }
}

TEST(MBC1, SwapRam_SelectAllROMBanks) {

    Cart::MBC1 controller;

    /* Enable swappable memory */
    controller.write_controller(0x6000, 1);

    /* Cycle through all 128 possible banks */

    for (uint16_t i = 0; i < 32; i++) {

        /* The lower 5 bits of the bank number go into the 0x2000 range */
        controller.write_controller(0x2000, i & 0b11111);

        /* Special case for bank 0 */
        if (i == 0) {
            EXPECT_EQ(controller.get_active_rom_bank(), 1);
        } else {
            EXPECT_EQ(controller.get_active_rom_bank(), i);
        }
    }
}

TEST(MBC1, SwapRam_BoundedROMBankNumbers) {
    Cart::MBC1 controller;

    /* Enable swappable memory */
    controller.write_controller(0x6000, 1);

    for (uint16_t i = 0; i < 256; i++) {
        for (uint16_t j = 0; j < 256; j++) {
            controller.write_controller(0x2000, i);
            controller.write_controller(0x4000, j);

            EXPECT_GT(controller.get_active_rom_bank(), 0);
            EXPECT_LE(controller.get_active_rom_bank(), 32);
        }
    }
}

TEST(MBC1, NoSwapRAM_OnlyOneRAMBank) {
    Cart::MBC1 controller;

    for (uint16_t i = 0; i < 256; i++) {
        controller.write_controller(0x4000, i);
        EXPECT_EQ(controller.get_active_ram_bank(), 0);
    }
}

TEST(MBC1, SwapRAM_FourRAMBank) {
    Cart::MBC1 controller;
    controller.write_controller(0x6000, 1);

    for (uint16_t i = 0; i < 256; i++) {
        controller.write_controller(0x4000, i);
        EXPECT_EQ(controller.get_active_ram_bank(), i % 4);
    }
}

TEST(MBC1, ToggleRAMEnable) {

    Cart::MBC1 controller;

    EXPECT_TRUE(controller.is_ram_enabled());

    controller.write_controller(0x0000, 0x00);
    EXPECT_FALSE(controller.is_ram_enabled());

    controller.write_controller(0x0000, 0x0A);
    EXPECT_TRUE(controller.is_ram_enabled());
}

TEST(MBC1, ROMSelectDowngradedWhenToggleSwapRAM) {

    Cart::MBC1 controller;

    controller.write_controller(0x2000, 0x1F);
    controller.write_controller(0x4000, 3);

    EXPECT_EQ(controller.get_active_rom_bank(), 127);

    controller.write_controller(0x6000, 1);
    EXPECT_EQ(controller.get_active_rom_bank(), 0x1F);

    controller.write_controller(0x6000, 0);
    EXPECT_EQ(controller.get_active_rom_bank(), 127);
}
