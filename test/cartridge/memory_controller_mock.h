#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <mbc1.h>

namespace Mocks {

class Memory_controller : public Cart::Gameboy::Memory_controller {
    public:
    Memory_controller(void){

    };

    MOCK_METHOD(uint8_t, get_active_rom_bank, (), (override));
    MOCK_METHOD(uint8_t, get_active_ram_bank, (), (override));
    MOCK_METHOD(bool, is_ram_enabled, (), (override));
};

}    // namespace Mocks