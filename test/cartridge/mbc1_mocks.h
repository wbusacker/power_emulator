#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <mbc1.h>

namespace Mocks {

class MBC1 : public Cart::MBC1 {
    public:
    MBC1(void){

    };

    MOCK_METHOD(uint8_t, read, (uint16_t), (override));
    MOCK_METHOD(void, write, (uint16_t, uint8_t), (override));
};

}    // namespace Mocks