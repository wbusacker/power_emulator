#include <cartridge.h>
#include <gtest/gtest.h>

using namespace testing;

const char* const TEST_FILE_NAME   = "GAMEBOY_CARTRIDGE_INIT_TEST.gb";
const uint16_t    TEST_FILE_LENGTH = 0x200;

class Gameboy_cartridge_init_test : public Test {

    public:
    Gameboy_cartridge_init_test() { }
    void SetUp() {

        /* Clear all of the data within the file data */
        memset(temp_file_data, 0, TEST_FILE_LENGTH);
    }

    uint8_t temp_file_data[TEST_FILE_LENGTH];
};

TEST_F(Gameboy_cartridge_init_test, MBC1_Small_RAM_Mode) {
    /* Set the needed bits for the MBC1 Small RAM Mode*/

    temp_file_data[0x147] = 1;
    temp_file_data[0x148] = 6; /* 2Mbyte ROM */
    temp_file_data[0x149] = 2; /* 8Kbyte RAM */

    /* Write the file out locally to test with */

    FILE* fp = fopen(TEST_FILE_NAME, "wb");
    fwrite(temp_file_data, 1, TEST_FILE_LENGTH, fp);
    fclose(fp);

    /* Run the constructor */
    Cart::Gameboy_cartridge cart(TEST_FILE_NAME);

    /* Check to make sure that the number of banks reported are correct */
    EXPECT_EQ(cart.get_cart_type(), 1);
    EXPECT_EQ(cart.get_rom_size(), 128);
    EXPECT_EQ(cart.get_ram_size(), 1);

    remove(TEST_FILE_NAME);
}

TEST_F(Gameboy_cartridge_init_test, MBC1_Large_RAM_Mode) {
    /* Set the needed bits for the MBC1 Large RAM Mode*/

    temp_file_data[0x147] = 1;
    temp_file_data[0x148] = 4; /* 512Kyte ROM */
    temp_file_data[0x149] = 3; /* 32Kbyte RAM */

    /* Write the file out locally to test with */

    FILE* fp = fopen(TEST_FILE_NAME, "wb");
    fwrite(temp_file_data, 1, TEST_FILE_LENGTH, fp);
    fclose(fp);

    /* Run the constructor */
    Cart::Gameboy_cartridge cart(TEST_FILE_NAME);

    /* Check to make sure that the number of banks reported are correct */
    EXPECT_EQ(cart.get_cart_type(), 1);
    EXPECT_EQ(cart.get_rom_size(), 32);
    EXPECT_EQ(cart.get_ram_size(), 4);

    remove(TEST_FILE_NAME);
}