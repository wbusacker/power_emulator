#include <cartridge.h>
#include <gtest/gtest.h>
#include <memory_controller_mock.h>

const char* const TEST_FILE_NAME = "GB_CARTRIDGE_RAM_TEST.gb";
const uint32_t    TEST_FILE_SIZE = 2 * 1024 * 1024;

class GB_cartridge_ram_test : public ::testing::Test {
    public:
    GB_cartridge_ram_test(void) { }

    void SetUp() {
        /* Create the file for the test to load in */
        FILE* fp = fopen(TEST_FILE_NAME, "w");

        /* Create the data to write into the file */
        uint8_t* temp_file_data;
        temp_file_data = (uint8_t*)(malloc(TEST_FILE_SIZE));

        /* Fill in the data
            The pattern is a 4 byte pattern of
            8bits 8bits        16bits
            BANK# Fill Pattern Index Number
        */
        uint32_t addr = 0;
        for (uint8_t bank = 0; bank < 128; bank++) {
            for (uint16_t index = 0; index < 4096; index++) {
                temp_file_data[addr++] = bank;
                temp_file_data[addr++] = 0x77;
                temp_file_data[addr++] = (index >> 8) & 0xFF;
                temp_file_data[addr++] = (index)&0xFF;
            }
        }

        /* Overwrit the metadata for the fake cartridge */
        temp_file_data[0x147] = 1; /* MBC1 Type  */
        temp_file_data[0x148] = 6; /* 2Mbyte ROM */
        temp_file_data[0x149] = 3; /* 8Kbyte RAM */

        uint8_t* bfr_ptr;
        bfr_ptr = temp_file_data;

        fwrite(bfr_ptr, sizeof(uint8_t), TEST_FILE_SIZE, fp);

        fclose(fp);

        free(temp_file_data);

        cart       = new Cart::Gameboy_cartridge(TEST_FILE_NAME);
        controller = new Mocks::Memory_controller;

        cart->override_dependencies(controller);
    }

    void TearDown() {

        remove(TEST_FILE_NAME);

        delete cart;
        /* We don't need to delete the controller here since cartridge normally does it */
    }

    Cart::Gameboy_cartridge*  cart;
    Mocks::Memory_controller* controller;
};

TEST_F(GB_cartridge_ram_test, RAM_ENABLED) {

    EXPECT_CALL(*controller, is_ram_enabled()).Times(3).WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*controller, get_active_ram_bank()).Times(3).WillRepeatedly(::testing::Return(0));

    /* Flip the bits in the ram location to ensure proper writing */
    uint8_t value = ~cart->read(0xC000);
    cart->write(0xC000, value);
    EXPECT_EQ(cart->read(0xC000), value);
}

/*
    We should read, write, and read back and see that we get zero every time
    when RAM is disabled, then when the RAM is enabled again, make sure the
    write didn't actually go through
*/
TEST_F(GB_cartridge_ram_test, RAM_DISABLED) {

    EXPECT_CALL(*controller, is_ram_enabled())
        .Times(5)
        .WillOnce(::testing::Return(true))
        .WillOnce(::testing::Return(false))
        .WillOnce(::testing::Return(false))
        .WillOnce(::testing::Return(false))
        .WillOnce(::testing::Return(true));

    EXPECT_CALL(*controller, get_active_ram_bank()).Times(2).WillRepeatedly(::testing::Return(0));

    /* Write a legitimate value into the area */
    cart->write(0xC000, 0x77);

    /* When we read, we should be back zero despite the actual value */
    EXPECT_EQ(cart->read(0xC000), 0);

    cart->write(0xC000, 0xAA);

    EXPECT_EQ(cart->read(0xC000), 0);

    /* When we read this time, it we should read the actual value */
    EXPECT_EQ(cart->read(0xC000), 0x77);
}

/* Write a value into Bank 0, switch to Bank 1 write into the same address
    and ensure that it wrote in. Then switch back to bank 0 and read the
    original set value
*/
TEST_F(GB_cartridge_ram_test, RAM_SWAP) {

    EXPECT_CALL(*controller, is_ram_enabled()).Times(4).WillRepeatedly(::testing::Return(true));

    EXPECT_CALL(*controller, get_active_ram_bank())
        .Times(4)
        .WillOnce(::testing::Return(0))
        .WillOnce(::testing::Return(1))
        .WillOnce(::testing::Return(1))
        .WillOnce(::testing::Return(0));

    cart->write(0xC000, 0x77);
    cart->write(0xC000, 0xAA);

    EXPECT_EQ(cart->read(0xC000), 0xAA);
    EXPECT_EQ(cart->read(0xC000), 0x77);
}