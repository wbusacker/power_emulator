#include <cartridge.h>
#include <gtest/gtest.h>
#include <memory_controller_mock.h>

const char* const TEST_FILE_NAME = "GB_CARTRIDGE_ROM_TEST.gb";
const uint32_t    TEST_FILE_SIZE = 2 * 1024 * 1024;

class GB_cartridge_rom_test : public ::testing::Test {
    public:
    GB_cartridge_rom_test(void) { }

    void SetUp() {
        /* Create the file for the test to load in */
        FILE* fp = fopen(TEST_FILE_NAME, "w");

        /* Create the data to write into the file */
        uint8_t* temp_file_data;
        temp_file_data = (uint8_t*)(malloc(TEST_FILE_SIZE));

        /* Fill in the data
            The pattern is a 4 byte pattern of
            8bits 8bits        16bits
            BANK# Fill Pattern Index Number
        */
        uint32_t addr = 0;
        for (uint8_t bank = 0; bank < 128; bank++) {
            for (uint16_t index = 0; index < 4096; index++) {
                temp_file_data[addr++] = bank;
                temp_file_data[addr++] = 0x77;
                temp_file_data[addr++] = (index >> 8) & 0xFF;
                temp_file_data[addr++] = (index)&0xFF;
            }
        }

        /* Overwrite the metadata for the fake cartridge */
        temp_file_data[0x147] = 1; /* MBC1 Type */
        temp_file_data[0x148] = 6; /* 2Mbyte ROM */
        temp_file_data[0x149] = 2; /* 8Kbyte RAM */

        fwrite(temp_file_data, sizeof(uint8_t), TEST_FILE_SIZE, fp);

        fclose(fp);

        free(temp_file_data);

        cart       = new Cart::Gameboy_cartridge(TEST_FILE_NAME);
        controller = new Mocks::Memory_controller;

        cart->override_dependencies(controller);
    }

    void TearDown() {

        remove(TEST_FILE_NAME);

        delete cart;
        /* We don't need to delete the controller here since cartridge normally does it */
    }

    Cart::Gameboy_cartridge*  cart;
    Mocks::Memory_controller* controller;
};

TEST_F(GB_cartridge_rom_test, ROM_BANK0_ACCESS) {

    EXPECT_CALL(*controller, get_active_rom_bank()).Times(0);

    uint8_t  values[4];
    uint16_t addr = 0;
    for (uint16_t index = 0; index < 0x1000; index++) {
        for (uint8_t byte = 0; byte < 4; byte++) {
            values[byte] = cart->read(addr++);
        }

        /* The cartridge meta data overrides this bank, so for indexes 81 and 82 this will
            provide bad results, so skip those */
        if ((index != 81) && (index != 82)) {

            EXPECT_EQ(values[0], 0);
            EXPECT_EQ(values[1], 0x77);
            uint16_t read_index = (static_cast<uint16_t>(values[2]) << 8) | values[3];
            EXPECT_EQ(read_index, index);
        }
    }
}

/* It takes far too much runtime to have every single bank have
    all of its memory addresses checked, so just check the first
    little bit and move on
*/
TEST_F(GB_cartridge_rom_test, SWAP_ROM_BANK_ACCESS) {

    ::testing::InSequence sequence;

    /* Setup test order expectations */
    for (uint16_t bank = 1; bank < 128; bank++) {
        for (uint16_t i = 0; i < 4; i++) {
            for (uint8_t byte = 0; byte < 4; byte++) {
                EXPECT_CALL(*controller, get_active_rom_bank()).WillOnce(::testing::Return(bank)).RetiresOnSaturation();
            }
        }
    }

    uint8_t values[4];
    for (uint16_t bank = 1; bank < 128; bank++) {
        uint16_t addr = 0x4000;
        for (uint16_t index = 0; index < 4; index++) {
            for (uint8_t byte = 0; byte < 4; byte++) {
                values[byte] = cart->read(addr++);
            }

            EXPECT_EQ(values[0], bank);
            EXPECT_EQ(values[1], 0x77);
            uint16_t read_index = (static_cast<uint16_t>(values[2]) << 8) | values[3];
            EXPECT_EQ(read_index, index);
        }
    }
}
