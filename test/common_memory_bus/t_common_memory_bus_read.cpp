#include <common_memory_bus_mocks.h>

using namespace testing;

class Common_memory_bus_read : public Test {

    public:
    Common_memory_bus_read() { }

    Common_memory::Common_memory_bus bus;

    Mocks::CMB_interface device;
};

TEST_F(Common_memory_bus_read, WhenAddressInRange_ExpectSingleByte) {

    bus.register_memory_module(&device, 0, 0x1000);

    EXPECT_CALL(device, read(0xA0)).Times(1).WillOnce(Return(0x77));

    EXPECT_EQ(bus.read(0xA0), 0x77);
}

TEST_F(Common_memory_bus_read, WhenAddressOutOfRange_ExpectZero) {

    bus.register_memory_module(&device, 0, 0x1000);

    EXPECT_CALL(device, read(_)).Times(0);

    EXPECT_EQ(bus.read(0xA000), 0);
}
