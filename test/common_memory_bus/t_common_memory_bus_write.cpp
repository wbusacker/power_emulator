#include <common_memory_bus_mocks.h>

using namespace testing;

class Common_memory_bus_write : public Test {

    public:
    Common_memory_bus_write() { }

    Common_memory::Common_memory_bus bus;

    Mocks::CMB_interface device;
};

TEST_F(Common_memory_bus_write, WhenAddressInRange_ExpectSingleByte) {

    bus.register_memory_module(&device, 0, 0x1000);

    EXPECT_CALL(device, write(0xA0, 0x77)).Times(1);

    bus.write(0xA0, 0x77);
}

TEST_F(Common_memory_bus_write, WhenAddressOutOfRange_ExpectZero) {

    bus.register_memory_module(&device, 0, 0x1000);

    EXPECT_CALL(device, write(_, _)).Times(0);

    bus.write(0xA000, 0x77);
}