#include <common_memory_bus_mocks.h>

using namespace testing;

class Common_memory_bus_register_memory_module : public Test {
    public:
    Common_memory_bus_register_memory_module() { }

    Common_memory::Common_memory_bus bus;
    Mocks::CMB_interface             device;
};

TEST_F(Common_memory_bus_register_memory_module, WhenOneModuleRegistered_ExpectSucces) {

    EXPECT_EQ(Common_memory::SUCCESS, bus.register_memory_module(&device, 0, 0xFFFF));
}

TEST_F(Common_memory_bus_register_memory_module, WhenMaxModuleRegistered_ExpectSucces) {

    for (uint8_t i = 0; i < 128; i++) {
        ASSERT_EQ(Common_memory::SUCCESS, bus.register_memory_module(&device, i * 10, (i + 1) * 10));
    }
}
