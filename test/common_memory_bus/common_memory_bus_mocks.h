#ifndef COMMON_MEMORY_BUS_MOCKS_H
#define COMMON_MEMORY_BUS_MOCKS_H

#include <common_memory_bus.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace Mocks {

class CMB_interface : public Common_memory::CMB_interface {
    public:
    CMB_interface(void){

    };

    MOCK_METHOD(void, cycle, (), (override));
    MOCK_METHOD(uint8_t, read, (uint16_t), (override));
    MOCK_METHOD(void, write, (uint16_t, uint8_t), (override));
};

class Common_memory_bus : public Common_memory::Common_memory_bus {
    public:
    Common_memory_bus(void) { }

    MOCK_METHOD(uint8_t, read, (uint16_t), (override));
    MOCK_METHOD(void, write, (uint16_t, uint8_t), (override));
};

}    // namespace Mocks

#endif