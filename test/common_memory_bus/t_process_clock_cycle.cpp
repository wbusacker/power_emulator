#include <common_memory_bus_mocks.h>

using namespace testing;

class Common_memory_bus_process_clock_cycle : public Test {

    public:
    Common_memory_bus_process_clock_cycle() : bus(1000000) { }

    Common_memory::Common_memory_bus bus;

    Mocks::CMB_interface device;
};

TEST_F(Common_memory_bus_process_clock_cycle, WhenRegistered_ExpectCall) {

    bus.register_cycle_module(&device);

    EXPECT_CALL(device, cycle()).Times(1);

    bus.process_clock_cycle();
}

TEST_F(Common_memory_bus_process_clock_cycle, WhenCycling_ExpectSlowDown) {

    /* Estimate how long the test will run for

        When running at exactly 1MHz, 100K clock cycles should take
        around 100 milliseconds to execute. Because there is slop in the sync
        of around +- 10ms, expect the time delta to be between 40ms and 60ms
    */

    /* Calculate the time before we go into the loop */
    timespec timer_get;
    clock_gettime(CLOCK_MONOTONIC, &timer_get);

    double start_time = timer_get.tv_sec;
    start_time += timer_get.tv_nsec / static_cast<double>(1E9);

    for (uint64_t i = 0; i < 100000; i++) {
        bus.process_clock_cycle();
    }

    /* Calculate the time it took */
    clock_gettime(CLOCK_MONOTONIC, &timer_get);

    double stop_time = timer_get.tv_sec;
    stop_time += timer_get.tv_nsec / static_cast<double>(1E9);

    double delta = stop_time - start_time;

    EXPECT_LE(0.09, delta);
    EXPECT_GE(0.11, delta);
}