#include <common_memory_bus_mocks.h>
#include <ram_block.h>

using namespace testing;

class Common_memory_bus : public Test {

    public:
    Common_memory_bus() { }

    Common_memory::Common_memory_bus bus;

    Mocks::CMB_interface device;
};

TEST_F(Common_memory_bus, read_write_memory_range) {

    RAM::Ram_block* block = new RAM::Ram_block(0, 65535);
    bus.register_memory_module(block, 0, 0xFFFF);
    bus.register_memory_module(block, 0xFFFF);

    for (uint32_t i = 0; i < 65536; i++) {
        EXPECT_EQ(bus.read(i), 0);
        bus.write(i, i % 256);
        EXPECT_EQ(bus.read(i), i % 256);
    }
}