#ifndef FUNCTION_TRACER_MOCK_H
#define FUNCTION_TRACER_MOCK_H
#include <function_tracer.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace Mocks {

class Function_tracer : public Debug::Function_tracer {
    public:
    Function_tracer(void) { }

    private:
};

}    // namespace Mocks

#endif
