#include <common_memory_bus_mocks.h>
#include <cpu_mock.h>
#include <gtest/gtest.h>
#include <picture_processing_unit.h>
#include <window_manager_mock.h>

using namespace testing;

class Gameboy_PPU_interpret_tile_map_indexing : public ::testing::TestWithParam<uint16_t> {
    public:
    Gameboy_PPU_interpret_tile_map_indexing(void) : cpu(&memory) {
        for (uint8_t i = 0; i < 16; i++) {
            pattern[i] = 0;
        }

        disp = new Mocks::Window_manager(144, 160, 1, true, "Test");

        ppu = new Display::Gameboy_picture_processing_unit(&memory, disp, &cpu);
    }

    ~Gameboy_PPU_interpret_tile_map_indexing() {
        delete ppu;
        delete disp;
    }

    void SetUp(void) {

        row_index             = GetParam();
        uint8_t pattern_index = row_index * 2;

        /* Fill up the pattern data based on what we want at the end

            Final Pattern
            0  1  2  3  2  3  0  1

            Decode Step
            0  1  2  3  2  3  0  1
            00 01 10 11 10 11 00 01

            Seprate bytes
            0  0  1  1  1  1  0  0
             0  1  0  1  0  1  0  1

            00111100
            01010101

        */

        pattern[pattern_index]     = 0x55;
        pattern[pattern_index + 1] = 0x3C;
    }

    Mocks::Window_manager*   disp;
    Mocks::Common_memory_bus memory;
    Mocks::Gameboy_cpu       cpu;

    Display::Gameboy_picture_processing_unit* ppu;
    uint8_t                                   pattern[16];
    uint16_t                                  row_index;
};

TEST_P(Gameboy_PPU_interpret_tile_map_indexing, Render_row) {

    uint8_t pixels[8];

    ppu->interpret_tile(pattern, row_index, pixels);

    EXPECT_EQ(pixels[0], 0);
    EXPECT_EQ(pixels[1], 1);
    EXPECT_EQ(pixels[2], 2);
    EXPECT_EQ(pixels[3], 3);
    EXPECT_EQ(pixels[4], 2);
    EXPECT_EQ(pixels[5], 3);
    EXPECT_EQ(pixels[6], 0);
    EXPECT_EQ(pixels[7], 1);
}

INSTANTIATE_TEST_CASE_P(Gameboy_PPU_interpret_tile_map_indexing,
                        Gameboy_PPU_interpret_tile_map_indexing,
                        testing::Values(0, 1, 2, 3, 4, 5, 6, 7));