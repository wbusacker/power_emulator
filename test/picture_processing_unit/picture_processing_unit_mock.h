#ifndef GAMEBOY_PICTURE_PROCESSING_UNIT_MOCK_H
#define GAMEBOY_PICTURE_PROCESSING_UNIT_MOCK_H
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <picture_processing_unit.h>

namespace Mocks {

class Gameboy_picture_processing_unit : public Display::Gameboy_picture_processing_unit {
    public:
    Gameboy_picture_processing_unit(void) { }

    private:
};

}    // namespace Mocks

#endif
