#ifndef GAMEBOY_DISPLAY_MOCK_H
#define GAMEBOY_DISPLAY_MOCK_H
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <window_manager.h>

namespace Mocks {

class Window_manager : public Display::Window_manager {
    public:
    Window_manager(uint64_t rows, uint64_t columns, uint8_t scaling, bool poll, const char* const window_name) :
        Display::Window_manager(rows, columns, scaling, poll, window_name) { }

    private:
};

}    // namespace Mocks

#endif
