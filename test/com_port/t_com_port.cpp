#include <com_port.h>
#include <gtest/gtest.h>

using namespace testing;

const char* const INPUT_FILE_NAME  = "GAMEBOY_COM_PORT_TEST_INPUT.txt";
const char* const OUTPUT_FILE_NAME = "GAMEBOY_COM_PORT_TEST_OUTPUT.txt";

class DISABLED_Gameboy_com_port_test : public Test {

    public:
    DISABLED_Gameboy_com_port_test() { }

    void SetUp() {

        /* Ensure that file's exist */
        input_file = fopen(INPUT_FILE_NAME, "w");
        fputc('B', input_file);
        fclose(input_file);

        input_file  = fopen(INPUT_FILE_NAME, "r");
        output_file = fopen(OUTPUT_FILE_NAME, "w");

        com = new Serial::Com_port(output_file, input_file);
    }

    void TearDown() {
        fclose(input_file);
        fclose(output_file);

        remove(INPUT_FILE_NAME);
        remove(OUTPUT_FILE_NAME);
    }

    Serial::Com_port* com;
    FILE*             input_file;
    FILE*             output_file;
};

TEST_F(DISABLED_Gameboy_com_port_test, WhenWriteData_ExpectDataOut) {

    /* Write a single character to the file */
    com->write(0xFF01, 'A');
    com->write(0xFF02, 0x81);

    /* Cycle the com port enough times to cause the character to actually be shifted out */

    for (uint16_t i = 0; i < 4097; i++) {
        com->cycle();
    }

    /* Reopen the output file to allow for read */
    fclose(output_file);

    output_file = fopen(OUTPUT_FILE_NAME, "r");

    char character;
    fread(&character, 1, 1, output_file);

    ASSERT_EQ(character, 'A');
}

TEST_F(DISABLED_Gameboy_com_port_test, WhenReadData_ExpectDataIn) {

    /* Begin a transfer */
    com->write(0xFF02, 0x81);

    /* Cycle the com port enough times to cause the character to actually be shifted out */
    for (uint16_t i = 0; i < 4097; i++) {
        com->cycle();
    }

    ASSERT_EQ(com->read(0xFF01), 'B');
}