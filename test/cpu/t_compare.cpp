#include <t_common_powerboy_fixture.h>

/* Cases

Zero |            |       |   1 ==   1
     | Half Carry | Carry |   1 ==   2
     | Half Carry |       |  16 ==   1
     |            | Carry |   1 ==  16
     |            |       |   2 ==   1

*/

using namespace testing;

class Gameboy_CPU_Compare_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Compare_fixture() {

        target_register = nullptr;
        first_arg       = 0;
        second_arg      = 0;
        immediate       = false;
        is_a            = false;
    }

    void SetUp() {

        uint8_t base_instruction = 0xB8;

        switch (GetParam()) {
            case 'A':
                is_a            = true;
                target_register = &register_file.A;
                instruction     = base_instruction + 7;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 6;
                immediate       = false;
                break;
            case 'M':
                target_register = nullptr;
                instruction     = 0xFE;
                immediate       = true;
                break;
        }
    }

    void common_test(void) {

        out_flags.subtract = true;

        register_file.flags.subtract   = ! out_flags.subtract;
        register_file.flags.zero       = ! out_flags.zero;
        register_file.flags.carry      = ! out_flags.carry;
        register_file.flags.half_carry = ! out_flags.half_carry;

        if (target_register == nullptr) {
            if (immediate) {
                EXPECT_CALL(mocked_bus, read(register_file.program_counter)).WillOnce(Return(second_arg));
            } else {
                /* This is the special instance of trying to use HL indirect */
                EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(second_arg));
            }
        } else {
            *target_register = second_arg;
        }

        /* Configure the A Register */
        register_file.A = first_arg;

        /* Call the add instruction processor */
        run_instruction();

        /* Have each of the other registers checked, if they were the target, then
        expect the assigned value otherwise zero
        */
        EXPECT_EQ(register_file.A, first_arg);
        EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? second_arg : 0);
        EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? second_arg : 0);
        EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? second_arg : 0);
        EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? second_arg : 0);

        /* Because HL can be used indirect and want to test non-zero access calls,
            these have special expectation calls
        */
        EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? second_arg : 0x01);
        EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? second_arg : 0x23);

        EXPECT_EQ(register_file.flags.subtract, out_flags.subtract);
        EXPECT_EQ(register_file.flags.zero, out_flags.zero);
        EXPECT_EQ(register_file.flags.carry, out_flags.carry);
        EXPECT_EQ(register_file.flags.half_carry, out_flags.half_carry);
    }

    uint8_t* target_register;
    uint8_t  first_arg;
    uint8_t  second_arg;
    bool     immediate;
    bool     is_a;
};

TEST_P(Gameboy_CPU_Compare_fixture, Zero_NoHalfCarry_NoCarry) {

    out_flags.zero       = true;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    first_arg  = 1;
    second_arg = 1;

    if (is_a == true) {
        first_arg  = 1;
        second_arg = first_arg;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Compare_fixture, NoZero_HalfCarry_Carry) {

    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = true;

    first_arg  = 1;
    second_arg = 2;

    if (is_a == true) {
        EXPECT_TRUE(true);
        return;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Compare_fixture, NoZero_HalfCarry_NoCarry) {

    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = false;

    first_arg  = 16;
    second_arg = 1;

    if (is_a == true) {
        EXPECT_TRUE(true);
        return;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Compare_fixture, NoZero_NoHalfCarry_Carry) {

    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = true;

    first_arg  = 1;
    second_arg = 16;

    if (is_a == true) {
        EXPECT_TRUE(true);
        return;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Compare_fixture, NoZero_NoHalfCarry_NoCarry) {

    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    first_arg  = 2;
    second_arg = 1;

    if (is_a == true) {
        EXPECT_TRUE(true);
        return;
    }

    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Compare,
                        Gameboy_CPU_Compare_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I', 'M'));