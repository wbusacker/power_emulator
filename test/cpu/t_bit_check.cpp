#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_bit_check_fixture : public ::testing::TestWithParam<std::string>, public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {

        std::string parameters = GetParam();

        uint8_t base_instruction = 0;

        /* Determine which bit to check */
        switch (parameters[0]) {
            case '0':
                bit_index        = 0;
                base_instruction = 0x40;
                break;
            case '1':
                bit_index        = 1;
                base_instruction = 0x48;
                break;
            case '2':
                bit_index        = 2;
                base_instruction = 0x50;
                break;
            case '3':
                bit_index        = 3;
                base_instruction = 0x58;
                break;
            case '4':
                bit_index        = 4;
                base_instruction = 0x60;
                break;
            case '5':
                bit_index        = 5;
                base_instruction = 0x68;
                break;
            case '6':
                bit_index        = 6;
                base_instruction = 0x70;
                break;
            case '7':

                /* Just set target register to the dummy target */
                bit_index        = 7;
                base_instruction = 0x78;
                break;
        }

        /* Determine which register we're looking at */
        switch (parameters[1]) {
            case 'A':
                source_register = &register_file.A;
                instruction     = base_instruction + 7;
                break;
            case 'B':
                source_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                source_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                source_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                source_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                source_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                source_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                source_register = nullptr;
                instruction     = base_instruction + 6;
                break;
        }

        /* Register that we've performed this instruction */
        cb_map[instruction]                          = true;
        cb_opcode_map[cb_unique_opcode[instruction]] = true;
    }

    uint8_t  instruction;
    uint8_t  bit_index;
    uint8_t* source_register;
};

TEST_P(Gameboy_CPU_bit_check_fixture, BitZero) {

    /* Configure the incoming state flags */
    register_file.flags.zero       = false;
    register_file.flags.subtract   = true;
    register_file.flags.half_carry = false;
    register_file.flags.carry      = true;

    /* Build up the data for the target register */
    uint8_t data = 0xFF & ~(1 << bit_index);

    if (source_register == nullptr) {
        /* Source is HL Indirect, so set an expectation there */
        EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(data));
    } else {
        *source_register = data;
    }

    cpu.set_register_state(&register_file);
    cpu.perform_cb_instruction(instruction);
    cpu.get_register_state(&register_file);

    /* Have all the register_file checked to see if they changed when they
        should have or didn't change when the should have
    */
    EXPECT_EQ(register_file.A, (source_register == &register_file.A) ? data : 0);
    EXPECT_EQ(register_file.B, (source_register == &register_file.B) ? data : 0);
    EXPECT_EQ(register_file.C, (source_register == &register_file.C) ? data : 0);
    EXPECT_EQ(register_file.D, (source_register == &register_file.D) ? data : 0);
    EXPECT_EQ(register_file.E, (source_register == &register_file.E) ? data : 0);

    /* Because HL can be used indirect and want to test non-zero access calls,
        these have special expectation calls
    */
    EXPECT_EQ(register_file.H, (source_register == &register_file.H) ? data : H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, (source_register == &register_file.L) ? data : L_INDIRECT_VALUE);

    /* Ensure that state flags weren't changed */
    EXPECT_TRUE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_TRUE(register_file.flags.half_carry);
    EXPECT_TRUE(register_file.flags.carry);
}

TEST_P(Gameboy_CPU_bit_check_fixture, BitNonZero) {

    /* Configure the incoming state flags */
    register_file.flags.zero       = true;
    register_file.flags.subtract   = true;
    register_file.flags.half_carry = false;
    register_file.flags.carry      = true;

    /* Build up the data for the target register */
    uint8_t data = 0xFF;

    if (source_register == nullptr) {
        /* Source is HL Indirect, so set an expectation there */
        EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(data));
    } else {
        *source_register = data;
    }

    cpu.set_register_state(&register_file);
    cpu.perform_cb_instruction(instruction);
    cpu.get_register_state(&register_file);

    /* Have all the register_file checked to see if they changed when they
        should have or didn't change when the should have
    */
    EXPECT_EQ(register_file.A, (source_register == &register_file.A) ? data : 0);
    EXPECT_EQ(register_file.B, (source_register == &register_file.B) ? data : 0);
    EXPECT_EQ(register_file.C, (source_register == &register_file.C) ? data : 0);
    EXPECT_EQ(register_file.D, (source_register == &register_file.D) ? data : 0);
    EXPECT_EQ(register_file.E, (source_register == &register_file.E) ? data : 0);

    /* Because HL can be used indirect and want to test non-zero access calls,
        these have special expectation calls
    */
    EXPECT_EQ(register_file.H, (source_register == &register_file.H) ? data : H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, (source_register == &register_file.L) ? data : L_INDIRECT_VALUE);

    /* Ensure that state flags weren't changed */
    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_TRUE(register_file.flags.half_carry);
    EXPECT_TRUE(register_file.flags.carry);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_bit_check,
                        Gameboy_CPU_bit_check_fixture,
                        testing::Values("0A",
                                        "0B",
                                        "0C",
                                        "0D",
                                        "0E",
                                        "0H",
                                        "0L",
                                        "0I",
                                        "1A",
                                        "1B",
                                        "1C",
                                        "1D",
                                        "1E",
                                        "1H",
                                        "1L",
                                        "1I",
                                        "2A",
                                        "2B",
                                        "2C",
                                        "2D",
                                        "2E",
                                        "2H",
                                        "2L",
                                        "2I",
                                        "3A",
                                        "3B",
                                        "3C",
                                        "3D",
                                        "3E",
                                        "3H",
                                        "3L",
                                        "3I",
                                        "4A",
                                        "4B",
                                        "4C",
                                        "4D",
                                        "4E",
                                        "4H",
                                        "4L",
                                        "4I",
                                        "5A",
                                        "5B",
                                        "5C",
                                        "5D",
                                        "5E",
                                        "5H",
                                        "5L",
                                        "5I",
                                        "6A",
                                        "6B",
                                        "6C",
                                        "6D",
                                        "6E",
                                        "6H",
                                        "6L",
                                        "6I",
                                        "7A",
                                        "7B",
                                        "7C",
                                        "7D",
                                        "7E",
                                        "7H",
                                        "7L",
                                        "7I"));
