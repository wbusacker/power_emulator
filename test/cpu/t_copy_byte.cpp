#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_copy_byte_fixture : public ::testing::TestWithParam<std::string>, public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        std::string parameters = GetParam();

        uint8_t r1 = parameters[1];
        uint8_t r2 = parameters[0];

        uint8_t base_instruction = 0;

        /* Determine the target register */
        switch (r1) {
            case 'A':
                target_register  = &register_file.A;
                base_instruction = 0x78;
                break;
            case 'B':
                target_register  = &register_file.B;
                base_instruction = 0x40;
                break;
            case 'C':
                target_register  = &register_file.C;
                base_instruction = 0x48;
                break;
            case 'D':
                target_register  = &register_file.D;
                base_instruction = 0x50;
                break;
            case 'E':
                target_register  = &register_file.E;
                base_instruction = 0x58;
                break;
            case 'H':
                target_register  = &register_file.H;
                base_instruction = 0x60;
                break;
            case 'L':
                target_register  = &register_file.L;
                base_instruction = 0x68;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register  = nullptr;
                base_instruction = 0x70;
                break;
        }

        switch (r2) {
            case 'A':
                source_register = &register_file.A;
                instruction     = base_instruction + 7;
                break;
            case 'B':
                source_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                source_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                source_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                source_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                source_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                source_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                source_register = nullptr;
                instruction     = base_instruction + 6;
                break;
        }

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    uint8_t  instruction;
    uint8_t* target_register;
    uint8_t* source_register;
};

TEST_P(Gameboy_CPU_copy_byte_fixture, DataCopy) {

    /* Set the source register */
    uint8_t data = 0x77;

    /* If the source is either H or L, we need to modify data expectations
        to allow those to function normally */
    if (source_register == &register_file.H) {
        data = 0x01;
    } else if (source_register == &register_file.L) {
        data = 0x23;
    }

    if (source_register == nullptr) {
        /* Source is HL Indirect, so set an expectation there */
        EXPECT_CALL(mocked_bus, read(0x0123)).WillOnce(Return(data));
    } else {
        *source_register = data;
    }

    /* If we're targeting HL indirect, setup an expectation for that */
    if (target_register == nullptr) {
        EXPECT_CALL(mocked_bus, write(0x0123, data));
    }

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    /* Have all the registers checked to see if they changed when they
        should have or didn't change when the should have
    */
    EXPECT_EQ(register_file.A,
              ((target_register == &register_file.A) || (source_register == &register_file.A)) ? data : 0);
    EXPECT_EQ(register_file.B,
              ((target_register == &register_file.B) || (source_register == &register_file.B)) ? data : 0);
    EXPECT_EQ(register_file.C,
              ((target_register == &register_file.C) || (source_register == &register_file.C)) ? data : 0);
    EXPECT_EQ(register_file.D,
              ((target_register == &register_file.D) || (source_register == &register_file.D)) ? data : 0);
    EXPECT_EQ(register_file.E,
              ((target_register == &register_file.E) || (source_register == &register_file.E)) ? data : 0);

    /* Because HL can be used indirect and want to test non-zero access calls,
        these have special expectation calls
    */
    EXPECT_EQ(register_file.H,
              ((target_register == &register_file.H) || (source_register == &register_file.H)) ? data : 0x01);
    EXPECT_EQ(register_file.L,
              ((target_register == &register_file.L) || (source_register == &register_file.L)) ? data : 0x23);

    /* Ensure that state flags weren't changed */
    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    EXPECT_FALSE(register_file.flags.carry);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_copy_byte,
                        Gameboy_CPU_copy_byte_fixture,
                        /* Instruciton case II doesn't actually exist */
                        testing::Values("AA",
                                        "AB",
                                        "AC",
                                        "AD",
                                        "AE",
                                        "AH",
                                        "AL",
                                        "AI",
                                        "BA",
                                        "BB",
                                        "BC",
                                        "BD",
                                        "BE",
                                        "BH",
                                        "BL",
                                        "BI",
                                        "CA",
                                        "CB",
                                        "CC",
                                        "CD",
                                        "CE",
                                        "CH",
                                        "CL",
                                        "CI",
                                        "DA",
                                        "DB",
                                        "DC",
                                        "DD",
                                        "DE",
                                        "DH",
                                        "DL",
                                        "DI",
                                        "EA",
                                        "EB",
                                        "EC",
                                        "ED",
                                        "EE",
                                        "EH",
                                        "EL",
                                        "EI",
                                        "HA",
                                        "HB",
                                        "HC",
                                        "HD",
                                        "HE",
                                        "HH",
                                        "HL",
                                        "HI",
                                        "LA",
                                        "LB",
                                        "LC",
                                        "LD",
                                        "LE",
                                        "LH",
                                        "LL",
                                        "LI",
                                        "IA",
                                        "IB",
                                        "IC",
                                        "ID",
                                        "IE",
                                        "IH",
                                        "IL"));