#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_call_fixture : public ::testing::TestWithParam<std::string>, public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        /* Specifically override SP and PC */
        register_file.stack_pointer   = 0x8000;
        register_file.program_counter = 0x1000;

        std::string parameters = GetParam();

        if (parameters.compare("NZ") == 0) {
            instruction              = 0xC4;
            register_file.flags.zero = false;
        } else if (parameters.compare("Z") == 0) {
            instruction              = 0xCC;
            register_file.flags.zero = true;
        } else if (parameters.compare("NC") == 0) {
            instruction               = 0xD4;
            register_file.flags.carry = false;
        } else if (parameters.compare("C") == 0) {
            instruction               = 0xDC;
            register_file.flags.carry = true;
        } else if (parameters.compare("-") == 0) {
            instruction = 0xCD;
        }

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    uint8_t  instruction;
    uint8_t* source_register;
};

TEST_P(Gameboy_CPU_call_fixture, ShouldCall) {

    /* Prepare the values for the system to jump to */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x1001)).WillOnce(Return(0xAA));

    /* Create expecations for writing to the stack */
    EXPECT_CALL(mocked_bus, write(0x8000 - 1, 0x10));
    EXPECT_CALL(mocked_bus, write(0x8000 - 2, 0x02));

    /* Call the add instruction processor */
    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0xAA77);
    EXPECT_EQ(register_file.stack_pointer, 0x8000 - 2);
}

TEST_P(Gameboy_CPU_call_fixture, ShouldNotCall) {

    /* The unconditional call will always jump, so short-circuit return */
    if (instruction == 0xCD) {
        EXPECT_TRUE(true);
        return;
    }

    /* Invert the flag conditions since test setup assumes call */
    register_file.flags.zero ^= true;
    register_file.flags.carry ^= true;

    /* Give the system addresses to expect calls for */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x1001)).WillOnce(Return(0xAA));
    EXPECT_CALL(mocked_bus, write(_, _)).Times(0);

    /* Call the add instruction processor */
    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x1002);
    EXPECT_EQ(register_file.stack_pointer, 0x8000);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_call, Gameboy_CPU_call_fixture, testing::Values("-", "NZ", "Z", "NC", "C"));