#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Copy_Stack_Pointer_fixture : public ::testing::Test, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Copy_Stack_Pointer_fixture() { }

    void common_test(void) {

        register_file.program_counter = 0x1000;
        register_file.stack_pointer   = stack_pointer;

        EXPECT_CALL(mocked_bus, read(0x1000)).Times(1).WillOnce(Return(offset_value));

        instruction = 0xF8;

        run_instruction();

        EXPECT_EQ(register_file.A, 0);
        EXPECT_EQ(register_file.B, 0);
        EXPECT_EQ(register_file.C, 0);
        EXPECT_EQ(register_file.D, 0);
        EXPECT_EQ(register_file.E, 0);

        EXPECT_EQ(register_file.H, expected_h);
        EXPECT_EQ(register_file.L, expected_l);

        EXPECT_EQ(register_file.program_counter, 0x1001);
        EXPECT_EQ(register_file.stack_pointer, stack_pointer);
    }

    uint16_t stack_pointer;
    uint8_t  offset_value;

    uint8_t expected_h;
    uint8_t expected_l;

    uint8_t out_carry;
    uint8_t out_half_carry;
};

TEST_F(Gameboy_CPU_Copy_Stack_Pointer_fixture, CopyToHL_HalfCarry_Carry) {
    stack_pointer = 0x00FF;
    offset_value  = 2;

    expected_h = 0x01;
    expected_l = 0x01;

    out_carry      = true;
    out_half_carry = true;

    common_test();
}

TEST_F(Gameboy_CPU_Copy_Stack_Pointer_fixture, CopyToHL_HalfCarry_NoCarry) {
    stack_pointer = 0x00F;
    offset_value  = 1;

    expected_h = 0x00;
    expected_l = 0x10;

    out_carry      = false;
    out_half_carry = true;

    common_test();
}

TEST_F(Gameboy_CPU_Copy_Stack_Pointer_fixture, CopyToHL_NoHalfCarry_Carry) {
    stack_pointer = 0x00F1;
    offset_value  = 16;

    expected_h = 0x01;
    expected_l = 0x01;

    out_carry      = true;
    out_half_carry = false;

    common_test();
}

TEST_F(Gameboy_CPU_Copy_Stack_Pointer_fixture, CopyToHL_NoHalfCarry_NoCarry) {
    stack_pointer = 0x0100;
    offset_value  = 1;

    expected_h = 0x01;
    expected_l = 0x01;

    out_carry      = false;
    out_half_carry = false;

    common_test();
}

TEST_F(Gameboy_CPU_Copy_Stack_Pointer_fixture, CopyToHL_Add_Backwards) {
    stack_pointer = 0x00E0;
    offset_value  = 0xF0;

    expected_h = 0x00;
    expected_l = 0xD0;

    out_carry      = true;
    out_half_carry = false;

    common_test();
}

TEST_F(Gameboy_CPU_Copy_Stack_Pointer_fixture, CopyToSP) {

    register_file.program_counter = 0x1000;
    register_file.stack_pointer   = 0x2000;

    instruction = 0xF9;

    run_instruction();

    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);

    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    EXPECT_FALSE(register_file.flags.carry);

    EXPECT_EQ(register_file.stack_pointer, HL_INDIRECT_ADDRESS);
    EXPECT_EQ(register_file.program_counter, 0x1000);
}
