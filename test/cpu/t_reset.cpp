#include <t_common_powerboy_fixture.h>

class Gameboy_CPU_Reset_fixture : public ::testing::TestWithParam<uint8_t>, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Reset_fixture() { }

    void SetUp() { instruction = GetParam(); }
};

TEST_P(Gameboy_CPU_Reset_fixture, IssueReset) {

    register_file.stack_pointer   = 0x8000;
    register_file.program_counter = 0x1234;
    EXPECT_CALL(mocked_bus, write(0x7FFF, 0x12)).Times(1);
    EXPECT_CALL(mocked_bus, write(0x7FFE, 0x34)).Times(1);

    /* Issue the instruction */
    run_instruction();

    /* Ensure that none of the registers changed state */
    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);

    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    EXPECT_FALSE(register_file.flags.carry);

    EXPECT_EQ(register_file.stack_pointer, 0x7FFE);
    /* Instruction C7 is the start of the resets, but they're all
        offset from there
    */
    EXPECT_EQ(register_file.program_counter, instruction - 0xC7);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Reset,
                        Gameboy_CPU_Reset_fixture,
                        testing::Values(0xC7, 0xCF, 0xD7, 0xDF, 0xE7, 0xEF, 0xF7, 0xFF));
