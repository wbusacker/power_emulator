#include <common_memory_bus_mocks.h>
#include <cpu.h>
#include <gtest/gtest.h>
#include <t_instruction_coverage.h>

using namespace testing;

TEST(Gameboy_CPU_Control, NOP) {

    Mocks::Common_memory_bus        mocked_bus;
    LR35902::Gameboy_cpu            cpu(&mocked_bus);
    struct LR35902::Register_file_t register_file;

    register_file.A                = 0;
    register_file.B                = 1;
    register_file.C                = 2;
    register_file.D                = 3;
    register_file.E                = 4;
    register_file.H                = 5;
    register_file.L                = 6;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 7;
    register_file.program_counter  = 8;

    /* Issue a nop and observe that nothing happens */
    cpu.set_register_state(&register_file);
    cpu.perform_instruction(0x00);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 1);
    EXPECT_EQ(register_file.C, 2);
    EXPECT_EQ(register_file.D, 3);
    EXPECT_EQ(register_file.E, 4);
    EXPECT_EQ(register_file.H, 5);
    EXPECT_EQ(register_file.L, 6);

    EXPECT_EQ(register_file.flags.carry, false);
    EXPECT_EQ(register_file.flags.half_carry, false);
    EXPECT_EQ(register_file.flags.subtract, false);
    EXPECT_EQ(register_file.flags.zero, false);

    EXPECT_EQ(register_file.stack_pointer, 7);
    EXPECT_EQ(register_file.program_counter, 8);

    inst_map[0x00]                  = true;
    opcode_map[unique_opcode[0x00]] = true;
}