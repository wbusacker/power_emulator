#include <cpu_instructions_groups.h>
#include <gtest/gtest.h>
#include <tuple>

using namespace testing;

inline void common_test(struct LR35902::CPU_state_flags_t* in_flags, uint8_t lhs, uint8_t rhs, uint8_t answer) {

    in_flags->subtract = false;

    struct LR35902::CPU_state_flags_t out_flags;
    out_flags.zero       = ! in_flags->zero;
    out_flags.carry      = ! in_flags->carry;
    out_flags.half_carry = ! in_flags->half_carry;
    out_flags.subtract   = ! in_flags->subtract;

    EXPECT_EQ(LR35902::common_subtract(in_flags, lhs, rhs), answer);

    EXPECT_EQ(out_flags.zero, in_flags->zero);
    EXPECT_EQ(out_flags.carry, in_flags->carry);
    EXPECT_EQ(out_flags.half_carry, in_flags->half_carry);
    EXPECT_EQ(out_flags.subtract, in_flags->subtract);
}

// TEST(Gameboy_common_subtract, Zero_HalfCarry_Carry)

// TEST(Gameboy_common_subtract, Zero_NoHalfCarry_Carry)

// TEST(Gameboy_common_subtract, Zero_HalfCarry_NoCarry)

TEST(Gameboy_common_subtract, Zero_NoHalfCarry_NoCarry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = false;
    flags.half_carry = true;
    flags.carry      = true;

    common_test(&flags, 0xFF, 0xFF, 0x00);
}

TEST(Gameboy_common_subtract, NoZero_HalfCarry_Carry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = true;
    flags.half_carry = false;
    flags.carry      = false;

    common_test(&flags, 0xFE, 0xFF, 0xFF);
}

TEST(Gameboy_common_subtract, NoZero_NoHalfCarry_Carry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = true;
    flags.half_carry = true;
    flags.carry      = false;

    common_test(&flags, 0xEF, 0xFF, 0xF0);

    flags.zero       = true;
    flags.half_carry = true;
    flags.carry      = false;
    common_test(&flags, 0xEF, 0xFE, 0xF1);
}

TEST(Gameboy_common_subtract, NoZero_HalfCarry_NoCarry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = true;
    flags.half_carry = false;
    flags.carry      = true;

    common_test(&flags, 0xFE, 0xEF, 0x0F);
}

TEST(Gameboy_common_subtract, NoZero_NoHalfCarry_NoCarry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = true;
    flags.half_carry = true;
    flags.carry      = true;

    common_test(&flags, 0xFF, 0xFE, 0x01);

    flags.zero       = true;
    flags.half_carry = true;
    flags.carry      = true;
    common_test(&flags, 0xFF, 0xF0, 0x0F);
}