#include <common_memory_bus_mocks.h>
#include <cpu.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <t_instruction_coverage.h>

TEST(Gameboy_CPU_Control, Halt) {

    struct LR35902::Register_file_t register_file;
    Mocks::Common_memory_bus        mocked_bus;
    LR35902::Gameboy_cpu            cpu(&mocked_bus);

    register_file.A                = 0;
    register_file.B                = 0;
    register_file.C                = 0;
    register_file.D                = 0;
    register_file.E                = 0;
    register_file.H                = 0;
    register_file.L                = 0;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 0;
    register_file.program_counter  = 0;

    cpu.set_register_state(&register_file);

    EXPECT_CALL(mocked_bus, read(0)).Times(1).WillOnce(::testing::Return(0x76));
    EXPECT_CALL(mocked_bus, read(1)).Times(0);

    /* Issue the Halt instruction then attempt to go again but have nothing happen */
    cpu.cycle();
    cpu.cycle();

    inst_map[0x76]                  = true;
    opcode_map[unique_opcode[0x76]] = true;
}

TEST(Gameboy_CPU_Control, Stop) {

    struct LR35902::Register_file_t register_file;
    Mocks::Common_memory_bus        mocked_bus;
    LR35902::Gameboy_cpu            cpu(&mocked_bus);

    register_file.A                = 0;
    register_file.B                = 0;
    register_file.C                = 0;
    register_file.D                = 0;
    register_file.E                = 0;
    register_file.H                = 0;
    register_file.L                = 0;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 0;
    register_file.program_counter  = 0;

    cpu.set_register_state(&register_file);

    EXPECT_CALL(mocked_bus, read(0)).Times(1).WillOnce(::testing::Return(0x10));
    EXPECT_CALL(mocked_bus, read(1)).Times(0);

    /* Issue the Halt instruction then attempt to go again but have nothing happen */
    cpu.cycle();
    cpu.cycle();

    inst_map[0x10]                  = true;
    opcode_map[unique_opcode[0x10]] = true;
}
