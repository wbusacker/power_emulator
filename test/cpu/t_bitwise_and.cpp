#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_bitwise_and_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_bitwise_and_fixture() {
        lhs             = 0;
        rhs             = 0;
        target_register = nullptr;
        immediate       = false;
    }
    void SetUp() {

        uint8_t base_instruction = 0xA0;

        switch (GetParam()) {
            case 'A':
                target_register = &register_file.A;
                instruction     = base_instruction + 7;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 6;
                immediate       = false;
                break;
            case 'M':
                target_register = nullptr;
                instruction     = 0xE6;
                immediate       = true;
                break;
        }
    }

    void common_test(void) {

        out_flags.subtract   = false;
        out_flags.carry      = false;
        out_flags.half_carry = true;

        register_file.flags.zero       = ! out_flags.zero;
        register_file.flags.subtract   = ! out_flags.subtract;
        register_file.flags.half_carry = ! out_flags.half_carry;
        register_file.flags.carry      = ! out_flags.carry;

        /* Setup the appropriate arguments */
        register_file.A = lhs;
        if (target_register == nullptr) {
            if (immediate) {
                EXPECT_CALL(mocked_bus, read(register_file.program_counter)).WillOnce(Return(rhs));
            } else {
                /* This is the special instance of trying to use HL indirect */
                EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(rhs));
            }
        } else {
            *target_register = rhs;
        }

        /* Call the add instruction processor */
        run_instruction();

        EXPECT_EQ(register_file.A, (lhs & rhs) & 0xFF);

        /* Have each of the other register_file checked, if they were the target, then
            expect the assigned value otherwise zero
        */
        EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? rhs : 0);
        EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? rhs : 0);
        EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? rhs : 0);
        EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? rhs : 0);

        /* Because HL can be used indirect and want to test non-zero access calls,
            these have special expectation calls
        */
        EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? rhs : H_INDIRECT_VALUE);
        EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? rhs : L_INDIRECT_VALUE);

        /* Expect the proper state flags */
        EXPECT_EQ(register_file.flags.zero, out_flags.zero);
        EXPECT_EQ(register_file.flags.subtract, out_flags.subtract);
        EXPECT_EQ(register_file.flags.half_carry, out_flags.half_carry);
        EXPECT_EQ(register_file.flags.carry, out_flags.carry);
    }

    uint8_t lhs;
    uint8_t rhs;

    uint8_t* target_register;
    bool     immediate;
};

TEST_P(Gameboy_CPU_bitwise_and_fixture, InZero_Zero) {

    /* Set the state flag bits */
    out_flags.zero = true;

    /* Provide the operands for the test */
    lhs = 0;
    rhs = 0x77;

    if (target_register == &register_file.A) {
        /* Special case where we are duplicating A's value */
        lhs = 0;
        rhs = lhs;
    }

    common_test();
}

TEST_P(Gameboy_CPU_bitwise_and_fixture, InNonZero_NonZero) {

    /* Set the state flag bits */
    out_flags.zero = false;

    /* Provide the operands for the test */
    lhs = 0xAA;
    rhs = 0x77;

    if (target_register == &register_file.A) {
        /* Special case where we are duplicating A's value */
        lhs = 0xAA;
        rhs = lhs;
    }

    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_bitwise_and,
                        Gameboy_CPU_bitwise_and_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I', 'M'));