#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Increment_16_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        uint8_t base_instruction = 0x03;

        switch (GetParam()) {
            case 'B':
                msb_register = &register_file.B;
                lsb_register = &register_file.C;
                instruction  = base_instruction + 0x00;
                break;
            case 'D':
                msb_register = &register_file.D;
                lsb_register = &register_file.E;
                instruction  = base_instruction + 0x10;
                break;
            case 'H':
                msb_register = &register_file.H;
                lsb_register = &register_file.L;
                instruction  = base_instruction + 0x20;
                break;
            case 'S':
                msb_register = nullptr;
                lsb_register = nullptr;
                instruction  = base_instruction + 0x30;
                break;
        }

        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    uint8_t                    instruction;
    uint8_t*                   target_register;
    LR35902::CPU_state_flags_t out_flags;

    uint8_t* lsb_register;
    uint8_t* msb_register;
};

TEST_P(Gameboy_CPU_Increment_16_fixture, WhenIncrement_ExpectRightAnswer) {

    /* Set the registers down to zero */
    if (msb_register == nullptr) {
        register_file.stack_pointer = 0;
    } else {
        *msb_register = 0;
        *lsb_register = 0;
    }

    cpu.set_register_state(&register_file);

    /* Roll through every possible number plus one for proper roll-over detection*/
    for (uint32_t i = 1; i < 0x10001; i++) {

        /* Trigger the instruction */
        cpu.perform_instruction(instruction);

        cpu.get_register_state(&register_file);

        /* Calculate what the expected values are */

        uint8_t expected_msb = (i >> 8) & 0xFF;
        uint8_t expected_lsb = i & 0xFF;

        /* Make sure that only the correct pairs have the right data */

        EXPECT_EQ(register_file.B, (msb_register == &register_file.B) ? expected_msb : 0);
        EXPECT_EQ(register_file.C, (lsb_register == &register_file.C) ? expected_lsb : 0);

        EXPECT_EQ(register_file.D, (msb_register == &register_file.D) ? expected_msb : 0);
        EXPECT_EQ(register_file.E, (lsb_register == &register_file.E) ? expected_lsb : 0);

        EXPECT_EQ(register_file.H, (msb_register == &register_file.H) ? expected_msb : H_INDIRECT_VALUE);
        EXPECT_EQ(register_file.L, (lsb_register == &register_file.L) ? expected_lsb : L_INDIRECT_VALUE);

        if (msb_register == nullptr) {
            EXPECT_EQ(register_file.stack_pointer, i & 0xFFFF);
        }
    }
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Increment_16,
                        Gameboy_CPU_Increment_16_fixture,
                        testing::Values('B', 'D', 'H', 'S'));