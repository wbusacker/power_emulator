#include <cpu_instructions_groups.h>
#include <gtest/gtest.h>
#include <tuple>

using namespace testing;

TEST(Gameboy_common_add, NonZero_NonCarry_NonHalfCarry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = true;
    flags.carry      = true;
    flags.half_carry = true;
    flags.subtract   = true;

    EXPECT_EQ(LR35902::common_add(&flags, 0x11, 0x22), 0x33);

    EXPECT_FALSE(flags.zero);
    EXPECT_FALSE(flags.carry);
    EXPECT_FALSE(flags.half_carry);
    EXPECT_FALSE(flags.subtract);
}

TEST(Gameboy_common_add, Zero_Carry_HalfCarry) {
    struct LR35902::CPU_state_flags_t flags;
    flags.zero       = false;
    flags.carry      = false;
    flags.half_carry = false;
    flags.subtract   = true;

    EXPECT_EQ(LR35902::common_add(&flags, 0x11, 0xEF), 0x00);

    EXPECT_TRUE(flags.zero);
    EXPECT_TRUE(flags.carry);
    EXPECT_TRUE(flags.half_carry);
    EXPECT_FALSE(flags.subtract);
}