#include <t_common_powerboy_fixture.h>

/* Test Cases

Lower Bits Only

Half Carry |Carry |   2 + 255 =   1
Half Carry |      |   1 +  15 =  16
           |Carry |  16 + 241 =   1
           |      |   0 +   1 =   1

*/

using namespace testing;

class Gameboy_CPU_Add_SP_fixture : public ::testing::Test, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        add_value     = 0;
        out_value     = 0;
        out_carry     = false;
        out_halfcarry = false;

        instruction = 0xE8;
    }

    void common_test(void) {
        EXPECT_CALL(mocked_bus, read(0x1000)).Times(1).WillOnce(Return(add_value));

        register_file.program_counter  = 0x1000;
        register_file.flags.carry      = ! out_carry;
        register_file.flags.half_carry = ! out_halfcarry;

        run_instruction();

        EXPECT_EQ(register_file.A, 0);
        EXPECT_EQ(register_file.B, 0);
        EXPECT_EQ(register_file.C, 0);
        EXPECT_EQ(register_file.D, 0);
        EXPECT_EQ(register_file.E, 0);
        EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
        EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);

        EXPECT_EQ(register_file.flags.zero, false);
        EXPECT_EQ(register_file.flags.subtract, false);
        EXPECT_EQ(register_file.flags.half_carry, out_halfcarry);
        EXPECT_EQ(register_file.flags.carry, out_carry);

        EXPECT_EQ(register_file.program_counter, 0x1001);
        EXPECT_EQ(register_file.stack_pointer, out_value);
    }

    uint16_t add_value;
    uint16_t out_value;
    bool     out_carry;
    bool     out_halfcarry;
};

TEST_F(Gameboy_CPU_Add_SP_fixture, HalfCarry_Carry) {

    out_halfcarry = true;
    out_carry     = true;

    register_file.stack_pointer = 255;
    add_value                   = 2;
    out_value                   = 257;

    common_test();
}

TEST_F(Gameboy_CPU_Add_SP_fixture, HalfCarry_NoCarry) {

    out_halfcarry = true;
    out_carry     = false;

    register_file.stack_pointer = 15;
    add_value                   = 1;
    out_value                   = 16;

    common_test();
}

TEST_F(Gameboy_CPU_Add_SP_fixture, NoHalfCarry_Carry) {

    out_halfcarry = false;
    out_carry     = true;

    register_file.stack_pointer = 241;
    add_value                   = 16;
    out_value                   = 257;

    common_test();
}

TEST_F(Gameboy_CPU_Add_SP_fixture, NoHalfCarry_NoCarry) {

    out_halfcarry = false;
    out_carry     = false;

    register_file.stack_pointer = 256;
    add_value                   = 1;
    out_value                   = 257;

    common_test();
}

TEST_F(Gameboy_CPU_Add_SP_fixture, Add_Backwards) {

    out_halfcarry = false;
    out_carry     = true;

    register_file.stack_pointer = 0x00E0;
    add_value                   = 0xF0;
    out_value                   = 0x00D0;

    common_test();
}
