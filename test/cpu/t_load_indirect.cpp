#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Load_Indirect_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        register_file.program_counter = 0x1000;

        uint8_t base_instruction = 0x0A;

        switch (GetParam()) {
            case 'B':
                register_file.B = 0x77;
                register_file.C = 0xAA;
                instruction     = base_instruction + 0x00;
                hl_expectation  = HL_INDIRECT_ADDRESS;
                break;
            case 'D':
                register_file.D = 0x77;
                register_file.E = 0xAA;
                instruction     = base_instruction + 0x10;
                hl_expectation  = HL_INDIRECT_ADDRESS;
                break;
            case 'P':
                register_file.H = 0x77;
                register_file.L = 0xAA;
                instruction     = base_instruction + 0x20;
                hl_expectation  = 0x77AB;
                break;
            case 'S':
                register_file.H = 0x77;
                register_file.L = 0xAA;
                instruction     = base_instruction + 0x30;
                hl_expectation  = 0x77A9;
                break;
            case 'I':
                register_file.program_counter = 0x1000;
                EXPECT_CALL(mocked_bus, read(0x1000)).Times(1).WillOnce(Return(0xAA));
                EXPECT_CALL(mocked_bus, read(0x1001)).Times(1).WillOnce(Return(0x77));
                instruction    = 0xFA;
                hl_expectation = HL_INDIRECT_ADDRESS;
                break;
        }
    }

    uint16_t hl_expectation;
};

TEST_P(Gameboy_CPU_Load_Indirect_fixture, LoadImmediate) {

    EXPECT_CALL(mocked_bus, read(0x77AA)).Times(1).WillOnce(Return(0x55));

    run_instruction();

    EXPECT_EQ(register_file.A, 0x55);

    uint16_t hl = (static_cast<uint16_t>(register_file.H) << 8) | register_file.L;
    EXPECT_EQ(hl_expectation, hl);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Load_Indirect,
                        Gameboy_CPU_Load_Indirect_fixture,
                        testing::Values('B', 'D', 'P', 'S', 'I'));