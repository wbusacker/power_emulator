#include <gtest/gtest.h>
#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_interrupt_processing : public TestWithParam<uint8_t>, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_interrupt_processing(void) {

        /* Enable all of the interrupts to be processed */
        cpu.write(0xFFFF, 0x1F);

        /* Configure the stack pointer to something that makes more sense */
        register_file.stack_pointer = 0xD000;
        cpu.set_register_state(&register_file);
    }
};

TEST_F(Gameboy_CPU_interrupt_processing, Master_Interrupts_Disabled) {

    /* Issue the Disable Interrupts instruction, then make sure that the when
        an interrupt is raised, that is is not processed
    */
    /* When we issue the DI instruction, we should see the IE location polled */

    EXPECT_CALL(mocked_bus, read(0x0000)).Times(1).WillOnce(Return(0xF3));
    EXPECT_CALL(mocked_bus, read(0x0001)).Times(1).WillOnce(Return(0x00));

    cpu.cycle();
    cpu.raise_interrupt(static_cast<LR35902::SERVICABLE_INTERRUPTS>(1));
    cpu.cycle();

    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x0002);

    /* Register that we've performed this instruction */
    inst_map[0xF3]                  = true;
    opcode_map[unique_opcode[0xF3]] = true;
}

TEST_F(Gameboy_CPU_interrupt_processing, Master_Interrupts_Enabled) {

    /* Issue the Disable Interrupts instruction, then make sure that the when
        an interrupt is raised, that is is not processed
    */
    /* When we issue the DI instruction, we should see the IE location polled */

    EXPECT_CALL(mocked_bus, read(0x0000)).Times(1).WillOnce(Return(0xF3));
    EXPECT_CALL(mocked_bus, read(0x0001)).Times(1).WillOnce(Return(0x00));
    EXPECT_CALL(mocked_bus, read(0x0002)).Times(1).WillOnce(Return(0xFB));
    EXPECT_CALL(mocked_bus, read(0x0040)).Times(1).WillOnce(Return(0x00));

    /* Issue the DI instruction */
    cpu.cycle();

    /* Raise the interupt and see that we did not process */
    cpu.raise_interrupt(static_cast<LR35902::SERVICABLE_INTERRUPTS>(1));
    cpu.cycle();

    cpu.get_register_state(&register_file);
    EXPECT_EQ(register_file.program_counter, 0x0002);

    /* Re-enable all interrupts */
    cpu.cycle();

    /* Cycle again and see that we pulled an instruction from the VBLANK address */
    cpu.cycle();

    /* Register that we've performed this instruction */
    inst_map[0xF3]                  = true;
    opcode_map[unique_opcode[0xF3]] = true;
}

TEST_P(Gameboy_CPU_interrupt_processing, Mask_Interrupt) {

    /* Set the IF flag, but ensure that bit in the IE is unset */

    uint8_t bit_placement = GetParam();

    uint8_t interrupt_flag = 1 << bit_placement;
    uint8_t interrupt_mask = ~interrupt_flag;

    /* Just issue a NOP instruction */
    EXPECT_CALL(mocked_bus, read(0x0000)).Times(1).WillOnce(Return(0x00));

    /* Set the pending interrupts  */
    cpu.write(0xFF0F, interrupt_flag);
    cpu.write(0xFFFF, interrupt_mask);

    cpu.cycle();

    /* Check to make sure that the program counter didn't jump */
    cpu.get_register_state(&register_file);
    EXPECT_EQ(register_file.program_counter, 0x0001);
}

TEST_P(Gameboy_CPU_interrupt_processing, Interrupt_Taken) {

    /* Set the IF flag, and make sure the IE lets it through */
    uint8_t bit_placement = GetParam();

    uint8_t interrupt_flag = 1 << bit_placement;
    uint8_t interrupt_mask = interrupt_flag;

    /* Just issue a NOP instruction */
    uint16_t new_pc = 0;
    switch (bit_placement) {
        case 0: /* V-Blank */
            new_pc = 0x0040;
            break;
        case 1: /* LCDC */
            new_pc = 0x0048;
            break;
        case 2: /* Timer Overflow */
            new_pc = 0x0050;
            break;
        case 3: /* SIO */
            new_pc = 0x0058;
            break;
        case 4: /* Controller Input */
            new_pc = 0x0060;
            break;
    }
    EXPECT_CALL(mocked_bus, read(new_pc)).Times(1).WillOnce(Return(0x00));

    /* Set the pending interrupts  */
    cpu.write(0xFF0F, interrupt_flag);
    cpu.write(0xFFFF, interrupt_mask);

    /* The current PC gets pushed onto the stack, so make sure that we expect that */
    register_file.stack_pointer   = 0x1000;
    register_file.program_counter = 0x1234;
    cpu.set_register_state(&register_file);
    EXPECT_CALL(mocked_bus, write(0x0FFF, 0x12)).Times(1);
    EXPECT_CALL(mocked_bus, write(0x0FFE, 0x34)).Times(1);

    cpu.cycle();

    /* When the interrupt gets serviced, the IF should be reset */
    EXPECT_EQ(0, cpu.read(0xFF0F));
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_interrupt_processing,
                        Gameboy_CPU_interrupt_processing,
                        testing::Values(0, 1, 2, 3, 4));