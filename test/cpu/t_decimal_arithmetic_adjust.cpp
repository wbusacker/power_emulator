#include <t_common_powerboy_fixture.h>
#include <tuple>

using namespace testing;

/*
  0 +   0 =   0 -> 0x00 + 0x00 = 0x00 -> 0x00   ot + 0x00
  1 +   9 =  10 -> 0x01 + 0x09 = 0x0A -> 0x10    t + 0x06
  1 +  99 =   0 -> 0x01 + 0x99 = 0x9A -> 0x00    t + 0x66
  7 +   9 =  16 -> 0x07 + 0x09 = 0x10 -> 0x16   ot + 0x06
  7 +  99 =   6 -> 0x07 + 0x99 = 0xA0 -> 0x06   o  + 0x66
  8 +   0 =   8 -> 0x08 + 0x00 = 0x08 -> 0x08 H ot + 0x00
  8 +   2 =  10 -> 0x08 + 0x02 = 0x0A -> 0x10 H  t + 0x06
  8 +   8 =  16 -> 0x08 + 0x08 = 0x10 -> 0x16 H ot + 0x06
  8 +  92 =   0 -> 0x08 + 0x92 = 0x9A -> 0x00 H  t + 0x66
  8 +  98 =   6 -> 0x08 + 0x98 = 0xA0 -> 0x06 H o  + 0x66
 10 +  90 =   0 -> 0x10 + 0x90 = 0xA0 -> 0x00   o  + 0x60
 11 +  99 =  10 -> 0x11 + 0x99 = 0xAA -> 0x10      + 0x66
 18 +  90 =   8 -> 0x18 + 0x90 = 0xA8 -> 0x08 H o  + 0x60
 18 +  92 =  10 -> 0x18 + 0x92 = 0xAA -> 0x10 H    + 0x66
 67 +  99 =  66 -> 0x67 + 0x99 = 0x00 -> 0x66  Cot + 0x66
 68 +  98 =  66 -> 0x68 + 0x98 = 0x00 -> 0x66 HCot + 0x66
 70 +  90 =  60 -> 0x70 + 0x90 = 0x00 -> 0x60  Cot + 0x60
 71 +  99 =  70 -> 0x71 + 0x99 = 0x0A -> 0x70  C t + 0x66
 78 +  90 =  68 -> 0x78 + 0x90 = 0x08 -> 0x68 HCot + 0x60
 78 +  92 =  70 -> 0x78 + 0x92 = 0x0A -> 0x70 HC t + 0x66

  0 -   0 =   0 -> 0x00 - 0x00 = 0x00 -> 0x00    - 0x00
  0 -  10 =  90 -> 0x00 - 0x10 = 0xF0 -> 0x90  C - 0xA0
 10 -   1 =   9 -> 0x10 - 0x01 = 0x0F -> 0x09 H  - 0xFA
  0 -   1 =  99 -> 0x00 - 0x01 = 0xFF -> 0x99 HC - 0x9A
 */

class Gameboy_CPU_decimal_arithmetic_adjust_fixture : public ::testing::Test, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() { instruction = 0x27; }
};

TEST_F(Gameboy_CPU_decimal_arithmetic_adjust_fixture, Addition_Adjust) {

    uint32_t failed_condition_count = 0;

    for (uint8_t i = 0; i < 100; i++) {
        for (uint8_t j = 0; j < 100; j++) {

            /* Convert the two numbers into BCD */
            uint8_t i_bcd = (i % 10) + ((i / 10) << 4);
            uint8_t j_bcd = (j % 10) + ((j / 10) << 4);

            register_file.A = i_bcd;
            register_file.B = j_bcd;

            cpu.set_register_state(&register_file);
            cpu.perform_instruction(0x80);
            cpu.perform_instruction(0x27);
            cpu.get_register_state(&register_file);

            /* Convert A back out of BCD */
            uint8_t output = (register_file.A & 0xF) + ((register_file.A >> 4) * 10);
            uint8_t ans    = ((i + j) % 100);
            EXPECT_EQ(output, ans);
            if (output != ans) {
                failed_condition_count++;
                printf("I %2d J %2d = %2d | Ibcd %02X Jbcd %02X = %02X\n", i, j, ans, i_bcd, j_bcd, register_file.A);
            }
        }
    }

    printf("Failed Condition Count %d\n", failed_condition_count);
}

TEST_F(Gameboy_CPU_decimal_arithmetic_adjust_fixture, Subtraction_Adjust) {

    uint32_t failed_condition_count = 0;

    for (uint8_t i = 0; i < 100; i++) {
        for (uint8_t j = 0; j < 100; j++) {

            /* Convert the two numbers into BCD */
            uint8_t i_bcd = (i % 10) + ((i / 10) << 4);
            uint8_t j_bcd = (j % 10) + ((j / 10) << 4);

            register_file.A = i_bcd;
            register_file.B = j_bcd;

            cpu.set_register_state(&register_file);
            cpu.perform_instruction(0x90);
            cpu.perform_instruction(0x27);
            cpu.get_register_state(&register_file);

            /* Convert A back out of BCD */
            uint8_t output = (register_file.A & 0xF) + ((register_file.A >> 4) * 10);
            int16_t ans    = i - j;
            if (ans < 0) {
                ans += 100;
            }
            EXPECT_EQ(output, ans);
            if (output != ans) {
                failed_condition_count++;
                printf("I %2d J %2d = %2d | Ibcd %02X Jbcd %02X = %02X\n", i, j, ans, i_bcd, j_bcd, register_file.A);
            }
        }
    }

    printf("Failed Condition Count %d\n", failed_condition_count);
}

class Gameboy_CPU_decimal_arithmetic_adjust_addition_fixture :
    public ::testing::TestWithParam<std::tuple<uint8_t, uint8_t, uint8_t>>,
    public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        instruction = 0x27;
        i           = std::get<0>(GetParam());
        j           = std::get<1>(GetParam());
        ans         = std::get<2>(GetParam());
    }

    uint8_t i;
    uint8_t j;
    uint8_t ans;
};

TEST_P(Gameboy_CPU_decimal_arithmetic_adjust_addition_fixture, unique_bcd_case) {

    /* Convert the two numbers into BCD */

    register_file.A = i;
    register_file.B = j;

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(0x80);
    cpu.perform_instruction(0x27);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.A, ans);
}

INSTANTIATE_TEST_SUITE_P(special_addition_tests,
                         Gameboy_CPU_decimal_arithmetic_adjust_addition_fixture,
                         ::testing::Values(make_tuple(0x00, 0x00, 0x00),
                                           make_tuple(0x01, 0x09, 0x10),
                                           make_tuple(0x01, 0x99, 0x00),
                                           make_tuple(0x07, 0x09, 0x16),
                                           make_tuple(0x07, 0x99, 0x06),
                                           make_tuple(0x08, 0x00, 0x08),
                                           make_tuple(0x08, 0x02, 0x10),
                                           make_tuple(0x08, 0x08, 0x16),
                                           make_tuple(0x08, 0x92, 0x00),
                                           make_tuple(0x08, 0x98, 0x06),
                                           make_tuple(0x10, 0x90, 0x00),
                                           make_tuple(0x11, 0x99, 0x10),
                                           make_tuple(0x18, 0x90, 0x08),
                                           make_tuple(0x18, 0x92, 0x10),
                                           make_tuple(0x67, 0x99, 0x66),
                                           make_tuple(0x68, 0x98, 0x66),
                                           make_tuple(0x70, 0x90, 0x60),
                                           make_tuple(0x71, 0x99, 0x70),
                                           make_tuple(0x78, 0x90, 0x68),
                                           make_tuple(0x78, 0x92, 0x70)));

class Gameboy_CPU_decimal_arithmetic_adjust_subtraction_fixture :
    public ::testing::TestWithParam<std::tuple<uint8_t, uint8_t, uint8_t>>,
    public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        instruction = 0x27;
        i           = std::get<0>(GetParam());
        j           = std::get<1>(GetParam());
        ans         = std::get<2>(GetParam());
    }

    uint8_t i;
    uint8_t j;
    uint8_t ans;
};

TEST_P(Gameboy_CPU_decimal_arithmetic_adjust_subtraction_fixture, unique_bcd_case) {

    /* Convert the two numbers into BCD */

    register_file.A = i;
    register_file.B = j;

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(0x90);
    cpu.perform_instruction(0x27);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.A, ans);
}

INSTANTIATE_TEST_SUITE_P(special_subtraction_tests,
                         Gameboy_CPU_decimal_arithmetic_adjust_subtraction_fixture,
                         ::testing::Values(make_tuple(0x00, 0x00, 0x00),
                                           make_tuple(0x00, 0x10, 0x90),
                                           make_tuple(0x10, 0x01, 0x09),
                                           make_tuple(0x00, 0x01, 0x99)));
