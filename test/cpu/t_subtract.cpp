#include <t_common_powerboy_fixture.h>
/* Cases

Zero |           |      |  16 -  16 =   0
     |Half Carry |Carry |   1 -   2 = 255
     |Half Carry |      |  16 -   1 =  15
     |           |Carry |   1 -  16 = 241
     |           |      |  17 -   1 =  16

*/

using namespace testing;

class Gameboy_CPU_Subtract_fixture : public ::testing::TestWithParam<char>, Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        uint8_t base_instruction = 0x90;

        is_a = false;
        switch (GetParam()) {
            case 'A':
                target_register = &register_file.A;
                instruction     = base_instruction + 7;
                is_a            = true;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 6;
                immediate       = false;
                break;
            case 'M':
                target_register = nullptr;
                instruction     = 0xD6;
                immediate       = true;
                break;
        }

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    void common_test(void) {

        /* Since this is an add test, always set the same expecations for subtract */
        out_flags.subtract = true;

        /* Configure the CPU's register file */
        register_file.flags.zero       = ! out_flags.zero;
        register_file.flags.subtract   = ! out_flags.subtract;
        register_file.flags.half_carry = ! out_flags.half_carry;
        register_file.flags.carry      = ! out_flags.carry;

        /* Setup the appropriate arguments */
        register_file.A = lhs;
        if (target_register == nullptr) {
            if (immediate) {
                EXPECT_CALL(mocked_bus, read(register_file.program_counter)).WillOnce(Return(rhs));
            } else {
                /* This is the special instance of trying to use HL indirect */
                EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(rhs));
            }
        } else {
            *target_register = rhs;
        }

        cpu.set_register_state(&register_file);

        /* Call the main instruction entry point */
        cpu.perform_instruction(instruction);

        /* Get back the Register file */
        cpu.get_register_state(&register_file);

        /* Have each of the other register_file checked, if they were the target, then
        expect the assigned value otherwise zero */
        EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? rhs : 0);
        EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? rhs : 0);
        EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? rhs : 0);
        EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? rhs : 0);

        /* Because HL can be used indirect and want to test non-zero access calls,
        these have special expectation calls */
        EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? rhs : (HL_INDIRECT_ADDRESS >> 8) & 0xFF);
        EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? rhs : HL_INDIRECT_ADDRESS & 0xFF);

        /* Check for the appropriate answer */
        EXPECT_EQ(register_file.A, answer);

        /* Expect the proper state flags */
        EXPECT_EQ(register_file.flags.zero, out_flags.zero);
        EXPECT_EQ(register_file.flags.subtract, out_flags.subtract);
        EXPECT_EQ(register_file.flags.half_carry, out_flags.half_carry);
        EXPECT_EQ(register_file.flags.carry, out_flags.carry);
    }

    /* General Purpose Test Configuration */
    uint8_t                    lhs = 0;
    uint8_t                    rhs = 0;
    uint8_t                    answer;
    bool                       is_a = false;
    LR35902::CPU_state_flags_t in_flags;
    LR35902::CPU_state_flags_t out_flags;

    uint8_t  instruction;
    uint8_t* target_register;
    bool     immediate;
};

// TEST_P(Gameboy_CPU_Subtract_fixture, Zero_HalfCarry_Carry) {}

// TEST_P(Gameboy_CPU_Subtract_fixture, Zero_HalfCarry_NoCarry) {}

// TEST_P(Gameboy_CPU_Subtract_fixture, Zero_NoHalfCarry_Carry) {}

TEST_P(Gameboy_CPU_Subtract_fixture, Zero_NoHalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = true;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* Provide operands for the test */
    lhs    = 16;
    rhs    = 16;
    answer = 0;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 16;
        rhs = lhs;
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Subtract_fixture, NoZero_HalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = false;

    /* Provide operands for the test */
    lhs    = 16;
    rhs    = 1;
    answer = 15;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Subtract_fixture, NoZero_NoHalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = true;

    /* Provide operands for the test */
    lhs    = 1;
    rhs    = 16;
    answer = 241;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Subtract_fixture, NoZero_NoHalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* Provide operands for the test */
    lhs    = 17;
    rhs    = 1;
    answer = 16;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Subtract,
                        Gameboy_CPU_Subtract_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I', 'M'));
// testing::Values('B'));