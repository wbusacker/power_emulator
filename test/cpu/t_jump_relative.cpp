#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Jump_relative_fixture :
    public ::testing::TestWithParam<std::string>,
    public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        register_file.stack_pointer   = 0x8000;
        register_file.program_counter = 0x1000;

        std::string parameters = GetParam();

        if (parameters.compare("NZ") == 0) {
            instruction              = 0x20;
            register_file.flags.zero = false;
        } else if (parameters.compare("Z") == 0) {
            instruction              = 0x28;
            register_file.flags.zero = true;
        } else if (parameters.compare("NC") == 0) {
            instruction               = 0x30;
            register_file.flags.carry = false;
        } else if (parameters.compare("C") == 0) {
            instruction               = 0x38;
            register_file.flags.carry = true;
        } else if (parameters.compare("-") == 0) {
            instruction = 0x18;
        }

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    uint8_t instruction;
};

TEST_P(Gameboy_CPU_Jump_relative_fixture, ShouldJumpForward) {

    /* Prepare the values for the system to jump to */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0x77));

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x1078);
}

TEST_P(Gameboy_CPU_Jump_relative_fixture, ShouldJumpBackward) {

    /* Prepare the values for the system to jump to */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0xF0));

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x0FF1);
}

TEST_P(Gameboy_CPU_Jump_relative_fixture, ShouldNotJump) {

    /* The unconditional jump will always go, so short-circuit return */
    if (instruction == 0x18) {
        EXPECT_TRUE(true);
        return;
    }

    /* Invert the flag conditions since test setup assumes jump */
    register_file.flags.zero ^= true;
    register_file.flags.carry ^= true;

    /* Give the system addresses to expect calls for */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0x77));

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x1001);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_call,
                        Gameboy_CPU_Jump_relative_fixture,
                        testing::Values("-", "NZ", "Z", "NC", "C"));
