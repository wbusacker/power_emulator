#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Ret_Call_fixture : public ::testing::TestWithParam<std::string>, public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        register_file.stack_pointer   = 0x8000;
        register_file.program_counter = 0x1000;

        std::string parameters = GetParam();

        if (parameters.compare("NZ") == 0) {
            instruction              = 0xC0;
            register_file.flags.zero = false;
        } else if (parameters.compare("Z") == 0) {
            instruction              = 0xC8;
            register_file.flags.zero = true;
        } else if (parameters.compare("NC") == 0) {
            instruction               = 0xD0;
            register_file.flags.carry = false;
        } else if (parameters.compare("C") == 0) {
            instruction               = 0xD8;
            register_file.flags.carry = true;
        } else if (parameters.compare("-") == 0) {
            instruction = 0xC9;
        } else if (parameters.compare("I") == 0) {
            /* We include the RETI instruction here without checking to see if its
                interrupt enabling capabilities actually happen. That is dealt with
                in the interrupt dedicated unit tests
            */
            instruction = 0xD9;
        }
    }
};

TEST_P(Gameboy_CPU_Ret_Call_fixture, ShouldJump) {

    /* Prepare the values for the system to jump to */
    EXPECT_CALL(mocked_bus, read(0x8000)).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x8001)).WillOnce(Return(0xAA));

    run_instruction();

    EXPECT_EQ(register_file.program_counter, 0xAA77);
    EXPECT_EQ(register_file.stack_pointer, 0x8002);
}

TEST_P(Gameboy_CPU_Ret_Call_fixture, ShouldNotJump) {

    /* The unconditional jump will always go, so short-circuit return */
    if ((instruction == 0xC9) || (instruction == 0xD9)) {
        EXPECT_TRUE(true);
        return;
    }

    /* Invert the flag conditions since test setup assumes jump */
    register_file.flags.zero ^= true;
    register_file.flags.carry ^= true;

    /* If we're not jumping, we shouldn't read any data */
    EXPECT_CALL(mocked_bus, read(_)).Times(0);

    run_instruction();

    EXPECT_EQ(register_file.program_counter, 0x1000);
    EXPECT_EQ(register_file.stack_pointer, 0x8000);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Ret_Call,
                        Gameboy_CPU_Ret_Call_fixture,
                        testing::Values("-", "NZ", "Z", "NC", "C", "I"));
