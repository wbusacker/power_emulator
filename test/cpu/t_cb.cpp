#include <common_memory_bus_mocks.h>
#include <cpu.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <t_instruction_coverage.h>

class Gameboy_cpu_cb_test_mock : public LR35902::Gameboy_cpu {
    public:
    Gameboy_cpu_cb_test_mock(Common_memory::Common_memory_bus* main_bus) : Gameboy_cpu(main_bus) { }

    MOCK_METHOD(void, perform_cb_instruction, (uint8_t), (override));
    MOCK_METHOD(void, interrupt_processing, ());
};

TEST(Gameboy_CPU_CB_Test, ExpectCBCall) {

    Mocks::Common_memory_bus        mocked_bus;
    Gameboy_cpu_cb_test_mock        cpu(&mocked_bus);
    struct LR35902::Register_file_t register_file;

    register_file.A                = 0;
    register_file.B                = 0;
    register_file.C                = 0;
    register_file.D                = 0;
    register_file.E                = 0;
    register_file.H                = 0;
    register_file.L                = 0;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 0;
    register_file.program_counter  = 0x1000;

    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(::testing::Return(0xCB));
    EXPECT_CALL(mocked_bus, read(0x1001)).WillOnce(::testing::Return(0x20));
    EXPECT_CALL(cpu, perform_cb_instruction(0x20));
    EXPECT_CALL(cpu, interrupt_processing);

    cpu.set_register_state(&register_file);
    cpu.cycle();

    /* Register that we've performed this instruction */
    inst_map[0xCB]                  = true;
    opcode_map[unique_opcode[0xCB]] = true;
}