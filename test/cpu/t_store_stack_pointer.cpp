#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Store_Stack_Pointer_fixture : public ::testing::Test, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Store_Stack_Pointer_fixture() { }
};

TEST_F(Gameboy_CPU_Store_Stack_Pointer_fixture, LoadImmediate) {

    register_file.program_counter = 0x1000;
    register_file.stack_pointer   = 0xBEEFU;

    EXPECT_CALL(mocked_bus, read(0x1000)).Times(1).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x1001)).Times(1).WillOnce(Return(0xAA));
    EXPECT_CALL(mocked_bus, write(0xAA77, 0xEF)).Times(1);
    EXPECT_CALL(mocked_bus, write(0xAA78, 0xBE)).Times(1);

    instruction = 0x08;

    run_instruction();

    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);

    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    EXPECT_FALSE(register_file.flags.carry);

    EXPECT_EQ(register_file.stack_pointer, 0xBEEF);
    EXPECT_EQ(register_file.program_counter, 0x1002);
}
