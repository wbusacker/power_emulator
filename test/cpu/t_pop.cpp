#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Pop_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        register_file.stack_pointer   = 0x8000;
        register_file.program_counter = 0x1000;

        switch (GetParam()) {
            case 'A':
                instruction   = 0xF1;
                upper_pointer = &register_file.A;
                lower_pointer = nullptr;
                break;
            case 'B':
                instruction   = 0xC1;
                upper_pointer = &register_file.B;
                lower_pointer = &register_file.C;
                break;
            case 'D':
                instruction   = 0xD1;
                upper_pointer = &register_file.D;
                lower_pointer = &register_file.E;
                break;
            case 'H':
                instruction   = 0xE1;
                upper_pointer = &register_file.H;
                lower_pointer = &register_file.L;
                break;
        }
    }

    uint8_t* lower_pointer;
    uint8_t* upper_pointer;
};

TEST_P(Gameboy_CPU_Pop_fixture, CorrectPop) {

    /* Make sure the lower returned byte is different between the two nibbles
        to make sure that the state flags get correctly mapped
    */
    EXPECT_CALL(mocked_bus, read(0x8000)).Times(1).WillOnce(Return(0xA5));
    EXPECT_CALL(mocked_bus, read(0x8001)).Times(1).WillOnce(Return(0x77));

    run_instruction();

    /* Check the Upper Bytes */
    EXPECT_EQ(register_file.A, (upper_pointer == &register_file.A) ? 0x77 : 0);
    EXPECT_EQ(register_file.B, (upper_pointer == &register_file.B) ? 0x77 : 0);
    EXPECT_EQ(register_file.D, (upper_pointer == &register_file.D) ? 0x77 : 0);
    EXPECT_EQ(register_file.H, (upper_pointer == &register_file.H) ? 0x77 : H_INDIRECT_VALUE);

    /* Check the Lower Bytes */
    EXPECT_EQ(register_file.C, (lower_pointer == &register_file.C) ? 0xA5 : 0);
    EXPECT_EQ(register_file.E, (lower_pointer == &register_file.E) ? 0xA5 : 0);
    EXPECT_EQ(register_file.L, (lower_pointer == &register_file.L) ? 0xA5 : L_INDIRECT_VALUE);

    if (lower_pointer == nullptr) {
        EXPECT_TRUE(register_file.flags.zero);
        EXPECT_FALSE(register_file.flags.subtract);
        EXPECT_TRUE(register_file.flags.half_carry);
        EXPECT_FALSE(register_file.flags.carry);
        EXPECT_EQ(register_file.flags.paddinging, 0x00);
    } else {
        EXPECT_FALSE(register_file.flags.zero);
        EXPECT_FALSE(register_file.flags.subtract);
        EXPECT_FALSE(register_file.flags.half_carry);
        EXPECT_FALSE(register_file.flags.carry);
    }

    EXPECT_EQ(register_file.program_counter, 0x1000);
    EXPECT_EQ(register_file.stack_pointer, 0x8002);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Pop, Gameboy_CPU_Pop_fixture, testing::Values('A', 'B', 'D', 'H'));
