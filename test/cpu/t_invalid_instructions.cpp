#include <t_common_powerboy_fixture.h>

class Gameboy_CPU_Invalid_Instruction_fixture :
    public ::testing::TestWithParam<uint8_t>,
    public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Invalid_Instruction_fixture() { }

    void SetUp() { instruction = GetParam(); }
};

TEST_P(Gameboy_CPU_Invalid_Instruction_fixture, InvalidOp) {
    register_file.A                = 0;
    register_file.B                = 1;
    register_file.C                = 2;
    register_file.D                = 3;
    register_file.E                = 4;
    register_file.H                = 5;
    register_file.L                = 6;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 7;
    register_file.program_counter  = 8;

    /* Issue the instruction and observe that nothing happens */
    run_instruction();

    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 1);
    EXPECT_EQ(register_file.C, 2);
    EXPECT_EQ(register_file.D, 3);
    EXPECT_EQ(register_file.E, 4);
    EXPECT_EQ(register_file.H, 5);
    EXPECT_EQ(register_file.L, 6);

    EXPECT_EQ(register_file.flags.carry, false);
    EXPECT_EQ(register_file.flags.half_carry, false);
    EXPECT_EQ(register_file.flags.subtract, false);
    EXPECT_EQ(register_file.flags.zero, false);

    EXPECT_EQ(register_file.stack_pointer, 7);
    EXPECT_EQ(register_file.program_counter, 8);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Invalid_Instruction,
                        Gameboy_CPU_Invalid_Instruction_fixture,
                        testing::Values(0xD3, 0xDB, 0xDD, 0xE3, 0xE4, 0xEB, 0xEC, 0xED, 0xF4, 0xFC, 0xFD));
