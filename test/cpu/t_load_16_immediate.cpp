#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Load_16_Immediate_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        register_file.program_counter = 0x1000;

        uint8_t base_instruction = 0x01;

        switch (GetParam()) {
            case 'B':
                msb_register = &register_file.B;
                lsb_register = &register_file.C;
                instruction  = base_instruction + 0x00;
                break;
            case 'D':
                msb_register = &register_file.D;
                lsb_register = &register_file.E;
                instruction  = base_instruction + 0x10;
                break;
            case 'H':
                msb_register = &register_file.H;
                lsb_register = &register_file.L;
                instruction  = base_instruction + 0x20;
                break;
            case 'S':
                msb_register = nullptr;
                lsb_register = nullptr;
                instruction  = base_instruction + 0x30;
                break;
        }

        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    uint8_t* lsb_register;
    uint8_t* msb_register;
    uint8_t  instruction;
};

TEST_P(Gameboy_CPU_Load_16_Immediate_fixture, LoadImmediate) {

    EXPECT_CALL(mocked_bus, read(0x1000)).Times(1).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x1001)).Times(1).WillOnce(Return(0xAA));

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x1002);
    if (msb_register != nullptr) {
        EXPECT_EQ(*msb_register, 0xAA);
        EXPECT_EQ(*lsb_register, 0x77);
    } else {
        EXPECT_EQ(register_file.stack_pointer, 0xAA77);
    }
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Load_16_Immediate,
                        Gameboy_CPU_Load_16_Immediate_fixture,
                        testing::Values('B', 'D', 'H', 'S'));