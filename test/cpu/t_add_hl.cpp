#include <t_common_powerboy_fixture.h>

/* Test Cases

Upper bits only

Normal Cases
Half Carry |Carry |   1 + 255 =   0
Half Carry |      |   1 +  15 =  16
           |Carry |  16 + 240 =   0
           |      |   1 +   2 =   3

Double Reference
Half Carry |Carry | 136 + 136 =  16
Half Carry |      |   8 +   8 =  16
           |Carry | 128 + 128 =   0
           |      |   1 +   1 =   2

*/

using namespace testing;

class Gameboy_CPU_Add_HL_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        uint8_t base_instruction = 0x09;

        switch (GetParam()) {
            case 'B':
                msb_register = &register_file.B;
                lsb_register = &register_file.C;
                instruction  = base_instruction + 0x00;
                break;
            case 'D':
                msb_register = &register_file.D;
                lsb_register = &register_file.E;
                instruction  = base_instruction + 0x10;
                break;
            case 'H':
                msb_register = &register_file.H;
                lsb_register = &register_file.L;
                instruction  = base_instruction + 0x20;
                break;
            case 'S':
                msb_register = nullptr;
                lsb_register = nullptr;
                instruction  = base_instruction + 0x30;
                break;
        }

        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    void common_test(void) {

        /* Set the LSBs to zero, as we don't care about them in this test */
        register_file.L = 0;
        if (lsb_register != nullptr) {
            /* The lower bits of the stack pointer get overriden downstream */
            *lsb_register = 0;
        }

        /* Configure the flags for the test */
        register_file.flags.zero       = false;
        register_file.flags.subtract   = true;
        register_file.flags.carry      = ! out_flags.carry;
        register_file.flags.half_carry = ! out_flags.half_carry;

        /* Configure the arguments */
        register_file.H = lhs;
        if (msb_register == nullptr) {
            register_file.stack_pointer = static_cast<uint16_t>(rhs) << 8;
        } else {
            *msb_register = rhs;
        }

        cpu.set_register_state(&register_file);
        cpu.perform_instruction(instruction);
        cpu.get_register_state(&register_file);

        EXPECT_EQ(register_file.flags.zero, false);
        EXPECT_EQ(register_file.flags.subtract, false);
        EXPECT_EQ(register_file.flags.carry, out_flags.carry);
        EXPECT_EQ(register_file.flags.half_carry, out_flags.half_carry);

        /* Check the other register MSBs */
        EXPECT_EQ(register_file.B, (msb_register == &register_file.B) ? rhs : 0);
        EXPECT_EQ(register_file.D, (msb_register == &register_file.D) ? rhs : 0);
        EXPECT_EQ(register_file.H, (lhs + rhs) & 0xFF);
        EXPECT_EQ(register_file.stack_pointer, (msb_register == nullptr) ? static_cast<uint16_t>(rhs) << 8 : 0);

        /* Check the remaining registers */
        EXPECT_EQ(register_file.A, 0);
        EXPECT_EQ(register_file.C, 0);
        EXPECT_EQ(register_file.E, 0);
        EXPECT_EQ(register_file.L, 0);
    }

    uint8_t                    instruction;
    uint8_t*                   target_register;
    LR35902::CPU_state_flags_t out_flags;
    uint8_t                    lhs;
    uint8_t                    rhs;
    uint8_t                    output;

    uint8_t* lsb_register;
    uint8_t* msb_register;
};

TEST_P(Gameboy_CPU_Add_HL_fixture, LSB_Modification) {

    /* Ensure that when we add together, we overflow into the MSB when appropriate and that
        no flags ever get changed based on this operation */

    for (uint16_t i = 0; i < 256; i++) {
        for (uint16_t j = 0; j < 256; j++) {

            /* Setup the variable register argument first */
            if (lsb_register == nullptr) {
                register_file.stack_pointer = j;
            } else {
                *lsb_register = j;
            }

            /* Because the variable register can also be L, always
                re-assert the value here. We'll check for correct behavior later
            */
            register_file.L = i;

            /* Always reset H before we do the test */
            register_file.H = 0;

            cpu.set_register_state(&register_file);
            cpu.perform_instruction(instruction);
            cpu.get_register_state(&register_file);

            uint16_t full_answer = i + j;

            if (lsb_register == &register_file.L) {
                full_answer = 2 * i;
            }

            /* Make sure the flags did not get altered */
            EXPECT_EQ(register_file.flags.zero, false);
            EXPECT_EQ(register_file.flags.subtract, false);
            EXPECT_EQ(register_file.flags.half_carry, false);
            EXPECT_EQ(register_file.flags.carry, false);

            /* Make sure L has the correct value */
            EXPECT_EQ(register_file.L, full_answer & 0xFF);
            EXPECT_EQ(register_file.H, (full_answer > 0xFF) ? 1 : 0);
        }
    }
}

TEST_P(Gameboy_CPU_Add_HL_fixture, HalfCarry_Carry) {

    out_flags.carry      = true;
    out_flags.half_carry = true;

    lhs = 1;
    rhs = 255;

    if (msb_register == &register_file.H) {
        lhs = 136;
        rhs = lhs;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Add_HL_fixture, HalfCarry_NoCarry) {

    out_flags.carry      = false;
    out_flags.half_carry = true;

    lhs = 1;
    rhs = 15;

    if (msb_register == &register_file.H) {
        lhs = 8;
        rhs = lhs;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Add_HL_fixture, NoHalfCarry_Carry) {

    out_flags.carry      = true;
    out_flags.half_carry = false;

    lhs = 16;
    rhs = 240;

    if (msb_register == &register_file.H) {
        lhs = 128;
        rhs = lhs;
    }

    common_test();
}

TEST_P(Gameboy_CPU_Add_HL_fixture, NoHalfCarry_NoCarry) {

    out_flags.carry      = false;
    out_flags.half_carry = false;

    lhs = 1;
    rhs = 2;

    if (msb_register == &register_file.H) {
        lhs = 1;
        rhs = lhs;
    }

    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Add_HL, Gameboy_CPU_Add_HL_fixture, testing::Values('B', 'D', 'H', 'S'));