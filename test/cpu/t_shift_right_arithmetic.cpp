#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Shift_Right_Arithmetic_fixture :
    public ::testing::TestWithParam<char>,
    public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Shift_Right_Arithmetic_fixture(void) {
        target_register = nullptr;
        value           = 0;
        answer          = 0;
        out_carry       = false;
        out_zero        = false;
    }

    void SetUp(void) {

        uint8_t base_instruction = 0x28;

        switch (GetParam()) {
            /* Purposefully have this case fall through to execute
                a normal instruction opcode
            */
            case 'A':
                target_register = &register_file.A;
                instruction     = base_instruction + 7;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 6;
                break;
        }
    }

    void common_test(void) {

        /* Assign the register to have the appropriate value */
        if (target_register == nullptr) {
            /* This is the special instance of trying to use HL indirect */
            EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(value));
            EXPECT_CALL(mocked_bus, write(HL_INDIRECT_ADDRESS, answer));
        } else {
            *target_register = value;
        }

        /* Set carry to the opposite of the expected output */
        register_file.flags.carry = ! out_carry;
        register_file.flags.zero  = ! out_zero;

        run_cb();

        /* Check all of the registers to see if they have the correct answer */
        EXPECT_EQ(register_file.A, (target_register == &register_file.A) ? answer : 0);
        EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? answer : 0);
        EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? answer : 0);
        EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? answer : 0);
        EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? answer : 0);

        /* Because HL can be used indirect and want to test non-zero access calls,
        these have special expectation calls */
        EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? answer : (HL_INDIRECT_ADDRESS >> 8) & 0xFF);
        EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? answer : HL_INDIRECT_ADDRESS & 0xFF);

        /* Subtract and Half Carry should always be unset */
        EXPECT_FALSE(register_file.flags.subtract);
        EXPECT_FALSE(register_file.flags.half_carry);

        /* Check the last two flags */

        /* Expect the proper state flags */
        EXPECT_EQ(register_file.flags.carry, out_carry);
        EXPECT_EQ(register_file.flags.zero, out_zero);
    }

    uint8_t* target_register;
    uint8_t  value;
    uint8_t  answer;
    bool     out_carry;
    bool     out_zero;
};

TEST_P(Gameboy_CPU_Shift_Right_Arithmetic_fixture, ZeroCarry) {
    out_zero  = true;
    out_carry = true;

    value  = 0b00000001;
    answer = 0b00000000;
    common_test();
}

TEST_P(Gameboy_CPU_Shift_Right_Arithmetic_fixture, ZeroNoCarry) {
    out_zero  = true;
    out_carry = false;

    value  = 0b00000000;
    answer = 0b00000000;
    common_test();
}
TEST_P(Gameboy_CPU_Shift_Right_Arithmetic_fixture, NoZeroCarry) {
    out_zero  = false;
    out_carry = true;

    value  = 0b00000011;
    answer = 0b00000001;
    common_test();
}
TEST_P(Gameboy_CPU_Shift_Right_Arithmetic_fixture, NoZeroNoCarry) {
    out_zero  = false;
    out_carry = false;

    value  = 0b10000000;
    answer = 0b11000000;
    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Shift_Right_Arithmetic,
                        Gameboy_CPU_Shift_Right_Arithmetic_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I'));