#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Load_Immediate_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        uint8_t base_instruction = 0x06;

        switch (GetParam()) {
            case 'A':
                target_register = &register_file.A;
                instruction     = base_instruction + 0x38;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0x00;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 0x08;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 0x10;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 0x18;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 0x20;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 0x28;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 0x30;
                break;
        }

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    uint8_t  instruction;
    uint8_t* target_register;
};

TEST_P(Gameboy_CPU_Load_Immediate_fixture, WhenLoaded_ExpectValue) {

    EXPECT_CALL(mocked_bus, read(0x1000)).Times(1).WillOnce(Return(0x77));

    if (target_register == nullptr) {
        EXPECT_CALL(mocked_bus, write(HL_INDIRECT_ADDRESS, 0x77));
    }

    register_file.program_counter = 0x1000;

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.A, (target_register == &register_file.A) ? 0x77 : 0);
    EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? 0x77 : 0);
    EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? 0x77 : 0);
    EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? 0x77 : 0);
    EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? 0x77 : 0);
    EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? 0x77 : 0x01);
    EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? 0x77 : 0x23);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Load_Immediate,
                        Gameboy_CPU_Load_Immediate_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I'));