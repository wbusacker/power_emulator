#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Jump_fixture : public ::testing::TestWithParam<std::string>, public Common_Gameboy_CPU_fixture {
    public:
    void SetUp() {
        register_file.stack_pointer   = 0x8000;
        register_file.program_counter = 0x1000;

        std::string parameters = GetParam();

        if (parameters.compare("NZ") == 0) {
            instruction              = 0xC2;
            register_file.flags.zero = false;
        } else if (parameters.compare("Z") == 0) {
            instruction              = 0xCA;
            register_file.flags.zero = true;
        } else if (parameters.compare("NC") == 0) {
            instruction               = 0xD2;
            register_file.flags.carry = false;
        } else if (parameters.compare("C") == 0) {
            instruction               = 0xDA;
            register_file.flags.carry = true;
        } else if (parameters.compare("-") == 0) {
            instruction = 0xC3;
        }
    }
};

TEST_P(Gameboy_CPU_Jump_fixture, ShouldJump) {

    /* Prepare the values for the system to jump to */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x1001)).WillOnce(Return(0xAA));

    run_instruction();

    EXPECT_EQ(register_file.program_counter, 0xAA77);
}

TEST_P(Gameboy_CPU_Jump_fixture, ShouldNotJump) {

    /* The unconditional jump will always go, so short-circuit return */
    if (instruction == 0xC3) {
        EXPECT_TRUE(true);
        return;
    }

    /* Invert the flag conditions since test setup assumes jump */
    register_file.flags.zero ^= true;
    register_file.flags.carry ^= true;

    /* Give the system addresses to expect calls for */
    EXPECT_CALL(mocked_bus, read(0x1000)).WillOnce(Return(0x77));
    EXPECT_CALL(mocked_bus, read(0x1001)).WillOnce(Return(0xAA));

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(instruction);
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x1002);
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Jump, Gameboy_CPU_Jump_fixture, testing::Values("-", "NZ", "Z", "NC", "C"));

TEST(Gameboy_CPU_Jump_HL, ShouldJump) {

    Mocks::Common_memory_bus        mocked_bus;
    struct LR35902::Register_file_t register_file;
    LR35902::Gameboy_cpu            cpu(&mocked_bus);

    register_file.A                = 0;
    register_file.B                = 0;
    register_file.C                = 0;
    register_file.D                = 0;
    register_file.E                = 0;
    register_file.H                = 0x01;
    register_file.L                = 0x23;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 0x8000;
    register_file.program_counter  = 0x1000;

    cpu.set_register_state(&register_file);
    cpu.perform_instruction(0xE9);
    inst_map[0xE9] = true;
    cpu.get_register_state(&register_file);

    EXPECT_EQ(register_file.program_counter, 0x0123);
}