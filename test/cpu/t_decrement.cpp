#include <t_common_powerboy_fixture.h>

/* Cases

Zero |           |      |   1 - 1 =   0
     |Half Carry |Carry |   0 - 1 = 255
     |Half Carry |      |  16 - 1 =  15
     |           |      |   2 - 1 =   1

*/

using namespace testing;

class Gameboy_CPU_Decrement_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    void SetUp() {

        uint8_t base_instruction = 0x05;

        switch (GetParam()) {
            case 'A':
                target_register = &register_file.A;
                instruction     = base_instruction + 0x38;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0x00;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 0x08;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 0x10;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 0x18;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 0x20;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 0x28;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 0x30;
                break;
        }

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    void common_test() {

        register_file.flags.subtract   = false;
        register_file.flags.zero       = ! out_flags.zero;
        register_file.flags.carry      = out_flags.carry;
        register_file.flags.half_carry = ! out_flags.half_carry;
        /* At this point all test data should be setup and ready to be used */

        /* This is the special instance of trying to use HL indirect */
        if (target_register == nullptr) {
            EXPECT_CALL(mocked_bus, read(0x0123)).WillOnce(Return(in_data));
            EXPECT_CALL(mocked_bus, write(0x0123, out_data));
        } else {
            *target_register = in_data;
        }

        cpu.set_register_state(&register_file);
        cpu.perform_instruction(instruction);
        cpu.get_register_state(&register_file);

        /* Have each of the other registers checked, if they were the target, then
        expect the assigned value otherwise zero
        */
        EXPECT_EQ(register_file.A, (target_register == &register_file.A) ? out_data : 0);
        EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? out_data : 0);
        EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? out_data : 0);
        EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? out_data : 0);
        EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? out_data : 0);

        /* Because HL can be used indirect and want to test non-zero access calls,
            these have special expectation calls
        */
        EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? out_data : 0x01);
        EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? out_data : 0x23);

        EXPECT_EQ(register_file.flags.subtract, true);
        EXPECT_EQ(register_file.flags.zero, out_flags.zero);
        EXPECT_EQ(register_file.flags.carry, out_flags.carry);
        EXPECT_EQ(register_file.flags.half_carry, out_flags.half_carry);
    }

    uint8_t                    instruction;
    uint8_t*                   target_register;
    LR35902::CPU_state_flags_t out_flags;

    uint8_t in_data;
    uint8_t out_data;
};

// TEST_P(Gameboy_CPU_Decrement_fixture, Zero_HalfCarry_Carry) {}
// TEST_P(Gameboy_CPU_Decrement_fixture, Zero_HalfCarry_NoCarry) {}
// TEST_P(Gameboy_CPU_Decrement_fixture, Zero_NoHalfCarry_Carry) {}

TEST_P(Gameboy_CPU_Decrement_fixture, Zero_NoHalfCarry_NoCarry) {

    out_flags.zero       = true;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* Set the data to decrement */
    in_data  = 1;
    out_data = 0;

    /* Perform the generic test */
    common_test();
}

TEST_P(Gameboy_CPU_Decrement_fixture, NoZero_HalfCarry_Carry) {

    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = true;

    /* Set the data to decrement */
    in_data  = 0;
    out_data = 255;

    /* Perform the generic test */
    common_test();
}

TEST_P(Gameboy_CPU_Decrement_fixture, NoZero_HalfCarry_NoCarry) {

    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = false;

    /* Set the data to decrement */
    in_data  = 16;
    out_data = 15;

    /* Perform the generic test */
    common_test();
}

// TEST_P(Gameboy_CPU_Decrement_fixture, NoZero_NoHalfCarry_Carry) {}

TEST_P(Gameboy_CPU_Decrement_fixture, NoZero_NoHalfCarry_NoCarry) {

    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* Set the data to decrement */
    in_data  = 2;
    out_data = 1;

    /* Perform the generic test */
    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Decrement,
                        Gameboy_CPU_Decrement_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I'));