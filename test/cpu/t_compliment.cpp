#include <common_memory_bus_mocks.h>
#include <cpu.h>
#include <gtest/gtest.h>
#include <t_instruction_coverage.h>

TEST(Gameboy_CPU_Complement, WhenCompliment_ExpectAnswer) {

    Mocks::Common_memory_bus        mocked_bus;
    LR35902::Gameboy_cpu            cpu(&mocked_bus);
    struct LR35902::Register_file_t register_file;

    register_file.A                = 0;
    register_file.B                = 0;
    register_file.C                = 0;
    register_file.D                = 0;
    register_file.E                = 0;
    register_file.H                = 0;
    register_file.L                = 0;
    register_file.flags.carry      = false;
    register_file.flags.half_carry = false;
    register_file.flags.subtract   = false;
    register_file.flags.zero       = false;
    register_file.stack_pointer    = 0;
    register_file.program_counter  = 0;

    cpu.set_register_state(&register_file);

    /* Flip everything */
    cpu.perform_instruction(0x2F);

    cpu.get_register_state(&register_file);
    EXPECT_EQ(register_file.A, 0xFF);

    cpu.perform_instruction(0x2F);

    cpu.get_register_state(&register_file);
    EXPECT_EQ(register_file.A, 0);

    EXPECT_EQ(register_file.flags.zero, false);
    EXPECT_EQ(register_file.flags.subtract, true);
    EXPECT_EQ(register_file.flags.half_carry, true);
    EXPECT_EQ(register_file.flags.carry, false);

    inst_map[0x2F]                  = true;
    opcode_map[unique_opcode[0x2F]] = true;
}