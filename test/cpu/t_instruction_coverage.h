#ifndef T_INSTRUCTION_COVERAGE_H
#define T_INSTRUCTION_COVERAGE_H

#include <stdint.h>

extern bool inst_map[256];
extern bool cb_map[256];
extern bool opcode_map[37];
extern bool cb_opcode_map[37];

extern uint16_t unique_opcode[256];
extern uint16_t cb_unique_opcode[256];

#endif