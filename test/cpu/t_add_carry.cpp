#include <common_memory_bus_mocks.h>
#include <gtest/gtest.h>
#include <t_common_powerboy_fixture.h>
#include <t_instruction_coverage.h>
#include <tuple>

/* Test Cases

With Carry
Normal Cases
Zero |Half Carry |Carry |   0 + (255 + 1) =   0
     |Half Carry |Carry |   1 + (255 + 1) =   1
     |Half Carry |      |   0 + ( 15 + 1) =  16
     |           |Carry |  16 + (240 + 1) =   1
     |           |      |   0 + (  0 + 1) =   1

Doubling Cases
     |Half Carry |Carry | 136 + (136 + 1) =  17
     |Half Carry |      |   8 + (  8 + 1) =  17
     |           |Carry | 128 + (128 + 1) =   1
     |           |      |   0 + (  0 + 1) =   1

Without Carry

Normal Cases
Zero |Half Carry |Carry |   1 + (255) =   0
Zero |           |Carry |  16 + (240) =   0
Zero |           |      |   0 + (  0) =   0
     |Half Carry |Carry |   2 + (255) =   1
     |Half Carry |      |   1 + ( 15) =  16
     |           |Carry |  16 + (241) =   1
     |           |      |   0 + (  1) =   1

Doubling Cases
Zero |           |Carry | 128 + (128) =   0
Zero |           |      |   0 + (  0) =   0
     |Half Carry |Carry | 136 + (136) =  16
     |Half Carry |      |   8 + (  8) =  16
     |           |Carry | 129 + (129) =   2
     |           |      |   1 + (  1) =   2

*/

using namespace testing;

class Gameboy_CPU_Add_Carry_fixture : public ::testing::TestWithParam<char>, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Add_Carry_fixture() {
        lhs             = 0;
        rhs             = 0;
        is_a            = false;
        target_register = nullptr;
        immediate       = false;
        in_carry        = false;
    }

    void SetUp() {

        uint8_t base_instruction = 0x88;

        switch (GetParam()) {
            case 'A':
                target_register = &register_file.A;
                instruction     = base_instruction + 7;
                is_a            = true;
                break;
            case 'B':
                target_register = &register_file.B;
                instruction     = base_instruction + 0;
                break;
            case 'C':
                target_register = &register_file.C;
                instruction     = base_instruction + 1;
                break;
            case 'D':
                target_register = &register_file.D;
                instruction     = base_instruction + 2;
                break;
            case 'E':
                target_register = &register_file.E;
                instruction     = base_instruction + 3;
                break;
            case 'H':
                target_register = &register_file.H;
                instruction     = base_instruction + 4;
                break;
            case 'L':
                target_register = &register_file.L;
                instruction     = base_instruction + 5;
                break;
            case 'I':

                /* Just set target register to the dummy target */
                target_register = nullptr;
                instruction     = base_instruction + 6;
                immediate       = false;
                break;
            case 'M':
                target_register = nullptr;
                instruction     = 0xCE;
                immediate       = true;
                break;
        }
    }

    void common_test(void) {

        /* Since this is an add test, always set the same expecations for subtract */
        out_flags.subtract = false;

        /* Configure the CPU's register file */
        register_file.flags.zero       = ! out_flags.zero;
        register_file.flags.subtract   = ! out_flags.subtract;
        register_file.flags.half_carry = ! out_flags.half_carry;
        register_file.flags.carry      = in_carry;

        /* Setup the appropriate arguments */
        register_file.A = lhs;
        if (target_register == nullptr) {
            if (immediate) {
                EXPECT_CALL(mocked_bus, read(register_file.program_counter)).WillOnce(Return(rhs));
            } else {
                /* This is the special instance of trying to use HL indirect */
                EXPECT_CALL(mocked_bus, read(HL_INDIRECT_ADDRESS)).WillOnce(Return(rhs));
            }
        } else {
            *target_register = rhs;
        }

        run_instruction();

        /* Have each of the other register_file checked, if they were the target, then
        expect the assigned value otherwise zero */
        EXPECT_EQ(register_file.B, (target_register == &register_file.B) ? rhs : 0);
        EXPECT_EQ(register_file.C, (target_register == &register_file.C) ? rhs : 0);
        EXPECT_EQ(register_file.D, (target_register == &register_file.D) ? rhs : 0);
        EXPECT_EQ(register_file.E, (target_register == &register_file.E) ? rhs : 0);

        /* Because HL can be used indirect and want to test non-zero access calls,
        these have special expectation calls */
        EXPECT_EQ(register_file.H, (target_register == &register_file.H) ? rhs : (HL_INDIRECT_ADDRESS >> 8) & 0xFF);
        EXPECT_EQ(register_file.L, (target_register == &register_file.L) ? rhs : HL_INDIRECT_ADDRESS & 0xFF);

        /* Check for the appropriate answer */
        uint8_t answer = (lhs + rhs) & 0xFF;
        if (in_carry) {
            answer++;
        }
        EXPECT_EQ(register_file.A, answer);

        /* Expect the proper state flags */
        EXPECT_EQ(register_file.flags.zero, out_flags.zero);
        EXPECT_EQ(register_file.flags.subtract, out_flags.subtract);
        EXPECT_EQ(register_file.flags.half_carry, out_flags.half_carry);
        EXPECT_EQ(register_file.flags.carry, out_flags.carry);
    }

    /* General Purpose Test Configuration */
    uint8_t lhs;
    uint8_t rhs;
    bool    is_a;
    bool    in_carry;

    uint8_t* target_register;
    bool     immediate;
};

TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_Zero_HalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = true;
    out_flags.half_carry = true;
    out_flags.carry      = true;

    /* We explicitly want input carry */
    in_carry = true;

    /* Provide operands for the test */
    lhs = 1;
    rhs = 254;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        /* Case does not exist */
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

// TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_Zero_HalfCarry_NoCarry) {}

// TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_Zero_NoHalfCarry_Carry) {}

// TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_Zero_NoHalfCarry_NoCarry) {}

TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_NoZero_HalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = true;

    /* We explicitly want input carry */
    in_carry = true;

    /* Provide operands for the test */
    lhs = 2;
    rhs = 254;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 136;
        rhs = lhs;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_NoZero_HalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = false;

    /* We explicitly want input carry */
    in_carry = true;

    /* Provide operands for the test */
    lhs = 1;
    rhs = 14;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 8;
        rhs = lhs;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_NoZero_NoHalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = true;

    /* We explicitly want input carry */
    in_carry = true;

    /* Provide operands for the test */
    lhs = 16;
    rhs = 240;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 128;
        rhs = lhs;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, InCarry_NoZero_NoHalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* We explicitly want input carry */
    in_carry = true;

    /* Provide operands for the test */
    lhs = 0;
    rhs = 1;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 1;
        rhs = lhs;

        /* Case does not exist */
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_Zero_HalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = true;
    out_flags.half_carry = true;
    out_flags.carry      = true;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 1;
    rhs = 255;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        /* Case does not exist */
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

// TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_Zero_HalfCarry_NoCarry) {}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_Zero_NoHalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = true;
    out_flags.half_carry = false;
    out_flags.carry      = true;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 16;
    rhs = 240;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 128;
        rhs = lhs;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_Zero_NoHalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = true;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 0;
    rhs = 0;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 0;
        rhs = lhs;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_NoZero_HalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = true;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 2;
    rhs = 255;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 136;
        rhs = lhs;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_NoZero_HalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = true;
    out_flags.carry      = false;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 1;
    rhs = 15;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 8;
        rhs = lhs;

        /* Case does not exist */
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_NoZero_NoHalfCarry_Carry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = true;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 16;
    rhs = 241;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 129;
        rhs = lhs;

        /* Case does not exist */
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

TEST_P(Gameboy_CPU_Add_Carry_fixture, NoInCarry_NoZero_NoHalfCarry_NoCarry) {

    /* Set the expected output flags */
    out_flags.zero       = false;
    out_flags.half_carry = false;
    out_flags.carry      = false;

    /* We explicitly don't want input carry */
    in_carry = false;

    /* Provide operands for the test */
    lhs = 0;
    rhs = 1;

    /* If this is an A + A situation, set the doubling case */
    if (is_a) {
        lhs = 1;
        rhs = lhs;

        /* Case does not exist */
        EXPECT_TRUE(true);
        return;
    }

    /* Let the common test finish the rest */
    common_test();
}

INSTANTIATE_TEST_CASE_P(Gameboy_CPU_Add_Carry,
                        Gameboy_CPU_Add_Carry_fixture,
                        testing::Values('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I', 'M'));