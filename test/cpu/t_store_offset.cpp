#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Store_Offset_fixture : public ::testing::Test, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Store_Offset_fixture() { }
};

TEST_F(Gameboy_CPU_Store_Offset_fixture, WhenOffsetC) {

    EXPECT_CALL(mocked_bus, write(0xFF77, 0xAA)).Times(1);

    register_file.A = 0xAA;
    register_file.C = 0x77;

    instruction = 0xE2;

    run_instruction();

    EXPECT_EQ(register_file.A, 0xAA);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0x77);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);

    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    EXPECT_FALSE(register_file.flags.carry);

    EXPECT_EQ(register_file.stack_pointer, 0);
    EXPECT_EQ(register_file.program_counter, 0);
}

TEST_F(Gameboy_CPU_Store_Offset_fixture, WhenOffsetImmediate) {

    EXPECT_CALL(mocked_bus, write(0xFF77, 0xAA)).Times(1);
    EXPECT_CALL(mocked_bus, read(0)).Times(1).WillOnce(Return(0x77));

    register_file.A = 0xAA;

    instruction = 0xE0;

    run_instruction();

    EXPECT_EQ(register_file.A, 0xAA);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);

    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    EXPECT_FALSE(register_file.flags.carry);

    EXPECT_EQ(register_file.stack_pointer, 0);
    EXPECT_EQ(register_file.program_counter, 1);
}
