#include <common_memory_bus_mocks.h>
#include <cpu.h>
#include <gtest/gtest.h>
#include <t_instruction_coverage.h>

const uint16_t L_INDIRECT_VALUE    = 0x23;
const uint16_t H_INDIRECT_VALUE    = 0x01;
const uint16_t HL_INDIRECT_ADDRESS = 0x0123;

class Common_Gameboy_CPU_fixture {

    public:
    Common_Gameboy_CPU_fixture() : cpu(&mocked_bus) {
        register_file.A                = 0;
        register_file.B                = 0;
        register_file.C                = 0;
        register_file.D                = 0;
        register_file.E                = 0;
        register_file.H                = H_INDIRECT_VALUE;
        register_file.L                = L_INDIRECT_VALUE;
        register_file.flags.carry      = false;
        register_file.flags.half_carry = false;
        register_file.flags.subtract   = false;
        register_file.flags.zero       = false;
        register_file.stack_pointer    = 0;
        register_file.program_counter  = 0;
        register_file.cb_mode          = false;
        register_file.cycle_time       = 0;

        out_flags.carry      = false;
        out_flags.half_carry = false;
        out_flags.subtract   = false;
        out_flags.zero       = false;

        instruction = 0;

        cpu.set_register_state(&register_file);
    }

    void run_instruction(void) {
        register_file.cb_mode = false;
        cpu.set_register_state(&register_file);
        cpu.perform_instruction(instruction);
        cpu.get_register_state(&register_file);

        /* Register that we've performed this instruction */
        inst_map[instruction]                  = true;
        opcode_map[unique_opcode[instruction]] = true;
    }

    void run_cb(void) {
        register_file.cb_mode = true;
        cpu.set_register_state(&register_file);
        cpu.perform_cb_instruction(instruction);
        cpu.get_register_state(&register_file);

        /* Register that we've performed this instruction */
        cb_map[instruction]                          = true;
        cb_opcode_map[cb_unique_opcode[instruction]] = true;
    }

    Mocks::Common_memory_bus        mocked_bus;
    LR35902::Gameboy_cpu            cpu;
    struct LR35902::Register_file_t register_file;
    LR35902::CPU_state_flags_t      out_flags;
    uint8_t                         instruction;
};