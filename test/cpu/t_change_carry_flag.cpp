#include <t_common_powerboy_fixture.h>

using namespace testing;

class Gameboy_CPU_Change_Carry_Flag_fixture : public Test, public Common_Gameboy_CPU_fixture {

    public:
    Gameboy_CPU_Change_Carry_Flag_fixture() { }
};

TEST_F(Gameboy_CPU_Change_Carry_Flag_fixture, Compliment_Carry) {
    instruction = 0x3F;

    register_file.flags.carry      = false;
    register_file.flags.zero       = false;
    register_file.flags.half_carry = true;
    register_file.flags.subtract   = true;

    run_instruction();

    EXPECT_TRUE(register_file.flags.carry);
    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);

    /* Make sure none of the other registers are effected */
    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);
    EXPECT_EQ(register_file.stack_pointer, 0);
    EXPECT_EQ(register_file.program_counter, 0);

    /* Set Zero to ensure that it is also not affected */
    register_file.flags.zero = true;

    run_instruction();
    EXPECT_FALSE(register_file.flags.carry);
    EXPECT_TRUE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);

    /* Make sure none of the other registers are effected */
    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);
    EXPECT_EQ(register_file.stack_pointer, 0);
    EXPECT_EQ(register_file.program_counter, 0);
}

TEST_F(Gameboy_CPU_Change_Carry_Flag_fixture, Set_Carry) {
    instruction = 0x37;

    register_file.flags.carry      = false;
    register_file.flags.zero       = false;
    register_file.flags.half_carry = true;
    register_file.flags.subtract   = true;

    run_instruction();

    EXPECT_TRUE(register_file.flags.carry);
    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    /* Make sure none of the other registers are effected */
    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);
    EXPECT_EQ(register_file.stack_pointer, 0);
    EXPECT_EQ(register_file.program_counter, 0);

    run_instruction();

    EXPECT_TRUE(register_file.flags.carry);
    EXPECT_FALSE(register_file.flags.zero);
    EXPECT_FALSE(register_file.flags.subtract);
    EXPECT_FALSE(register_file.flags.half_carry);
    /* Make sure none of the other registers are effected */
    EXPECT_EQ(register_file.A, 0);
    EXPECT_EQ(register_file.B, 0);
    EXPECT_EQ(register_file.C, 0);
    EXPECT_EQ(register_file.D, 0);
    EXPECT_EQ(register_file.E, 0);
    EXPECT_EQ(register_file.H, H_INDIRECT_VALUE);
    EXPECT_EQ(register_file.L, L_INDIRECT_VALUE);
    EXPECT_EQ(register_file.stack_pointer, 0);
    EXPECT_EQ(register_file.program_counter, 0);
}