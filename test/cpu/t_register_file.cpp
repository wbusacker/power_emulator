#include <cpu_types_data.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string.h>

/** This test ensures that the compiler is correctly ordering the bit
 *  positions of the CPU state flags within their bit-packed struct
 */
TEST(Gameboy_CPU_Register_File, Zero_correct_position) {

    struct LR35902::Register_file_t register_file;

    memset(&register_file, 0, sizeof(struct LR35902::Register_file_t));

    /* Set the flag we want to watch */
    register_file.flags.zero = true;

    /* Grab the raw memory address */

    uint8_t* direct_access = reinterpret_cast<uint8_t*>(&(register_file.flags));

    EXPECT_EQ(*direct_access, 0b10000000);
}

TEST(Gameboy_CPU_Register_File, Subtract_correct_position) {

    struct LR35902::Register_file_t register_file;

    memset(&register_file, 0, sizeof(struct LR35902::Register_file_t));

    /* Set the flag we want to watch */
    register_file.flags.subtract = true;

    /* Grab the raw memory address */

    uint8_t* direct_access = reinterpret_cast<uint8_t*>(&(register_file.flags));

    EXPECT_EQ(*direct_access, 0b01000000);
}

TEST(Gameboy_CPU_Register_File, half_carry_correct_position) {

    struct LR35902::Register_file_t register_file;

    memset(&register_file, 0, sizeof(struct LR35902::Register_file_t));

    /* Set the flag we want to watch */
    register_file.flags.half_carry = true;

    /* Grab the raw memory address */

    uint8_t* direct_access = reinterpret_cast<uint8_t*>(&(register_file.flags));

    EXPECT_EQ(*direct_access, 0b00100000);
}

TEST(Gameboy_CPU_Register_File, Carry_correct_position) {

    struct LR35902::Register_file_t register_file;

    memset(&register_file, 0, sizeof(struct LR35902::Register_file_t));

    /* Set the flag we want to watch */
    register_file.flags.carry = true;

    /* Grab the raw memory address */

    uint8_t* direct_access = reinterpret_cast<uint8_t*>(&(register_file.flags));

    EXPECT_EQ(*direct_access, 0b00010000);
}

TEST(Gameboy_CPU_Register_File, Padding_correct_position) {

    struct LR35902::Register_file_t register_file;

    memset(&register_file, 0, sizeof(struct LR35902::Register_file_t));

    /* Set the flag we want to watch */
    register_file.flags.paddinging = 0xF;

    /* Grab the raw memory address */

    uint8_t* direct_access = reinterpret_cast<uint8_t*>(&(register_file.flags));

    EXPECT_EQ(*direct_access, 0b00001111);
}

TEST(Gameboy_CPU_Register_File, Padding_overflow_correct_position) {

    struct LR35902::Register_file_t register_file;

    memset(&register_file, 0, sizeof(struct LR35902::Register_file_t));

/* We are deliberatly overflowing here, so tell the compiler to calm down */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
    register_file.flags.paddinging = 0x1F;
#pragma GCC diagnostic pop

    /* Grab the raw memory address */

    uint8_t* direct_access = reinterpret_cast<uint8_t*>(&(register_file.flags));

    EXPECT_EQ(*direct_access, 0b00001111);
}

TEST(Gameboy_CPU_Register_File, MapsToOneByte) {

    EXPECT_EQ(sizeof(struct LR35902::CPU_state_flags_t), 1);
}