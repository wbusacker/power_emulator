#ifndef GP_TIMERS_MOCK_H
#define GP_TIMERS_MOCK_H
#include <cpu.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace Mocks {

class Gameboy_cpu : public LR35902::Gameboy_cpu {
    public:
    Gameboy_cpu(Common_memory::Common_memory_bus* main_bus) : LR35902::Gameboy_cpu(main_bus) { }

    private:
};

}    // namespace Mocks

#endif
