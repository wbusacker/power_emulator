#include <gtest/gtest.h>
#include <t_instruction_coverage.h>

int main(int argc, char** argv) {

    for (uint16_t i = 0; i < 256; i++) {
        inst_map[i] = false;
        cb_map[i]   = false;
    }

    for (uint8_t i = 0; i < 37; i++) {
        opcode_map[i]    = false;
        cb_opcode_map[i] = false;
    }

    testing::InitGoogleTest(&argc, argv);

    int status = RUN_ALL_TESTS();

    uint16_t covered_instructions = 0;
    /* Print out the instruction coverage mapping */
    // printf("Instruction Coverage                               | ");
    // printf("CB Coverage\n");
    // printf("   x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 xA xB xC xD xE xF | ");
    // printf("   x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 xA xB xC xD xE xF\n");
    // for (uint8_t i = 0; i < 16; i++) {
    //     /* Print out the normal instruction map row */
    //     printf("%01Xx ", i);
    //     for (uint8_t j = 0; j < 16; j++) {
    //         uint8_t index = (i * 16) + j;
    //         printf(" %c ", inst_map[index] ? 'X' : ' ');
    //         covered_instructions += inst_map[index] ? 1 : 0;
    //     }
    //     /* Print out the CB map row */
    //     printf("| %01Xx ", i);
    //     for (uint8_t j = 0; j < 16; j++) {
    //         uint8_t index = (i * 16) + j;
    //         printf(" %c ", cb_map[index] ? 'X' : ' ');
    //         covered_instructions += cb_map[index] ? 1 : 0;
    //     }
    //     printf("\n");
    // }

    // printf("%d of 512: %06.3f%% Instruction Coverage\n", covered_instructions, (covered_instructions / 512.0) *
    // 100.0);

    /* Count number of covered opcodes */
    // uint8_t opcode_count = 0;
    // for (uint8_t i = 0; i < 37; i++) {
    //     if (opcode_map[i]) {
    //         opcode_count++;
    //     }
    //     if (cb_opcode_map[i]) {
    //         opcode_count++;
    //     }
    // }

    // printf("%d of 48: %06.3F%% Opcode Coverage\n", opcode_count, (opcode_count / 48.0) * 100.0);

    return status;
}