# Power Emulator
Power Emulator is a CPU only emulator framework designed to emulate Nintendo consoles

# Test Status

## Blargg's Tests

### CPU Instruction Tests

| Test Number  | Status | Missing |
| --- | --- | --- |
| 1 Special    | FAIL | DAA |
| 2 Interrupts | FAIL | Needs Timer Implementation |
| 3 OP SP,HL   | PASS | |
| 4 OP R,imm   | PASS | |
| 5 OP RP      | PASS | |
| 6 LD R,R     | PASS | |
| 7 JR, JP, CALL, RET, RST | PASS | |
| 8 Misc Instructions | PASS | |
| 9 OP R,R     | PASS | |
| 10 Bit Ops   | PASS | |
| 11 OP A,[HL] | FAIL | 27 |

# Known Bugs

## Picture Processing Unit
* Y Axis sprite flipping is bugged

## Sound Synthesizer
* Wayyyyy too many issues to list (and I'm not even sure of all of them)
# Sources

## Gameboy (Light | Pocket)

1. Joonas Javanainen "Game Boy: Complete Technical Reference" Rev. 100 Feb 2, 2020 https://gekkio.fi/files/gb-docs/gbctr.pdf
2. Pan of Antrhox, GABY, Marat Fayzullin, Pascal Felber, Paul Robson, Martin Korth, kOOPa, Bowser "Game Boy CPU Manual" V 1.01 March 1, 1998 http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf
3. Natesh Narain "Gameboy LCD Controller" September 9, 2016 https://nnarain.github.io/2016/09/09/Gameboy-LCD-Controller.html