#include <power_emulator.h>
#include <stdio.h>

int main(int argc, char** argv) {

    /* Check to see if we had a name passed in to load */
    if (argc < 2) {
        printf("Usage: %s filename.gb\n", argv[0]);
    } else {
        Power::Power_emulator emulator(Power::POWERBOY, argv[1]);
        emulator.execute();
    }

    return 0;
}
