#ifndef DEBUGGER_H
#define DEBUGGER_H
#include <common_memory_bus_types_data.h>
#include <cpu.h>
#include <debugger_const.h>
#include <debugger_types_data.h>
#include <picture_processing_unit.h>
#include <pthread.h>
#include <window_manager.h>

namespace Debug {

class Debugger : public Common_memory::CMB_interface {
    public:
    Debugger(Cart::Gameboy::Memory_controller* mem_ctrl,
             Common_memory::Common_memory_bus* mem_bus,
             LR35902::Gameboy_cpu*             cpu);
    ~Debugger(void);

    void cycle(void);

    static void* input_processor(void* arg);
    void         command_processor(const char* command);

    private:
    void update_vram_viewer(void);
    void print_cpu(void);

    pthread_t thread_handle;

    Cart::Gameboy::Memory_controller* memory_controller;
    Common_memory::Common_memory_bus* memory_bus;
    LR35902::Gameboy_cpu*             cpu_core;

    Display::Window_manager* graphics_display;

    struct Breakpoint                breakpoint[Debug::MAX_NUM_BREAKPOINTS];
    struct Watchpoint                watchpoint[Debug::MAX_NUM_BREAKPOINTS];
    volatile uint64_t                num_breakpoints;
    volatile uint64_t                num_watchpoints;
    volatile struct Debugger_actions action;

    struct History backtrace[HISTORY_TRACE_BUFFER_LEN];
    int16_t        backtrace_index;

    volatile bool exiting;

    uint16_t tile_pattern_row_index;
    uint32_t render_cycles;
};

}    // namespace Debug

#endif
