#include <debugger.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

namespace Debug {

int c_read(int fd, char* buf, int size) {
    return read(fd, buf, size);
}

void* Debugger::input_processor(void* arg) {

    Debug::Debugger* dbg = reinterpret_cast<Debug::Debugger*>(arg);

    char     input_buffer[CONSOLE_LINE_LENGTH];
    uint64_t buffer_index = 0;

    /* Set stdin as non blocking */

    fcntl(fileno(stdin), F_SETFL, fcntl(fileno(stdin), F_GETFL) | O_NONBLOCK);

    while (! dbg->exiting) {
        /* Grab a single character from stdin */
        int ret_val = c_read(fileno(stdin), &(input_buffer[buffer_index]), 1);

        /* Check to see if we got a character */
        if (ret_val == 1) {
            /* We got a character, check to see if it was newline */
            if (input_buffer[buffer_index] != '\n') {
                buffer_index++;
            } else {
                input_buffer[buffer_index + 1] = '\0';
                dbg->command_processor(input_buffer);
                buffer_index = 0;
            }
        }

        usleep(Debug::DEBUGGER_SLEEP_POLL_DURATION_US);
    }

    return nullptr;
}

}    // namespace Debug
