#include <debugger.h>
#include <errno.h>
#include <regex>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>

namespace Debug {

void Debugger::command_processor(const char* command) {

    std::string string_command(command);

    /* Capture the regex for the different commands */
    static std::regex set_breakpoint(Debug::SET_BREAKPOINT_MATCH);
    static std::regex disable_breakpoint(Debug::DISABLE_BREAKPOINT_MATCH);
    static std::regex set_watchpoint(Debug::SET_WATCHPOINT_MATCH);
    static std::regex disable_watchpoint(Debug::DISABLE_WATCHPOINT_MATCH);
    static std::regex display_cpu_reg(Debug::DISPLAY_CPU_REG_MATCH);
    static std::regex halt(Debug::HALT_MATCH);
    static std::regex cont(Debug::CONTINUE_MATCH);
    static std::regex write_memory(Debug::WRITE_MEMORY_MATCH);
    static std::regex read_memory(Debug::READ_MEMORY_MATCH);
    static std::regex read_multiple_memory(Debug::READ_MULTIPLE_MEMORY_MATCH);
    static std::regex step(Debug::STEP_MATCH);
    static std::regex step_multiple(Debug::STEP_MULTIPLE_MATCH);
    static std::regex step_frame(Debug::STEP_FRAME_MATCH);
    static std::regex back_trace(Debug::BACKTRACE_MATCH);

    std::smatch matches;

    /* Check to see if a breakpoint is being set */
    if (std::regex_search(string_command, matches, disable_breakpoint) != 0) {
        uint64_t bp_index = std::stoi(matches[1].str());
        if (bp_index < num_breakpoints) {
            breakpoint[bp_index].enabled = false;
        }
    } else if (std::regex_search(string_command, matches, disable_watchpoint) != 0) {
        uint64_t bp_index = std::stoi(matches[1].str());
        if (bp_index < num_watchpoints) {
            watchpoint[bp_index].enabled = false;
        }
    } else if (std::regex_search(string_command, matches, set_breakpoint) != 0) {

        struct Breakpoint* bp = &(breakpoint[num_breakpoints]);

        bp->rom_bank = std::stoi(matches[1].str());
        bp->addr     = std::stoi(matches[2].str(), 0, 16);
        bp->enabled  = true;

        printf("Set breakpoint %ld at bank:%d addr:%04X\n", num_breakpoints, bp->rom_bank, bp->addr);

        num_breakpoints++;

    } else if (std::regex_search(string_command, matches, set_watchpoint) != 0) {

        struct Watchpoint* wp = &(watchpoint[num_watchpoints]);

        wp->addr           = std::stoi(matches[1].str(), 0, 16);
        wp->enabled        = true;
        wp->previous_value = memory_bus->read(wp->addr);

        printf("Set watchpoint %ld at addr:%04X\n", num_watchpoints, wp->addr);

        num_watchpoints++;

    } else if (std::regex_search(string_command, matches, display_cpu_reg) != 0) {
        print_cpu();
    } else if (std::regex_search(string_command, matches, halt) != 0) {
        action.halt_execution = true;
    } else if (std::regex_search(string_command, matches, cont) != 0) {
        action.halt_execution = false;
    } else if (std::regex_search(string_command, matches, write_memory) != 0) {
        printf("Modifying Memory\n");
        fflush(stdout);
        uint16_t addr = std::stoi(matches[1].str(), 0, 16);
        uint8_t  data = std::stoi(matches[2].str(), 0, 16);
        memory_bus->write(addr, data);
        printf("%04X: %02X\n", addr, memory_bus->read(addr));
    } else if (std::regex_search(string_command, matches, read_multiple_memory) != 0) {
        uint16_t count     = std::stoi(matches[2].str());
        uint16_t addr      = std::stoi(matches[1].str(), 0, 16);
        bool     first_row = true;
        for (uint16_t i = 0; i < count; i++) {
            if ((i % 16) == 0) {
                if (first_row == false) {
                    printf("\n");
                } else {
                    first_row = false;
                }
                printf("%04X: ", addr);
            }
            printf("%02X ", memory_bus->read(addr));
            fflush(stdout);
            addr++;
        }
        printf("\n");
    } else if (std::regex_search(string_command, matches, read_memory) != 0) {
        uint16_t addr = std::stoi(matches[1].str(), 0, 16);
        printf("%04X: %02X\n", addr, memory_bus->read(addr));
    } else if (std::regex_search(string_command, matches, step_multiple) != 0) {
        action.step_cycle     = std::stoi(matches[1].str());
        action.halt_execution = false;
    } else if (std::regex_search(string_command, matches, step) != 0) {
        action.step_cycle     = 1;
        action.halt_execution = false;
    } else if (std::regex_search(string_command, matches, step_frame) != 0) {
        action.step_frame     = 1;
        action.halt_execution = false;
        action.intra_frame    = ((memory_bus->read(0xFF41) & 0b11) != 0b01);
    } else if (std::regex_search(string_command, matches, back_trace) != 0) {
        int16_t trace_start = backtrace_index - HISTORY_TRACE_BUFFER_LEN;
        if (trace_start < 0) {
            trace_start += HISTORY_TRACE_BUFFER_LEN;
        }

        LR35902::Register_file_t active_registers;
        cpu_core->get_register_state(&active_registers);

        for (uint16_t i = 0; i < HISTORY_TRACE_BUFFER_LEN; i++) {
            memory_controller->set_rom_bank(backtrace[trace_start].rom_bank);
            memory_controller->set_ram_bank(backtrace[trace_start].ram_bank);
            cpu_core->set_register_state(&(backtrace[trace_start].registers));
            print_cpu();
            trace_start++;
            if (trace_start >= HISTORY_TRACE_BUFFER_LEN) {
                trace_start = 0;
            }
        }

        cpu_core->set_register_state(&active_registers);
    }
}

}    // namespace Debug
