#include <debugger.h>
#include <unistd.h>

namespace Debug {

void Debugger::cycle(void) {

    /* Check to see if if we've hit any active breakpoints */

    LR35902::Register_file_t registers;
    cpu_core->get_register_state(&registers);

    /* Log the instruction executed */
    if (registers.program_counter != backtrace[backtrace_index].addr) {
        backtrace_index++;

        if (backtrace_index >= HISTORY_TRACE_BUFFER_LEN) {
            backtrace_index = 0;
        }

        backtrace[backtrace_index].addr     = registers.program_counter;
        backtrace[backtrace_index].rom_bank = memory_controller->get_active_rom_bank();
        backtrace[backtrace_index].ram_bank = memory_controller->get_active_ram_bank();
        cpu_core->get_register_state(&(backtrace[backtrace_index].registers));
    }

    for (uint64_t i = 0; i < num_breakpoints; i++) {

        if (registers.program_counter == 0x46DF) {
            // printf("Hit Instruciton\n");
        }

        if ((breakpoint[i].enabled == true) && (breakpoint[i].addr == registers.program_counter) &&
            ((breakpoint[i].addr < 0x4000) || (breakpoint[i].addr >= 0x8000) ||
             (breakpoint[i].rom_bank == memory_controller->get_active_rom_bank()))) {
            action.halt_execution = true;
            printf("Breakpoint %ld\n", i);
            break;
        }
    }

    for (uint64_t i = 0; i < num_watchpoints; i++) {

        if (watchpoint[i].enabled == true) {
            uint8_t current_value = memory_bus->read(watchpoint[i].addr);
            if (current_value != watchpoint[i].previous_value) {
                action.halt_execution = true;
                printf("Watchpoint %ld. Addr %04X set to %02X from %02X\n",
                       i,
                       watchpoint[i].addr,
                       current_value,
                       watchpoint[i].previous_value);
                watchpoint[i].previous_value = current_value;
            }
        }
    }

    while (action.halt_execution == true) {
        usleep(Debug::DEBUGGER_SLEEP_POLL_DURATION_US);
    }

    if (action.step_cycle != 0) {
        action.step_cycle--;
        if (action.step_cycle == 0) {
            action.halt_execution = true;
        }
        print_cpu();
    }

    if (action.step_frame != 0) {
        /* Get current frame status */
        bool in_vblank = ((memory_bus->read(0xFF41) & 0b11) == 0b01);
        if ((in_vblank == true) && (action.intra_frame == true)) {
            /* We just transitioned into the frame */
            action.halt_execution = true;
            print_cpu();
            action.step_frame--;
        }
        action.intra_frame = ! in_vblank;
    }

    if (render_cycles == Debug::VRAM_VIEWER_CYCLES_PER_FRAME) {
        update_vram_viewer();
        render_cycles = 0;
    } else {
        render_cycles++;
    }
}

}    // namespace Debug