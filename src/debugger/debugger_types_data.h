#ifndef DEBUGGER_TYPES_DATA_H
#define DEBUGGER_TYPES_DATA_H
#include <cpu.h>
#include <debugger_const.h>

namespace Debug {

struct Debugger_actions {
    bool     halt_execution;
    bool     print_register_state;
    uint64_t step_cycle;
    uint64_t step_frame;
    bool     intra_frame;
};

struct Breakpoint {
    uint8_t  rom_bank;
    uint16_t addr;
    bool     enabled;
};

struct Watchpoint {
    uint8_t  previous_value;
    uint16_t addr;
    bool     enabled;
};

struct History {
    uint8_t                  rom_bank;
    uint8_t                  ram_bank;
    uint16_t                 addr;
    LR35902::Register_file_t registers;
};

}    // namespace Debug

#endif
