#include <debugger.h>
#include <errno.h>
#include <regex>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>

namespace Debug {

void Debugger::print_cpu(void) {

    /* Print memory bank configuration */
    printf("ROM %2d RAM %2d ", memory_controller->get_active_rom_bank(), memory_controller->get_active_ram_bank());

    LR35902::Register_file_t registers;
    cpu_core->get_register_state(&registers);
    /* Display current CPU Flag States */
    printf("%s%s%s%s ",
           (registers.flags.zero == true) ? "Z" : "-",
           (registers.flags.subtract == true) ? "N" : "-",
           (registers.flags.half_carry == true) ? "H" : "-",
           (registers.flags.carry == true) ? "C" : "-");

    /* Display all of the registers */
    printf("A: %02X BC: %02X%02X DE: %02X%02X HL: %02X%02X ",
           registers.A,
           registers.B,
           registers.C,
           registers.D,
           registers.E,
           registers.H,
           registers.L);

    /* Print Stack Pointer and program counter */
    printf("SP = %04X PC = %04X ", registers.stack_pointer, registers.program_counter);

    printf("%14s", LR35902::mnemonics[memory_bus->read(registers.program_counter)]);

    /* If there are any extra bytes for this instruction, go grab them */
    for (uint16_t i = 1; i < LR35902::instr_lengths[memory_bus->read(registers.program_counter)]; i++) {
        printf("%02X ", memory_bus->read(registers.program_counter + i));
    }

    printf("\n");
    fflush(stdout);
}

}    // namespace Debug
