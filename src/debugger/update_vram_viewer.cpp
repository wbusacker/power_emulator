#include <debugger.h>
#include <unistd.h>

namespace Debug {

void Debugger::update_vram_viewer(void) {

    // return;

    uint16_t tile_map;
    uint16_t pattern_buffer;
    uint16_t pixel_r_offset;
    uint16_t pixel_c_offset;

    for (uint8_t i = 0; i < 4; i++) {

        switch (i) {
            case 0:
                tile_map       = 0x9800;
                pattern_buffer = 0x9000;
                pixel_c_offset = 0;
                pixel_r_offset = 0;
                break;
            case 1:
                tile_map       = 0x9800;
                pattern_buffer = 0x8000;
                pixel_c_offset = 256;
                pixel_r_offset = 0;
                break;
            case 2:
                tile_map       = 0x9C00;
                pattern_buffer = 0x9000;
                pixel_c_offset = 0;
                pixel_r_offset = 256;
                break;
            case 3:
                tile_map       = 0x9C00;
                pattern_buffer = 0x8000;
                pixel_c_offset = 256;
                pixel_r_offset = 256;
                break;
        }

        /* Draw tile map 0 using pattern buffer 0 */
        for (uint16_t tile_r = 0; tile_r < 32; tile_r++) {
            for (uint16_t tile_c = 0; tile_c < 32; tile_c++) {
                uint16_t map_index     = tile_c + (tile_r * 32) + tile_map;
                int16_t  pattern_index = memory_bus->read(map_index);

                if (pattern_buffer == 0x9000) {
                    if (pattern_index & 0x80) {
                        pattern_index |= 0xFF00;
                    }
                }

                for (uint8_t pattern_r = 0; pattern_r < 8; pattern_r++) {

                    uint16_t pattern_addr = pattern_buffer + (pattern_index * 16) + (pattern_r * 2);

                    uint8_t bottom_byte = memory_bus->read(pattern_addr);
                    uint8_t top_byte    = memory_bus->read(pattern_addr + 1);

                    for (uint8_t pattern_c = 0; pattern_c < 8; pattern_c++) {
                        uint8_t shift_count = (8 - pattern_c) - 1;
                        uint8_t color_code  = ((top_byte >> shift_count) & 0b1) << 1;
                        color_code |= ((bottom_byte >> shift_count) & 0b1);

                        double color;
                        switch (color_code) {
                            case 0:
                                color = Display::GRAY_0_3;
                                break;
                            case 1:
                                color = Display::GRAY_1_3;
                                break;
                            case 2:
                                color = Display::GRAY_2_3;
                                break;
                            case 3:
                                color = Display::GRAY_3_3;
                                break;
                            default:
                                color = 0;
                        }

                        uint16_t window_r = pattern_r + (tile_r * 8) + pixel_r_offset;
                        uint16_t window_c = pattern_c + (tile_c * 8) + pixel_c_offset;

                        graphics_display->set_pixel_color(window_r, window_c, color, color, color);
                    }
                }
            }
        }
    }

    /* Get the tile pattern buffers, we purposefully draw these pixels bigger */
    for (uint16_t pattern_tile_r = 0; pattern_tile_r < 24; pattern_tile_r++) {
        for (uint8_t pattern_tile_c = 0; pattern_tile_c < 8; pattern_tile_c++) {
            for (uint8_t pattern_r = 0; pattern_r < 8; pattern_r++) {
                uint16_t pattern_index = (pattern_tile_r * 8) + pattern_tile_c;
                uint16_t pattern_addr  = 0x8000 + (pattern_index * 16) + (pattern_r * 2);

                uint8_t bottom_byte = memory_bus->read(pattern_addr);
                uint8_t top_byte    = memory_bus->read(pattern_addr + 1);

                for (uint8_t pattern_c = 0; pattern_c < 8; pattern_c++) {
                    uint8_t shift_count = (8 - pattern_c) - 1;
                    uint8_t color_code  = ((top_byte >> shift_count) & 0b1) << 1;
                    color_code |= ((bottom_byte >> shift_count) & 0b1);

                    double color;
                    switch (color_code) {
                        case 0:
                            color = Display::GRAY_0_3;
                            break;
                        case 1:
                            color = Display::GRAY_1_3;
                            break;
                        case 2:
                            color = Display::GRAY_2_3;
                            break;
                        case 3:
                            color = Display::GRAY_3_3;
                            break;
                        default:
                            color = 0;
                    }

                    uint16_t window_r = (pattern_r * 2) + (pattern_tile_r * 16);
                    uint16_t window_c = (pattern_c * 2) + (pattern_tile_c * 16) + 512;

                    for (uint8_t i = 0; i < 2; i++) {
                        for (uint8_t j = 0; j < 2; j++) {
                            graphics_display->set_pixel_color(window_r + i, window_c + j, color, color, color);
                        }
                    }
                }
            }
        }
    }

    graphics_display->signal_frame();
}

}    // namespace Debug