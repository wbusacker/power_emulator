#include <debugger.h>

namespace Debug {

Debugger::Debugger(Cart::Gameboy::Memory_controller* mem_ctrl,
                   Common_memory::Common_memory_bus* mem_bus,
                   LR35902::Gameboy_cpu*             cpu) {

    graphics_display = new Display::Window_manager(512, 640, 1, false, DEBUGGER_WINDOW_NAME);

    for (uint16_t r = 0; r < 512; r++) {
        for (uint16_t c = 0; c < 640; c++) {
            graphics_display->set_pixel_color(r, c, 0, 1, 0);
        }
    }
    graphics_display->signal_frame();

    render_cycles          = 0;
    tile_pattern_row_index = 0;

    memory_controller = mem_ctrl;
    memory_bus        = mem_bus;
    cpu_core          = cpu;

    for (uint64_t i = 0; i < Debug::MAX_NUM_BREAKPOINTS; i++) {
        breakpoint[i].addr     = 0;
        breakpoint[i].rom_bank = 0;
        breakpoint[i].enabled  = false;

        watchpoint[i].addr           = 0;
        watchpoint[i].previous_value = 0;
        watchpoint[i].enabled        = false;
    }
    num_breakpoints = 0;
    num_watchpoints = 0;

    action.halt_execution       = false;
    action.print_register_state = false;
    action.step_cycle           = 0;

    exiting = false;

    for (uint16_t i = 0; i < HISTORY_TRACE_BUFFER_LEN; i++) {
        backtrace[i].addr                       = 0;
        backtrace[i].rom_bank                   = 0;
        backtrace[i].ram_bank                   = 0;
        backtrace[i].registers.A                = 0;
        backtrace[i].registers.B                = 0;
        backtrace[i].registers.C                = 0;
        backtrace[i].registers.D                = 0;
        backtrace[i].registers.E                = 0;
        backtrace[i].registers.H                = 0;
        backtrace[i].registers.L                = 0;
        backtrace[i].registers.flags.paddinging = 0;
        backtrace[i].registers.flags.carry      = 0;
        backtrace[i].registers.flags.half_carry = 0;
        backtrace[i].registers.flags.subtract   = 0;
        backtrace[i].registers.flags.zero       = 0;
        backtrace[i].registers.stack_pointer    = 0;
        backtrace[i].registers.program_counter  = 0;
        backtrace[i].registers.stack_pointer    = 0;
        backtrace[i].registers.cb_mode          = false;
        backtrace[i].registers.cycle_time       = 0;
    }

    pthread_create(&thread_handle, NULL, Debugger::input_processor, this);
}

Debugger::~Debugger(void) {
    exiting = true;

    pthread_join(thread_handle, nullptr);

    delete graphics_display;
}

}    // namespace Debug
