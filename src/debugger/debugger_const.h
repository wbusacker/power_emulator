#ifndef DEBUGGER_CONST_H
#define DEBUGGER_CONST_H
#include <power_emulator_const.h>
#include <stdint.h>

namespace Debug {

const char* const SET_BREAKPOINT_MATCH       = "bp ([0-9]*) ([0-9A-Fa-f]*).*";
const char* const DISABLE_BREAKPOINT_MATCH   = "bp disable ([0-9]*)";
const char* const SET_WATCHPOINT_MATCH       = "wp ([0-9A-Fa-f]*).*";
const char* const DISABLE_WATCHPOINT_MATCH   = "wp disable ([0-9]*)";
const char* const DISPLAY_CPU_REG_MATCH      = "reg";
const char* const HALT_MATCH                 = "halt";
const char* const CONTINUE_MATCH             = "cont.*";
const char* const READ_MEMORY_MATCH          = "mem ([0-9A-Fa-f]*).*";
const char* const READ_MULTIPLE_MEMORY_MATCH = "mem ([0-9A-Fa-f]*) ([0-9]*).*";
const char* const WRITE_MEMORY_MATCH         = "wmem ([0-9A-Fa-f]*) ([0-9A-Fa-f]*)";
const char* const STEP_MATCH                 = "step";
const char* const STEP_MULTIPLE_MATCH        = "step ([0-9]*)";
const char* const STEP_FRAME_MATCH           = "frame";
const char* const BACKTRACE_MATCH            = "bt";

const char* const DEBUGGER_WINDOW_NAME = "Power Emulator VRAM Viewer";

const uint32_t DEBUGGER_SLEEP_POLL_DURATION_US = 10000;
const uint64_t CONSOLE_LINE_LENGTH             = 1024;

const uint16_t HISTORY_TRACE_BUFFER_LEN = 256;

const uint64_t MAX_NUM_BREAKPOINTS = 256;

const uint64_t VRAM_VIEWER_FRAME_RATE       = 1;
const uint64_t VRAM_VIEWER_CYCLES_PER_FRAME = Power::POWERBOY_CLOCK_SPEED / VRAM_VIEWER_FRAME_RATE;

}    // namespace Debug

#endif
