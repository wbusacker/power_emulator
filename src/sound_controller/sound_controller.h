#ifndef SOUND_CONTROLLER_H
#define SOUND_CONTROLLER_H
#include <alsa/asoundlib.h>
#include <common_memory_bus.h>
#include <pthread.h>
#include <sound_controller_const.h>
#include <sound_controller_types_data.h>

namespace Audio {

class Sound_controller : public Common_memory::CMB_interface {
    public:
    Sound_controller(void);
    ~Sound_controller(void);

    virtual void    cycle(void);
    virtual uint8_t read(uint16_t addr);
    virtual void    write(uint16_t addr, uint8_t data);

    static void* audio_thread(void* arg);

    private:
    /* Internal Voice functions */
    void update_volume_sweep(struct Audio::Volume_sweep* volume);
    bool
    update_play_time(struct Audio::Duty_cycle_and_loading* loading, enum Playtime_format format, bool current_state);

    /* Voice functions */
    uint8_t squarewave_voice(struct Audio::Squarewave_voice* voice, bool allow_sweep = false);
    uint8_t sample_voice(struct Audio::Sample_voice* voice);
    uint8_t noise_voice(struct Audio::Noise_voice* voice);

    snd_pcm_t*        driver_handle;
    snd_pcm_uframes_t frame_count;

    uint32_t actual_sample_rate;
    double   time_to_next_audio_output;

    uint8_t internal_channel_buffers[2][WORKING_SAMPLE_BUFFER_SIZE];
    // uint32_t                         last_written_sample;
    double read_sample_time;

    uint32_t frame_duration_us;
    double   sample_duration_s;
    uint64_t num_cycles;

    volatile uint32_t outbound_sample_buffer_size;
    int32_t*          outbound_sample_buffer;
    uint32_t          channel_outbound_sample_buffer_size;
    double            outbound_samples_per_internal_samples;

    bool boundary_256;
    bool boundary_128;
    bool boundary_64;
    bool boundary_8;

    struct Audio::Squarewave_voice voice_1;
    struct Audio::Squarewave_voice voice_2;
    struct Audio::Sample_voice     voice_3;
    struct Audio::Noise_voice      voice_4;
};

}    // namespace Audio

#endif
