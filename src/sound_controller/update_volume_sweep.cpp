#include <sound_controller.h>

namespace Audio {

void Sound_controller::update_volume_sweep(struct Audio::Volume_sweep* volume) {

    /* Only modify volume on 64Hz boundaries */

    if (boundary_64 && (volume->timer_reload != 0)) {
        volume->timer_count--;

        if (volume->timer_count == 0) {
            volume->timer_count = volume->timer_reload;

            if ((volume->volume_direction == Audio::VOLUME_AMPLIFY) && (volume->current_volume != 0xF)) {
                volume->current_volume++;
            }

            if ((volume->volume_direction == Audio::VOLUME_ATTENUATE) && (volume->current_volume != 0x0)) {
                volume->current_volume--;
            }
        }
    }
}

}    // namespace Audio