#ifndef SOUND_CONTROLLER_CONST_H
#define SOUND_CONTROLLER_CONST_H

#include <power_emulator_const.h>
#include <stdint.h>

namespace Audio {

const uint8_t     NUM_OUTPUT_SOUND_CHANNELS   = 2;
const uint32_t    OUTPUT_SOUND_SAMPLE_RATE    = 44100;
const uint32_t    TARGET_SOUND_PERIOD_SIZE    = 512;
const uint8_t     NUM_INTERNAL_SOUND_CHANNELS = 4;
const char* const PCM_CHANNEL                 = "default";

const uint64_t WORKING_SAMPLE_BUFFER_SIZE = 65536;
const uint8_t  LEFT_CHANNEL_BUFFER        = 0;
const uint8_t  RIGHT_CHANNEL_BUFFER       = 1;
const double   WORKING_BUFFER_LENGTH_S = static_cast<double>(WORKING_SAMPLE_BUFFER_SIZE) / Power::POWERBOY_CLOCK_SPEED;

const uint32_t CYCLES_PER_64_HZ_PULSE  = Power::POWERBOY_CLOCK_SPEED / 64;
const uint32_t CYCLES_PER_128_HZ_PULSE = Power::POWERBOY_CLOCK_SPEED / 128;
const uint32_t CYCLES_PER_256_HZ_PULSE = Power::POWERBOY_CLOCK_SPEED / 256;

const uint32_t TONE_HIGH = 0x1FFFFFFF;

const double CPU_CYCLES_PER_SAMPLE = Power::POWERBOY_CLOCK_SPEED / static_cast<double>(OUTPUT_SOUND_SAMPLE_RATE);

const uint64_t MAXIMUM_SYNTH_FREQUENCY  = 131072;
const uint64_t CPU_CYCLES_PER_MAX_CYCLE = Power::POWERBOY_CLOCK_SPEED / MAXIMUM_SYNTH_FREQUENCY;

const uint16_t NR_10 = 0xFF10;
const uint16_t NR_11 = 0xFF11;
const uint16_t NR_12 = 0xFF12;
const uint16_t NR_13 = 0xFF13;
const uint16_t NR_14 = 0xFF14;

const uint16_t NR_21 = 0xFF16;
const uint16_t NR_22 = 0xFF17;
const uint16_t NR_23 = 0xFF18;
const uint16_t NR_24 = 0xFF19;

const uint16_t NR_30 = 0xFF1A;
const uint16_t NR_31 = 0xFF1B;
const uint16_t NR_32 = 0xFF1C;
const uint16_t NR_33 = 0xFF1D;
const uint16_t NR_34 = 0xFF1E;

const uint16_t NR_41 = 0xFF20;
const uint16_t NR_42 = 0xFF21;
const uint16_t NR_43 = 0xFF22;
const uint16_t NR_44 = 0xFF23;

const uint16_t MASTER_VOLUME_CONTROL_ADDR = 0xFF24;
const uint16_t SOUND_ROUTING_CONTROL_ADDR = 0xFF25;
const uint16_t MASTER_SOUND_CONTROL_ADDR  = 0xFF26;

const uint16_t SAMPLE_PATTERN_START_ADDR = 0xFF30;
const uint16_t SAMPLE_PATTERN_END_ADDR = 0xFF40;

const uint16_t FREQUENCY_MASK = 0x7FF;

const uint8_t MAX_VOLUME            = 15 * NUM_INTERNAL_SOUND_CHANNELS;
const double  VOLUME_SWEEP_PERIOD_S = 1.0 / 64;

const double FREQUENCY_SWEEP_CHANGE_BASE = 128.0;

const uint8_t SAMPLE_BUFFER_SIZE = 32;
const uint8_t SAMPLES_PER_BYTE   = 2;
const double  MAX_SAMPLE_VALUE   = 15.0;

/* Constants for the various equations used throughout the soundcard */
const uint8_t PLAY_TIME_OFFSET = 64;
const double  PLAY_TIME_SCALER = (1.0 / 256);

const double FREQUENCY_OFFSET = 2048.0;
const double FREQUENCY_SCALER = 131072.0;

const double SAMPLE_PLAYTIME_OFFSET = 256;
const double SAMPLE_PLAYTIME_SCALER = 0.5;

const double SAMPLE_FREQUENCY_SCALER = 65536.0;

const double DUTY_CYCLE_VALUES[4]                = {0.125, 0.25, 0.50, 0.75};


}    // namespace Audio

#endif
