#include <sound_controller.h>

namespace Audio {

void Sound_controller::cycle(void) {

    /* Figure out if we're on a boundary period or not */

    num_cycles++;

    boundary_256 = ((num_cycles % Audio::CYCLES_PER_256_HZ_PULSE) == 0);
    boundary_128 = ((num_cycles % Audio::CYCLES_PER_128_HZ_PULSE) == 0);
    boundary_64  = ((num_cycles % Audio::CYCLES_PER_64_HZ_PULSE) == 0);

    /* Let the different voices figure out if they need to do something new */
    uint8_t total_volume = 0;

    total_volume += squarewave_voice(&voice_1, true);
    total_volume += squarewave_voice(&voice_2, false);
    total_volume += sample_voice(&voice_3);
    total_volume += noise_voice(&voice_4);

    /* Place this cycle's total volume in the buffer */
    uint32_t cycle_buffer_index = num_cycles % WORKING_SAMPLE_BUFFER_SIZE;

    internal_channel_buffers[0][cycle_buffer_index] = total_volume;
    internal_channel_buffers[1][cycle_buffer_index] = total_volume;

    /* Forward sound data off to the host's sound card if needed */

    time_to_next_audio_output -= (1.0 / Power::POWERBOY_CLOCK_SPEED);

    if (time_to_next_audio_output <= 0) {

        uint32_t buffer_index = 0;
        /* Fill up the outbound sample buffer */
        for (uint32_t i = 0; i < channel_outbound_sample_buffer_size; i++) {
            read_sample_time += (1.0 / actual_sample_rate);

            /* Ensure we don't try to read past the circular buffer */
            if (read_sample_time > WORKING_BUFFER_LENGTH_S) {
                read_sample_time -= WORKING_BUFFER_LENGTH_S;
            }

            /* Figure out what the corresponding index is for this time in the buffer */
            buffer_index = (read_sample_time * Power::POWERBOY_CLOCK_SPEED);

            /* We need to make sure that we don't run past the end of the synth sample buffer
                This can happen and sometimes results in collecting a sample just barely past
                the end. When this happens, take the last sample loaded into the synth buffer
                and snap the read_sample_time to that buffer index
            */
            if ((buffer_index > cycle_buffer_index) && (i == (channel_outbound_sample_buffer_size - 1))) {
                double sample_delta = buffer_index - cycle_buffer_index;
                buffer_index        = cycle_buffer_index;
                read_sample_time -= sample_delta / Power::POWERBOY_CLOCK_SPEED;
            }

            for (uint8_t channel = 0; channel < 2; channel++) {
                double volume =
                    (static_cast<double>(internal_channel_buffers[channel][buffer_index]) / Audio::MAX_VOLUME) *
                    Audio::TONE_HIGH;
                outbound_sample_buffer[(i * Audio::NUM_OUTPUT_SOUND_CHANNELS) + channel] = volume;
            }
        }

        int ret_val = 0;

        ret_val = snd_pcm_writei(driver_handle, outbound_sample_buffer, channel_outbound_sample_buffer_size);
        if (ret_val < 0) {

            /* If we failed to write sound this time, drop the data
                and re-prepare the device
            */
            ret_val = snd_pcm_prepare(driver_handle);
            if (ret_val < 0) {
                perror("Failed to prepare sound driver\n");
                printf("%s\n", snd_strerror(ret_val));
            }
        }
        time_to_next_audio_output += frame_duration_us / 1.0E6;
    }

    return;
}

}    // namespace Audio
