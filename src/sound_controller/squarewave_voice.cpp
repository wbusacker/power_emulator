#include <sound_controller.h>

namespace Audio {

uint8_t Sound_controller::squarewave_voice(struct Audio::Squarewave_voice* voice, bool allow_sweep) {

    uint8_t output_level = 0;

    /* Check to see if an initialization event has happened */

    if (voice->control.reinitialize) {

        voice->voice_enable = true;

        if (voice->duty_cycle.short_length_load == 0) {
            voice->duty_cycle.short_length_load = 64;
        }

        voice->cycle_counter = 0;

        voice->volume.timer_count = voice->volume.timer_reload;

        voice->sweep.shadow_frequency = voice->frequency;
        voice->sweep.timer_value      = voice->sweep.timer_reload;
    }

    /* Frequency sweep logic

        Check to see if we're on a 128Hz boundary, or if we're processing
        a reinitialization event
    */
    if ((boundary_128 || voice->control.reinitialize) && allow_sweep) {

        struct Frequency_sweep* sweep = &(voice->sweep);

        /* Decrement the timer value */
        sweep->timer_value--;

        bool update_frequency = voice->control.reinitialize;

        if (sweep->timer_value == 0) {
            sweep->timer_value = sweep->timer_reload;
            update_frequency |= true;
        }

        if (update_frequency && (sweep->sweep_amount != 0)) {

            uint16_t new_frequency = sweep->shadow_frequency;
            switch (sweep->sweep_direction) {
                case Audio::FREQUENCY_SHIFT_UP:
                    new_frequency <<= sweep->sweep_amount;
                    break;
                case Audio::FREQUENCY_SHIFT_DOWN:
                    new_frequency >>= sweep->sweep_amount;
                    break;
            }

            if (new_frequency >= Audio::FREQUENCY_MASK) {
                voice->voice_enable = false;
            } else {
                sweep->shadow_frequency = new_frequency;
                voice->frequency        = new_frequency;
            }
        }
    }

    /* Apply time loading */
    if (voice->control.continuous == false) {
        voice->voice_enable = update_play_time(&(voice->duty_cycle), Audio::SHORT_FORMAT, voice->voice_enable);
    }

    /* Apply time loading */

    // if (voice->duty_cycle.reload_timer) {
    //     voice->duty_cycle.reload_timer   = false;
    //     voice->duty_cycle.length_counter = 64 - voice->duty_cycle.short_length_load;
    // }

    // if (boundary_256 && (voice->duty_cycle.length_counter != 0) && ! voice->control.continuous) {
    //     voice->duty_cycle.length_counter--;

    //     if (voice->duty_cycle.short_length_load == 0) {
    //         voice->voice_enable = false;
    //     }
    // }

    /* Figure out if the squarewave is high or not */
    bool output_high = false;

    double cycle_period      = CPU_CYCLES_PER_MAX_CYCLE * (FREQUENCY_OFFSET - voice->frequency);
    double cycle_high_period = cycle_period * voice->duty_cycle.duty_cycle;

    voice->cycle_counter++;

    if (voice->cycle_counter < cycle_high_period) {
        output_high = true;
    }

    if (voice->cycle_counter > cycle_period) {
        voice->cycle_counter = 0;
    }

    update_volume_sweep(&(voice->volume));

    /* If the voice is disabled, always force output level to be zero */
    if (voice->voice_enable == false) {
        output_level = 0;
    } else {
        output_level = output_high ? voice->volume.current_volume : 0;
    }

    /* If we re-initialized this cycle, don't reinitialize next cycle */
    if (voice->control.reinitialize) {
        voice->control.reinitialize = false;
    }

    return output_level;
}

}    // namespace Audio