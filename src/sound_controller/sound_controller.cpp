#include <math.h>
#include <sound_controller.h>

namespace Audio {

Sound_controller::Sound_controller(void) {

    /* Open the low level driver handle */
    snd_pcm_open(&driver_handle, PCM_CHANNEL, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK);

    /* Configure the parameters for driver access */
    snd_pcm_hw_params_t* parameters;
    snd_pcm_hw_params_alloca(&parameters);
    snd_pcm_hw_params_any(driver_handle, parameters);

    actual_sample_rate = Audio::OUTPUT_SOUND_SAMPLE_RATE;

    snd_pcm_hw_params_set_access(driver_handle, parameters, SND_PCM_ACCESS_RW_INTERLEAVED);
    snd_pcm_hw_params_set_format(driver_handle, parameters, SND_PCM_FORMAT_S32);
    snd_pcm_hw_params_set_channels(driver_handle, parameters, NUM_OUTPUT_SOUND_CHANNELS);
    snd_pcm_hw_params_set_rate_near(driver_handle, parameters, &actual_sample_rate, 0);
    snd_pcm_hw_params(driver_handle, parameters);

    printf("Requested sample rate %6d got sample rate %6d\n", Audio::OUTPUT_SOUND_SAMPLE_RATE, actual_sample_rate);

    /* Calculate how large the sample buffer needs to be */
    sample_duration_s                     = 1.0 / actual_sample_rate;
    outbound_samples_per_internal_samples = Power::POWERBOY_CLOCK_SPEED / actual_sample_rate;

    frame_count = TARGET_SOUND_PERIOD_SIZE;
    snd_pcm_hw_params_set_period_size_near(driver_handle, parameters, &frame_count, 0);
    snd_pcm_hw_params_get_period_size(parameters, &frame_count, 0);

    frame_duration_us = 1.0E6 * sample_duration_s * frame_count / Audio::NUM_OUTPUT_SOUND_CHANNELS;
    printf("Frame count %ld for duration %dus\n", frame_count, frame_duration_us);

    outbound_sample_buffer_size         = frame_count;
    outbound_sample_buffer              = new int32_t[outbound_sample_buffer_size];
    channel_outbound_sample_buffer_size = frame_count / Audio::NUM_OUTPUT_SOUND_CHANNELS;

    for (uint32_t i = 0; i < outbound_sample_buffer_size; i++) {
        outbound_sample_buffer[i] = 0;
    }

    const double CYCLES_PER_SECOND  = (44100.0 / 512.0) * 4.0;
    const double SAMPLES_PER_CYCLE  = 44100.0 / CYCLES_PER_SECOND;
    const double CYCLE_PER_SAMPLE   = 1.0 / SAMPLES_PER_CYCLE;
    const double RADIANS_PER_SAMPLE = CYCLE_PER_SAMPLE * 2 * 3.141592654;

    for (uint64_t i = 0; i < channel_outbound_sample_buffer_size; i++) {
        double  angle = sin(i * RADIANS_PER_SAMPLE);
        int32_t value = angle * TONE_HIGH;
        // printf("%9.6f -> %4d\n", angle, value);
        outbound_sample_buffer[(i * 2)] = value;
        // outbound_sample_buffer[i] = angle * 0x3FFFFFFF;
        outbound_sample_buffer[(i * 2) + 1] = value;
    }

    uint8_t state = 0;
    for (uint32_t i = 0; i < WORKING_SAMPLE_BUFFER_SIZE; i++) {
        if ((i % 1024) == 0) {
            state ^= 15;
        }
        internal_channel_buffers[0][i] = state;
        internal_channel_buffers[1][i] = state;
    }

    /* Initialize Voice 1 */
    write(0xFF10, 0x80);
    write(0xFF11, 0xBF);
    write(0xFF12, 0xF3);
    write(0xFF13, 0x00);
    write(0xFF14, 0xBF);

    /* Initialize Voice 2 */
    /* Initialize Voice 3 */
    write(0xFF1A, 0x7F);
    write(0xFF1B, 0xFF);
    write(0xFF1C, 0x9F);
    write(0xFF1D, 0xFF);
    write(0xFF1E, 0xBF);


    /* Initialize Voice 4 */
    write(0xFF20, 0xFF);
    write(0xFF21, 0x00);
    write(0xFF22, 0x00);
    write(0xFF23, 0x80);

    time_to_next_audio_output = (frame_duration_us / 1.0E6);
    // last_written_sample = 0;
    // read_sample_time = 0;

    // cpu_cycle_per_output_buffer = (Power::POWERBOY_CLOCK_SPEED * frame_duration_us / 1E6);
    // // cpu_cycle_per_output_buffer = 100;
    // num_cycles                  = 0;
    // next_cycle_pulse            = cpu_cycle_per_output_buffer;

    // struct Audio::Squarewave_channel* channel;
    // for (uint8_t i = 0; i < 2; i++) {
    //     if (i == 0) {
    //         channel = &voice_1_data;
    //     } else {
    //         channel = &voice_2_data;
    //     }

    //     channel->sweep.time                 = 0;
    //     channel->sweep.up                   = 0;
    //     channel->sweep.counter              = 0;
    //     channel->duty_cycle                 = 0;
    //     channel->play_time                  = 0;
    //     channel->frequency                  = 0x3FF;
    //     channel->volume.current_volume      = MAX_VOLUME;
    //     channel->volume.counter             = 0;
    //     channel->volume.time_to_next_adjust = 0;
    //     channel->volume.change_direction    = VOLUME_AMPLIFY;
    //     channel->sound_restart              = 0;
    //     channel->counter_mode               = 0;
    //     channel->cycle.current_s            = 0;
    //     channel->cycle.cycle_length_s       = 0;
    //     channel->cycle.transition_high_s    = 0;
    //     channel->frequency_change           = true;
    //     channel->wave_form = new uint32_t[channel_outbound_sample_buffer_size];

    //     for(uint32_t j = 0; j < channel_outbound_sample_buffer_size; j++){
    //         channel->wave_form[j] = 0;
    //     }
    // }

    // voice_3_data.output_state        = 0;
    // voice_3_data.play_time           = 0;
    // voice_3_data.output_level        = 0;
    // voice_3_data.frequency           = 0;
    // voice_3_data.frequency_change    = true;
    // voice_3_data.sound_restart       = true;
    // voice_3_data.counter_mode        = 0;
    // voice_3_data.sample_buffer_index = 0;

    // for (uint8_t i = 0; i < SAMPLE_BUFFER_SIZE; i++) {
    //     voice_3_data.sample_buffer[i] = 0;
    // }

    // voice_4_data.play_time                    = 0;
    // voice_4_data.volume.current_volume        = MAX_VOLUME;
    // voice_4_data.volume.counter               = 0;
    // voice_4_data.volume.time_to_next_adjust   = 0;
    // voice_4_data.frequency                    = 0;
    // voice_4_data.step_selection               = 0;
    // voice_4_data.divider_ratio                = 0;
    // voice_4_data.sound_restart                = 0;
    // voice_4_data.counter_mode                 = 1;
    // voice_4_data.lfsr                         = 0xFFFF;
    // voice_4_data.last_output                  = 0;
    // voice_4_data.lfsr_down_counter            = 0;
    // voice_4_data.lfsr_down_counter_reload_bit = 0;
    // voice_4_data.lfsr_clock_divider           = 0;
    // voice_4_data.lfsr_clock_divider_count     = 0;
    // voice_4_data.wave_form                    = new uint32_t[channel_outbound_sample_buffer_size];

    // for (uint8_t i = 0; i < Audio::NUM_INTERNAL_SOUND_CHANNELS; i++) {
    //     voice_on[i] = true;
    // }

    // master_sound_disable = false;
}

Sound_controller::~Sound_controller(void) {

    snd_pcm_drain(driver_handle);
    snd_pcm_close(driver_handle);
    delete outbound_sample_buffer;
}

}    // namespace Audio
