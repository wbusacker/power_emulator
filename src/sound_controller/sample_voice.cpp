#include <sound_controller.h>

namespace Audio {

uint8_t Sound_controller::sample_voice(struct Audio::Sample_voice* voice) {

    uint8_t output_level = 0;

    /* Check to see if an initialization event has happened */

    if (voice->control.reinitialize) {

        voice->voice_enable = true;

        if (voice->duty_cycle.long_length_load == 0) {
            voice->duty_cycle.long_length_load = 256;
        }

        voice->sample_index = 0;

    }


    /* Apply time loading */
    if (voice->control.continuous == false) {
        voice->voice_enable = update_play_time(&(voice->duty_cycle), Audio::LONG_FORMAT, voice->voice_enable);
    }

    /* Calculate when the next pulse will be */
    bool output_high = false;

    double cycle_period      = CPU_CYCLES_PER_MAX_CYCLE * (1.0 / 16) * (FREQUENCY_OFFSET - voice->frequency);

    voice->cycle_counter++;

    if (voice->cycle_counter > cycle_period) {
        voice->cycle_counter = 0;
        voice->sample_index++;

        if(voice->sample_index == SAMPLE_BUFFER_SIZE){
            voice->sample_index = 0;
        }
    }

    /* If the voice is disabled, always force output level to be zero */
    if (voice->voice_enable == false) {
        output_level = 0;
    } else {
        output_level = voice->sample_buffer[voice->sample_index];

        switch(voice->volume_level){
            case 0:
                output_level = 0;
                break;
            case 1:
                break;
            case 2:
                output_level >>= 1;
                break;
            case 3:
                output_level >>= 2;
                break;
        }
    }

    /* If we re-initialized this cycle, don't reinitialize next cycle */
    if (voice->control.reinitialize) {
        voice->control.reinitialize = false;
    }

    return output_level;
}

}    // namespace Audio