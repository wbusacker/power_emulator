#include <sound_controller.h>

namespace Audio {

bool Sound_controller::update_play_time(struct Audio::Duty_cycle_and_loading* loading,
                                        enum Playtime_format                  format,
                                        bool                                  current_state) {

    bool should_play = current_state;

    if (loading->reload_timer) {
        loading->reload_timer = false;
        switch (format) {
            case Audio::LONG_FORMAT:
                loading->length_counter = 256 - loading->long_length_load;
                break;
            case Audio::SHORT_FORMAT:
                loading->length_counter = 64 - loading->short_length_load;
                break;
        }
    }

    if (boundary_256 && (loading->length_counter != 0)) {
        loading->length_counter--;

        if (loading->length_counter == 0) {
            should_play = false;
        }
    }

    return should_play;
}

}    // namespace Audio