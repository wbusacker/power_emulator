#include <cpu_const.h>
#include <math.h>
#include <sound_controller.h>

namespace Audio {

uint8_t Sound_controller::read(uint16_t addr) {

    uint8_t data = 0;

    // printf("READ %04X\n", addr);

    return data;
}

/* Handful of helper functions just to make write processing easier */
void process_nr_x1(struct Audio::Duty_cycle_and_loading* dcl, uint8_t data) {
    dcl->duty_cycle        = DUTY_CYCLE_VALUES[(data >> 6) & 0x3];
    dcl->short_length_load = data & 0x3F;
    dcl->long_length_load  = data;
    dcl->reload_timer      = true;
}

void process_nr_x2(struct Audio::Volume_sweep* volume, uint8_t data) {
    volume->current_volume   = (data >> 4) & 0xF;
    volume->volume_direction = (data & 0x8) == 0 ? Audio::VOLUME_ATTENUATE : Audio::VOLUME_AMPLIFY;
    volume->timer_reload     = data & 0x7;
}

void process_nr_x4(struct Audio::Voice_control* control, uint8_t data) {
    control->reinitialize = (data & 0b10000000) == 0 ? false : true;
    control->continuous   = (data & 0b01000000) == 0 ? true : false;
}

void Sound_controller::write(uint16_t addr, uint8_t data) {

    switch (addr) {

        /* NR x0 processing */
        case Audio::NR_10:
            voice_1.sweep.timer_reload = (data >> 4) & 0x7;
            voice_1.sweep.sweep_direction =
                (data & 0x08) == 0 ? Audio::FREQUENCY_SHIFT_UP : Audio::FREQUENCY_SHIFT_DOWN;
            voice_1.sweep.sweep_amount = (data >> 0) & 0x7;
            break;
        case Audio::NR_30:
            voice_3.voice_enable = (data & 0x80) != 0;
            break;

        /* NR x1 processing (duty cycle & length load) */
        case Audio::NR_11:
            process_nr_x1(&(voice_1.duty_cycle), data);
            break;
        case Audio::NR_21:
            process_nr_x1(&(voice_2.duty_cycle), data);
            break;
        case Audio::NR_31:
            process_nr_x1(&(voice_3.duty_cycle), data);
            break;
        case Audio::NR_41:
            process_nr_x1(&(voice_4.duty_cycle), data);
            break;

        /* NR x2 processing (volume) */
        case Audio::NR_12:
            process_nr_x2(&(voice_1.volume), data);
            break;
        case Audio::NR_22:
            process_nr_x2(&(voice_2.volume), data);
            break;
        case Audio::NR_32:
            voice_3.volume_level = (data >> 5) & 0x3;
            break;
        case Audio::NR_42:
            process_nr_x2(&(voice_4.volume), data);
            break;

        /* NR x3 processing (Frequency Control) */
        case Audio::NR_13:
            voice_1.frequency = (voice_1.frequency & 0xFF00) | data;
            break;
        case Audio::NR_23:
            voice_2.frequency = (voice_2.frequency & 0xFF00) | data;
            break;
        case Audio::NR_33:
            voice_3.frequency = (voice_3.frequency & 0xFF00) | data;
            break;
        case Audio::NR_43:
            voice_4.shift_clock_frequency = data >> 4;
            voice_4.mode                  = (data & 0x8) == 0 ? FULL_MODE : HALF_MODE;
            voice_4.divider_ratio         = data & 0x7;

            voice_4.pulse_counter_reload =
                Power::POWERBOY_CLOCK_SPEED / (524288.0 / ((voice_4.divider_ratio == 0 ? 0.5 : voice_4.divider_ratio) *
                                                           pow(2, voice_4.shift_clock_frequency + 1)));
            break;

        /* NR x4 processing (state control) */
        case Audio::NR_14:
            process_nr_x4(&(voice_1.control), data);
            voice_1.frequency = (voice_1.frequency & 0xFF) | ((data & 0x7) << 8);
            break;
        case Audio::NR_24:
            process_nr_x4(&(voice_2.control), data);
            voice_2.frequency = (voice_2.frequency & 0xFF) | ((data & 0x7) << 8);
            break;
        case Audio::NR_34:
            process_nr_x4(&(voice_3.control), data);
            voice_3.frequency = (voice_3.frequency & 0xFF) | ((data & 0x7) << 8);
            break;
        case Audio::NR_44:
            process_nr_x4(&(voice_4.control), data);
            break;
    }

    if((Audio::SAMPLE_PATTERN_START_ADDR <= addr) && (addr < Audio::SAMPLE_PATTERN_END_ADDR)){
        uint16_t lower_index = (addr - Audio::SAMPLE_PATTERN_START_ADDR) * 2;
        uint16_t upper_index = lower_index + 1;

        voice_3.sample_buffer[lower_index] = data >> 4;
        voice_3.sample_buffer[upper_index] = data & 0xF;
    }

    return;
}

}    // namespace Audio
