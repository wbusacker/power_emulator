#include <sound_controller.h>

namespace Audio {

uint8_t Sound_controller::noise_voice(struct Audio::Noise_voice* voice) {

    uint8_t output_level = 0;

    /* Check to see if an initialization event has happened */

    if (voice->control.reinitialize) {

        voice->voice_enable = true;

        if (voice->duty_cycle.short_length_load == 0) {
            voice->duty_cycle.short_length_load = 64;
        }

        voice->volume.timer_count = voice->volume.timer_reload;

        voice->pulse_counter = voice->pulse_counter_reload;

        voice->lfsr = 0xFFFF;
    }

    /* Apply time loading */
    if (voice->control.continuous == false) {
        voice->voice_enable = update_play_time(&(voice->duty_cycle), Audio::SHORT_FORMAT, voice->voice_enable);
    }

    /* Pulse the LFSR */
    voice->pulse_counter--;
    if (voice->pulse_counter == 0) {
        voice->pulse_counter = voice->pulse_counter_reload;

        /* Actually shift the LFSR */
        uint16_t bit_0 = voice->lfsr & 1;
        uint16_t bit_1 = (voice->lfsr >> 1) & 1;

        uint16_t new_bit = bit_0 ^ bit_1;

        voice->lfsr >>= 1;

        voice->lfsr &= ~(1 << 14);
        voice->lfsr |= new_bit << 14;

        if (voice->mode == Audio::HALF_MODE) {
            voice->lfsr &= ~(1 << 6);
            voice->lfsr |= new_bit << 6;
        }
    }

    update_volume_sweep(&(voice->volume));

    /* If the voice is disabled, always force output level to be zero */
    if (voice->voice_enable == false) {
        output_level = 0;
    } else {
        output_level = ((voice->lfsr & 1) ^ 1) * voice->volume.current_volume;
    }

    /* If we re-initialized this cycle, don't reinitialize next cycle */
    if (voice->control.reinitialize) {
        voice->control.reinitialize = false;
    }

    return output_level;
}

}    // namespace Audio