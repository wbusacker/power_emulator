#ifndef SOUND_CONTROLLER_TYPES_DATA_H
#define SOUND_CONTROLLER_TYPES_DATA_H
#include <sound_controller_const.h>

namespace Audio {

enum Frequency_sweep_direction : uint8_t { FREQUENCY_SHIFT_UP = 0, FREQUENCY_SHIFT_DOWN = 1 };

enum Volume_sweep_direction : uint8_t { VOLUME_AMPLIFY = 0, VOLUME_ATTENUATE = 1 };

enum LFSR_mode : uint8_t { FULL_MODE = 0, HALF_MODE = 1 };

enum Playtime_format : uint8_t { LONG_FORMAT, SHORT_FORMAT };
struct Frequency_sweep {
    uint8_t                        timer_reload;
    uint8_t                        timer_value;
    enum Frequency_sweep_direction sweep_direction;
    uint8_t                        sweep_amount;
    uint16_t                       shadow_frequency;
};

struct Duty_cycle_and_loading {
    double   duty_cycle;
    uint16_t short_length_load;
    uint16_t long_length_load;
    uint16_t length_counter;
    bool     reload_timer;
};

struct Volume_sweep {
    uint8_t                     current_volume;
    enum Volume_sweep_direction volume_direction;
    uint8_t                     timer_reload;
    uint8_t                     timer_count;
};

struct Voice_control {
    bool reinitialize;
    bool continuous;
};

struct Squarewave_voice {

    bool voice_enable;

    struct Frequency_sweep        sweep;
    struct Duty_cycle_and_loading duty_cycle;
    struct Volume_sweep           volume;
    struct Voice_control          control;

    uint16_t frequency;
    uint64_t cycle_counter;
};

struct Noise_voice {

    bool voice_enable;

    struct Duty_cycle_and_loading duty_cycle;
    struct Volume_sweep           volume;
    struct Voice_control          control;

    enum LFSR_mode mode;

    uint32_t shift_clock_frequency;
    uint32_t divider_ratio;

    uint32_t pulse_counter_reload;
    uint32_t pulse_counter;

    uint16_t lfsr;
};

struct Sample_voice {

    bool                          voice_enable;
    bool                          playback_enable;
    struct Duty_cycle_and_loading duty_cycle;
    struct Voice_control          control;
    uint8_t                       volume_level;
    uint16_t                      frequency;
    uint64_t cycle_counter;

    uint8_t sample_buffer[SAMPLE_BUFFER_SIZE];
    uint8_t sample_index;

};

}    // namespace Audio

#endif
