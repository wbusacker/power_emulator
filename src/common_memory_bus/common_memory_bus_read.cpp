#include <common_memory_bus.h>
#include <stdio.h>

namespace Common_memory {

uint8_t Common_memory_bus::read(uint16_t address) {

    uint8_t return_data = 0;
    bool    found_data  = false;

    /* Only check if the space makes sense if the module
        contains memory space
    */
    if (memory_address_modules[address] != nullptr) {
        return_data = memory_address_modules[address]->read(address);
        found_data  = true;
    }

    if (found_data == false) {
        // printf("Warning: Invalid address read %04X\n", address);
    }

    // printf("R[%04X]%02X ", address, return_data);

    return return_data;
}

}    // namespace Common_memory
