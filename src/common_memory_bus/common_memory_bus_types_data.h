#ifndef COMMON_MEMORY_BUS_TYPES_DATA_H
#define COMMON_MEMORY_BUS_TYPES_DATA_H
#include <common_memory_bus_const.h>
#include <stdint.h>

namespace Common_memory {

class CMB_interface {
    public:
    CMB_interface(void) { }
    virtual ~CMB_interface() { }

    virtual void    cycle(void) { }
    virtual uint8_t read(uint16_t addr) { return 0; }
    virtual void    write(uint16_t addr, uint8_t data) { }
};

struct CMB_module {
    uint16_t       lower_address;
    uint16_t       upper_address;
    CMB_interface* interface;
};

}    // namespace Common_memory

#endif
