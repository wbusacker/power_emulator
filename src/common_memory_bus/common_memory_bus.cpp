#include <common_memory_bus.h>
#include <stdio.h>
#include <time.h>

#ifdef _MSC_VER
    #include <Windows.h>
#endif

namespace Common_memory {

Common_memory_bus::Common_memory_bus(void) {

    num_memory_modules = 0;
    num_cycle_modules  = 0;
    system_clock_count = 0;

    /* Calculate clock timing metrics */
    clock_counts_per_sync = 0x7FFFFFFFFFFFFFFF;
    time_sync_period_s    = 1E12;
    next_time_sync_s      = 1E12;

    sync_time_s = 0;
    sync_counts = 0;
}

Common_memory_bus::Common_memory_bus(uint64_t system_clock_speed_hz) {

    num_memory_modules = 0;
    num_cycle_modules  = 0;
    system_clock_count = 0;

    for (uint32_t i = 0; i < Common_memory::ADDRESS_SPACE_SIZE; i++) {
        memory_address_modules[i] = nullptr;
    }

    /* Calculate clock timing metrics */

    clock_counts_per_sync = system_clock_speed_hz / Common_memory::CPU_CLOCK_SYNC_RATE_HZ;
    time_sync_period_s    = 1.0 / Common_memory::CPU_CLOCK_SYNC_RATE_HZ;

#ifndef _MSC_VER

    timespec timer_get;
    clock_gettime(CLOCK_MONOTONIC, &timer_get);

    next_time_sync_s = timer_get.tv_sec;
    next_time_sync_s += timer_get.tv_nsec / static_cast<double>(1E9);

#else

    LARGE_INTEGER frequency;
    LARGE_INTEGER counter;
    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&counter);

    double freq  = frequency.QuadPart;
    double count = counter.QuadPart;

    next_time_sync_s = count / freq;

#endif
}

Common_memory_bus::~Common_memory_bus(void) {
    printf("Average Sync Loading %5.2f%%\n", 100 * ((sync_time_s / sync_counts) / time_sync_period_s));
}

}    // namespace Common_memory
