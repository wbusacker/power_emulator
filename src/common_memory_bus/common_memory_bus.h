#ifndef COMMON_MEMORY_BUS_H
#define COMMON_MEMORY_BUS_H
#include <common_memory_bus_const.h>
#include <common_memory_bus_types_data.h>

namespace Common_memory {

class Common_memory_bus {
    public:
    Common_memory_bus(void);
    Common_memory_bus(uint64_t system_clock_speed_hz);
    virtual ~Common_memory_bus(void);

    virtual Error_states
                         register_memory_module(CMB_interface* interface, uint32_t lower_address, uint32_t upper_address);
    virtual Error_states register_memory_module(CMB_interface* interface, uint32_t address);
    virtual Error_states register_cycle_module(CMB_interface* interface);

    virtual void process_clock_cycle(void);

    virtual uint8_t read(uint16_t address);
    virtual void    write(uint16_t address, uint8_t data);

    private:
    CMB_interface* memory_address_modules[Common_memory::ADDRESS_SPACE_SIZE];
    CMB_interface* cycle_modules[MAX_MODULES_ON_BUS];
    uint8_t        num_memory_modules;
    uint8_t        num_cycle_modules;

    uint64_t system_clock_count;
    uint64_t clock_counts_per_sync;
    double   time_sync_period_s;
    double   next_time_sync_s;

    double sync_time_s;
    double sync_counts;
};

}    // namespace Common_memory

#endif
