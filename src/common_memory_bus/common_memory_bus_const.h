#ifndef COMMON_MEMORY_BUS_CONST_H
#define COMMON_MEMORY_BUS_CONST_H
#include <stdint.h>

namespace Common_memory {

enum Error_states {
    SUCCESS = 0,         // Default state
    TOO_MANY_MODULES,    // Attempts to add too many modules onto the bus
    MODULE_OVERLAP,      // A module overlaps memory space with others
    INVALID_ADDRESS      // Requested address for operation does not have attached module
};

const uint8_t MAX_MODULES_ON_BUS = 128;

const uint32_t CPU_CLOCK_SYNC_RATE_HZ = 120;    // Frequency at which the memory bus attempts to re-align timing

const uint32_t ADDRESS_SPACE_SIZE = 0x10000;

}    // namespace Common_memory

#endif
