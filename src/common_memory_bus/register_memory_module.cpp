#include <common_memory_bus.h>

namespace Common_memory {

Error_states
Common_memory_bus::register_memory_module(CMB_interface* interface, uint32_t lower_address, uint32_t upper_address) {

    Error_states status = SUCCESS;
    /* Make sure that the address range makes sense */
    // if ((lower_address < upper_address) && (upper_address < Common_memory::ADDRESS_SPACE_SIZE)) {
    /* Go through and put the pointer to the interface in every address in range */
    for (uint32_t i = lower_address; i < upper_address; i++) {
        memory_address_modules[i] = interface;
    }
    // } else {
    //     status = INVALID_ADDRESS;
    // }

    return status;
}

Error_states Common_memory_bus::register_memory_module(CMB_interface* interface, uint32_t address) {

    Error_states status = SUCCESS;
    /* Make sure that the address range makes sense */
    if (address < Common_memory::ADDRESS_SPACE_SIZE) {
        memory_address_modules[address] = interface;
    } else {
        status = INVALID_ADDRESS;
    }

    return status;
}

}    // namespace Common_memory
