#include <common_memory_bus.h>
#include <stdio.h>

namespace Common_memory {

void Common_memory_bus::write(uint16_t address, uint8_t data) {

    bool found_data = false;
    /* Only check if the space makes sense if the module
        contains memory space
    */
    if (memory_address_modules[address] != nullptr) {
        memory_address_modules[address]->write(address, data);
        found_data = true;
    }

    if (found_data == false) {
        // printf("Warning: Invalid address write %04X\n", address);
    }

    // printf("W[%04X]%02X ", address, data);
}

}    // namespace Common_memory
