#include <common_memory_bus.h>

namespace Common_memory {

Error_states Common_memory_bus::register_cycle_module(CMB_interface* interface) {

    /* If we've exceeded the number of busses that can be on this device, ensure
         that we don't overwrite any more
    */
    Error_states status = SUCCESS;

    if (num_cycle_modules != MAX_MODULES_ON_BUS) {
        cycle_modules[num_cycle_modules] = interface;
        num_cycle_modules++;

    } else {
        status = TOO_MANY_MODULES;
    }

    return status;
}

}    // namespace Common_memory
