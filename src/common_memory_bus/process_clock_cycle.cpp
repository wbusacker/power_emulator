#include <common_memory_bus.h>
#include <stdio.h>
#include <time.h>

#ifndef _MSC_VER
    #include <unistd.h>
#else
    #include <Windows.h>
#endif

namespace Common_memory {

void Common_memory_bus::process_clock_cycle(void) {

    /* Up the number of cycles we've been going for */
    system_clock_count += 1;

    /* Check if we need to try and sync up the time */
    if ((system_clock_count % clock_counts_per_sync) == 0) {
        /* We're at a sync period */

#ifndef _MSC_VER

        /* Calculate remaining time in sync window */
        timespec timer_get;
        clock_gettime(CLOCK_MONOTONIC, &timer_get);

        double current_time = timer_get.tv_sec;
        current_time += timer_get.tv_nsec / static_cast<double>(1E9);

#else

        LARGE_INTEGER frequency;
        LARGE_INTEGER counter;
        QueryPerformanceFrequency(&frequency);
        QueryPerformanceCounter(&counter);

        double freq  = frequency.QuadPart;
        double count = counter.QuadPart;

        double current_time = count / freq;

#endif

        int64_t remaining_time_us = static_cast<int64_t>((next_time_sync_s - current_time) * 1E6);

        if (remaining_time_us > 0) {
#ifndef _MSC_VER
            usleep(remaining_time_us);
#else
            Sleep(remaining_time_us / 1E3);
#endif
            // } else if (remaining_time_us < 0){
            //     printf("WARNING: Frame Overflow\n");
        }

        /* Display the % realtime we're holding to */
        double spent_time = time_sync_period_s - (remaining_time_us / 1.0E6);
        sync_time_s += spent_time;
        sync_counts++;

        /* Calculate new sync time, keep it ahead of current time */
        next_time_sync_s += time_sync_period_s;
        while (next_time_sync_s < current_time) {
            next_time_sync_s += time_sync_period_s;
        }
    }

    /* Iterate through all devices to kick their cycle routines */

    for (uint8_t i = 0; i < num_cycle_modules; i++) {
        cycle_modules[i]->cycle();
    }
}

}    // namespace Common_memory
