#ifndef FUNCTION_TRACER_CONST_H
#define FUNCTION_TRACER_CONST_H

#include <stdint.h>

namespace Debug {

const uint16_t MAX_NUM_ROM_BANKS       = 128;
const uint16_t MAX_NUM_RAM_BANKS       = 4;
const uint16_t MAX_NUM_UNIQUE_CONTEXTS = MAX_NUM_RAM_BANKS * MAX_NUM_ROM_BANKS;
const uint16_t UPPER_RAM_SIZE          = 0x7F;

}    // namespace Debug

#endif
