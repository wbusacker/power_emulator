#ifndef FUNCTION_TRACER_TYPES_DATA_H
#define FUNCTION_TRACER_TYPES_DATA_H
#include <cartridge_const.h>
#include <function_tracer_const.h>
#include <stdint.h>

namespace Debug {

struct Function_recorder_ll {
    uint16_t                     bank;
    uint16_t                     addr;
    struct Function_recorder_ll* next;
};

struct Addr_context {
    bool     has_entry;
    uint16_t rom_bank_context_bitfield[MAX_NUM_ROM_BANKS];
    uint64_t execution_count;
};

struct Memory_map_context {
    struct Addr_context rom_bank_0[Cart::Gameboy::ROM_BANK_SIZE_BYTES];
    struct Addr_context rom_swap[MAX_NUM_ROM_BANKS][Cart::Gameboy::ROM_BANK_SIZE_BYTES];
    struct Addr_context ram_swap[MAX_NUM_RAM_BANKS][Cart::Gameboy::RAM_BANK_SIZE_BYTES];
    struct Addr_context ram_internal_lower[Cart::Gameboy::RAM_BANK_SIZE_BYTES];
    struct Addr_context ram_internal_upper[UPPER_RAM_SIZE];
};

}    // namespace Debug

#endif
