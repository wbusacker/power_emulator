#include <function_tracer.h>
#include <stdlib.h>

namespace Debug {

void Function_tracer::pull_function_trace(uint16_t addr) {

    uint16_t pc                = addr;
    bool     function_returned = false;
    while (function_returned == false) {
        /* Get the instruction at this location */
        uint8_t instruction = memory_bus->read(pc);

        /* Print out the mnemonic */
        fprintf(trace_log, "%04X %s ", pc, LR35902::mnemonics[instruction]);

        if (instruction == 0xCB) {
            pc++;
            uint8_t cb_instruction = memory_bus->read(pc);
            fprintf(trace_log, "\n%04X %s ", pc, LR35902::cb_mnemonics[cb_instruction]);
        } else {

            /* If there are any extra bytes associated with the instruction, print that out */
            for (uint16_t i = 1; i < LR35902::instr_lengths[instruction]; i++) {
                fprintf(trace_log, "%02X ", memory_bus->read(pc + i));
            }
        }

        fprintf(trace_log, "\n");

        pc += (LR35902::instr_lengths[instruction] == 0) ? 1 : LR35902::instr_lengths[instruction];

        if ((instruction == LR35902::RET) || (instruction == LR35902::RETI) || (instruction == LR35902::RST_00) ||
            (instruction == LR35902::RST_08) || (instruction == LR35902::RST_10) || (instruction == LR35902::RST_18) ||
            (instruction == LR35902::RST_20) || (instruction == LR35902::RST_28) || (instruction == LR35902::RST_30) ||
            (instruction == LR35902::RST_38)) {
            function_returned = true;
        }
    }

    fprintf(trace_log, "\n");
    fflush(trace_log);
}

}    // namespace Debug