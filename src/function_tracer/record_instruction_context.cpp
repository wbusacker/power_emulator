#include <function_tracer.h>

namespace Debug {

void Function_tracer::record_instruction_context(uint16_t addr) {

    /* Get the current ram and rom banks */
    uint8_t rom_bank = memory_controller->get_active_rom_bank();
    uint8_t ram_bank = memory_controller->get_active_ram_bank();

    struct Addr_context* context = nullptr;

    if (addr == 0x5C70) {
        printf("Hit Addr\n");
    }

    /* Figure out which context set we should store in */

    if ((0x0000 <= addr) && (addr < 0x4000)) {
        context = &(instruction_contexts.rom_bank_0[addr]);
    } else if ((0x4000 <= addr) && (addr < 0x8000)) {
        context = &(instruction_contexts.rom_swap[rom_bank][addr % Cart::Gameboy::ROM_BANK_SIZE_BYTES]);
    } else if ((0xA000 <= addr) && (addr < 0xC000)) {
        context = &(instruction_contexts.ram_swap[ram_bank][addr % Cart::Gameboy::RAM_BANK_SIZE_BYTES]);
    } else if ((0xC000 <= addr) && (addr < 0xFE00)) {
        context = &(instruction_contexts.ram_internal_lower[addr % Cart::Gameboy::RAM_BANK_SIZE_BYTES]);
    } else if ((0xFF80 <= addr) && (addr < 0xFFFF)) {
        context = &(instruction_contexts.ram_internal_upper[addr - 0xFF80]);
    }

    context->has_entry = true;
    context->rom_bank_context_bitfield[rom_bank] |= 1 << ram_bank;
    context->execution_count++;
}

}    // namespace Debug