#include <function_tracer.h>
#include <stdlib.h>

namespace Debug {

void Function_tracer::trace_function(uint16_t addr) {

    /* Check to see if we've seen this function before */
    bool already_traced = false;

    struct Function_recorder_ll* working_node = traced_list;
    while (working_node->next != nullptr) {
        working_node = working_node->next;
        /* First check if the address matches */
        uint8_t rom_bank = memory_controller->get_active_rom_bank();
        if (addr < 0x4000) {
            rom_bank = 0;
        }
        if ((working_node->addr == addr) &&
            ((working_node->bank == rom_bank) || (working_node->bank == memory_controller->get_active_ram_bank()) ||
             (working_node->bank == 0xFF))) {
            already_traced = true;
        }
    }

    if (already_traced == false) {

        working_node->next =
            reinterpret_cast<struct Function_recorder_ll*>(malloc(sizeof(struct Function_recorder_ll)));
        working_node = working_node->next;

        working_node->next = nullptr;

        working_node->addr = addr;

        if ((addr < 0x8000) && (addr >= 0x0000)) {
            uint8_t bank = memory_controller->get_active_rom_bank();
            if (addr < 0x4000) {
                bank = 0;
            }
            fprintf(trace_log, "Subroutine ROM Bank %d: Addr 0x%04X\n", bank, addr);
            working_node->bank = bank;
        } else if ((addr < 0xFE00) && (addr >= 0x8000)) {
            fprintf(trace_log, "Subroutine RAM Bank %d: Addr 0x%04X\n", memory_controller->get_active_ram_bank(), addr);
            working_node->bank = memory_controller->get_active_rom_bank();
        } else {
            fprintf(trace_log, "Subroutine General Memory 0: Addr %04X\n", addr);
            working_node->bank = 0xFF;
        }

        pull_function_trace(addr);
    }
}

}    // namespace Debug