#include <function_tracer.h>
#include <stdio.h>

namespace Debug {

void Function_tracer::dump_instruction_contexts(void) {

    bool is_cb = false;

    char  instruction_trace_file_name[256];
    FILE* fp;
    for (uint16_t i = 0; i < 65535; i++) {
        sprintf(instruction_trace_file_name, "instruction_context_trace_%04X.json", i);
        /* Attempt to read the file */
        fp = fopen(instruction_trace_file_name, "r");
        if (fp == NULL) {
            /* No file exists here, break */
            break;
        } else {
            fclose(fp);
        }
    }

    fp = fopen(instruction_trace_file_name, "w");

    printf("Saving instruction context trace to %s\n", instruction_trace_file_name);

    /* Open JSON Structure */
    fprintf(fp, "{\n");

    /* Open rom bank contexts */
    fprintf(fp, "  \"rom_bank_contexts\" : [\n");

    /* Open Bank */
    fprintf(fp, "    {\n");
    fprintf(fp, "      \"bank\" : \"0\",\n");
    /* Open Address Lists */
    fprintf(fp, "      \"addresses\" : [\n");
    bool more_than_one_addr = false;
    for (uint16_t addr = 0; addr < Cart::Gameboy::ROM_BANK_SIZE_BYTES; addr++) {

        /* Check if this addr has any markings at all */
        if (instruction_contexts.rom_bank_0[addr].has_entry == true) {
            if (more_than_one_addr == true) {
                fprintf(fp, ",\n");
            } else {
                more_than_one_addr = true;
            }
            /* Open Addr Object */
            fprintf(fp, "        {\n");
            fprintf(fp, "          \"addr\" : \"%04X\",\n", addr);

            /* Open mnemonic trace */
            fprintf(fp, "          \"mnemonic\" : \"");

            /* Force switch the memory controller banks */
            memory_controller->set_rom_bank(1);
            uint16_t pc          = addr;
            uint8_t  instruction = memory_bus->read(pc);

            if (is_cb == true) {
                fprintf(fp, "%04X %s ", pc, LR35902::cb_mnemonics[instruction]);
            } else {
                fprintf(fp, "%04X %s ", pc, LR35902::mnemonics[instruction]);
            }

            is_cb = false;
            if (instruction == 0xCB) {
                /* The next instruction will have the same execution context as this one so mark that */
                is_cb = true;
            } else {
                /* If there are any extra bytes associated with the instruction, print that out */
                for (uint16_t i = 1; i < LR35902::instr_lengths[instruction]; i++) {
                    fprintf(fp, "%02X ", memory_bus->read(pc + i));
                }
            }
            /* Close mnemonic trace */
            fprintf(fp, "\",\n");

            /* Marker for subroutine encapsulation */
            fprintf(fp, "          \"encapsulated\" : false,\n");

            /* Number of execution points */
            fprintf(fp, "          \"count\" : \"%ld\",\n", instruction_contexts.rom_bank_0[addr].execution_count);

            /* Open Addr Context Lists */
            fprintf(fp, "          \"contexts\" : [\n");
            bool more_than_one_context = false;
            for (uint16_t rom_bank = 0; rom_bank < MAX_NUM_ROM_BANKS; rom_bank++) {
                for (uint16_t ram_bank = 0; ram_bank < MAX_NUM_RAM_BANKS; ram_bank++) {
                    uint16_t bit_check = 1 << ram_bank;
                    if ((bit_check & instruction_contexts.rom_bank_0[addr].rom_bank_context_bitfield[rom_bank]) != 0) {
                        if (more_than_one_context == true) {
                            fprintf(fp, ",\n");
                        } else {
                            more_than_one_context = true;
                        }
                        /* Open & Close context structure */
                        fprintf(fp, "            { \"rom_bank\" : \"%d\", \"ram_bank\" : \"%d\"}", rom_bank, ram_bank);
                    }
                }

                if (is_cb) {
                    instruction_contexts.rom_bank_0[addr + 1].rom_bank_context_bitfield[rom_bank] =
                        instruction_contexts.rom_bank_0[addr].rom_bank_context_bitfield[rom_bank];
                }
            }
            fprintf(fp, "\n");
            /* Close Addr Context list */
            fprintf(fp, "          ]\n");
            /* Close Addr Object */
            fprintf(fp, "        }");
        }
    }
    fprintf(fp, "\n");
    /* Close Addr Lists */
    fprintf(fp, "      ]\n");
    /* Close Bank */
    fprintf(fp, "    }");

    for (uint16_t executing_rom_bank = 1; executing_rom_bank < MAX_NUM_ROM_BANKS; executing_rom_bank++) {

        /* Check to see if this bank has any markings at all */
        bool has_data = false;
        for (uint16_t addr = 0; addr < Cart::Gameboy::ROM_BANK_SIZE_BYTES; addr++) {
            if (instruction_contexts.rom_swap[executing_rom_bank][addr].has_entry == true) {
                has_data = true;
            }
        }

        if (has_data) {
            fprintf(fp, ",\n");

            /* Open Bank */
            fprintf(fp, "    {\n");
            fprintf(fp, "      \"bank\" : \"%d\",\n", executing_rom_bank);
            /* Open Address Lists */
            fprintf(fp, "      \"addresses\" : [\n");
            bool more_than_one_addr = false;
            for (uint16_t addr = 0; addr < Cart::Gameboy::ROM_BANK_SIZE_BYTES; addr++) {

                /* Check if this addr has any markings at all */
                if (instruction_contexts.rom_swap[executing_rom_bank][addr].has_entry == true) {
                    if (more_than_one_addr == true) {
                        fprintf(fp, ",\n");
                    } else {
                        more_than_one_addr = true;
                    }
                    /* Open Addr Object */
                    fprintf(fp, "        {\n");
                    fprintf(fp, "          \"addr\" : \"%04X\",\n", addr + 0x4000);

                    /* Open mnemonic trace */
                    fprintf(fp, "          \"mnemonic\" : \"");
                    /* Force switch the memory controller banks */
                    memory_controller->set_rom_bank(executing_rom_bank);
                    uint16_t pc          = addr + 0x4000;
                    uint8_t  instruction = memory_bus->read(pc);

                    if (is_cb == true) {
                        fprintf(fp, "%04X %s ", pc, LR35902::cb_mnemonics[instruction]);
                    } else {
                        fprintf(fp, "%04X %s ", pc, LR35902::mnemonics[instruction]);
                    }

                    is_cb = false;
                    if (instruction == 0xCB) {
                        /* The next instruction will have the same execution context as this one so mark that */
                        is_cb = true;
                    } else {
                        /* If there are any extra bytes associated with the instruction, print that out */
                        for (uint16_t i = 1; i < LR35902::instr_lengths[instruction]; i++) {
                            fprintf(fp, "%02X ", memory_bus->read(pc + i));
                        }
                    }
                    /* Close mnemonic trace */
                    fprintf(fp, "\",\n");

                    /* Marker for subroutine encapsulation */
                    fprintf(fp, "          \"encapsulated\" : false,\n");

                    /* Number of execution points */
                    fprintf(fp,
                            "          \"count\" : \"%ld\",\n",
                            instruction_contexts.rom_swap[executing_rom_bank][addr].execution_count);

                    /* Open Addr Context Lists */
                    fprintf(fp, "          \"contexts\" : [\n");
                    bool more_than_one_context = false;
                    for (uint16_t rom_bank = 0; rom_bank < MAX_NUM_ROM_BANKS; rom_bank++) {
                        for (uint16_t ram_bank = 0; ram_bank < MAX_NUM_RAM_BANKS; ram_bank++) {
                            uint16_t bit_check = 1 << ram_bank;
                            if ((bit_check & instruction_contexts.rom_swap[executing_rom_bank][addr]
                                                 .rom_bank_context_bitfield[rom_bank]) != 0) {
                                if (more_than_one_context == true) {
                                    fprintf(fp, ",\n");
                                } else {
                                    more_than_one_context = true;
                                }
                                /* Open & Close context structure */
                                fprintf(fp,
                                        "            { \"rom_bank\" : \"%d\", \"ram_bank\" : \"%d\"}",
                                        rom_bank,
                                        ram_bank);
                            }
                        }
                        if (is_cb) {
                            instruction_contexts.rom_swap[executing_rom_bank][addr + 1]
                                .rom_bank_context_bitfield[rom_bank] =
                                instruction_contexts.rom_swap[executing_rom_bank][addr]
                                    .rom_bank_context_bitfield[rom_bank];
                        }
                    }
                    fprintf(fp, "\n");
                    /* Close Addr Context list */
                    fprintf(fp, "          ]\n");
                    /* Close Addr Object */
                    fprintf(fp, "        }");
                }
            }
            fprintf(fp, "\n");
            /* Close Addr Lists */
            fprintf(fp, "      ]\n");
            /* Close Bank */
            fprintf(fp, "    }");
        }
    }

    /* Close rom bank contexts */
    fprintf(fp, "\n  ]\n");

    /* Close JSON structure */
    fprintf(fp, "}\n");

    fclose(fp);
}

}    // namespace Debug