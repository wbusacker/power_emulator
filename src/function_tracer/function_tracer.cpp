#include <cpu_const.h>
#include <function_tracer.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace Debug {

Function_tracer::Function_tracer(Cart::Gameboy::Memory_controller* mem_ctrl,
                                 Common_memory::Common_memory_bus* mem_bus) {
    memory_controller = mem_ctrl;
    memory_bus        = mem_bus;

    char function_trace_file_name[256];
    for (uint16_t i = 0; i < 65535; i++) {
        sprintf(function_trace_file_name, "function_trace_%04X.txt", i);
        /* Attempt to read the file */
        trace_log = fopen(function_trace_file_name, "r");
        if (trace_log == NULL) {
            /* No file exists here, break */
            break;
        } else {
            fclose(trace_log);
        }
    }

    trace_log = fopen(function_trace_file_name, "w");

    /* Always start out by tracing at address 0x0100 */
    traced_list       = reinterpret_cast<struct Function_recorder_ll*>(malloc(sizeof(struct Function_recorder_ll)));
    traced_list->addr = 0x0100;
    traced_list->bank = 0;

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0040\n");
    pull_function_trace(0x0040);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0048\n");
    pull_function_trace(0x0048);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0050\n");
    pull_function_trace(0x0050);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0058\n");
    pull_function_trace(0x0058);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0060\n");
    pull_function_trace(0x0060);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0000\n");
    pull_function_trace(0x0000);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0008\n");
    pull_function_trace(0x0008);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0010\n");
    pull_function_trace(0x0010);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0018\n");
    pull_function_trace(0x0018);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0020\n");
    pull_function_trace(0x0020);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0028\n");
    pull_function_trace(0x0028);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0030\n");
    pull_function_trace(0x0030);

    fprintf(trace_log, "Subroutine ROM Bank 0: Addr 0x0038\n");
    pull_function_trace(0x0038);

    memset(&instruction_contexts, 0, sizeof(struct Memory_map_context));
}

Function_tracer::~Function_tracer(void) {

    printf("Tracer Destructor\n");
    fflush(stdout);

    fclose(trace_log);

    struct Function_recorder_ll* working_node = traced_list;
    while (working_node != nullptr) {
        struct Function_recorder_ll* next_node = working_node->next;
        free(working_node);
        working_node = next_node;
    }

    dump_instruction_contexts();
}

}    // namespace Debug
