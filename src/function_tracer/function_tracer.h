#ifndef FUNCTION_TRACER_H
#define FUNCTION_TRACER_H
#include <cartridge_types_data.h>
#include <common_memory_bus.h>
#include <cpu_instructions_groups.h>
#include <function_tracer_const.h>
#include <function_tracer_types_data.h>
#include <stdio.h>

namespace Debug {

class Function_tracer {
    public:
    Function_tracer(Cart::Gameboy::Memory_controller* mem_ctrl, Common_memory::Common_memory_bus* mem_bus);
    ~Function_tracer(void);

    void trace_function(uint16_t addr);
    void record_instruction_context(uint16_t addr);

    private:
    void dump_instruction_contexts(void);
    void pull_function_trace(uint16_t addr);

    Cart::Gameboy::Memory_controller* memory_controller;
    Common_memory::Common_memory_bus* memory_bus;

    struct Function_recorder_ll* traced_list;

    FILE* trace_log;

    struct Memory_map_context instruction_contexts;
};

}    // namespace Debug

#endif
