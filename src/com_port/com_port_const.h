#ifndef COM_PORT_CONST_H
#define COM_PORT_CONST_H

#include <stdint.h>

namespace Serial {

const uint32_t INTERNAL_CLOCK_FREQUENCY_DIVIDER = 4194304 / 8192;
const uint8_t  BITS_PER_CHARACTER               = 8;

const uint16_t LOWER_REGISTER_ADDR = 0xFF01;
const uint16_t UPPER_REGISTER_ADDR = 0xFF02;
const uint16_t COM_MEMORY_START    = 0xFF01;
const uint16_t COM_MEMORY_END      = 0xFF03;

const uint16_t PORT_BUFFER_ADDR  = Serial::LOWER_REGISTER_ADDR;
const uint16_t PORT_CONTROL_ADDR = Serial::UPPER_REGISTER_ADDR;

const uint16_t CONTROL_TRANSFER_BIT = 7;
const uint16_t CONTROL_SPEED_BIT    = 0;

}    // namespace Serial

#endif
