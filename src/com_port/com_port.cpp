#include <com_port.h>
#include <string.h>
#ifndef _MSC_VER
    #include <termios.h>
#endif

namespace Serial {

Com_port::Com_port(FILE* output, FILE* input) {

    /* We need to make sure the two file's are configured
        such that we can work with them non-blocking
    */

#ifndef _MSC_VER
    struct termios serial_config;

    memset(&serial_config, 0, sizeof(struct termios));

    tcgetattr(fileno(output), &serial_config);

    serial_config.c_cc[VTIME] = 0;
    serial_config.c_cc[VMIN]  = 0;

    tcsetattr(fileno(output), TCSANOW, &serial_config);

    tcgetattr(fileno(input), &serial_config);

    serial_config.c_cc[VTIME] = 0;
    serial_config.c_cc[VMIN]  = 0;

    tcsetattr(fileno(input), TCSANOW, &serial_config);

    output_file = output;
    input_file  = input;

    in_char             = 0;
    out_char            = 0;
    visible_char        = 0;
    clock_cycle_counter = 0;
    bit_cycle_counter   = 0;
    active_transfer     = false;
#endif
}

}    // namespace Serial
