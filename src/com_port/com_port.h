#ifndef COM_PORT_H
#define COM_PORT_H
#include <com_port_const.h>
#include <com_port_types_data.h>
#include <common_memory_bus_types_data.h>
#include <stdio.h>

namespace Serial {

class Com_port : public Common_memory::CMB_interface {
    public:
    Com_port(FILE* output, FILE* input);

    void    cycle(void);
    uint8_t read(uint16_t addr);
    void    write(uint16_t addr, uint8_t data);

    private:
    FILE* output_file;
    FILE* input_file;

    uint8_t in_char;
    uint8_t out_char;
    uint8_t visible_char;

    uint16_t clock_cycle_counter;
    uint8_t  bit_cycle_counter;

    bool active_transfer;
};

}    // namespace Serial

#endif
