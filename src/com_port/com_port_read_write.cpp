#include <com_port.h>

namespace Serial {

uint8_t Com_port::read(uint16_t addr) {

    uint8_t return_byte = 0;

    if (addr == Serial::PORT_BUFFER_ADDR) {
        /* Just send out the currently visible buffer */
        return_byte = visible_char;
    } else if (addr == Serial::PORT_CONTROL_ADDR) {

        return_byte = 0;

        /* Set the active transfer bit */
        if (active_transfer) {
            return_byte |= 1 << Serial::CONTROL_TRANSFER_BIT;
        }

        /* We always use the internal clock speeds so report that */
        return_byte |= 1 << Serial::CONTROL_SPEED_BIT;

    } else {
        /* Nothing needs to happen in this case */
    }

    return return_byte;
}

void Com_port::write(uint16_t addr, uint8_t data) {

    // if ((addr == Serial::PORT_BUFFER_ADDR) && (! active_transfer)) {
    if (addr == Serial::PORT_BUFFER_ADDR) {

        FILE* fp = fopen("com_port.log", "a");
        fwrite(&data, 1, 1, fp);
        fclose(fp);

        // fputc(data, stdout);

        // putc(data, output_file);
        /* Only allow writes when we're not doing a write */
        // out_char     = data;
        // visible_char = data;
    } else if (addr == Serial::PORT_CONTROL_ADDR) {

        /* We only allow starting a data transfer and ignore the baudrate */
        if ((data & Serial::CONTROL_TRANSFER_BIT) != 0) {
            active_transfer = true;
        }

    } else {
        /* Nothing needs to happen in this case */
    }
}

}    // namespace Serial