#include <com_port.h>
#ifndef _MSC_VER
    #include <unistd.h>
#endif

/* We explicitly have this function to avoid shenanigans with GCC being unable
    to resolve the C STL read function with the class method also named read
*/

#ifndef _MSC_VER
inline ssize_t c_read(int fd, void* buf, size_t count) {
    return read(fd, buf, count);
}
#endif

namespace Serial {

void Com_port::cycle(void) {

    /* If we're actively transmitting, shift the bytes out
        at the specified frequency.
    */
    // if (active_transfer == true) {

    //     /* If we've just started, read in a single character from the input file */
    //     if ((clock_cycle_counter == 0) && (bit_cycle_counter == 0)) {
    //         // int num_bytes = c_read(fileno(input_file), &in_char, 1);
    //         int num_bytes = 1;
    //         in_char = ' ';
    //         if (num_bytes == 0) {
    //             /* We didn't actually read a character, so set to zero */
    //             in_char = 0;
    //         }
    //     }

    //     /* Increment how long we've been at this */
    //     clock_cycle_counter++;

    //     if (clock_cycle_counter == Serial::INTERNAL_CLOCK_FREQUENCY_DIVIDER) {
    //         /* We've passed enough counter cycles to shift a single bit out */
    //         bit_cycle_counter++;

    //         /* Move the visible char by one bit */
    //         visible_char >>= 1;

    //         /* Grab the lsb of the in_char, and apply that to the top of visible char */
    //         visible_char |= (in_char & 1) << (Serial::BITS_PER_CHARACTER - 1);

    //         /* Move in_char by one bit */
    //         in_char >>= 1;

    //         if (bit_cycle_counter == Serial::BITS_PER_CHARACTER) {
    //             /* If we've reached the end of the transfer,
    //                 reset back to zero and halt the transfer.
    //                 While we're here, send out the original byte */
    //             bit_cycle_counter = 0;
    //             active_transfer   = false;
    //             fputc(out_char, output_file);
    //             fflush(output_file);
    //         }

    //         /* Now that the cycle counter has finished a bit, set cycle counter back to zero */
    //         clock_cycle_counter = 0;
    //     }
    // }
}

}    // namespace Serial