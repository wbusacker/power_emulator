#include <picture_processing_unit.h>
#include <stdio.h>

namespace Display {

void Gameboy_picture_processing_unit::write(uint16_t addr, uint8_t data) {

    /* We need to determine which internal data structure the corresponding address
        belongs to and then how to write into that data structure
    */

    /* Tile pattern buffers? */
    if ((Display::PATTERN_BUFFER_0_START_ADDR <= addr) && (addr < Display::PATTERN_BUFFER_END_ADDR)) {
        uint16_t offset_addr = addr - PATTERN_BUFFER_0_START_ADDR;

        /* Which pattern are we in? */
        uint16_t pattern_index = offset_addr / Display::PATTERN_SIZE_BYTES;

        /* Calculate the byte in the pattern we want */
        uint8_t pattern_row_index = offset_addr % Display::PATTERN_SIZE_BYTES;

        pattern_buffer[pattern_index][pattern_row_index] = data;

    } else if ((Display::TILE_MAP_0_START_ADDR <= addr) && (addr < Display::TILE_MAP_END_ADDR)) {
        uint16_t offset_addr = addr - TILE_MAP_0_START_ADDR;

        uint8_t tile_map_number = offset_addr / Display::TILE_MAP_SIZE_BYTES;

        /* Bring the offset addres in range of the tile map */
        offset_addr = offset_addr % Display::TILE_MAP_SIZE_BYTES;

        uint8_t tile_row = offset_addr / Display::TILE_MAP_AXIS_LEN;
        uint8_t tile_col = offset_addr % Display::TILE_MAP_AXIS_LEN;

        tile_map[tile_map_number][tile_row][tile_col] = data;
    } else if ((Display::OAM_START_ADDR <= addr) && (addr < Display::OAM_END_ADDR)) {

        /* Figure out which sprite this is going into */
        uint16_t offset_addr = addr - Display::OAM_START_ADDR;

        uint8_t oam_index = offset_addr / Display::OAM_SPRITE_DATA_SIZE;
        uint8_t oam_byte  = offset_addr % Display::OAM_SPRITE_DATA_SIZE;

        switch (oam_byte) {
            case OAM_X_DATA_LOCATION:
                oam_data[oam_index].x_pos = data;
                break;
            case OAM_Y_DATA_LOCATION:
                oam_data[oam_index].y_pos = data;
                break;
            case OAM_PATTERN_DATA_LOCATION:
                oam_data[oam_index].pattern = data;
                break;
            case OAM_FLAGS_DATA_LOCATION:
                oam_data[oam_index].priority       = (data >> 7) & 0b1;
                oam_data[oam_index].y_flip         = (data >> 6) & 0b1;
                oam_data[oam_index].x_flip         = (data >> 5) & 0b1;
                oam_data[oam_index].pallete_number = (data >> 4) & 0b1;
                break;
        }
    } else {
        switch (addr) {
            case 0xFF40:
                lcdc.lcd_control                     = (data >> 7) & 0b1;
                lcdc.window_tile_map_select          = (data >> 6) & 0b1;
                lcdc.window_control                  = (data >> 5) & 0b1;
                lcdc.bg_window_pattern_buffer_select = (data >> 4) & 0b1;
                lcdc.bg_tile_map_select              = (data >> 3) & 0b1;
                lcdc.sprite_size                     = (data >> 2) & 0b1;
                lcdc.sprite_control                  = (data >> 1) & 0b1;
                lcdc.bg_window_control               = (data >> 0) & 0b1;
                break;
            case 0xFF41:
                status.line_y_coincidence_interrupt = (data >> 6) & 0b1;
                status.mode_2_interrupt             = (data >> 5) & 0b1;
                status.mode_1_interrupt             = (data >> 4) & 0b1;
                status.mode_0_interrupt             = (data >> 3) & 0b1;

                /* Coincidence Flag and Current mode cannot be written */

                break;
            case 0xFF42:
                scroll_y = data;
                break;
            case 0xFF43:
                scroll_x = data;
                break;
            case 0xFF44:
                current_row = 0;
                break;
            case 0xFF45:
                lcd_y_compare = data;
                break;
            case 0xFF46:
                dma_address       = data;
                dma_cycle_counter = 0;
                break;
            case 0xFF47:
                background_window_pallete.color_0 = (data >> 0) & 0b11;
                background_window_pallete.color_1 = (data >> 2) & 0b11;
                background_window_pallete.color_2 = (data >> 4) & 0b11;
                background_window_pallete.color_3 = (data >> 6) & 0b11;
                break;
            case 0xFF48:
                object_pallete_0.color_0 = (data >> 0) & 0b11;
                object_pallete_0.color_1 = (data >> 2) & 0b11;
                object_pallete_0.color_2 = (data >> 4) & 0b11;
                object_pallete_0.color_3 = (data >> 6) & 0b11;
                break;
            case 0xFF49:
                object_pallete_1.color_0 = (data >> 0) & 0b11;
                object_pallete_1.color_1 = (data >> 2) & 0b11;
                object_pallete_1.color_2 = (data >> 4) & 0b11;
                object_pallete_1.color_3 = (data >> 6) & 0b11;
                break;
            case 0xFF4A:
                window_y = data;
                break;
            case 0xFF4B:
                window_x = data;
                break;
        }
    }

    /* All other data modifications just go into the ether */
}

uint8_t Gameboy_picture_processing_unit::read(uint16_t addr) {

    /* We need to determine which internal data structure the corresponding address
        belongs to and then how to write into that data structure
    */

    uint8_t data = 0;

    /* Tile pattern buffers? */
    if ((Display::PATTERN_BUFFER_0_START_ADDR <= addr) && (addr < Display::PATTERN_BUFFER_END_ADDR)) {
        uint16_t offset_addr = addr - PATTERN_BUFFER_0_START_ADDR;

        /* Which pattern are we in? */
        uint16_t pattern_index = offset_addr / Display::PATTERN_SIZE_BYTES;

        /* Calculate the byte in the pattern we want */
        uint8_t pattern_row_index = offset_addr % Display::PATTERN_SIZE_BYTES;

        data = pattern_buffer[pattern_index][pattern_row_index];

    } else if ((Display::TILE_MAP_0_START_ADDR <= addr) && (addr < Display::TILE_MAP_END_ADDR)) {
        uint16_t offset_addr = addr - TILE_MAP_0_START_ADDR;

        uint8_t tile_map_number = offset_addr / Display::TILE_MAP_SIZE_BYTES;

        /* Bring the offset addres in range of the tile map */
        offset_addr = offset_addr % Display::TILE_MAP_SIZE_BYTES;

        uint8_t tile_row = offset_addr / Display::TILE_MAP_AXIS_LEN;
        uint8_t tile_col = offset_addr % Display::TILE_MAP_AXIS_LEN;

        data = tile_map[tile_map_number][tile_row][tile_col];
    } else {
        switch (addr) {
            case 0xFF40:
                data |= (lcdc.lcd_control & 0b1) << 7;
                data |= (lcdc.window_tile_map_select & 0b1) << 6;
                data |= (lcdc.window_control & 0b1) << 5;
                data |= (lcdc.bg_window_pattern_buffer_select & 0b1) << 4;
                data |= (lcdc.bg_tile_map_select & 0b1) << 3;
                data |= (lcdc.sprite_size & 0b1) << 2;
                data |= (lcdc.sprite_control & 0b1) << 1;
                data |= (lcdc.bg_window_control & 0b1) << 0;
                break;
            case 0xFF41:
                data |= (status.line_y_coincidence_interrupt & 0b1) << 6;
                data |= (status.mode_2_interrupt & 0b1) << 5;
                data |= (status.mode_1_interrupt & 0b1) << 4;
                data |= (status.mode_0_interrupt & 0b1) << 3;
                data |= (status.coincidence_control & 0b1) << 2;
                data |= (status.current_mode & 0b11);
                break;
            case 0xFF42:
                data = scroll_y;
                break;
            case 0xFF43:
                data = scroll_x;
                break;
            case 0xFF44:
                data = current_row;
                break;
            case 0xFF45:
                data = lcd_y_compare;
                break;
            // case 0xFF46: /* Write only */
            case 0xFF47:
                data |= background_window_pallete.color_3 << 6;
                data |= background_window_pallete.color_2 << 4;
                data |= background_window_pallete.color_1 << 2;
                data |= background_window_pallete.color_0 << 0;
                break;
            case 0xFF48:
                data |= object_pallete_0.color_3 << 6;
                data |= object_pallete_0.color_2 << 4;
                data |= object_pallete_0.color_1 << 2;
                data |= object_pallete_0.color_0 << 0;
                break;
            case 0xFF49:
                data |= object_pallete_1.color_3 << 6;
                data |= object_pallete_1.color_2 << 4;
                data |= object_pallete_1.color_1 << 2;
                data |= object_pallete_1.color_0 << 0;
                break;
            case 0xFF4A:
                data = window_y;
                break;
            case 0xFF4B:
                data = window_x;
                break;
        }
    }

    return data;
}

}    // namespace Display