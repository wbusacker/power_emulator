#include <picture_processing_unit.h>

namespace Display {

void Gameboy_picture_processing_unit::process_mode_2(void) {
    /* Read Sprite data */

    if (mode_cycle_counter == Display::MODE_2_CLOCKS) {
        status.current_mode = Display::MODE_3;
        mode_transition     = true;
    }
}

}    // namespace Display