#include <cpu_const.h>
#include <picture_processing_unit.h>
#include <stdio.h>

namespace Display {

void Gameboy_picture_processing_unit::cycle(void) {

    display_cycle_counter++;
    mode_cycle_counter++;

    previous_mode = status.current_mode;

    switch (status.current_mode) {
        case MODE_0:
            process_mode_0();
            break;
        case MODE_1:
            process_mode_1();
            break;
        case MODE_2:
            process_mode_2();
            break;
        case MODE_3:
            process_mode_3();
            break;
    }

    /* If we need to transfer data to the Sprite area, copy a single byte every cycle pass */
    if ((Display::OAM_START_ADDR + dma_cycle_counter) < Display::OAM_END_ADDR) {
        /* Calculate which sprite buffer we're reading into */
        uint16_t source_addr = (static_cast<uint16_t>(dma_address) << LR35902::BITS_PER_BYTE) | dma_cycle_counter;

        /* Call our own write function and let it deal with interpreting the OAM data */
        write(Display::OAM_START_ADDR + dma_cycle_counter, main_memory->read(source_addr));

        dma_cycle_counter++;
    }

    /* Check if we need to issue any mode interrupts */
    if (((status.mode_0_interrupt == 1) && mode_transition && (status.current_mode == MODE_0)) ||
        ((status.mode_1_interrupt == 1) && mode_transition && (status.current_mode == MODE_1)) ||
        ((status.mode_2_interrupt == 1) && mode_transition && (status.current_mode == MODE_2))) {
        cpu->raise_interrupt(LR35902::LCDC_INTERRUPT);
    }

    /* If we just switched into V-Blank, fire V-Blank interrupt */
    if (mode_transition && (status.current_mode == Display::MODE_1)) {
        cpu->raise_interrupt(LR35902::VBLANK_INTERRUPT);
    }

    /* If we just changed modes, reset the mode counter */
    if (mode_transition) {
        mode_cycle_counter = 0;
        mode_transition    = false;
    }

    /* Always count up clocks if the display cycle counter is mode line rate */
    if ((display_cycle_counter % Display::LINE_RATE_CLOCKS) == 0) {
        current_row++;
        if (current_row == 154) {
            current_row = 0;
        }

        /* Check if we just hit the same line as LYC */
        if ((status.line_y_coincidence_interrupt == 1) && (current_row == lcd_y_compare)) {
            cpu->raise_interrupt(LR35902::LCDC_INTERRUPT);
        }
    }
}

}    // namespace Display