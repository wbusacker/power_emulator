#include <cpu_const.h>
#include <picture_processing_unit.h>
#include <stdio.h>

namespace Display {

void Gameboy_picture_processing_unit::draw_sprites(void) {

    /* Record which sprites need to all be drawn */
    bool    draw_sprite[Display::OAM_SPRITE_COUNT];
    uint8_t draw_count = 0;

    /* Generate sprite height */
    uint8_t sprite_height = (lcdc.sprite_size == 0) ? Display::SHORT_SPRITE_HEIGHT : Display::TALL_SPRITE_HEIGHT;

    /* Search across all of the pixels to figure out if any are drawn in this line */
    for (uint8_t sprite_num = 0; sprite_num < Display::OAM_SPRITE_COUNT; sprite_num++) {

        /* Check all of the conditions that would allow this sprite to be drawn

            They are, in order:
            X = 0 hides a sprite
            Y = 0 hides a sprite
            Starting X coordinate needs to be on screen
            Starting Y coordinate needs to be on screen
            Current row must be on or after the Y coordinate
            Current row must be before the end of the sprite

         */

        struct Display::Sprite_data* sprite = &(oam_data[sprite_num]);
        if ((sprite->x_pos != 0) && /* 0 on X hides a sprite */
            (sprite->y_pos != 0) && /* 0 on Y hides a sprite */
            (sprite->x_pos < (Display::GAMEBOY_SCREEN_COL_COUNT + Display::SPRITE_X_OFFSET)) &&
            (sprite->y_pos < (Display::GAMEBOY_SCREEN_ROW_COUNT + Display::SPRITE_Y_OFFSET)) &&
            ((sprite->y_pos - Display::SPRITE_Y_OFFSET) <= current_row) &&
            ((sprite->y_pos - Display::SPRITE_Y_OFFSET + sprite_height) > current_row)) {
            draw_sprite[sprite_num] = true;
            draw_count++;
        } else {

            draw_sprite[sprite_num] = false;
        }

        if (draw_count == Display::MAX_SPRITES_PER_LINE) {
            /* Only allow so many sprites to be drawn on screen */
            break;
        }
    }

    /* Draw the sprites from right to left on the screen */
    while (draw_count != 0) {

        uint8_t rightmost_sprite   = 0;
        uint8_t rightmost_position = 0;

        /* Search for the rightmost sprite */
        for (uint8_t search_num = 0; search_num < Display::OAM_SPRITE_COUNT; search_num++) {
            if ((oam_data[search_num].x_pos > rightmost_position) && (draw_sprite[search_num] == true)) {
                rightmost_position = oam_data[search_num].x_pos;
                rightmost_sprite   = search_num;
            }
        }

        Display::Sprite_data* sprite = &(oam_data[rightmost_sprite]);

        /* Figure out what color pallete we're using */
        Display::Pallete* pallete = (sprite->pallete_number == 0) ? &object_pallete_0 : &object_pallete_1;

        /* Grab the correct row data for the tile */
        uint8_t tile_data[Display::TILE_DIMENSION];

        uint8_t tile_row = current_row - (sprite->y_pos - Display::SPRITE_Y_OFFSET);

        /* Y Axis flipping control */
        if (sprite->y_flip == 1) {
            int8_t range     = 8;
            int8_t mid_point = range / 2;
            int8_t distance  = range - tile_row;
            tile_row         = mid_point + distance - 1;
        }

        /* Get the pattern pointer */
        uint8_t pattern_num = sprite->pattern;
        if (lcdc.sprite_size == 1) {
            /* Tall sprite processing */

            /* Drop off the LSB */
            pattern_num &= ~LR35902::BIT_0_MASK;

            if (tile_row > Display::TILE_DIMENSION) {
                /* Since we're reading the next tile we need to reduce
                    down tile_row to properly index inside of the pattern
                */
                pattern_num++;
                tile_row -= Display::TILE_DIMENSION;
            }
        }

        uint8_t* pattern = pattern_buffer[pattern_num];

        interpret_tile(pattern, tile_row, tile_data);

        /* X Axis flipping control */
        if (sprite->x_flip == 1) {
            uint8_t flipped_data[Display::TILE_DIMENSION];

            for (uint8_t i = 0; i < Display::TILE_DIMENSION; i++) {
                flipped_data[i] = tile_data[Display::TILE_DIMENSION - i - 1];
            }

            for (uint8_t i = 0; i < Display::TILE_DIMENSION; i++) {
                tile_data[i] = flipped_data[i];
            }
        }

        /* Load the tile data onto the pattern buffer */
        for (uint8_t i = 0; i < Display::TILE_DIMENSION; i++) {
            /* Because sprites can be cut off on the screen,
                check to make sure we're not overwriting the line */
            uint8_t line_pos = i + sprite->x_pos - Display::SPRITE_X_OFFSET;
            if (line_pos < Display::GAMEBOY_SCREEN_COL_COUNT) {

                /* Some special rules for drawing sprites */

                /* Only draw if the pattern color is not zero */
                if (tile_data[i] != 0) {

                    /* If this sprite is priority mode, only draw on top
                        of background and window pixels 0
                    */
                    if (sprite->priority == 1) {
                        if (bg_window_pattern[current_row][line_pos] == 0) {
                            /* Apply the color pallete */
                            switch (tile_data[i]) {
                                case Display::COLOR_0:
                                    frame_buffer[current_row][line_pos] = pallete->color_0;
                                    break;
                                case Display::COLOR_1:
                                    frame_buffer[current_row][line_pos] = pallete->color_1;
                                    break;
                                case Display::COLOR_2:
                                    frame_buffer[current_row][line_pos] = pallete->color_2;
                                    break;
                                case Display::COLOR_3:
                                    frame_buffer[current_row][line_pos] = pallete->color_3;
                                    break;
                            }
                        }
                    } else {
                        /* In Priority 0, we can always overwrite with color */
                        switch (tile_data[i]) {
                            case Display::COLOR_0:
                                frame_buffer[current_row][line_pos] = pallete->color_0;
                                break;
                            case Display::COLOR_1:
                                frame_buffer[current_row][line_pos] = pallete->color_1;
                                break;
                            case Display::COLOR_2:
                                frame_buffer[current_row][line_pos] = pallete->color_2;
                                break;
                            case Display::COLOR_3:
                                frame_buffer[current_row][line_pos] = pallete->color_3;
                                break;
                        }
                    }
                }
            }
        }

        /* Mark this sprite as no draw and decrement the draw counter */
        draw_sprite[rightmost_sprite] = false;
        draw_count--;
    }
}

}    // namespace Display