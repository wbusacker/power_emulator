#include <picture_processing_unit.h>

namespace Display {

void Gameboy_picture_processing_unit::process_mode_1(void) {
    /* Forward all of the pixel data into the frame buffer

        Only forward one row at a time and once the mode counter
        is too high, just don't do anything
     */

    double color_mapping[4] = {GRAY_3_3, GRAY_2_3, GRAY_1_3, GRAY_0_3};

    if (mode_cycle_counter <= Display::GAMEBOY_SCREEN_ROW_COUNT) {
        uint8_t r = mode_cycle_counter - 1;
        for (uint8_t c = 0; c < Display::GAMEBOY_SCREEN_COL_COUNT; c++) {
            double color = color_mapping[frame_buffer[r][c]];

            display->set_pixel_color(r, c, color, color, color);
        }
    }

    /* If we just finished transmitting all of the rows, signal the display to draw it */
    if (mode_cycle_counter == Display::GAMEBOY_SCREEN_ROW_COUNT) {
        display->signal_frame();
    }

    if (mode_cycle_counter == Display::MODE_1_CLOCKS) {
        status.current_mode = Display::MODE_2;
        mode_transition     = true;

        /* Buffer the WINY value */
        window_y_buffered = window_y;
        window_row        = 0;
    }
}

}    // namespace Display