#ifndef GAMEBOY_PICTURE_PROCESSING_UNIT_H
#define GAMEBOY_PICTURE_PROCESSING_UNIT_H
#include <common_memory_bus.h>
#include <cpu.h>
#include <picture_processing_unit_const.h>
#include <picture_processing_unit_types_data.h>
#include <window_manager.h>

namespace Display {

class Gameboy_picture_processing_unit : public Common_memory::CMB_interface {
    public:
    Gameboy_picture_processing_unit(Common_memory::Common_memory_bus* memory,
                                    Display::Window_manager*          disp,
                                    LR35902::Gameboy_cpu*             cpu_core);
    Gameboy_picture_processing_unit();

    void cycle(void);

    void    write(uint16_t addr, uint8_t data);
    uint8_t read(uint16_t addr);

    void process_mode_0(void);
    void process_mode_1(void);
    void process_mode_2(void);
    void process_mode_3(void);

    void draw_background_line(void);
    void draw_sprites(void);
    void draw_window(void);

    void interpret_tile(uint8_t* pattern_pointer, uint8_t pattern_line, uint8_t* pixel_buffer);

    private:
    /* Specialized registers */
    struct Display::LCDC lcdc;
    struct Display::STAT status;

    uint32_t               display_cycle_counter;
    uint32_t               mode_cycle_counter;
    uint8_t                current_row;
    Display::Display_modes previous_mode;
    bool                   mode_transition;
    uint8_t                scroll_y;
    uint8_t                scroll_x;
    uint8_t                lcd_y_compare;
    uint8_t                dma_address;
    uint16_t               dma_cycle_counter;

    Display::Pallete background_window_pallete;
    Display::Pallete object_pallete_0;
    Display::Pallete object_pallete_1;

    uint8_t window_y;
    uint8_t window_y_buffered;
    uint8_t window_x;
    uint8_t window_row;

    uint8_t tile_map[Display::NUM_TILE_MAPS][Display::TILE_MAP_AXIS_LEN][Display::TILE_MAP_AXIS_LEN];
    uint8_t pattern_buffer[Display::TOTAL_PATTERN_COUNT][Display::PATTERN_SIZE_BYTES];
    uint8_t frame_buffer[Display::GAMEBOY_SCREEN_ROW_COUNT][Display::GAMEBOY_SCREEN_COL_COUNT];
    uint8_t bg_window_pattern[Display::GAMEBOY_SCREEN_ROW_COUNT][Display::GAMEBOY_SCREEN_COL_COUNT];
    struct Display::Sprite_data oam_data[Display::OAM_SPRITE_COUNT];

    Common_memory::Common_memory_bus* main_memory;
    Display::Window_manager*          display;
    LR35902::Gameboy_cpu*             cpu;
};

}    // namespace Display

#endif
