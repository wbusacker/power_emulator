#include <picture_processing_unit.h>
#include <stdio.h>

namespace Display {

Gameboy_picture_processing_unit::Gameboy_picture_processing_unit() {
    printf("Default constructor called, this is bad\n");
    fflush(stdout);
}
Gameboy_picture_processing_unit::Gameboy_picture_processing_unit(Common_memory::Common_memory_bus* memory,
                                                                 Display::Window_manager*          disp,
                                                                 LR35902::Gameboy_cpu*             cpu_core) {

    main_memory = memory;
    display     = disp;
    cpu         = cpu_core;

    /* Configure default LCDC status */
    lcdc.lcd_control                     = 1;
    lcdc.window_tile_map_select          = 0;
    lcdc.window_control                  = 0;
    lcdc.bg_window_pattern_buffer_select = 1;
    lcdc.bg_tile_map_select              = 0;
    lcdc.sprite_size                     = 0;
    lcdc.sprite_control                  = 0;
    lcdc.bg_window_control               = 1;

    status.line_y_coincidence_interrupt = 0;
    status.mode_0_interrupt             = 0;
    status.mode_1_interrupt             = 0;
    status.mode_2_interrupt             = 0;
    status.coincidence_control          = 0;
    status.current_mode                 = Display::MODE_0;

    display_cycle_counter = 0;
    mode_cycle_counter    = 0;
    current_row           = 0;
    previous_mode         = Display::MODE_0;
    mode_transition       = false;

    scroll_y      = 0;
    scroll_x      = 0;
    lcd_y_compare = 0;

    dma_address       = 0;
    dma_cycle_counter = Display::OAM_SPRITE_COUNT * Display::OAM_SPRITE_DATA_SIZE;

    window_y          = 0;
    window_y_buffered = 0;
    window_x          = 0;
    window_row        = 0;

    for (uint16_t map = 0; map < Display::NUM_TILE_MAPS; map++) {
        for (uint16_t r = 0; r < Display::TILE_MAP_AXIS_LEN; r++) {
            for (uint16_t c = 0; c < Display::TILE_MAP_AXIS_LEN; c++) {
                tile_map[map][r][c] = 0;
            }
        }
    }

    for (uint16_t pattern = 0; pattern < Display::TOTAL_PATTERN_COUNT; pattern++) {
        for (uint16_t byte = 0; byte < Display::PATTERN_SIZE_BYTES; byte++) {
            pattern_buffer[pattern][byte] = 0;
        }
    }

    for (uint16_t r = 0; r < Display::GAMEBOY_SCREEN_ROW_COUNT; r++) {
        for (uint16_t c = 0; c < Display::GAMEBOY_SCREEN_COL_COUNT; c++) {
            frame_buffer[r][c] = 0;
        }
    }

    for (uint8_t sprite = 0; sprite < Display::OAM_SPRITE_COUNT; sprite++) {
        oam_data[sprite].x_pos          = 0;
        oam_data[sprite].y_pos          = 0;
        oam_data[sprite].pattern        = 0;
        oam_data[sprite].priority       = 0;
        oam_data[sprite].x_flip         = 0;
        oam_data[sprite].y_flip         = 0;
        oam_data[sprite].pallete_number = 0;
    }
}

}    // namespace Display
