#include <picture_processing_unit.h>

namespace Display {

void Gameboy_picture_processing_unit::interpret_tile(uint8_t* pattern_pointer,
                                                     uint8_t  pattern_line,
                                                     uint8_t* pixel_buffer) {

    /* Index into the pattern to obtain the top and bottom rows */

    uint8_t bottom_byte = pattern_pointer[BYTES_PER_PATTERN_ROW * pattern_line];
    uint8_t top_byte    = pattern_pointer[(BYTES_PER_PATTERN_ROW * pattern_line) + 1];

    /* Read in the data from the two bytes and apply to the pixel buffer */
    for (uint8_t i = 0; i < Display::BITS_PER_BYTE; i++) {
        uint8_t shift_count = (Display::BITS_PER_BYTE - i) - 1;
        uint8_t color_code  = ((top_byte >> shift_count) & 0b1) << 1;
        color_code |= ((bottom_byte >> shift_count) & 0b1);

        pixel_buffer[i] = color_code;
    }
}

}    // namespace Display