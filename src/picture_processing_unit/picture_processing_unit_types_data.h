#ifndef GAMEBOY_PICTURE_PROCESSING_UNIT_TYPES_DATA_H
#define GAMEBOY_PICTURE_PROCESSING_UNIT_TYPES_DATA_H
#include <picture_processing_unit_const.h>
#include <stdint.h>

namespace Display {

enum Display_modes : uint8_t { MODE_0 = 0, MODE_1 = 1, MODE_2 = 2, MODE_3 = 3 };

struct Pallete {
    uint8_t color_0;
    uint8_t color_1;
    uint8_t color_2;
    uint8_t color_3;
};

struct LCDC {
    uint8_t lcd_control;
    uint8_t window_tile_map_select;
    uint8_t window_control;
    uint8_t bg_window_pattern_buffer_select;
    uint8_t bg_tile_map_select;
    uint8_t sprite_size;
    uint8_t sprite_control;
    uint8_t bg_window_control;
};

struct STAT {
    uint8_t                line_y_coincidence_interrupt;
    uint8_t                mode_2_interrupt;
    uint8_t                mode_1_interrupt;
    uint8_t                mode_0_interrupt;
    uint8_t                coincidence_control;
    Display::Display_modes current_mode;
};

struct Sprite_data {
    uint8_t x_pos;
    uint8_t y_pos;
    uint8_t pattern;
    uint8_t priority;
    uint8_t y_flip;
    uint8_t x_flip;
    uint8_t pallete_number;
};

}    // namespace Display

#endif
