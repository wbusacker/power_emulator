#include <cpu_const.h>
#include <picture_processing_unit.h>
#include <stdio.h>

namespace Display {

void Gameboy_picture_processing_unit::process_mode_3(void) {

    /* Only do things right at the start end of the mode */
    if (mode_cycle_counter == Display::MODE_3_CLOCKS) {

        /* Draw the background line first into the frame buffer */

        /* Only draw if BG and Window are active */
        if (lcdc.bg_window_control == 1) {
            draw_background_line();

            /* Draw if window is active */
            if (lcdc.window_control == 1) {
                draw_window();
            }
        }

        /* Draw sprites last */
        if (lcdc.sprite_control == 1) {
            draw_sprites();
        }

        status.current_mode = Display::MODE_0;
        mode_transition     = true;
    }
}

}    // namespace Display