#include <picture_processing_unit.h>

namespace Display {

void Gameboy_picture_processing_unit::process_mode_0(void) {
    /* Nothing really happens in Mode 0 */
    if (mode_cycle_counter == Display::MODE_0_CLOCKS) {
        /* Normally transition into mode 2, unless the row counter is at the end
            of the display
        */
        if (current_row == Display::GAMEBOY_SCREEN_ROW_COUNT) {
            status.current_mode = Display::MODE_1;
        } else {
            status.current_mode = Display::MODE_2;
        }

        mode_transition = true;
    }
}

}    // namespace Display