#ifndef GAMEBOY_PICTURE_PROCESSING_UNIT_CONST_H
#define GAMEBOY_PICTURE_PROCESSING_UNIT_CONST_H
#include <power_emulator_const.h>
#include <window_manager_const.h>

namespace Display {

const uint16_t MODE_0_CLOCKS = 51;
const uint16_t MODE_1_CLOCKS = 1140;
const uint16_t MODE_2_CLOCKS = 20;
const uint16_t MODE_3_CLOCKS = 43;

const uint32_t LINE_RATE_CLOCKS  = Display::MODE_0_CLOCKS + Display::MODE_2_CLOCKS + Display::MODE_3_CLOCKS;
const uint32_t FRAME_RATE_CLOCKS = MODE_1_CLOCKS + (Display::GAMEBOY_SCREEN_ROW_COUNT * Display::LINE_RATE_CLOCKS);

const double DISPLAY_FRAME_RATE = Power::POWERBOY_CLOCK_SPEED / static_cast<double>(FRAME_RATE_CLOCKS);

const uint8_t NUM_TILE_MAPS = 2;

const uint8_t  TILE_MAP_AXIS_LEN          = 32;
const uint8_t  TILE_DIMENSION             = 8;
const uint8_t  PATTERN_SIZE_BYTES         = 16;
const uint16_t PATTERN_BUFFER_GROUP_COUNT = 256;
const uint16_t PATTERN_BUFFER_GROUP_SIZE  = PATTERN_SIZE_BYTES * PATTERN_BUFFER_GROUP_COUNT;
const uint16_t TOTAL_PATTERN_COUNT        = PATTERN_BUFFER_GROUP_COUNT * 1.5;

const uint8_t OAM_SPRITE_COUNT          = 40;
const uint8_t OAM_SPRITE_DATA_SIZE      = 4;
const uint8_t OAM_X_DATA_LOCATION       = 1;
const uint8_t OAM_Y_DATA_LOCATION       = 0;
const uint8_t OAM_PATTERN_DATA_LOCATION = 2;
const uint8_t OAM_FLAGS_DATA_LOCATION   = 3;
const uint8_t SPRITE_Y_OFFSET           = 16;
const uint8_t SPRITE_X_OFFSET           = 8;
const uint8_t MAX_SPRITES_PER_LINE      = 10;
const uint8_t SHORT_SPRITE_HEIGHT       = 8;
const uint8_t TALL_SPRITE_HEIGHT        = 16;

const uint16_t TILE_MAP_0_START_ADDR = 0x9800;
const uint16_t TILE_MAP_1_START_ADDR = 0x9C00;
const uint16_t TILE_MAP_SIZE_BYTES   = TILE_MAP_AXIS_LEN * TILE_MAP_AXIS_LEN;
const uint16_t TILE_MAP_END_ADDR     = TILE_MAP_0_START_ADDR + (NUM_TILE_MAPS * TILE_MAP_SIZE_BYTES);

const uint16_t PATTERN_BUFFER_0_START_ADDR = 0x8000;
const uint16_t PATTERN_BUFFER_1_START_ADDR = 0x8800;
const uint16_t PATTERN_BUFFER_END_ADDR     = 0x9800;

const uint16_t WINDOW_X_OFFSET = 7;

const uint16_t OAM_START_ADDR = 0xFE00;
const uint16_t OAM_END_ADDR   = 0xFEA0;

const uint16_t CONTROL_REGISTER_START_ADDR = 0xFF40;
const uint16_t CONTROL_REGISTER_END_ADDR   = 0xFF4B;

const uint8_t BYTES_PER_PATTERN_ROW = 2;
const uint8_t BITS_PER_BYTE         = 8;

const uint8_t COLOR_0 = 0;
const uint8_t COLOR_1 = 1;
const uint8_t COLOR_2 = 2;
const uint8_t COLOR_3 = 3;

const double GRAY_0_3 = 0 / 3.0;
const double GRAY_1_3 = 1 / 3.0;
const double GRAY_2_3 = 2 / 3.0;
const double GRAY_3_3 = 3 / 3.0;

}    // namespace Display

#endif
