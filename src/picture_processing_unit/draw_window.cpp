#include <cpu_const.h>
#include <picture_processing_unit.h>
#include <stdio.h>

namespace Display {

void Gameboy_picture_processing_unit::draw_window(void) {

    /* Check to see if the window should be drawn */
    if (current_row >= window_y) {

        uint16_t tile_row    = window_row / Display::TILE_DIMENSION;
        uint8_t  pattern_row = window_row % Display::TILE_DIMENSION;

        /* Copy all of this row's data into a buffer */
        uint8_t row_data[Display::TILE_DIMENSION * Display::TILE_MAP_AXIS_LEN];

        for (uint8_t tile = 0; tile < Display::TILE_MAP_AXIS_LEN; tile++) {

            /* Figure out which pattern we are going into */
            uint8_t pattern_index = tile_map[lcdc.window_tile_map_select][tile_row][tile];

            /* Get the pattern buffer pointer */
            uint16_t pattern_buffer_index = 0;
            if (lcdc.bg_window_pattern_buffer_select == 0) {

                /* The second buffer index is signed 2c off of the end of the first pattern buffer
                    so cast the pattern index to a signed value and then add it to that index */
                int16_t signed_pattern_index = pattern_index;
                if (pattern_index & LR35902::BIT_7_MASK) {
                    signed_pattern_index |= LR35902::BYTE_MASK << LR35902::BITS_PER_BYTE;
                }
                pattern_buffer_index =
                    (Display::PATTERN_BUFFER_GROUP_SIZE / Display::PATTERN_SIZE_BYTES) + signed_pattern_index;

            } else {
                /* Normal indexing off of the start */
                pattern_buffer_index = pattern_index;
            }

            /* Get the pattern pointer */
            uint8_t* pattern = pattern_buffer[pattern_buffer_index];

            interpret_tile(pattern, pattern_row, &row_data[tile * Display::TILE_DIMENSION]);
        }

        /* Now that we have the entire row's data, place the correct offset into the frame buffer */
        for (uint8_t i = 0; i < Display::GAMEBOY_SCREEN_COL_COUNT; i++) {

            uint8_t row_position = i;

            if (row_position < Display::GAMEBOY_SCREEN_COL_COUNT) {
                bg_window_pattern[current_row][row_position] = row_data[i];

                /* Apply the color pallete */
                switch (bg_window_pattern[current_row][row_position]) {
                    case Display::COLOR_0:
                        frame_buffer[current_row][row_position] = background_window_pallete.color_0;
                        break;
                    case Display::COLOR_1:
                        frame_buffer[current_row][row_position] = background_window_pallete.color_1;
                        break;
                    case Display::COLOR_2:
                        frame_buffer[current_row][row_position] = background_window_pallete.color_2;
                        break;
                    case Display::COLOR_3:
                        frame_buffer[current_row][row_position] = background_window_pallete.color_3;
                        break;
                }
            }
        }

        /* Increment the window row counter */
        window_row++;
    }
}

}    // namespace Display