#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <common_memory_bus.h>
#include <controller_const.h>
#include <controller_types_data.h>
#include <cpu.h>
#include <window_manager_types_data.h>

namespace Input {

class Controller : public Common_memory::CMB_interface, public Display::Keyboard_input_interface {
    public:
    Controller(LR35902::Gameboy_cpu* processor);

    void    cycle(void);
    uint8_t read(uint16_t addr);
    void    write(uint16_t addr, uint8_t data);

    void keyboard_input(const struct Display::Keyboard_event event);

    private:
    bool read_d_pad;
    bool read_buttons;

    struct Buttons current_buttons;
    struct Buttons previous_buttons;

    LR35902::Gameboy_cpu* cpu;
};

}    // namespace Input

#endif
