#include <GLFW/glfw3.h>
#include <controller.h>
#include <stdio.h>
namespace Input {

void Controller::keyboard_input(const struct Display::Keyboard_event event) {

    switch (event.key) {
        case GLFW_KEY_W:
            current_buttons.up = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_A:
            current_buttons.left = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_S:
            current_buttons.down = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_D:
            current_buttons.right = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_J:
            current_buttons.a = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_K:
            current_buttons.b = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_U:
            current_buttons.select = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
        case GLFW_KEY_I:
            current_buttons.start = (event.action == GLFW_RELEASE) ? Input::LIFTED : Input::PRESSED;
            break;
    }
}

}    // namespace Input
