#include <controller.h>

namespace Input {

void Controller::cycle(void) {

    /* Check if any of the current buttons went from lifted to pressed */
    bool new_button_pressed = false;

    if ((current_buttons.a == Input::PRESSED) && (previous_buttons.a == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.b == Input::PRESSED) && (previous_buttons.b == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.select == Input::PRESSED) && (previous_buttons.select == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.start == Input::PRESSED) && (previous_buttons.start == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.up == Input::PRESSED) && (previous_buttons.up == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.down == Input::PRESSED) && (previous_buttons.down == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.left == Input::PRESSED) && (previous_buttons.left == Input::LIFTED)) {
        new_button_pressed = true;
    }
    if ((current_buttons.right == Input::PRESSED) && (previous_buttons.right == Input::LIFTED)) {
        new_button_pressed = true;
    }

    /* Raise the interrupt */
    if (new_button_pressed == true) {
        cpu->raise_interrupt(LR35902::CONTROLLER_INTERRUPT);
    }

    /* Copy current buttons to previous buttons */
    previous_buttons.a      = current_buttons.a;
    previous_buttons.b      = current_buttons.b;
    previous_buttons.select = current_buttons.select;
    previous_buttons.start  = current_buttons.start;
    previous_buttons.up     = current_buttons.up;
    previous_buttons.down   = current_buttons.down;
    previous_buttons.left   = current_buttons.left;
    previous_buttons.right  = current_buttons.right;
}

}    // namespace Input