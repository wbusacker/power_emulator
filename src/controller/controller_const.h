#ifndef CONTROLLER_CONST_H
#define CONTROLLER_CONST_H

#include <stdint.h>

namespace Input {

const uint8_t JOYPAD_RIGHT_IN  = 1 << 0;
const uint8_t JOYPAD_A_IN      = 1 << 0;
const uint8_t JOYPAD_LEFT_IN   = 1 << 1;
const uint8_t JOYPAD_B_IN      = 1 << 1;
const uint8_t JOYPAD_UP_IN     = 1 << 2;
const uint8_t JOYPAD_SELECT_IN = 1 << 2;
const uint8_t JOYPAD_DOWN_IN   = 1 << 3;
const uint8_t JOYPAD_START_IN  = 1 << 3;

const uint8_t JOYPAD_READ_DPAD    = 1 << 4;
const uint8_t JOYPAD_READ_BUTTONS = 1 << 5;

enum Button_state { PRESSED, LIFTED };

const uint16_t JOYPAD_REGISTER_ADDR = 0xFF00;

}    // namespace Input

#endif
