#include <controller.h>

namespace Input {

uint8_t Controller::read(uint16_t addr) {

    /* Figure out what this should be set to */
    uint8_t joypad_state = 0;

    if (read_d_pad) {
        joypad_state |= (current_buttons.right == Input::LIFTED) ? JOYPAD_RIGHT_IN : 0;
        joypad_state |= (current_buttons.left == Input::LIFTED) ? JOYPAD_LEFT_IN : 0;
        joypad_state |= (current_buttons.up == Input::LIFTED) ? JOYPAD_UP_IN : 0;
        joypad_state |= (current_buttons.down == Input::LIFTED) ? JOYPAD_DOWN_IN : 0;
    } else {
        joypad_state |= Input::JOYPAD_READ_DPAD;
    }

    if (read_buttons) {
        joypad_state |= (current_buttons.a == Input::LIFTED) ? JOYPAD_A_IN : 0;
        joypad_state |= (current_buttons.b == Input::LIFTED) ? JOYPAD_B_IN : 0;
        joypad_state |= (current_buttons.select == Input::LIFTED) ? JOYPAD_SELECT_IN : 0;
        joypad_state |= (current_buttons.start == Input::LIFTED) ? JOYPAD_START_IN : 0;
    } else {
        joypad_state |= Input::JOYPAD_READ_BUTTONS;
    }

    return joypad_state;
}

void Controller::write(uint16_t addr, uint8_t data) {

    /* Only allow these two bits to be written to */

    read_buttons = ((data & Input::JOYPAD_READ_BUTTONS) != 0) ? false : true;
    read_d_pad   = ((data & Input::JOYPAD_READ_DPAD) != 0) ? false : true;
}

}    // namespace Input