#include <controller.h>
namespace Input {

Controller::Controller(LR35902::Gameboy_cpu* processor) {

    cpu = processor;

    read_d_pad             = false;
    read_buttons           = false;
    current_buttons.up     = Input::LIFTED;
    current_buttons.down   = Input::LIFTED;
    current_buttons.left   = Input::LIFTED;
    current_buttons.right  = Input::LIFTED;
    current_buttons.a      = Input::LIFTED;
    current_buttons.b      = Input::LIFTED;
    current_buttons.start  = Input::LIFTED;
    current_buttons.select = Input::LIFTED;
}

}    // namespace Input
