#ifndef CONTROLLER_TYPES_DATA_H
#define CONTROLLER_TYPES_DATA_H
#include <controller_const.h>

namespace Input {

struct Buttons {
    Button_state up;
    Button_state down;
    Button_state right;
    Button_state left;
    Button_state start;
    Button_state select;
    Button_state a;
    Button_state b;
};

}    // namespace Input

#endif
