#ifndef GAMEBOY_CARTRIDGE_H
#define GAMEBOY_CARTRIDGE_H
#include <cartridge_const.h>
#include <cartridge_types_data.h>
#include <common_memory_bus_types_data.h>

namespace Cart {

class Gameboy_cartridge : public Common_memory::CMB_interface {
    public:
    Gameboy_cartridge(const char* const file_name);
    virtual ~Gameboy_cartridge(void);

    Gameboy::Cart_types get_cart_type(void) const { return cartridge_type; }
    Gameboy::Rom_sizes  get_rom_size(void) const { return rom_size; }
    Gameboy::Ram_sizes  get_ram_size(void) const { return ram_size; }

    virtual uint8_t read(uint16_t addr);
    virtual void    write(uint16_t addr, uint8_t data);

    void override_dependencies(Gameboy::Memory_controller* cart) { cartridge_controller = cart; }

    bool get_load_status(void) { return successful_load; }

    Gameboy::Memory_controller* get_controller(void) { return cartridge_controller; }

    private:
    Gameboy::Cart_types cartridge_type;
    Gameboy::Rom_sizes  rom_size;
    Gameboy::Ram_sizes  ram_size;

    /* Cartridge specific memory spaces */
    uint8_t** rom_banks;
    uint8_t** swap_ram;

    Gameboy::Memory_controller* cartridge_controller;

    bool successful_load;

    bool save_ram;
    char save_file[Gameboy::MAX_FILENAME_LENGTH];

    void clean_pointers(void);
};

}    // namespace Cart

#endif
