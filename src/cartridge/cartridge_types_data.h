#ifndef GAMEBOY_CARTRIDGE_TYPES_DATA_H
#define GAMEBOY_CARTRIDGE_TYPES_DATA_H
#include <cartridge_const.h>
#include <stdio.h>

namespace Cart {

namespace Gameboy {

enum Cart_types : uint8_t {
    ROM_ONLY                  = 0x00,
    ROM_MBC1                  = 0x01,
    ROM_MBC1_RAM              = 0x02,
    ROM_MBC1_RAM_BATT         = 0x03,
    ROM_MBC2                  = 0x05,
    ROM_MBC2_BATT             = 0x06,
    ROM_RAM                   = 0x08,
    ROM_RAM_BATT              = 0x09,
    ROM_MMMO1                 = 0x0B,
    ROM_MMMO1_SRAM            = 0x0C,
    ROM_MMMO2_SRAM_BATT       = 0x0D,
    ROM_MBC3_TIMER_BATT       = 0x0F,
    ROM_MBC3_TIMER_RAM_BATT   = 0x10,
    ROM_MBC3                  = 0x11,
    ROM_MBC3_RAM              = 0x12,
    ROM_MBC3_RAM_BATT         = 0x13,
    ROM_MBC5                  = 0x19,
    ROM_MBC5_RAM              = 0x1A,
    ROM_MBC5_RAM_BATT         = 0x1B,
    ROM_MBC5_RUMBLE           = 0x1C,
    ROM_MBC5_RUMBLE_SRAM      = 0x1D,
    ROM_MBC5_RUMBLE_SRAM_BATT = 0x1E,
    POCKET_CAMERA             = 0x1F,
    TAMA5                     = 0xFD,
    HUC3                      = 0xFE,
    HUC1                      = 0xFF
};

enum Rom_size_types : uint8_t {
    TYPE_ROM_BANK2   = 0,
    TYPE_ROM_BANK4   = 1,
    TYPE_ROM_BANK8   = 2,
    TYPE_ROM_BANK16  = 3,
    TYPE_ROM_BANK32  = 4,
    TYPE_ROM_BANK64  = 5,
    TYPE_ROM_BANK128 = 6,
    TYPE_ROM_BANK72  = 0x52,
    TYPE_ROM_BANK80  = 0x53,
    TYPE_ROM_BANK96  = 0x54
};

/* Number of banks available to ROM */
enum Rom_sizes {
    ROM_BANK2   = 2,
    ROM_BANK4   = 4,
    ROM_BANK8   = 8,
    ROM_BANK16  = 16,
    ROM_BANK32  = 32,
    ROM_BANK64  = 64,
    ROM_BANK128 = 128,
    ROM_BANK72  = 72,
    ROM_BANK80  = 80,
    ROM_BANK96  = 96,
    INVALID_ROM = -1
};

enum Ram_size_types : uint8_t {
    TYPE_RAM_BANK0       = 0,
    TYPE_RAM_BANK1_SMALL = 1,
    TYPE_RAM_BANK1_LARGE = 2,
    TYPE_RAM_BANK4       = 3,
    TYPE_RAM_BANK16      = 4
};

enum Ram_sizes {
    RAM_BANK0       = 0,
    RAM_BANK1_SMALL = 1,
    RAM_BANK1_LARGE = 1,
    RAM_BANK4       = 4,
    RAM_BANK16      = 16,
    INVALID_RAM     = -1

};

class Memory_controller {
    public:
    Memory_controller();
    virtual ~Memory_controller() { }

    virtual uint8_t get_active_rom_bank(void);
    virtual uint8_t get_active_ram_bank(void);
    virtual bool    is_ram_enabled(void);

    void set_rom_bank(uint8_t index) { swap_rom_index = index; }

    void set_ram_bank(uint8_t index) { swap_ram_index = index; }

    virtual void write_controller(uint16_t addr, uint8_t val);

    protected:
    uint8_t swap_rom_index;
    uint8_t swap_ram_index;
    bool    ram_enabled;
};

}    // namespace Gameboy

}    // namespace Cart

#endif
