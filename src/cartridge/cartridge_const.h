#ifndef GAMEBOY_CARTRIDGE_CONST_H
#define GAMEBOY_CARTRIDGE_CONST_H

#include <stdint.h>

namespace Cart {

namespace Gameboy {

const uint16_t CARTRIDGE_ROM_START = 0x0000;
const uint16_t CARTRIDGE_ROM_END   = 0x8000;
const uint16_t CARTRIDGE_RAM_START = 0xA000;
const uint16_t CARTRIDGE_RAM_END   = 0xC000;

const uint16_t ROM_BANK_SIZE_BYTES  = 0x4000;
const uint16_t ROM_BANK_SWAP_ADDR   = 0x4000;
const uint16_t ROM_BANK_STATIC_ADDR = 0x0000;

const uint16_t RAM_BANK_SIZE_BYTES       = 0x2000;
const uint16_t RAM_BANK_SMALL_SIZE_BYTES = 0x0800;
const uint16_t RAM_BANK_SWAP_ADDR        = 0xA000;
const uint16_t RAM_MIRROR_SWAP_ADDR      = 0xC000;

const uint16_t BOOTSTRAP_JUMP_ADDR = 0x0100;
const uint16_t CARTRIDGE_TYPE_ADDR = 0x0147;
const uint16_t ROM_SIZE_ADDR       = 0x0148;
const uint16_t RAM_SIZE_ADDR       = 0x0149;
const uint16_t GAME_NAME_ADDR      = 0x0134;
const uint16_t GAME_NAME_LENGTH    = 16;

const uint16_t CONTROLLER_RAM_ENABLE_LOWER_BOUND = 0x0000;
const uint16_t CONTROLLER_RAM_ENABLE_UPPER_BOUND = 0x2000;
const uint16_t CONTROLLER_ROM_SWAP_LOWER_BOUND   = CONTROLLER_RAM_ENABLE_UPPER_BOUND;
const uint16_t CONTROLLER_ROM_SWAP_UPPER_BOUND   = 0x4000;
const uint16_t CONTROLLER_RAM_SWAP_LOWER_BOUND   = CONTROLLER_ROM_SWAP_UPPER_BOUND;
const uint16_t CONTROLLER_RAM_SWAP_UPPER_BOUND   = 0x6000;
const uint16_t CONTROLLER_SWAP_MODE_LOWER_BOUND  = CONTROLLER_RAM_SWAP_UPPER_BOUND;
const uint16_t CONTROLLER_SWAP_MODE_UPPER_BOUND  = 0x8000;

const uint8_t RAM_ENABLE_MASK  = 0b1111;
const uint8_t ROM_SWAP_MASK    = 0b11111;
const uint8_t RAM_SWAP_MASK    = 0b11;
const uint8_t SWAP_MODE_MASK   = 0b1;
const uint8_t RAM_ENABLE_VALUE = 0x0A;
const uint8_t ROM_SWAP_BITS    = 5;

const uint8_t     SHA_512_DIGEST_LEN_BYTES                     = 64;
const uint8_t     SHA_512_DIGEST_LEN_HEX_CHAR                  = SHA_512_DIGEST_LEN_BYTES * 2;
const uint8_t     BYTE_HEX_STR_LEN                             = 3;
const uint16_t    RAM_SAVE_SPACE_HOLDING_DIRECTORY_PERMISSIONS = 0744;
const char* const RAM_SAVE_SPACE_HOLDING_DIRECTORY             = "%s/.power_emulator";
const char* const RAM_SAVE_SPACE_DIRECTORY_FORMAT              = "%s/.power_emulator/%s.per";
const uint16_t    MAX_FILENAME_LENGTH                          = 256;

}    // namespace Gameboy

}    // namespace Cart

#endif
