#include <cartridge_types_data.h>

namespace Cart {

namespace Gameboy {

Memory_controller::Memory_controller() {
    swap_rom_index = 1;
    swap_ram_index = 0;
    ram_enabled    = true;
}

uint8_t Memory_controller::get_active_rom_bank(void) {
    return swap_rom_index;
}
uint8_t Memory_controller::get_active_ram_bank(void) {
    return swap_ram_index;
}
bool Memory_controller::is_ram_enabled(void) {
    return ram_enabled;
}
void Memory_controller::write_controller(uint16_t addr, uint8_t val) {
    return;
}

}    // namespace Gameboy

}    // namespace Cart