#include <cartridge.h>
#include <errno.h>
#include <iostream>
#include <mbc1.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace Cart {

Gameboy_cartridge::Gameboy_cartridge(const char* const file_name) {

    /* Assume we were able to initialize correctly,
        and let further down functions assert false
    */
    successful_load = true;
    save_ram        = false;

    /* Grab the very basic cartridge data */
    FILE* fp = fopen(file_name, "rb");

    if (fp == NULL) {

        perror("Failed to open Gameboy Cartridge");
        successful_load = false;

        cartridge_controller = nullptr;
        rom_banks            = nullptr;
        swap_ram             = nullptr;

    } else {

        /* Get the number of ROM banks */
        Gameboy::Rom_size_types rom_size_type;
        fseek(fp, Gameboy::ROM_SIZE_ADDR, 0);
        fread(&rom_size_type, 1, 1, fp);

        /* Only implement MBC1 combinations right now */
        switch (rom_size_type) {
            case Gameboy::TYPE_ROM_BANK2:
                rom_size = Gameboy::ROM_BANK2;
                break;
            case Gameboy::TYPE_ROM_BANK4:
                rom_size = Gameboy::ROM_BANK4;
                break;
            case Gameboy::TYPE_ROM_BANK32:
                rom_size = Gameboy::ROM_BANK32;
                break;
            case Gameboy::TYPE_ROM_BANK128:
                rom_size = Gameboy::ROM_BANK128;
                break;
            default:
                rom_size = Gameboy::INVALID_ROM;
        }

        /* Get the number of ROM banks */
        Gameboy::Ram_size_types ram_size_type;
        fseek(fp, Gameboy::RAM_SIZE_ADDR, 0);
        fread(&ram_size_type, 1, 1, fp);

        /* Only implement MBC1 combinations right now */
        switch (ram_size_type) {
            case Gameboy::TYPE_RAM_BANK0:
                ram_size = Gameboy::RAM_BANK0;
                break;
            case Gameboy::TYPE_RAM_BANK1_SMALL:
                ram_size = Gameboy::RAM_BANK1_SMALL;
                break;
            case Gameboy::TYPE_RAM_BANK1_LARGE:
                ram_size = Gameboy::RAM_BANK1_LARGE;
                break;
            case Gameboy::TYPE_RAM_BANK4:
                ram_size = Gameboy::RAM_BANK4;
                break;
            case Gameboy::TYPE_RAM_BANK16:
                ram_size = Gameboy::RAM_BANK16;
                break;
            default:
                ram_size = Gameboy::INVALID_RAM;
        }

        /* Get the cartridge type */
        fseek(fp, Gameboy::CARTRIDGE_TYPE_ADDR, 0);
        fread(&cartridge_type, 1, 1, fp);
        /* Initialize the memory controller */
        switch (cartridge_type) {
            case Gameboy::ROM_MBC1:
            case Gameboy::ROM_MBC1_RAM:
            case Gameboy::ROM_MBC1_RAM_BATT:
                save_ram             = true;
                cartridge_controller = new Cart::MBC1();
                break;
            default:
                printf("Unsuported cartridge type %d\n", cartridge_type);
                cartridge_controller = nullptr;
        }

        /* Only finish initialization if the RAM/ROM config makes sense */
        if ((ram_size != Gameboy::INVALID_RAM) && (rom_size != Gameboy::INVALID_ROM)) {

            /* Create memory spaces dependant on the cartridge */
            rom_banks = new uint8_t*[rom_size];

            for (uint16_t i = 0; i < rom_size; i++) {
                rom_banks[i] = new uint8_t[Gameboy::ROM_BANK_SIZE_BYTES];
                memset(rom_banks[i], 0, Gameboy::ROM_BANK_SIZE_BYTES);
            }

            swap_ram = new uint8_t*[ram_size];

            for (uint16_t i = 0; i < ram_size; i++) {
                /* Even if the cartridge only supported smaller ram size, use the larger blocks anyways */
                swap_ram[i] = new uint8_t[Gameboy::RAM_BANK_SIZE_BYTES];
                memset(swap_ram[i], 0, Gameboy::RAM_BANK_SIZE_BYTES);
            }

            /* With the memory banks created, fill them! */
            fseek(fp, 0, 0);
            for (uint16_t i = 0; i < rom_size; i++) {
                if (fread(rom_banks[i], 1, Gameboy::ROM_BANK_SIZE_BYTES, fp) != Gameboy::ROM_BANK_SIZE_BYTES) {
                    printf("Failed load a ROM bank\n");
                    successful_load = false;
                    break;
                }
            }

            if (save_ram) {

                sprintf(save_file, Gameboy::RAM_SAVE_SPACE_HOLDING_DIRECTORY, getenv("HOME"));

                /* Ensure that the holding directory exists */
                struct stat dir_status = {0};
                if (stat(save_file, &dir_status) == -1) {
                    printf("Making directory\n");
                    int ret_val = mkdir(save_file, Gameboy::RAM_SAVE_SPACE_HOLDING_DIRECTORY_PERMISSIONS);
                    if (ret_val != 0) {
                        perror("Failed to create directory");
                    }
                }

                /* Generate the save file name */
                SHA512_CTX context;
                SHA512_Init(&context);
                uint64_t file_len    = Gameboy::ROM_BANK_SIZE_BYTES * rom_size;
                uint8_t* hash_buffer = new uint8_t[file_len];
                fseek(fp, 0, 0);
                fread(hash_buffer, 1, file_len, fp);
                SHA512_Update(&context, hash_buffer, file_len);

                uint8_t digest[Gameboy::SHA_512_DIGEST_LEN_BYTES];
                SHA512_Final(digest, &context);

                std::string digest_hex("");

                for (uint8_t i = 0; i < Gameboy::SHA_512_DIGEST_LEN_BYTES; i++) {
                    char temp[Gameboy::BYTE_HEX_STR_LEN];
                    sprintf(temp, "%02X", digest[i]);
                    digest_hex.append(temp);
                }

                sprintf(save_file, Gameboy::RAM_SAVE_SPACE_DIRECTORY_FORMAT, getenv("HOME"), digest_hex.c_str());

                printf("Loading save file %s\n", save_file);

                FILE* ram_fp = fopen(save_file, "r");

                if (ram_fp != NULL) {
                    for (uint16_t i = 0; i < ram_size; i++) {
                        if (fread(swap_ram[i], 1, Gameboy::RAM_BANK_SIZE_BYTES, ram_fp) !=
                            Gameboy::RAM_BANK_SIZE_BYTES) {
                            printf("Failed to load a RAM Bank\n");
                            successful_load = false;
                            break;
                        }
                    }
                    fclose(ram_fp);
                } else {
                    printf("No existing save file\n");
                }
            }

        } else {
            rom_banks       = nullptr;
            swap_ram        = nullptr;
            successful_load = false;
        }
    }

    fclose(fp);

    if (successful_load == false) {
        printf("Warning, Cartridge failed to load\n");
    }
}

Gameboy_cartridge::~Gameboy_cartridge(void) {

    if (save_ram) {
        FILE* ram_fp = fopen(save_file, "w");
        if (ram_fp != NULL) {
            /* Dump the contents of the RAM into the save file */
            for (uint16_t i = 0; i < ram_size; i++) {
                if (fwrite(swap_ram[i], 1, Gameboy::RAM_BANK_SIZE_BYTES, ram_fp) != Gameboy::RAM_BANK_SIZE_BYTES) {
                    printf("Failed to load a RAM Bank\n");
                    successful_load = false;
                    break;
                }
            }
            fclose(ram_fp);
        } else {
            printf("Failed to save RAM file\n");
            perror("Errno");
        }
    }

    if (successful_load) {
        clean_pointers();
    }
}

void Gameboy_cartridge::clean_pointers(void) {

    /* Get rid of everything in the order it was created */
    if (cartridge_controller != nullptr) {
        delete cartridge_controller;
    }

    if (rom_banks != nullptr) {
        for (uint16_t i = 0; i < rom_size; i++) {
            delete[] rom_banks[i];
            rom_banks[i] = nullptr;
        }
        delete[] rom_banks;
        rom_banks = nullptr;
    }

    if (swap_ram != nullptr) {
        for (uint16_t i = 0; i < ram_size; i++) {
            delete[] swap_ram[i];
            swap_ram[i] = nullptr;
        }
        delete[] swap_ram;
        swap_ram = nullptr;
    }
}

}    // namespace Cart
