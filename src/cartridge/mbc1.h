#ifndef MBC_1_H
#define MBC_1_H

#include <cartridge_types_data.h>

namespace Cart {

class MBC1 : public Gameboy::Memory_controller {

    public:
    MBC1();
    ~MBC1();
    virtual void write_controller(uint16_t addr, uint8_t val);

    private:
    bool swappable_ram;

    uint8_t rom_swap_reg;
    uint8_t ram_swap_reg;
};

}    // namespace Cart

#endif