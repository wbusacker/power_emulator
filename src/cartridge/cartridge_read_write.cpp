#include <cartridge.h>

namespace Cart {

uint8_t Gameboy_cartridge::read(uint16_t addr) {

    uint8_t memory_data = 0;

    /* Do different things if we're accessing the different memory regions */
    if ((Gameboy::ROM_BANK_STATIC_ADDR <= addr) && (addr < Gameboy::ROM_BANK_SWAP_ADDR)) {

        /* We're accessing Bank 0, so just go grab that value */
        memory_data = rom_banks[0][addr % Gameboy::ROM_BANK_SIZE_BYTES];

    } else if ((Gameboy::ROM_BANK_SWAP_ADDR <= addr) &&
               (addr < (Gameboy::ROM_BANK_SWAP_ADDR + Gameboy::ROM_BANK_SIZE_BYTES))) {

        /* Figure out which ROM bank we're accessing */
        uint8_t active_rom_bank = cartridge_controller->get_active_rom_bank();
        memory_data             = rom_banks[active_rom_bank][addr % Gameboy::ROM_BANK_SIZE_BYTES];

    } else if (cartridge_controller->is_ram_enabled() &&
               (((Gameboy::RAM_BANK_SWAP_ADDR <= addr) &&
                 (addr < (Gameboy::RAM_BANK_SWAP_ADDR + Gameboy::RAM_BANK_SIZE_BYTES))) ||
                ((Gameboy::RAM_MIRROR_SWAP_ADDR <= addr) &&
                 (addr < (Gameboy::RAM_MIRROR_SWAP_ADDR + Gameboy::RAM_BANK_SIZE_BYTES))))) {

        memory_data = swap_ram[cartridge_controller->get_active_ram_bank()][addr % Gameboy::RAM_BANK_SIZE_BYTES];
    }

    return memory_data;
}

void Gameboy_cartridge::write(uint16_t addr, uint8_t data) {
    /* If we're in the ROM space, have the controller deal with things */
    if ((Gameboy::ROM_BANK_STATIC_ADDR <= addr) &&
        (addr < (Gameboy::ROM_BANK_SWAP_ADDR + Gameboy::ROM_BANK_SIZE_BYTES))) {

        cartridge_controller->write_controller(addr, data);

    } else if (cartridge_controller->is_ram_enabled() &&
               (((Gameboy::RAM_BANK_SWAP_ADDR <= addr) &&
                 (addr < (Gameboy::RAM_BANK_SWAP_ADDR + Gameboy::RAM_BANK_SIZE_BYTES))) ||
                ((Gameboy::RAM_MIRROR_SWAP_ADDR <= addr) &&
                 (addr < (Gameboy::RAM_MIRROR_SWAP_ADDR + Gameboy::RAM_BANK_SIZE_BYTES))))) {

        uint8_t active_ram_bank = cartridge_controller->get_active_ram_bank();

        swap_ram[active_ram_bank][addr % Gameboy::RAM_BANK_SIZE_BYTES] = data;
    } else {
        printf("Warning: Write to swap ram when its disabled\n");
        fflush(stdout);
    }
}

}    // namespace Cart