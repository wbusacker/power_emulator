#include <mbc1.h>
#include <stdio.h>

namespace Cart {

MBC1::MBC1() {
    swappable_ram = false;

    rom_swap_reg = 0;
    ram_swap_reg = 0;
}

MBC1::~MBC1(void) {
}

}    // namespace Cart