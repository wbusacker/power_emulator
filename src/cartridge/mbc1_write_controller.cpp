#include <mbc1.h>
#include <stdio.h>

namespace Cart {

void MBC1::write_controller(uint16_t addr, uint8_t val) {

    if ((Gameboy::CONTROLLER_RAM_ENABLE_LOWER_BOUND <= addr) && (addr < Gameboy::CONTROLLER_RAM_ENABLE_UPPER_BOUND)) {
        /* Mask off the value and check if it matches the enable value */
        if (Gameboy::RAM_ENABLE_VALUE == (val & Gameboy::RAM_ENABLE_MASK)) {
            ram_enabled = true;
        } else {
            ram_enabled = false;
        }
    } else if ((Gameboy::CONTROLLER_ROM_SWAP_LOWER_BOUND <= addr) &&
               (addr < Gameboy::CONTROLLER_ROM_SWAP_UPPER_BOUND)) {

        rom_swap_reg = val & Gameboy::ROM_SWAP_MASK;

    } else if ((Gameboy::CONTROLLER_RAM_SWAP_LOWER_BOUND <= addr) &&
               (addr < Gameboy::CONTROLLER_RAM_SWAP_UPPER_BOUND)) {

        ram_swap_reg = val & Gameboy::RAM_SWAP_MASK;
        printf("Changing RAM/Upper Rom Selection %04X %02X\n", addr, val);
    } else if ((Gameboy::CONTROLLER_SWAP_MODE_LOWER_BOUND <= addr) &&
               (addr < Gameboy::CONTROLLER_SWAP_MODE_UPPER_BOUND)) {

        printf("Changing Swap Mode %04X %02X\n", addr, val);
        if ((val & Gameboy::SWAP_MODE_MASK) == 0) {
            swappable_ram = false;
        } else {
            swappable_ram = true;
        }
    }

    /* No matter what, whenever the controller is written to, recalculate bank indexes */

    /* Can always apply these bits to the index */
    swap_rom_index = rom_swap_reg;

    if (swappable_ram == false) {
        /* If RAM swap is off, always index zero */
        swap_ram_index = 0;

        /* Shift the bits up and apply to rom index */
        swap_rom_index |= (ram_swap_reg << Gameboy::ROM_SWAP_BITS);
    } else {
        swap_ram_index = ram_swap_reg;
    }

    /* Swap Rom cannot be zero */
    if (swap_rom_index == 0) {
        swap_rom_index = 1;
    }

    if (swap_rom_index > 31) {
        printf("Warning! Exceeded ROM Bank range %02X ram swap reg %02X rom swap reg\n", ram_swap_reg, rom_swap_reg);
        while (1)
            ;
    }
}

}    // namespace Cart