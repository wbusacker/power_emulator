#include <cpu_instructions_groups.h>

namespace LR35902 {

void bit_reset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    /* Figure out which bit we are setting */
    uint8_t bit_index = (instruction >> 3) & 0x7;

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        arg_variable = get_hl_indirect(memory, registers);
        registers->cycle_time += LR35902::CB_INDIRECT_ADDITIONAL_CYCLES;
    } else {
        arg_variable = *arg_source;
    }

    arg_variable &= ~(1 << bit_index);

    /* Put the value back */
    if (arg_source == nullptr) {
        store_hl_indirect(memory, registers, arg_variable);
    } else {
        *arg_source = arg_variable;
    }
}

}    // namespace LR35902