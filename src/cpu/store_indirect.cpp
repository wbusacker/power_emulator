#include <cpu_instructions_groups.h>

namespace LR35902 {

void store_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out the address to use */
    uint16_t indirect_address = 0;

    /* Figure out where to put them */
    switch (instruction) {
        case LD_IBC_A:
            indirect_address = (static_cast<uint16_t>(registers->B) << BITS_PER_BYTE) | registers->C;
            break;
        case LD_IDE_A:
            indirect_address = (static_cast<uint16_t>(registers->D) << BITS_PER_BYTE) | registers->E;
            break;
        case LD_IHLP_A:
            indirect_address = (static_cast<uint16_t>(registers->H) << BITS_PER_BYTE) | registers->L;

            /* Increment HL */
            registers->H = (indirect_address + 1) >> BITS_PER_BYTE;
            registers->L = (indirect_address + 1) & BYTE_MASK;
            break;
        case LD_IHLS_A:
            indirect_address = (static_cast<uint16_t>(registers->H) << BITS_PER_BYTE) | registers->L;

            /* Decrement HL */
            registers->H = (indirect_address - 1) >> BITS_PER_BYTE;
            registers->L = (indirect_address - 1) & BYTE_MASK;
            break;

        case LD_INN_A:
            uint8_t indirect_l = get_next_pc(memory, registers);
            uint8_t indirect_h = get_next_pc(memory, registers);
            indirect_address   = (static_cast<uint16_t>(indirect_h) << BITS_PER_BYTE) | indirect_l;
            break;
    }

    /* Grab the value and assign it into A */
    memory->write(indirect_address, registers->A);
}

}    // namespace LR35902