#include <cpu.h>

namespace LR35902 {

Gameboy_cpu::Gameboy_cpu(Common_memory::Common_memory_bus* main_bus) : memory(main_bus) {

    master_interrupt_flag = true;
    cpu_active_flag       = true;
    last_instruction      = 0;
    last_address          = 0;

    interrupt_enable_flags  = 0;
    interrupt_pending_flags = 0;

    cycles = 0;

    // registers.flags.zero       = true;
    // registers.flags.subtract   = false;
    // registers.flags.half_carry = true;
    // registers.flags.carry      = true;
    // registers.flags.paddinging = 0;
    // registers.A                = 0x11;
    // registers.B                = 0x00;
    // registers.C                = 0x13;
    // registers.D                = 0x00;
    // registers.E                = 0xD8;
    // registers.H                = 0x01;
    // registers.L                = 0x4D;
    // registers.program_counter  = 0x0100; /* Bypass the bootloader for now */
    // registers.stack_pointer    = 0xFFFE;
    // registers.cb_mode          = false;
    // registers.cycle_time       = 0;

    registers.flags.zero       = true;
    registers.flags.subtract   = false;
    registers.flags.half_carry = false;
    registers.flags.carry      = false;
    registers.flags.paddinging = 0;
    registers.A                = 0x01;
    registers.B                = 0x00;
    registers.C                = 0x00;
    registers.D                = 0xFF;
    registers.E                = 0x56;
    registers.H                = 0x00;
    registers.L                = 0x0D;
    registers.program_counter  = 0x0100; /* Bypass the bootloader for now */
    registers.stack_pointer    = 0xFFFE;
    registers.cb_mode          = false;
    registers.cycle_time       = 0;

    function_tracer = nullptr;

    /* Force all main instructions to nullptr */
    for (uint16_t i = 0; i <= RST_38; i++) {
        main_map[i] = nullptr;
    }

    /* Setup the instruction mapping */
    /* 0x */
    main_map[NOP]       = nullptr;
    main_map[LD_BC_NN]  = LR35902::load_16_immediate;
    main_map[LD_IBC_A]  = LR35902::store_indirect;
    main_map[INC_BC]    = LR35902::increment_16;
    main_map[INC_B]     = LR35902::increment;
    main_map[DEC_B]     = LR35902::decrement;
    main_map[LD_B_N]    = LR35902::load_immediate;
    main_map[RLCA]      = LR35902::rotate_left_carry;
    main_map[LD_INN_SP] = LR35902::store_stack_pointer;
    main_map[ADD_HL_BC] = LR35902::add_hl;
    main_map[LD_A_IBC]  = LR35902::load_indirect;
    main_map[DEC_BC]    = LR35902::decrement_16;
    main_map[INC_C]     = LR35902::increment;
    main_map[DEC_C]     = LR35902::decrement;
    main_map[LD_C_N]    = LR35902::load_immediate;
    main_map[RRCA]      = LR35902::rotate_right_carry;

    /* 1x */
    main_map[STOP]      = nullptr;
    main_map[LD_DE_NN]  = LR35902::load_16_immediate;
    main_map[LD_IDE_A]  = LR35902::store_indirect;
    main_map[INC_DE]    = LR35902::increment_16;
    main_map[INC_D]     = LR35902::increment;
    main_map[DEC_D]     = LR35902::decrement;
    main_map[LD_D_N]    = LR35902::load_immediate;
    main_map[RLA]       = LR35902::rotate_left;
    main_map[JR_N]      = LR35902::jump_relative;
    main_map[ADD_HL_DE] = LR35902::add_hl;
    main_map[LD_A_IDE]  = LR35902::load_indirect;
    main_map[DEC_DE]    = LR35902::decrement_16;
    main_map[INC_E]     = LR35902::increment;
    main_map[DEC_E]     = LR35902::decrement;
    main_map[LD_E_N]    = LR35902::load_immediate;
    main_map[RRA]       = LR35902::rotate_right;

    /* 2x */
    main_map[JR_NZ_N]   = LR35902::jump_relative;
    main_map[LD_HL_NN]  = LR35902::load_16_immediate;
    main_map[LD_IHLP_A] = LR35902::store_indirect;
    main_map[INC_HL]    = LR35902::increment_16;
    main_map[INC_H]     = LR35902::increment;
    main_map[DEC_H]     = LR35902::decrement;
    main_map[LD_H_N]    = LR35902::load_immediate;
    main_map[DAA]       = LR35902::decimal_arithmetic_adjust;
    main_map[JR_Z_N]    = LR35902::jump_relative;
    main_map[ADD_HL_HL] = LR35902::add_hl;
    main_map[LD_A_IHLP] = LR35902::load_indirect;
    main_map[DEC_HL]    = LR35902::decrement_16;
    main_map[INC_L]     = LR35902::increment;
    main_map[DEC_L]     = LR35902::decrement;
    main_map[LD_L_N]    = LR35902::load_immediate;
    main_map[CPL]       = LR35902::complement;

    /* 3x */
    main_map[JR_NC_N]   = LR35902::jump_relative;
    main_map[LD_SP_NN]  = LR35902::load_16_immediate;
    main_map[LD_IHLS_A] = LR35902::store_indirect;
    main_map[INC_SP]    = LR35902::increment_16;
    main_map[INC_IHL]   = LR35902::increment;
    main_map[DEC_IHL]   = LR35902::decrement;
    main_map[LD_IHL_N]  = LR35902::load_immediate;
    main_map[SCF]       = LR35902::change_carry_flag;
    main_map[JR_C_N]    = LR35902::jump_relative;
    main_map[ADD_HL_SP] = LR35902::add_hl;
    main_map[LD_A_IHLS] = LR35902::load_indirect;
    main_map[DEC_SP]    = LR35902::decrement_16;
    main_map[INC_A]     = LR35902::increment;
    main_map[DEC_A]     = LR35902::decrement;
    main_map[LD_A_N]    = LR35902::load_immediate;
    main_map[CCF]       = LR35902::change_carry_flag;

    /* 4x */
    main_map[LD_B_B]   = LR35902::copy_byte;
    main_map[LD_B_C]   = LR35902::copy_byte;
    main_map[LD_B_D]   = LR35902::copy_byte;
    main_map[LD_B_E]   = LR35902::copy_byte;
    main_map[LD_B_H]   = LR35902::copy_byte;
    main_map[LD_B_L]   = LR35902::copy_byte;
    main_map[LD_B_IHL] = LR35902::copy_byte;
    main_map[LD_B_A]   = LR35902::copy_byte;
    main_map[LD_C_B]   = LR35902::copy_byte;
    main_map[LD_C_C]   = LR35902::copy_byte;
    main_map[LD_C_D]   = LR35902::copy_byte;
    main_map[LD_C_E]   = LR35902::copy_byte;
    main_map[LD_C_H]   = LR35902::copy_byte;
    main_map[LD_C_L]   = LR35902::copy_byte;
    main_map[LD_C_IHL] = LR35902::copy_byte;
    main_map[LD_C_A]   = LR35902::copy_byte;

    /* 5x */
    main_map[LD_D_B]   = LR35902::copy_byte;
    main_map[LD_D_C]   = LR35902::copy_byte;
    main_map[LD_D_D]   = LR35902::copy_byte;
    main_map[LD_D_E]   = LR35902::copy_byte;
    main_map[LD_D_H]   = LR35902::copy_byte;
    main_map[LD_D_L]   = LR35902::copy_byte;
    main_map[LD_D_IHL] = LR35902::copy_byte;
    main_map[LD_D_A]   = LR35902::copy_byte;
    main_map[LD_E_B]   = LR35902::copy_byte;
    main_map[LD_E_C]   = LR35902::copy_byte;
    main_map[LD_E_D]   = LR35902::copy_byte;
    main_map[LD_E_E]   = LR35902::copy_byte;
    main_map[LD_E_H]   = LR35902::copy_byte;
    main_map[LD_E_L]   = LR35902::copy_byte;
    main_map[LD_E_IHL] = LR35902::copy_byte;
    main_map[LD_E_A]   = LR35902::copy_byte;

    /* 6x */
    main_map[LD_H_B]   = LR35902::copy_byte;
    main_map[LD_H_C]   = LR35902::copy_byte;
    main_map[LD_H_D]   = LR35902::copy_byte;
    main_map[LD_H_E]   = LR35902::copy_byte;
    main_map[LD_H_H]   = LR35902::copy_byte;
    main_map[LD_H_L]   = LR35902::copy_byte;
    main_map[LD_H_IHL] = LR35902::copy_byte;
    main_map[LD_H_A]   = LR35902::copy_byte;
    main_map[LD_L_B]   = LR35902::copy_byte;
    main_map[LD_L_C]   = LR35902::copy_byte;
    main_map[LD_L_D]   = LR35902::copy_byte;
    main_map[LD_L_E]   = LR35902::copy_byte;
    main_map[LD_L_H]   = LR35902::copy_byte;
    main_map[LD_L_L]   = LR35902::copy_byte;
    main_map[LD_L_IHL] = LR35902::copy_byte;
    main_map[LD_L_A]   = LR35902::copy_byte;

    /* 7x */
    main_map[LD_IHL_B] = LR35902::copy_byte;
    main_map[LD_IHL_C] = LR35902::copy_byte;
    main_map[LD_IHL_D] = LR35902::copy_byte;
    main_map[LD_IHL_E] = LR35902::copy_byte;
    main_map[LD_IHL_H] = LR35902::copy_byte;
    main_map[LD_IHL_L] = LR35902::copy_byte;
    main_map[HALT]     = nullptr;
    main_map[LD_IHL_A] = LR35902::copy_byte;
    main_map[LD_A_B]   = LR35902::copy_byte;
    main_map[LD_A_C]   = LR35902::copy_byte;
    main_map[LD_A_D]   = LR35902::copy_byte;
    main_map[LD_A_E]   = LR35902::copy_byte;
    main_map[LD_A_H]   = LR35902::copy_byte;
    main_map[LD_A_L]   = LR35902::copy_byte;
    main_map[LD_A_IHL] = LR35902::copy_byte;
    main_map[LD_A_A]   = LR35902::copy_byte;

    /* 8x */
    main_map[ADD_A_B]   = LR35902::add;
    main_map[ADD_A_C]   = LR35902::add;
    main_map[ADD_A_D]   = LR35902::add;
    main_map[ADD_A_E]   = LR35902::add;
    main_map[ADD_A_H]   = LR35902::add;
    main_map[ADD_A_L]   = LR35902::add;
    main_map[ADD_A_IHL] = LR35902::add;
    main_map[ADD_A_A]   = LR35902::add;
    main_map[ADC_A_B]   = LR35902::add_carry;
    main_map[ADC_A_C]   = LR35902::add_carry;
    main_map[ADC_A_D]   = LR35902::add_carry;
    main_map[ADC_A_E]   = LR35902::add_carry;
    main_map[ADC_A_H]   = LR35902::add_carry;
    main_map[ADC_A_L]   = LR35902::add_carry;
    main_map[ADC_A_IHL] = LR35902::add_carry;
    main_map[ADC_A_A]   = LR35902::add_carry;

    /* 9x */
    main_map[SUB_B]     = LR35902::subtract;
    main_map[SUB_C]     = LR35902::subtract;
    main_map[SUB_D]     = LR35902::subtract;
    main_map[SUB_E]     = LR35902::subtract;
    main_map[SUB_H]     = LR35902::subtract;
    main_map[SUB_L]     = LR35902::subtract;
    main_map[SUB_IHL]   = LR35902::subtract;
    main_map[SUB_A]     = LR35902::subtract;
    main_map[SBC_A_B]   = LR35902::subtract_carry;
    main_map[SBC_A_C]   = LR35902::subtract_carry;
    main_map[SBC_A_D]   = LR35902::subtract_carry;
    main_map[SBC_A_E]   = LR35902::subtract_carry;
    main_map[SBC_A_H]   = LR35902::subtract_carry;
    main_map[SBC_A_L]   = LR35902::subtract_carry;
    main_map[SBC_A_IHL] = LR35902::subtract_carry;
    main_map[SBC_A_A]   = LR35902::subtract_carry;

    /* Ax */
    main_map[AND_B]   = LR35902::bitwise_and;
    main_map[AND_C]   = LR35902::bitwise_and;
    main_map[AND_D]   = LR35902::bitwise_and;
    main_map[AND_E]   = LR35902::bitwise_and;
    main_map[AND_H]   = LR35902::bitwise_and;
    main_map[AND_L]   = LR35902::bitwise_and;
    main_map[AND_IHL] = LR35902::bitwise_and;
    main_map[AND_A]   = LR35902::bitwise_and;
    main_map[XOR_B]   = LR35902::bitwise_xor;
    main_map[XOR_C]   = LR35902::bitwise_xor;
    main_map[XOR_D]   = LR35902::bitwise_xor;
    main_map[XOR_E]   = LR35902::bitwise_xor;
    main_map[XOR_H]   = LR35902::bitwise_xor;
    main_map[XOR_L]   = LR35902::bitwise_xor;
    main_map[XOR_IHL] = LR35902::bitwise_xor;
    main_map[XOR_A]   = LR35902::bitwise_xor;

    /* Bx */
    main_map[OR_B]   = LR35902::bitwise_or;
    main_map[OR_C]   = LR35902::bitwise_or;
    main_map[OR_D]   = LR35902::bitwise_or;
    main_map[OR_E]   = LR35902::bitwise_or;
    main_map[OR_H]   = LR35902::bitwise_or;
    main_map[OR_L]   = LR35902::bitwise_or;
    main_map[OR_IHL] = LR35902::bitwise_or;
    main_map[OR_A]   = LR35902::bitwise_or;
    main_map[CP_B]   = LR35902::compare;
    main_map[CP_C]   = LR35902::compare;
    main_map[CP_D]   = LR35902::compare;
    main_map[CP_E]   = LR35902::compare;
    main_map[CP_H]   = LR35902::compare;
    main_map[CP_L]   = LR35902::compare;
    main_map[CP_IHL] = LR35902::compare;
    main_map[CP_A]   = LR35902::compare;

    /* Cx */
    main_map[RET_NZ]     = LR35902::ret_call;
    main_map[POP_BC]     = LR35902::pop;
    main_map[JP_NZ_NN]   = LR35902::jump_target;
    main_map[JP_NN]      = LR35902::jump_target;
    main_map[CALL_NZ_NN] = LR35902::call;
    main_map[PUSH_BC]    = LR35902::push;
    main_map[ADD_N]      = LR35902::add;
    main_map[RST_00]     = LR35902::reset;
    main_map[RET_Z]      = LR35902::ret_call;
    main_map[RET]        = LR35902::ret_call;
    main_map[JP_Z_NN]    = LR35902::jump_target;
    main_map[PREFIX_CB]  = nullptr;
    main_map[CALL_Z_NN]  = LR35902::call;
    main_map[CALL_NN]    = LR35902::call;
    main_map[ADC_N]      = LR35902::add_carry;
    main_map[RST_08]     = LR35902::reset;

    /* Dx */
    main_map[RET_NC]   = LR35902::ret_call;
    main_map[POP_DE]   = LR35902::pop;
    main_map[JP_NC_NN] = LR35902::jump_target;
    /* Invalid OpCode D3 */
    main_map[CALL_NC_NN] = LR35902::call;
    main_map[PUSH_DE]    = LR35902::push;
    main_map[SUB_N]      = LR35902::subtract;
    main_map[RST_10]     = LR35902::reset;
    main_map[RET_C]      = LR35902::ret_call;
    main_map[RETI]       = LR35902::ret_call;
    main_map[JP_C_NN]    = LR35902::jump_target;
    /* Invalid OpCode DB */
    main_map[CALL_C_NN] = LR35902::call;
    /* Invalid OpCode DD */
    main_map[SBC_N]  = LR35902::subtract_carry;
    main_map[RST_18] = LR35902::reset;

    /* Ex */
    main_map[LDH_IN_A] = LR35902::store_offset;
    main_map[POP_HL]   = LR35902::pop;
    main_map[LD_IC_A]  = LR35902::store_offset;
    /* Invalid OpCode E3 */
    /* Invalid OpCode E4 */
    main_map[PUSH_HL]  = LR35902::push;
    main_map[AND_N]    = LR35902::bitwise_and;
    main_map[RST_20]   = LR35902::reset;
    main_map[ADD_SP_N] = LR35902::add_sp;
    main_map[JP_IHL]   = LR35902::jump_target;
    main_map[LD_INN_A] = LR35902::store_indirect;
    /* Invalid OpCode EB */
    /* Invalid OpCode EC */
    /* Invalid OpCode ED */
    main_map[XOR_N]  = LR35902::bitwise_xor;
    main_map[RST_28] = LR35902::reset;

    /* Fx */
    main_map[LDH_A_IN] = LR35902::load_offset;
    main_map[POP_AF]   = LR35902::pop;
    main_map[LD_A_IC]  = LR35902::load_offset;
    main_map[DI]       = nullptr;
    /* Invalid OpCode F4 */
    main_map[PUSH_AF]      = LR35902::push;
    main_map[OR_N]         = LR35902::bitwise_or;
    main_map[RST_30]       = LR35902::reset;
    main_map[LD_HL_SP_INP] = LR35902::copy_stack_pointer;
    main_map[LD_SP_HL]     = LR35902::copy_stack_pointer;
    main_map[LD_A_INN]     = LR35902::load_indirect;
    main_map[EI]           = nullptr;
    /* Invalid OpCode FC */
    /* Invalid OpCode FD */
    main_map[CP_N]   = LR35902::compare;
    main_map[RST_38] = LR35902::reset;

    /* CB Instruction Map */
    cb_map[RLC_B]     = LR35902::rotate_left_carry;
    cb_map[RLC_C]     = LR35902::rotate_left_carry;
    cb_map[RLC_D]     = LR35902::rotate_left_carry;
    cb_map[RLC_E]     = LR35902::rotate_left_carry;
    cb_map[RLC_H]     = LR35902::rotate_left_carry;
    cb_map[RLC_L]     = LR35902::rotate_left_carry;
    cb_map[RLC_IHL]   = LR35902::rotate_left_carry;
    cb_map[RLC_A]     = LR35902::rotate_left_carry;
    cb_map[RRC_B]     = LR35902::rotate_right_carry;
    cb_map[RRC_C]     = LR35902::rotate_right_carry;
    cb_map[RRC_D]     = LR35902::rotate_right_carry;
    cb_map[RRC_E]     = LR35902::rotate_right_carry;
    cb_map[RRC_H]     = LR35902::rotate_right_carry;
    cb_map[RRC_L]     = LR35902::rotate_right_carry;
    cb_map[RRC_IHL]   = LR35902::rotate_right_carry;
    cb_map[RRC_A]     = LR35902::rotate_right_carry;
    cb_map[RL_B]      = LR35902::rotate_left;
    cb_map[RL_C]      = LR35902::rotate_left;
    cb_map[RL_D]      = LR35902::rotate_left;
    cb_map[RL_E]      = LR35902::rotate_left;
    cb_map[RL_H]      = LR35902::rotate_left;
    cb_map[RL_L]      = LR35902::rotate_left;
    cb_map[RL_IHL]    = LR35902::rotate_left;
    cb_map[RL_A]      = LR35902::rotate_left;
    cb_map[RR_B]      = LR35902::rotate_right;
    cb_map[RR_C]      = LR35902::rotate_right;
    cb_map[RR_D]      = LR35902::rotate_right;
    cb_map[RR_E]      = LR35902::rotate_right;
    cb_map[RR_H]      = LR35902::rotate_right;
    cb_map[RR_L]      = LR35902::rotate_right;
    cb_map[RR_IHL]    = LR35902::rotate_right;
    cb_map[RR_A]      = LR35902::rotate_right;
    cb_map[SLA_B]     = LR35902::shift_left;
    cb_map[SLA_C]     = LR35902::shift_left;
    cb_map[SLA_D]     = LR35902::shift_left;
    cb_map[SLA_E]     = LR35902::shift_left;
    cb_map[SLA_H]     = LR35902::shift_left;
    cb_map[SLA_L]     = LR35902::shift_left;
    cb_map[SLA_IHL]   = LR35902::shift_left;
    cb_map[SLA_A]     = LR35902::shift_left;
    cb_map[SRA_B]     = LR35902::shift_right_arithmetic;
    cb_map[SRA_C]     = LR35902::shift_right_arithmetic;
    cb_map[SRA_D]     = LR35902::shift_right_arithmetic;
    cb_map[SRA_E]     = LR35902::shift_right_arithmetic;
    cb_map[SRA_H]     = LR35902::shift_right_arithmetic;
    cb_map[SRA_L]     = LR35902::shift_right_arithmetic;
    cb_map[SRA_IHL]   = LR35902::shift_right_arithmetic;
    cb_map[SRA_A]     = LR35902::shift_right_arithmetic;
    cb_map[SWAP_B]    = LR35902::swap;
    cb_map[SWAP_C]    = LR35902::swap;
    cb_map[SWAP_D]    = LR35902::swap;
    cb_map[SWAP_E]    = LR35902::swap;
    cb_map[SWAP_H]    = LR35902::swap;
    cb_map[SWAP_L]    = LR35902::swap;
    cb_map[SWAP_IHL]  = LR35902::swap;
    cb_map[SWAP_A]    = LR35902::swap;
    cb_map[SRL_B]     = LR35902::shift_right_logical;
    cb_map[SRL_C]     = LR35902::shift_right_logical;
    cb_map[SRL_D]     = LR35902::shift_right_logical;
    cb_map[SRL_E]     = LR35902::shift_right_logical;
    cb_map[SRL_H]     = LR35902::shift_right_logical;
    cb_map[SRL_L]     = LR35902::shift_right_logical;
    cb_map[SRL_IHL]   = LR35902::shift_right_logical;
    cb_map[SRL_A]     = LR35902::shift_right_logical;
    cb_map[BIT_0_B]   = LR35902::bit_check;
    cb_map[BIT_0_C]   = LR35902::bit_check;
    cb_map[BIT_0_D]   = LR35902::bit_check;
    cb_map[BIT_0_E]   = LR35902::bit_check;
    cb_map[BIT_0_H]   = LR35902::bit_check;
    cb_map[BIT_0_L]   = LR35902::bit_check;
    cb_map[BIT_0_IHL] = LR35902::bit_check;
    cb_map[BIT_0_A]   = LR35902::bit_check;
    cb_map[BIT_1_B]   = LR35902::bit_check;
    cb_map[BIT_1_C]   = LR35902::bit_check;
    cb_map[BIT_1_D]   = LR35902::bit_check;
    cb_map[BIT_1_E]   = LR35902::bit_check;
    cb_map[BIT_1_H]   = LR35902::bit_check;
    cb_map[BIT_1_L]   = LR35902::bit_check;
    cb_map[BIT_1_IHL] = LR35902::bit_check;
    cb_map[BIT_1_A]   = LR35902::bit_check;
    cb_map[BIT_2_B]   = LR35902::bit_check;
    cb_map[BIT_2_C]   = LR35902::bit_check;
    cb_map[BIT_2_D]   = LR35902::bit_check;
    cb_map[BIT_2_E]   = LR35902::bit_check;
    cb_map[BIT_2_H]   = LR35902::bit_check;
    cb_map[BIT_2_L]   = LR35902::bit_check;
    cb_map[BIT_2_IHL] = LR35902::bit_check;
    cb_map[BIT_2_A]   = LR35902::bit_check;
    cb_map[BIT_3_B]   = LR35902::bit_check;
    cb_map[BIT_3_C]   = LR35902::bit_check;
    cb_map[BIT_3_D]   = LR35902::bit_check;
    cb_map[BIT_3_E]   = LR35902::bit_check;
    cb_map[BIT_3_H]   = LR35902::bit_check;
    cb_map[BIT_3_L]   = LR35902::bit_check;
    cb_map[BIT_3_IHL] = LR35902::bit_check;
    cb_map[BIT_3_A]   = LR35902::bit_check;
    cb_map[BIT_4_B]   = LR35902::bit_check;
    cb_map[BIT_4_C]   = LR35902::bit_check;
    cb_map[BIT_4_D]   = LR35902::bit_check;
    cb_map[BIT_4_E]   = LR35902::bit_check;
    cb_map[BIT_4_H]   = LR35902::bit_check;
    cb_map[BIT_4_L]   = LR35902::bit_check;
    cb_map[BIT_4_IHL] = LR35902::bit_check;
    cb_map[BIT_4_A]   = LR35902::bit_check;
    cb_map[BIT_5_B]   = LR35902::bit_check;
    cb_map[BIT_5_C]   = LR35902::bit_check;
    cb_map[BIT_5_D]   = LR35902::bit_check;
    cb_map[BIT_5_E]   = LR35902::bit_check;
    cb_map[BIT_5_H]   = LR35902::bit_check;
    cb_map[BIT_5_L]   = LR35902::bit_check;
    cb_map[BIT_5_IHL] = LR35902::bit_check;
    cb_map[BIT_5_A]   = LR35902::bit_check;
    cb_map[BIT_6_B]   = LR35902::bit_check;
    cb_map[BIT_6_C]   = LR35902::bit_check;
    cb_map[BIT_6_D]   = LR35902::bit_check;
    cb_map[BIT_6_E]   = LR35902::bit_check;
    cb_map[BIT_6_H]   = LR35902::bit_check;
    cb_map[BIT_6_L]   = LR35902::bit_check;
    cb_map[BIT_6_IHL] = LR35902::bit_check;
    cb_map[BIT_6_A]   = LR35902::bit_check;
    cb_map[BIT_7_B]   = LR35902::bit_check;
    cb_map[BIT_7_C]   = LR35902::bit_check;
    cb_map[BIT_7_D]   = LR35902::bit_check;
    cb_map[BIT_7_E]   = LR35902::bit_check;
    cb_map[BIT_7_H]   = LR35902::bit_check;
    cb_map[BIT_7_L]   = LR35902::bit_check;
    cb_map[BIT_7_IHL] = LR35902::bit_check;
    cb_map[BIT_7_A]   = LR35902::bit_check;
    cb_map[RES_0_B]   = LR35902::bit_reset;
    cb_map[RES_0_C]   = LR35902::bit_reset;
    cb_map[RES_0_D]   = LR35902::bit_reset;
    cb_map[RES_0_E]   = LR35902::bit_reset;
    cb_map[RES_0_H]   = LR35902::bit_reset;
    cb_map[RES_0_L]   = LR35902::bit_reset;
    cb_map[RES_0_IHL] = LR35902::bit_reset;
    cb_map[RES_0_A]   = LR35902::bit_reset;
    cb_map[RES_1_B]   = LR35902::bit_reset;
    cb_map[RES_1_C]   = LR35902::bit_reset;
    cb_map[RES_1_D]   = LR35902::bit_reset;
    cb_map[RES_1_E]   = LR35902::bit_reset;
    cb_map[RES_1_H]   = LR35902::bit_reset;
    cb_map[RES_1_L]   = LR35902::bit_reset;
    cb_map[RES_1_IHL] = LR35902::bit_reset;
    cb_map[RES_1_A]   = LR35902::bit_reset;
    cb_map[RES_2_B]   = LR35902::bit_reset;
    cb_map[RES_2_C]   = LR35902::bit_reset;
    cb_map[RES_2_D]   = LR35902::bit_reset;
    cb_map[RES_2_E]   = LR35902::bit_reset;
    cb_map[RES_2_H]   = LR35902::bit_reset;
    cb_map[RES_2_L]   = LR35902::bit_reset;
    cb_map[RES_2_IHL] = LR35902::bit_reset;
    cb_map[RES_2_A]   = LR35902::bit_reset;
    cb_map[RES_3_B]   = LR35902::bit_reset;
    cb_map[RES_3_C]   = LR35902::bit_reset;
    cb_map[RES_3_D]   = LR35902::bit_reset;
    cb_map[RES_3_E]   = LR35902::bit_reset;
    cb_map[RES_3_H]   = LR35902::bit_reset;
    cb_map[RES_3_L]   = LR35902::bit_reset;
    cb_map[RES_3_IHL] = LR35902::bit_reset;
    cb_map[RES_3_A]   = LR35902::bit_reset;
    cb_map[RES_4_B]   = LR35902::bit_reset;
    cb_map[RES_4_C]   = LR35902::bit_reset;
    cb_map[RES_4_D]   = LR35902::bit_reset;
    cb_map[RES_4_E]   = LR35902::bit_reset;
    cb_map[RES_4_H]   = LR35902::bit_reset;
    cb_map[RES_4_L]   = LR35902::bit_reset;
    cb_map[RES_4_IHL] = LR35902::bit_reset;
    cb_map[RES_4_A]   = LR35902::bit_reset;
    cb_map[RES_5_B]   = LR35902::bit_reset;
    cb_map[RES_5_C]   = LR35902::bit_reset;
    cb_map[RES_5_D]   = LR35902::bit_reset;
    cb_map[RES_5_E]   = LR35902::bit_reset;
    cb_map[RES_5_H]   = LR35902::bit_reset;
    cb_map[RES_5_L]   = LR35902::bit_reset;
    cb_map[RES_5_IHL] = LR35902::bit_reset;
    cb_map[RES_5_A]   = LR35902::bit_reset;
    cb_map[RES_6_B]   = LR35902::bit_reset;
    cb_map[RES_6_C]   = LR35902::bit_reset;
    cb_map[RES_6_D]   = LR35902::bit_reset;
    cb_map[RES_6_E]   = LR35902::bit_reset;
    cb_map[RES_6_H]   = LR35902::bit_reset;
    cb_map[RES_6_L]   = LR35902::bit_reset;
    cb_map[RES_6_IHL] = LR35902::bit_reset;
    cb_map[RES_6_A]   = LR35902::bit_reset;
    cb_map[RES_7_B]   = LR35902::bit_reset;
    cb_map[RES_7_C]   = LR35902::bit_reset;
    cb_map[RES_7_D]   = LR35902::bit_reset;
    cb_map[RES_7_E]   = LR35902::bit_reset;
    cb_map[RES_7_H]   = LR35902::bit_reset;
    cb_map[RES_7_L]   = LR35902::bit_reset;
    cb_map[RES_7_IHL] = LR35902::bit_reset;
    cb_map[RES_7_A]   = LR35902::bit_reset;
    cb_map[SET_0_B]   = LR35902::bit_set;
    cb_map[SET_0_C]   = LR35902::bit_set;
    cb_map[SET_0_D]   = LR35902::bit_set;
    cb_map[SET_0_E]   = LR35902::bit_set;
    cb_map[SET_0_H]   = LR35902::bit_set;
    cb_map[SET_0_L]   = LR35902::bit_set;
    cb_map[SET_0_IHL] = LR35902::bit_set;
    cb_map[SET_0_A]   = LR35902::bit_set;
    cb_map[SET_1_B]   = LR35902::bit_set;
    cb_map[SET_1_C]   = LR35902::bit_set;
    cb_map[SET_1_D]   = LR35902::bit_set;
    cb_map[SET_1_E]   = LR35902::bit_set;
    cb_map[SET_1_H]   = LR35902::bit_set;
    cb_map[SET_1_L]   = LR35902::bit_set;
    cb_map[SET_1_IHL] = LR35902::bit_set;
    cb_map[SET_1_A]   = LR35902::bit_set;
    cb_map[SET_2_B]   = LR35902::bit_set;
    cb_map[SET_2_C]   = LR35902::bit_set;
    cb_map[SET_2_D]   = LR35902::bit_set;
    cb_map[SET_2_E]   = LR35902::bit_set;
    cb_map[SET_2_H]   = LR35902::bit_set;
    cb_map[SET_2_L]   = LR35902::bit_set;
    cb_map[SET_2_IHL] = LR35902::bit_set;
    cb_map[SET_2_A]   = LR35902::bit_set;
    cb_map[SET_3_B]   = LR35902::bit_set;
    cb_map[SET_3_C]   = LR35902::bit_set;
    cb_map[SET_3_D]   = LR35902::bit_set;
    cb_map[SET_3_E]   = LR35902::bit_set;
    cb_map[SET_3_H]   = LR35902::bit_set;
    cb_map[SET_3_L]   = LR35902::bit_set;
    cb_map[SET_3_IHL] = LR35902::bit_set;
    cb_map[SET_3_A]   = LR35902::bit_set;
    cb_map[SET_4_B]   = LR35902::bit_set;
    cb_map[SET_4_C]   = LR35902::bit_set;
    cb_map[SET_4_D]   = LR35902::bit_set;
    cb_map[SET_4_E]   = LR35902::bit_set;
    cb_map[SET_4_H]   = LR35902::bit_set;
    cb_map[SET_4_L]   = LR35902::bit_set;
    cb_map[SET_4_IHL] = LR35902::bit_set;
    cb_map[SET_4_A]   = LR35902::bit_set;
    cb_map[SET_5_B]   = LR35902::bit_set;
    cb_map[SET_5_C]   = LR35902::bit_set;
    cb_map[SET_5_D]   = LR35902::bit_set;
    cb_map[SET_5_E]   = LR35902::bit_set;
    cb_map[SET_5_H]   = LR35902::bit_set;
    cb_map[SET_5_L]   = LR35902::bit_set;
    cb_map[SET_5_IHL] = LR35902::bit_set;
    cb_map[SET_5_A]   = LR35902::bit_set;
    cb_map[SET_6_B]   = LR35902::bit_set;
    cb_map[SET_6_C]   = LR35902::bit_set;
    cb_map[SET_6_D]   = LR35902::bit_set;
    cb_map[SET_6_E]   = LR35902::bit_set;
    cb_map[SET_6_H]   = LR35902::bit_set;
    cb_map[SET_6_L]   = LR35902::bit_set;
    cb_map[SET_6_IHL] = LR35902::bit_set;
    cb_map[SET_6_A]   = LR35902::bit_set;
    cb_map[SET_7_B]   = LR35902::bit_set;
    cb_map[SET_7_C]   = LR35902::bit_set;
    cb_map[SET_7_D]   = LR35902::bit_set;
    cb_map[SET_7_E]   = LR35902::bit_set;
    cb_map[SET_7_H]   = LR35902::bit_set;
    cb_map[SET_7_L]   = LR35902::bit_set;
    cb_map[SET_7_IHL] = LR35902::bit_set;
    cb_map[SET_7_A]   = LR35902::bit_set;
}

}    // namespace LR35902
