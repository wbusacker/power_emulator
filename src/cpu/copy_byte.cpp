#include <cpu_instructions_groups.h>

namespace LR35902 {

void copy_byte(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where which registers stores the data to move */
    uint8_t* source_reg      = get_register_pointer(registers, instruction & 0x7);
    uint8_t* destination_reg = get_register_pointer(registers, (instruction >> 3) & 0x7);

    /* Get the data to store away */
    uint8_t data = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (source_reg == nullptr) {
        data = get_hl_indirect(memory, registers);
    } else {
        data = *source_reg;
    }

    /* If the target is nullptr, store the data there */
    if (destination_reg == nullptr) {
        store_hl_indirect(memory, registers, data);
    } else {
        *destination_reg = data;
    }
}

}    // namespace LR35902