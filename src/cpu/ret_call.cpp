#include <cpu.h>

namespace LR35902 {

void ret_call(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out if we should actually return or not */
    bool should_return = false;
    switch (instruction) {
        case LR35902::RET_NZ:
            should_return = ! registers->flags.zero;
            break;
        case LR35902::RET_Z:
            should_return = registers->flags.zero;
            break;
        case LR35902::RET_NC:
            should_return = ! registers->flags.carry;
            break;
        case LR35902::RET_C:
            should_return = registers->flags.carry;
            break;
        case LR35902::RET:
        case LR35902::RETI:
            should_return = true;
            break;
    }

    if (should_return) {
        /* Pull the address off of the stack pointer */
        uint8_t pc_l               = memory->read(registers->stack_pointer++);
        uint8_t pc_h               = memory->read(registers->stack_pointer++);
        registers->program_counter = (static_cast<uint16_t>(pc_h) << LR35902::BITS_PER_BYTE) | pc_l;

        /* Since we're jumping, increase the cycle time delay */
        registers->cycle_time += LR35902::RETURN_ADDITIONAL_CYCLES;
    }
}

}    // namespace LR35902
