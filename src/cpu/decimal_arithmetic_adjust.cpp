#include <cpu_instructions_groups.h>

namespace LR35902 {

void decimal_arithmetic_adjust(Common_memory::Common_memory_bus* memory,
                               Register_file_t*                  registers,
                               uint8_t                           instruction) {

    /* Check to see if the previous instruction was a subtraction */
    uint16_t working_value = registers->A;
    if (registers->flags.subtract == false) {

        if ((registers->flags.half_carry == true) ||
            ((working_value & LR35902::NIBBLE_MASK) > DAA_DECIMAL_OVERFLOW_CHECK)) {
            working_value += DAA_ONES_OVERFLOW_COMPENSATION;
        }

        if ((registers->flags.carry == true) ||
            ((working_value >> LR35902::BITS_PER_NIBBLE) > DAA_DECIMAL_OVERFLOW_CHECK)) {
            working_value += DAA_TENS_OVERFLOW_COMPENSATION;
        }

        if (working_value > LR35902::BYTE_MASK) {
            registers->flags.carry = true;
        } else {
            registers->flags.carry = false;
        }

    } else {

        /* All of the subtract modes are liable to generate a full borrow, so prepare for that */
        working_value |= LR35902::BIT_8_MASK;

        if ((registers->flags.half_carry == true) && (registers->flags.carry == true)) {
            working_value -= 102;
            registers->flags.carry = true;
        } else if (registers->flags.half_carry == true) {
            working_value -= 6;
            registers->flags.carry = false;
        } else if (registers->flags.carry == true) {
            working_value -= 96;
            registers->flags.carry = true;
        }
    }

    registers->A = working_value & LR35902::BYTE_MASK;

    if (registers->A == 0) {
        registers->flags.zero = true;
    } else {
        registers->flags.zero = false;
    }

    registers->flags.half_carry = false;
}

}    // namespace LR35902