#include <cpu_instructions_groups.h>

namespace LR35902 {

void add_carry(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        /* Figure out if this is the immediate instruction or not */
        if (instruction == LR35902::ADC_N) {
            arg_variable = get_next_pc(memory, registers);
        } else {
            arg_variable = get_hl_indirect(memory, registers);
        }
    } else {
        arg_variable = *arg_source;
    }

    bool do_carry = registers->flags.carry;

    /* Do the first add normally */
    registers->A = common_add(&registers->flags, registers->A, arg_variable);

    /* If the carry flag wasn't set before, don't make any adjustments */
    if (do_carry) {
        /* Record if the normal add triggered either condition */
        bool carry      = registers->flags.carry;
        bool half_carry = registers->flags.half_carry;

        /* Add just one more to A */
        registers->A = common_add(&registers->flags, registers->A, 1);

        /* If this round of addition or the last caused either carry, record that */
        registers->flags.carry |= carry;
        registers->flags.half_carry |= half_carry;
    }
}

}    // namespace LR35902