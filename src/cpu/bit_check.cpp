#include <cpu_instructions_groups.h>

namespace LR35902 {

void bit_check(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    /* Figure out which bit we are checking */
    uint8_t bit_index = (instruction >> 3) & 0x7;

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        arg_variable = get_hl_indirect(memory, registers);
        registers->cycle_time += LR35902::CB_INDIRECT_ADDITIONAL_CYCLES;
    } else {
        arg_variable = *arg_source;
    }

    if (((arg_variable >> bit_index) & 0b1) == 1) {
        registers->flags.zero = false;
    } else {
        registers->flags.zero = true;
    }

    registers->flags.subtract   = false;
    registers->flags.half_carry = true;
}

}    // namespace LR35902