#include <cpu.h>

namespace LR35902 {

void Gameboy_cpu::perform_instruction(uint8_t instruction) {

    LR35902::Instruction_function operation = main_map[instruction];

    /* There is the potential here for bad instructions, so don't allow them */
    if (operation != nullptr) {
        operation(memory, &registers, instruction);
    }
}

}    // namespace LR35902