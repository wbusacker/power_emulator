#include <cpu_instructions_groups.h>

namespace LR35902 {

void store_stack_pointer(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Obtain the address to store at */
    uint8_t addr_l = get_next_pc(memory, registers);
    uint8_t addr_h = get_next_pc(memory, registers);

    uint16_t addr = (static_cast<uint16_t>(addr_h) << LR35902::BITS_PER_BYTE) | addr_l;

    /* Store Stack Pointer's LSB first */
    memory->write(addr, registers->stack_pointer & LR35902::BYTE_MASK);
    memory->write(addr + 1, (registers->stack_pointer >> LR35902::BITS_PER_BYTE) & LR35902::BYTE_MASK);
}

}    // namespace LR35902
