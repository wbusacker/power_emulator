#include <cpu.h>

namespace LR35902 {

void reset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Take the current address and push it onto the stack. MSB first */

    memory->write(--(registers->stack_pointer),
                  (registers->program_counter >> LR35902::BITS_PER_BYTE) & LR35902::BYTE_MASK);
    memory->write(--(registers->stack_pointer), registers->program_counter & LR35902::BYTE_MASK);

    switch (instruction) {
        case RST_00:
            registers->program_counter = RESET_00;
            break;
        case RST_08:
            registers->program_counter = RESET_08;
            break;
        case RST_10:
            registers->program_counter = RESET_10;
            break;
        case RST_18:
            registers->program_counter = RESET_18;
            break;
        case RST_20:
            registers->program_counter = RESET_20;
            break;
        case RST_28:
            registers->program_counter = RESET_28;
            break;
        case RST_30:
            registers->program_counter = RESET_30;
            break;
        case RST_38:
            registers->program_counter = RESET_38;
            break;
    }
}

}    // namespace LR35902