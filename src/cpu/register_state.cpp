#include <cpu.h>

namespace LR35902 {

void Gameboy_cpu::set_register_state(Register_file_t* set) {
    registers.A = set->A;
    registers.B = set->B;
    registers.C = set->C;
    registers.D = set->D;
    registers.E = set->E;
    registers.H = set->H;
    registers.L = set->L;

    registers.flags.zero       = set->flags.zero;
    registers.flags.subtract   = set->flags.subtract;
    registers.flags.half_carry = set->flags.half_carry;
    registers.flags.carry      = set->flags.carry;
    registers.flags.paddinging = set->flags.paddinging;

    registers.stack_pointer   = set->stack_pointer;
    registers.program_counter = set->program_counter;
    registers.cb_mode         = set->cb_mode;
    registers.cycle_time      = set->cycle_time;
}

void Gameboy_cpu::get_register_state(Register_file_t* get) {
    get->A = registers.A;
    get->B = registers.B;
    get->C = registers.C;
    get->D = registers.D;
    get->E = registers.E;
    get->H = registers.H;
    get->L = registers.L;

    get->flags.zero       = registers.flags.zero;
    get->flags.subtract   = registers.flags.subtract;
    get->flags.half_carry = registers.flags.half_carry;
    get->flags.carry      = registers.flags.carry;
    get->flags.paddinging = registers.flags.paddinging;

    get->stack_pointer   = registers.stack_pointer;
    get->program_counter = registers.program_counter;
    get->cb_mode         = registers.cb_mode;
    get->cycle_time      = registers.cycle_time;
}

}    // namespace LR35902