#ifndef GAMEBOY_CPU_INSTRUCTION_GROUPS_H
#define GAMEBOY_CPU_INSTRUCTION_GROUPS_H
#include <common_memory_bus.h>
#include <cpu_types_data.h>

namespace LR35902 {

typedef void (*Instruction_function)(Common_memory::Common_memory_bus* memory,
                                     Register_file_t*                  registers,
                                     uint8_t                           instruction);

/* A lot of functions use the add mechanism under the hood, so to avoid duplication
    do that same thing here
*/
uint8_t common_add(CPU_state_flags_t* flags, uint8_t left_arg, uint8_t right_arg);
uint8_t common_subtract(CPU_state_flags_t* flags, uint8_t left_arg, uint8_t right_arg);

/* The LR35902 Mnemonics call all memory movement "load".
    That's annoying and I don't like it, so I'm applying some other names on things

    Load means taking from a 16-bit memory address and placing into the CPU Register file

    Store means taking from the CPU register file and moving into a 16-bit memory address

    Copy means copying data within the CPU register file itself and not accessing main memory
*/

/* Copy group */
void copy_byte(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void copy_word(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* Load Group */
void load_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void load_immediate(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void load_offset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* Store Group */
void store_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void store_offset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* 16-bit Copy Group */
void copy_stack_pointer(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* 16-bit Load Group */
void load_16_immediate(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void load_16_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* 16-bit Store Group */
void store_stack_pointer(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* 8-bit Arithmetic Group */
void add(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void add_carry(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void subtract(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void subtract_carry(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void increment(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void decrement(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* 16-bit Arithmetic Group */
void add_hl(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void add_sp(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void increment_16(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void decrement_16(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* 8-bit Logical Group */
void bitwise_and(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void bitwise_or(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void bitwise_xor(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void compare(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* Register File Manipulation */
void decimal_arithmetic_adjust(Common_memory::Common_memory_bus* memory,
                               Register_file_t*                  registers,
                               uint8_t                           instruction);
void complement(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void change_carry_flag(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* Flow Controls */
void call(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void jump_target(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void jump_relative(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void reset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void ret_call(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* Stack Control */
void pop(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void push(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* CB Group */
void bit_check(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void bit_set(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void bit_reset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

void rotate_left_carry(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void rotate_left(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void rotate_right(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void rotate_right_carry(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

void shift_left(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void shift_right_arithmetic(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);
void shift_right_logical(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

void swap(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction);

/* These are a set of commonly used functions that for efficiency should be inlined */

inline uint8_t get_next_pc(Common_memory::Common_memory_bus* memory, Register_file_t* registers) {
    return memory->read(registers->program_counter++);
}

inline uint8_t get_hl_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers) {

    /* Put together the address to collect from */
    uint16_t addr = (static_cast<uint16_t>(registers->H) << 8) | registers->L;
    return memory->read(addr);
}

inline void store_hl_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t data) {
    uint16_t addr = (static_cast<uint16_t>(registers->H) << 8) | registers->L;
    memory->write(addr, data);
}

inline uint8_t* get_register_pointer(Register_file_t* registers, uint8_t register_number) {

    uint8_t* result = nullptr;

    switch (register_number) {
        case 0:
            result = &registers->B;
            break;
        case 1:
            result = &registers->C;
            break;
        case 2:
            result = &registers->D;
            break;
        case 3:
            result = &registers->E;
            break;
        case 4:
            result = &registers->H;
            break;
        case 5:
            result = &registers->L;
            break;
        case 6:
            result = nullptr;
            break;
        case 7:
            result = &registers->A;
            break;
        default:
            result = nullptr;
    }

    return result;
}

}    // namespace LR35902

#endif