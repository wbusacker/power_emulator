#include <cpu_instructions_groups.h>

namespace LR35902 {

void load_offset(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our offset value from */
    uint16_t target_address = LR35902::OFFSET_BASE;

    switch (instruction) {
        case LD_A_IC:
            target_address += registers->C;
            break;
        case LDH_A_IN:
            target_address += get_next_pc(memory, registers);
            break;
    }

    registers->A = memory->read(target_address);
}

}    // namespace LR35902
