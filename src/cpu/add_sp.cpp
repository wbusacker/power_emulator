#include <cpu_instructions_groups.h>

namespace LR35902 {

void add_sp(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Get the byte to add to the stack pointer */
    uint16_t offset = get_next_pc(memory, registers);

    /* Sign extend the offset if needed */
    if ((offset & LR35902::BIT_7_MASK) != 0) {
        offset |= (static_cast<uint16_t>(LR35902::BYTE_MASK) << LR35902::BITS_PER_BYTE);
    }

    uint16_t value = registers->stack_pointer + offset;

    /* Figure out if a half carry happened */
    if (((registers->stack_pointer & LR35902::NIBBLE_MASK) + (offset & LR35902::NIBBLE_MASK)) > LR35902::NIBBLE_MASK) {
        registers->flags.half_carry = true;
    } else {
        registers->flags.half_carry = false;
    }

    /* Figure out if full carry happened */
    if (((registers->stack_pointer & LR35902::BYTE_MASK) + (offset & LR35902::BYTE_MASK)) > LR35902::BYTE_MASK) {
        registers->flags.carry = true;
    } else {
        registers->flags.carry = false;
    }

    /* Put the value back in the stack_pointer */
    registers->stack_pointer = value;

    /* Always reset zero and subtract */
    registers->flags.zero     = false;
    registers->flags.subtract = false;
}

}    // namespace LR35902