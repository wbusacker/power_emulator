#include <cpu_instructions_groups.h>

namespace LR35902 {

void jump_relative(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Get the offset to jump by, relative jumps are signed values */
    int8_t offset = get_next_pc(memory, registers);

    /* Figure out if we actually should jump or not */
    bool should_jump = false;
    switch (instruction) {
        case LR35902::JR_NZ_N:
            should_jump = ! registers->flags.zero;
            break;
        case LR35902::JR_Z_N:
            should_jump = registers->flags.zero;
            break;
        case LR35902::JR_NC_N:
            should_jump = ! registers->flags.carry;
            break;
        case LR35902::JR_C_N:
            should_jump = registers->flags.carry;
            break;
        case LR35902::JR_N:
        default: /* Jump Unconditionally*/
            should_jump = true;
    }

    if (should_jump) {

        /* Reconfigure the program counter */
        registers->program_counter += offset;

        /* Since we're jumping, increase the cycle time delay */
        registers->cycle_time += LR35902::JUMP_ADDITIONAL_CYCLES;
    }
}

}    // namespace LR35902