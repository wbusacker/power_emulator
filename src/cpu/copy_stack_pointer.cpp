#include <cpu_instructions_groups.h>

namespace LR35902 {

void copy_stack_pointer(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* There are only two ways this can go, and they don't share a lot in common
        but they both do the same operation which is why they both live in this function
        They could possible be broken out from eachother with no harm
    */
    switch (instruction) {
        case LD_HL_SP_INP: {
            /* Get the value to add to stack pointer when copying */
            uint16_t offset = get_next_pc(memory, registers);

            /* Sign extend the offset if needed */
            if ((offset & LR35902::BIT_7_MASK) != 0) {
                offset |= (static_cast<uint16_t>(LR35902::BYTE_MASK) << LR35902::BITS_PER_BYTE);
            }

            uint16_t value = registers->stack_pointer + offset;

            /* Figure out if a half carry happened */
            if (((registers->stack_pointer & LR35902::NIBBLE_MASK) + (offset & LR35902::NIBBLE_MASK)) >
                LR35902::NIBBLE_MASK) {
                registers->flags.half_carry = true;
            } else {
                registers->flags.half_carry = false;
            }

            /* Figure out if full carry happened */
            if (((registers->stack_pointer & LR35902::BYTE_MASK) + (offset & LR35902::BYTE_MASK)) >
                LR35902::BYTE_MASK) {
                registers->flags.carry = true;
            } else {
                registers->flags.carry = false;
            }

            /* Always clear subtract and zero flags */
            registers->flags.subtract = false;
            registers->flags.zero     = false;

            registers->H = (value >> LR35902::BITS_PER_BYTE) & LR35902::BYTE_MASK;
            registers->L = value & LR35902::BYTE_MASK;
        } break;
        case LD_SP_HL:
            registers->stack_pointer = ((static_cast<uint16_t>(registers->H) << LR35902::BITS_PER_BYTE) | registers->L);
            break;
    }
}

}    // namespace LR35902
