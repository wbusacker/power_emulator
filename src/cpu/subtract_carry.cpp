#include <cpu_instructions_groups.h>

namespace LR35902 {

void subtract_carry(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        /* Figure out if this is the immediate instruction or not */
        if (instruction == LR35902::SBC_N) {
            arg_variable = get_next_pc(memory, registers);
        } else {
            arg_variable = get_hl_indirect(memory, registers);
        }
    } else {
        arg_variable = *arg_source;
    }

    bool apply_carry = registers->flags.carry;

    struct LR35902::CPU_state_flags_t holdover_flags = {false, false, false, false};

    /* If we need to carry in, first perform an addition */
    if (apply_carry) {
        arg_variable = common_add(&registers->flags, arg_variable, 1);

        /* Save the holdover flags */
        holdover_flags.carry      = registers->flags.carry;
        holdover_flags.half_carry = registers->flags.half_carry;
    }

    /* Do the normal addition */
    registers->A = common_subtract(&registers->flags, registers->A, arg_variable);

    if (apply_carry == true) {
        /* Merge the two flag operations */

        registers->flags.carry |= holdover_flags.carry;
        registers->flags.half_carry |= holdover_flags.half_carry;
    }
}

}    // namespace LR35902