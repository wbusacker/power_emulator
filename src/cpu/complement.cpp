#include <cpu_instructions_groups.h>

namespace LR35902 {

void complement(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Flip A */
    registers->A ^= LR35902::BYTE_MASK;

    /* Always set the flags for subtract and half carry */
    registers->flags.subtract   = true;
    registers->flags.half_carry = true;
}

}    // namespace LR35902