#include <cpu_instructions_groups.h>

namespace LR35902 {

void add_hl(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Obtain the two 16-bit values to add */
    uint32_t lhs = (static_cast<uint32_t>(registers->H) << LR35902::BITS_PER_BYTE) | registers->L;

    uint32_t rhs = 0;
    switch (instruction) {
        case ADD_HL_BC:
            rhs = (static_cast<uint32_t>(registers->B) << LR35902::BITS_PER_BYTE) | registers->C;
            break;
        case ADD_HL_DE:
            rhs = (static_cast<uint32_t>(registers->D) << LR35902::BITS_PER_BYTE) | registers->E;
            break;
        case ADD_HL_HL:
            rhs = lhs;
            break;
        case ADD_HL_SP:
            rhs = registers->stack_pointer;
    }

    uint32_t full_answer = lhs + rhs;

    /* Check if a carry out would have occured */
    if (full_answer > LR35902::WORD_MASK) {
        registers->flags.carry = true;
    } else {
        registers->flags.carry = false;
    }

    /* Check if a half-carry would have occured */
    if (((lhs & LR35902::BIT_12_MASK) + (rhs & LR35902::BIT_12_MASK)) > LR35902::BIT_12_MASK) {
        registers->flags.half_carry = true;
    } else {
        registers->flags.half_carry = false;
    }

    /* Always set subtract to false */
    registers->flags.subtract = false;

    /* Zero flag is unaltered in this instruction */

    /* Keep the answer in the correct range */
    full_answer &= LR35902::WORD_MASK;

    /* Put the answer back into HL */
    registers->H = (full_answer >> LR35902::BITS_PER_BYTE) & LR35902::BYTE_MASK;
    registers->L = full_answer & LR35902::BYTE_MASK;
}

}    // namespace LR35902