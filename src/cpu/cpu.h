#ifndef GAMEBOY_CPU_H
#define GAMEBOY_CPU_H
#include <common_memory_bus.h>
#include <cpu_const.h>
#include <cpu_instructions_groups.h>
#include <cpu_types_data.h>
#include <function_tracer.h>
namespace LR35902 {

class Gameboy_cpu : public Common_memory::CMB_interface {
    public:
    Gameboy_cpu(Common_memory::Common_memory_bus* main_bus);

    void    cycle(void);
    uint8_t read(uint16_t addr);
    void    write(uint16_t addr, uint8_t data);

    void set_register_state(Register_file_t* set);
    void get_register_state(Register_file_t* get);

    virtual void perform_instruction(uint8_t instruction);
    virtual void perform_cb_instruction(uint8_t instruction);

    void raise_interrupt(LR35902::SERVICABLE_INTERRUPTS vector) { interrupt_pending_flags |= vector; }

    void install_function_tracer(Debug::Function_tracer* tracer) { function_tracer = tracer; }

    private:
    bool     master_interrupt_flag;
    bool     cpu_active_flag;
    uint8_t  last_instruction;
    uint16_t last_address;

    uint8_t interrupt_pending_flags;
    uint8_t interrupt_enable_flags;

    uint64_t     cycles;
    void         print_cpu_state(void);
    virtual void interrupt_processing(void);

    Common_memory::Common_memory_bus* memory;
    Debug::Function_tracer*           function_tracer;

    Register_file_t registers;

    LR35902::Instruction_function main_map[LR35902::INSTRUCTION_SET_SIZE];
    LR35902::Instruction_function cb_map[LR35902::INSTRUCTION_SET_SIZE];
};

}    // namespace LR35902

#endif
