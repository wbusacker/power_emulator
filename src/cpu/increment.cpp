#include <cpu_instructions_groups.h>

namespace LR35902 {

void increment(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source =
        get_register_pointer(registers, (instruction >> LR35902::R_FIELD_SIZE) & LR35902::R_FIELD_MASK);

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        arg_variable = get_hl_indirect(memory, registers);
    } else {
        arg_variable = *arg_source;
    }

    /* Save off the carry flag so it isn't modified by this instruction */
    bool carry = registers->flags.carry;

    /* Effectively treat this as a normal add 1 */
    arg_variable = common_add(&registers->flags, arg_variable, 1);

    /* Reset the carry flag */
    registers->flags.carry = carry;

    /* Put the value back */
    if (arg_source == nullptr) {
        store_hl_indirect(memory, registers, arg_variable);
    } else {
        *arg_source = arg_variable;
    }
}

}    // namespace LR35902