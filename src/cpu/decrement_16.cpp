#include <cpu_instructions_groups.h>

namespace LR35902 {

void decrement_16(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    switch (instruction) {
        case DEC_BC:
            registers->C--;
            if (registers->C == 0xFF) {
                registers->B--;
            }
            break;
        case DEC_DE:
            registers->E--;
            if (registers->E == 0xFF) {
                registers->D--;
            }
            break;
        case DEC_HL:
            registers->L--;
            if (registers->L == 0xFF) {
                registers->H--;
            }
            break;
        case DEC_SP:
            registers->stack_pointer--;
            break;
    }
}

}    // namespace LR35902