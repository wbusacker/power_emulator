#include <cpu_instructions_groups.h>

namespace LR35902 {

void increment_16(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    switch (instruction) {
        case INC_BC:
            registers->C++;
            if (registers->C == 0) {
                registers->B++;
            }
            break;
        case INC_DE:
            registers->E++;
            if (registers->E == 0) {
                registers->D++;
            }
            break;
        case INC_HL:
            registers->L++;
            if (registers->L == 0) {
                registers->H++;
            }
            break;
        case INC_SP:
            registers->stack_pointer++;
            break;
    }
}

}    // namespace LR35902