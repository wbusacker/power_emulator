#include <cpu_instructions_groups.h>

namespace LR35902 {

void change_carry_flag(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Change behavior on the exact instruction */
    switch (instruction) {
        case LR35902::CCF:
            registers->flags.carry ^= true;
            break;
        case LR35902::SCF:
            registers->flags.carry = true;
            break;
    }

    /* Always reset subtract and half_carry */
    registers->flags.half_carry = false;
    registers->flags.subtract   = false;
}

}    // namespace LR35902