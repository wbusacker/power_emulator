#include <cpu_instructions_groups.h>

namespace LR35902 {

void load_indirect(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out the address to use */
    uint16_t indirect_address = 0;

    /* Figure out where to put them */
    switch (instruction) {
        case LD_A_IBC:
            indirect_address = (static_cast<uint16_t>(registers->B) << BITS_PER_BYTE) | registers->C;
            break;
        case LD_A_IDE:
            indirect_address = (static_cast<uint16_t>(registers->D) << BITS_PER_BYTE) | registers->E;
            break;
        case LD_A_IHLP:
            indirect_address = (static_cast<uint16_t>(registers->H) << BITS_PER_BYTE) | registers->L;

            /* Increment HL */
            registers->H = (indirect_address + 1) >> BITS_PER_BYTE;
            registers->L = (indirect_address + 1) & BYTE_MASK;
            break;
        case LD_A_IHLS:
            indirect_address = (static_cast<uint16_t>(registers->H) << BITS_PER_BYTE) | registers->L;

            /* Decrement HL */
            registers->H = (indirect_address - 1) >> BITS_PER_BYTE;
            registers->L = (indirect_address - 1) & BYTE_MASK;
            break;
        case LD_A_INN:
            uint8_t indirect_l = get_next_pc(memory, registers);
            uint8_t indirect_h = get_next_pc(memory, registers);
            indirect_address   = (static_cast<uint16_t>(indirect_h) << BITS_PER_BYTE) | indirect_l;
            break;
    }

    /* Grab the value and assign it into A */
    registers->A = memory->read(indirect_address);
}

}    // namespace LR35902