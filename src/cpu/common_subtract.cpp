#include <cpu_instructions_groups.h>

namespace LR35902 {

uint8_t common_subtract(CPU_state_flags_t* flags, uint8_t left_arg, uint8_t right_arg) {

    /* Perform subtraction on the lower nibble to see if we would borrow */

    uint8_t nibble_lhs = LR35902::BIT_4_MASK | (left_arg & LR35902::NIBBLE_MASK);
    uint8_t nibble_rhs = right_arg & LR35902::NIBBLE_MASK;

    uint8_t nibble_result = nibble_lhs - nibble_rhs;

    /* Check to see if we borrowed */
    if ((nibble_result & LR35902::BIT_4_MASK) != 0) {
        /* Borrow did not happen, unset half carry */
        flags->half_carry = false;
    } else {
        flags->half_carry = true;
    }

    /* Do the full byte subtraction */
    uint16_t full_lhs = LR35902::BIT_8_MASK | left_arg;
    uint16_t full_rhs = right_arg;

    uint16_t full_result = full_lhs - full_rhs;

    /* Check to see if we borrowed */
    if ((full_result & LR35902::BIT_8_MASK) != 0) {
        /* Borrow did not happen, unset full carry */
        flags->carry = false;
    } else {
        flags->carry = true;
    }

    /* Check to see if we landed at zero */
    if ((full_result & LR35902::BYTE_MASK) == 0) {
        flags->zero = true;
    } else {
        flags->zero = false;
    }

    /* Always set subtract true*/
    flags->subtract = true;

    return (full_result & LR35902::BYTE_MASK);
}

}    // namespace LR35902