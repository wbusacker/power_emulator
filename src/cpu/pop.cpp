#include <cpu_instructions_groups.h>

namespace LR35902 {

void pop(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Grab the two pieces of data from memory */
    uint8_t lower_byte = memory->read(registers->stack_pointer++);
    uint8_t upper_byte = memory->read(registers->stack_pointer++);

    /* Figure out where these two pieces of data go */
    switch (instruction) {
        case POP_AF:
            registers->A = upper_byte;
            {
                /* Special handling for the data flags, this should
                    coerce the compiler into doing the same single
                    byte assignment like the others */
                LR35902::CPU_state_flags_t* struct_ptr = &(registers->flags);
                uint8_t*                    int_ptr    = reinterpret_cast<uint8_t*>(struct_ptr);
                *int_ptr = lower_byte & (LR35902::NIBBLE_MASK << LR35902::BITS_PER_NIBBLE);
            }
            break;
        case POP_BC:
            registers->B = upper_byte;
            registers->C = lower_byte;
            break;
        case POP_DE:
            registers->D = upper_byte;
            registers->E = lower_byte;
            break;
        case POP_HL:
            registers->H = upper_byte;
            registers->L = lower_byte;
            break;
    }
}

}    // namespace LR35902
