#include <cpu.h>
#include <stdio.h>

namespace LR35902 {

void Gameboy_cpu::cycle(void) {

    /* Even if the CPU is halted, always check for interrupts */
    interrupt_processing();

    /* Always increment number of passed cycles */
    cycles++;

    /* Only process an instruction if the CPU is not halted */
    if (cpu_active_flag == true) {
        if (registers.cycle_time <= 0) {
            /* We're free to issue another instruction */

            /* Fetch the instruction to operate on */
            last_address        = registers.program_counter;
            uint8_t instruction = get_next_pc(memory, &registers);
            last_instruction    = instruction;
            registers.cb_mode   = false;
            // print_cpu_state();

            // if ((function_tracer != nullptr) &&
            //     ((instruction == LR35902::CALL_NN) || (instruction == LR35902::CALL_C_NN) ||
            //      (instruction == LR35902::CALL_Z_NN) || (instruction == LR35902::CALL_NC_NN) ||
            //      (instruction == LR35902::CALL_NZ_NN))) {
            //     /* Read in the next two bytes from the instruction to get the address to being the trace */
            //     uint16_t lower_addr = memory->read(registers.program_counter);
            //     uint16_t upper_addr = memory->read(registers.program_counter + 1);
            //     uint16_t func_addr  = (upper_addr << 8) | lower_addr;

            //     function_tracer->trace_function(func_addr);
            // }

            // if (function_tracer != nullptr) {
            //     function_tracer->record_instruction_context(last_address);
            // }

            if (last_address == 0x4624) {
                // printf("Entering Function\n");
            }

            registers.cycle_time = instruction_timing[instruction];
            /* If the instruction is a nullptr, check to see if we have special
                handling for that instruction
            */
            if (main_map[instruction] == nullptr) {
                switch (instruction) {
                    case LR35902::HALT:
                    case LR35902::STOP:
                        cpu_active_flag = false;
                        break;
                    case LR35902::DI:
                        master_interrupt_flag = false;
                        break;
                    case LR35902::EI:
                        master_interrupt_flag = true;
                        break;
                    case LR35902::PREFIX_CB:
                        registers.cb_mode = true;
                        // if (function_tracer != nullptr) {
                        //     function_tracer->record_instruction_context(registers.program_counter);
                        // }
                        perform_cb_instruction(get_next_pc(memory, &registers));
                        break;
                    case LR35902::NOP:
                        break;
                    default:
                        printf("WARNING: Unimplemented instruction %02X\n", instruction);
                        break;
                }
            } else {
                if (instruction == LR35902::RETI) {
                    master_interrupt_flag = true;
                }
                perform_instruction(instruction);
            }
        }

        /* Always decrement the instruction counter */
        registers.cycle_time--;
    }
}

}    // namespace LR35902