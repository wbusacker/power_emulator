#include <cpu_instructions_groups.h>

namespace LR35902 {

void jump_target(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    uint8_t lower_addr = 0;
    uint8_t upper_addr = 0;
    /* If we're doing an HL Indirect jump, use that for
        addresses instead
    */
    if (instruction == LR35902::JP_IHL) {
        lower_addr = registers->L;
        upper_addr = registers->H;
    } else {
        /* Grab the next two bytes from memory */
        lower_addr = get_next_pc(memory, registers);
        upper_addr = get_next_pc(memory, registers);
    }

    /* Figure out if we actually should jump or not */
    bool should_jump = false;
    switch (instruction) {
        case LR35902::JP_NZ_NN:
            should_jump = ! registers->flags.zero;
            break;
        case LR35902::JP_Z_NN:
            should_jump = registers->flags.zero;
            break;
        case LR35902::JP_NC_NN:
            should_jump = ! registers->flags.carry;
            break;
        case LR35902::JP_C_NN:
            should_jump = registers->flags.carry;
            break;
        case LR35902::JP_IHL:
        case LR35902::JP_NN:
        default: /* Jump Unconditionally*/
            should_jump = true;
    }

    if (should_jump) {
        /* Reconfigure the program counter */
        registers->program_counter = (static_cast<uint16_t>(upper_addr) << LR35902::BITS_PER_BYTE) | lower_addr;
        /* Since we're jumping, increase the cycle time delay */
        registers->cycle_time += LR35902::JUMP_ADDITIONAL_CYCLES;
    }
}

}    // namespace LR35902