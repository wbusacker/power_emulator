#include <cpu_instructions_groups.h>

namespace LR35902 {

void swap(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {
    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    uint8_t value = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        /* Figure out if this is the immediate instruction or not */
        value = get_hl_indirect(memory, registers);
        registers->cycle_time += LR35902::CB_INDIRECT_ADDITIONAL_CYCLES;
    } else {
        value = *arg_source;
    }

    uint8_t lower_nibble = value & LR35902::NIBBLE_MASK;
    value                = (value >> LR35902::BITS_PER_NIBBLE) & LR35902::NIBBLE_MASK;
    value |= (lower_nibble << LR35902::BITS_PER_NIBBLE);

    /* Put the value back */
    if (arg_source == nullptr) {
        store_hl_indirect(memory, registers, value);
    } else {
        *arg_source = value;
    }

    /* If the result is zero, record that */
    if (value == 0) {
        registers->flags.zero = true;
    } else {
        registers->flags.zero = false;
    }

    registers->flags.carry      = false;
    registers->flags.half_carry = false;
    registers->flags.subtract   = false;
}

}    // namespace LR35902
