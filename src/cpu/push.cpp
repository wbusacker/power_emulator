#include <cpu_instructions_groups.h>
#include <string.h>

namespace LR35902 {

void push(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Collect the two pieces of data to store */
    uint8_t lower_byte = 0;
    uint8_t upper_byte = 0;

    /* Figure out where these two pieces of data go */
    switch (instruction) {
        case PUSH_AF:
            upper_byte = registers->A;
            {
                /* Special handling for the data flags, this should
                    coerce the compiler into doing the same single
                    byte assignment like the others */
                LR35902::CPU_state_flags_t* struct_ptr = &(registers->flags);
                uint8_t*                    int_ptr    = reinterpret_cast<uint8_t*>(struct_ptr);
                lower_byte                             = *int_ptr;
            }
            break;
        case PUSH_BC:
            upper_byte = registers->B;
            lower_byte = registers->C;
            break;
        case PUSH_DE:
            upper_byte = registers->D;
            lower_byte = registers->E;
            break;
        case PUSH_HL:
            upper_byte = registers->H;
            lower_byte = registers->L;
            break;
    }

    /* Store off the data */
    memory->write(--(registers->stack_pointer), upper_byte);
    memory->write(--(registers->stack_pointer), lower_byte);
}

}    // namespace LR35902
