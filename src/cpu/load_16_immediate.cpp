#include <cpu_instructions_groups.h>

namespace LR35902 {

void load_16_immediate(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Get the 16 bits to read in */

    uint8_t lsb = get_next_pc(memory, registers);
    uint8_t msb = get_next_pc(memory, registers);

    /* Figure out where to put them */
    switch (instruction) {
        case LD_BC_NN:
            registers->B = msb;
            registers->C = lsb;
            break;
        case LD_DE_NN:
            registers->D = msb;
            registers->E = lsb;
            break;
        case LD_HL_NN:
            registers->H = msb;
            registers->L = lsb;
            break;
        case LD_SP_NN:
            registers->stack_pointer = (static_cast<uint16_t>(msb) << BITS_PER_BYTE) | lsb;
            break;
    }
}

}    // namespace LR35902