#include <cpu_instructions_groups.h>

namespace LR35902 {

void call(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Grab the next two bytes from memory */
    uint8_t lower_addr = get_next_pc(memory, registers);
    uint8_t upper_addr = get_next_pc(memory, registers);

    /* Figure out if we actually should jump or not */
    bool should_jump = true;
    switch (instruction) {
        case LR35902::CALL_NZ_NN:
            should_jump = ! registers->flags.zero;
            break;
        case LR35902::CALL_Z_NN:
            should_jump = registers->flags.zero;
            break;
        case LR35902::CALL_NC_NN:
            should_jump = ! registers->flags.carry;
            break;
        case LR35902::CALL_C_NN:
            should_jump = registers->flags.carry;
            break;
        default: /* CALL Unconditionally*/
            should_jump = true;
    }

    if (should_jump) {

        /* Store the current program counter into the stack */
        memory->write(--(registers->stack_pointer),
                      (registers->program_counter >> LR35902::BITS_PER_BYTE) & LR35902::BYTE_MASK);
        memory->write(--(registers->stack_pointer), registers->program_counter & LR35902::BYTE_MASK);

        /* Reconfigure the program counter */
        registers->program_counter = (static_cast<uint16_t>(upper_addr) << LR35902::BITS_PER_BYTE) | lower_addr;

        /* Since we're jumping, increase the cycle time delay */
        registers->cycle_time += LR35902::CALL_ADDITIONAL_CYCLES;
    }
}

}    // namespace LR35902