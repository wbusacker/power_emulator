#include <cpu_instructions_groups.h>

namespace LR35902 {

void load_immediate(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* No matter what, go grab the value to obtain */
    uint8_t data = get_next_pc(memory, registers);

    /* Figure out where we are storing it */
    uint8_t* target_register =
        get_register_pointer(registers, (instruction >> LR35902::R_FIELD_SIZE) & LR35902::R_FIELD_MASK);

    /* If this is an indirect call, write back out to memory */
    if (target_register == nullptr) {
        store_hl_indirect(memory, registers, data);
    } else {
        *target_register = data;
    }
}

}    // namespace LR35902