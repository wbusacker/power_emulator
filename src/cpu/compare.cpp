#include <cpu_instructions_groups.h>

namespace LR35902 {

void compare(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        /* Figure out if this is the immediate instruction or not */
        if (instruction == LR35902::CP_N) {
            arg_variable = get_next_pc(memory, registers);
        } else {
            arg_variable = get_hl_indirect(memory, registers);
        }
    } else {
        arg_variable = *arg_source;
    }

    // if (registers->A == arg_variable) {
    //     registers->flags.zero = true;
    // } else {
    //     registers->flags.zero = false;
    // }

    // if (registers->A < arg_variable) {
    //     registers->flags.carry = true;
    // } else {
    //     registers->flags.carry = false;
    // }

    // /* Since we are doing subtraction, apply 2's compliment to the arg variable */
    // arg_variable ^= BYTE_MASK;
    // arg_variable += 1;

    // /* We don't actually store the results of this subtraction */
    // common_add(&registers->flags, registers->A, arg_variable);

    /* Ensure that the subtract flag is set */
    // registers->flags.subtract = true;

    common_subtract(&registers->flags, registers->A, arg_variable);
}

}    // namespace LR35902