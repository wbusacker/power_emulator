#include <cpu.h>
#include <stdio.h>

namespace LR35902 {

void Gameboy_cpu::write(uint16_t addr, uint8_t data) {

    switch (addr) {
        case LR35902::INTERRUPT_ENABLE_ADDR:
            interrupt_enable_flags = data;
            break;
        case LR35902::INTERRUPT_FLAG_ADDR:
            interrupt_pending_flags = data;
            break;
    }
}

uint8_t Gameboy_cpu::read(uint16_t addr) {

    uint8_t data = 0;

    switch (addr) {
        case LR35902::INTERRUPT_ENABLE_ADDR:
            data = interrupt_enable_flags;
            break;
        case LR35902::INTERRUPT_FLAG_ADDR:
            data = interrupt_pending_flags;
            break;
    }

    return data;
}

}    // namespace LR35902