#ifndef GAMEBOY_CPU_CONST_H
#define GAMEBOY_CPU_CONST_H

#include <stdint.h>

namespace LR35902 {

const uint8_t  BYTE_MASK            = 0xFF;
const uint8_t  NIBBLE_MASK          = 0xF;
const uint16_t WORD_MASK            = 0xFFFF;
const uint16_t BIT_12_MASK          = 0xFFF;
const uint8_t  BITS_PER_BYTE        = 8;
const uint8_t  BITS_PER_NIBBLE      = 4;
const uint8_t  R_FIELD_MASK         = 0x7;
const uint8_t  R_FIELD_SIZE         = 3;
const uint8_t  UNSIGNED_NEGATIVE_1  = 0xFF;
const uint16_t INSTRUCTION_SET_SIZE = 256;
const uint8_t  BIT_0_MASK           = 0x01;
const uint8_t  BIT_2_MASK           = 0x04;
const uint8_t  BIT_4_MASK           = 0x10;
const uint8_t  BIT_7_MASK           = 0x80;
const uint16_t BIT_8_MASK           = 0x100;
const uint16_t OFFSET_BASE          = 0xFF00;

const uint16_t INTERRUPT_FLAG_ADDR       = 0xFF0F;
const uint16_t INTERRUPT_ENABLE_ADDR     = 0xFFFF;
const uint8_t  SERVICABLE_INTERRUPT_MASK = 0x1F;

const uint8_t JUMP_ADDITIONAL_CYCLES        = 1;
const uint8_t CALL_ADDITIONAL_CYCLES        = 3;
const uint8_t RETURN_ADDITIONAL_CYCLES      = 3;
const uint8_t CB_INDIRECT_ADDITIONAL_CYCLES = 2;

const uint8_t DAA_ONES_OVERFLOW_COMPENSATION = 0x06;
const uint8_t DAA_TENS_OVERFLOW_COMPENSATION = 0x60;
const uint8_t DAA_DECIMAL_OVERFLOW_CHECK     = 9;

enum RESET_TARGETS : uint8_t {
    RESET_00 = 0x00,
    RESET_08 = 0x08,
    RESET_10 = 0x10,
    RESET_18 = 0x18,
    RESET_20 = 0x20,
    RESET_28 = 0x28,
    RESET_30 = 0x30,
    RESET_38 = 0x38
};

enum SERVICABLE_INTERRUPTS : uint8_t {
    VBLANK_INTERRUPT     = 0b00001,
    LCDC_INTERRUPT       = 0b00010,
    TIMER_INTERRUPT      = 0b00100,
    SIO_INTERRUPT        = 0b01000,
    CONTROLLER_INTERRUPT = 0b10000,
};

enum INTERRUPT_JUMP_POINT : uint16_t {
    VBLANK_INTERRUPT_TARGET     = 0x0040,
    LCDC_INTERRUPT_TARGET       = 0x0048,
    TIMER_INTERRUPT_TARGET      = 0x0050,
    SIO_INTERRUPT_TARGET        = 0x0058,
    CONTROLLER_INTERRUPT_TARGET = 0x0060
};

/* Special character meaning in this chart

I  = Indirect, take the proceeding register and use the value at that address
N  = Immediate Byte
NN = Immediate Word
P  = Immediate Increment
S  = Immediate Subtract

*/

// clang-format off

enum INSTRUCTION_MNEMONICS : uint8_t {
    NOP      = 0x00, LD_BC_NN = 0x01, LD_IBC_A  = 0x02, INC_BC     = 0x03, INC_B      = 0x04, DEC_B    = 0x05, LD_B_N    = 0x06, RLCA     = 0x07, LD_INN_SP    = 0x08, ADD_HL_BC = 0x09, LD_A_IBC  = 0x0A, DEC_BC     = 0x0B, INC_C      = 0x0C, DEC_C      = 0x0D, LD_C_N    = 0x0E, RRCA    = 0x0F,
    STOP     = 0x10, LD_DE_NN = 0x11, LD_IDE_A  = 0x12, INC_DE     = 0x13, INC_D      = 0x14, DEC_D    = 0x15, LD_D_N    = 0x16, RLA      = 0x17, JR_N         = 0x18, ADD_HL_DE = 0x19, LD_A_IDE  = 0x1A, DEC_DE     = 0x1B, INC_E      = 0x1C, DEC_E      = 0x1D, LD_E_N    = 0x1E, RRA     = 0x1F,
    JR_NZ_N  = 0x20, LD_HL_NN = 0x21, LD_IHLP_A = 0x22, INC_HL     = 0x23, INC_H      = 0x24, DEC_H    = 0x25, LD_H_N    = 0x26, DAA      = 0x27, JR_Z_N       = 0x28, ADD_HL_HL = 0x29, LD_A_IHLP = 0x2A, DEC_HL     = 0x2B, INC_L      = 0x2C, DEC_L      = 0x2D, LD_L_N    = 0x2E, CPL     = 0x2F,
    JR_NC_N  = 0x30, LD_SP_NN = 0x31, LD_IHLS_A = 0x32, INC_SP     = 0x33, INC_IHL    = 0x34, DEC_IHL  = 0x35, LD_IHL_N  = 0x36, SCF      = 0x37, JR_C_N       = 0x38, ADD_HL_SP = 0x39, LD_A_IHLS = 0x3A, DEC_SP     = 0x3B, INC_A      = 0x3C, DEC_A      = 0x3D, LD_A_N    = 0x3E, CCF     = 0x3F,
    LD_B_B   = 0x40, LD_B_C   = 0x41, LD_B_D    = 0x42, LD_B_E     = 0x43, LD_B_H     = 0x44, LD_B_L   = 0x45, LD_B_IHL  = 0x46, LD_B_A   = 0x47, LD_C_B       = 0x48, LD_C_C    = 0x49, LD_C_D    = 0x4A, LD_C_E     = 0x4B, LD_C_H     = 0x4C, LD_C_L     = 0x4D, LD_C_IHL  = 0x4E, LD_C_A  = 0x4F,
    LD_D_B   = 0x50, LD_D_C   = 0x51, LD_D_D    = 0x52, LD_D_E     = 0x53, LD_D_H     = 0x54, LD_D_L   = 0x55, LD_D_IHL  = 0x56, LD_D_A   = 0x57, LD_E_B       = 0x58, LD_E_C    = 0x59, LD_E_D    = 0x5A, LD_E_E     = 0x5B, LD_E_H     = 0x5C, LD_E_L     = 0x5D, LD_E_IHL  = 0x5E, LD_E_A  = 0x5F,
    LD_H_B   = 0x60, LD_H_C   = 0x61, LD_H_D    = 0x62, LD_H_E     = 0x63, LD_H_H     = 0x64, LD_H_L   = 0x65, LD_H_IHL  = 0x66, LD_H_A   = 0x67, LD_L_B       = 0x68, LD_L_C    = 0x69, LD_L_D    = 0x6A, LD_L_E     = 0x6B, LD_L_H     = 0x6C, LD_L_L     = 0x6D, LD_L_IHL  = 0x6E, LD_L_A  = 0x6F,
    LD_IHL_B = 0x70, LD_IHL_C = 0x71, LD_IHL_D  = 0x72, LD_IHL_E   = 0x73, LD_IHL_H   = 0x74, LD_IHL_L = 0x75, HALT      = 0x76, LD_IHL_A = 0x77, LD_A_B       = 0x78, LD_A_C    = 0x79, LD_A_D    = 0x7A, LD_A_E     = 0x7B, LD_A_H     = 0x7C, LD_A_L     = 0x7D, LD_A_IHL  = 0x7E, LD_A_A  = 0x7F,
    ADD_A_B  = 0x80, ADD_A_C  = 0x81, ADD_A_D   = 0x82, ADD_A_E    = 0x83, ADD_A_H    = 0x84, ADD_A_L  = 0x85, ADD_A_IHL = 0x86, ADD_A_A  = 0x87, ADC_A_B      = 0x88, ADC_A_C   = 0x89, ADC_A_D   = 0x8A, ADC_A_E    = 0x8B, ADC_A_H    = 0x8C, ADC_A_L    = 0x8D, ADC_A_IHL = 0x8E, ADC_A_A = 0x8F,
    SUB_B    = 0x90, SUB_C    = 0x91, SUB_D     = 0x92, SUB_E      = 0x93, SUB_H      = 0x94, SUB_L    = 0x95, SUB_IHL   = 0x96, SUB_A    = 0x97, SBC_A_B      = 0x98, SBC_A_C   = 0x99, SBC_A_D   = 0x9A, SBC_A_E    = 0x9B, SBC_A_H    = 0x9C, SBC_A_L    = 0x9D, SBC_A_IHL = 0x9E, SBC_A_A = 0x9F,
    AND_B    = 0xA0, AND_C    = 0xA1, AND_D     = 0xA2, AND_E      = 0xA3, AND_H      = 0xA4, AND_L    = 0xA5, AND_IHL   = 0xA6, AND_A    = 0xA7, XOR_B        = 0xA8, XOR_C     = 0xA9, XOR_D     = 0xAA, XOR_E      = 0xAB, XOR_H      = 0xAC, XOR_L      = 0xAD, XOR_IHL   = 0xAE, XOR_A   = 0xAF,
    OR_B     = 0xB0, OR_C     = 0xB1, OR_D      = 0xB2, OR_E       = 0xB3, OR_H       = 0xB4, OR_L     = 0xB5, OR_IHL    = 0xB6, OR_A     = 0xB7, CP_B         = 0xB8, CP_C      = 0xB9, CP_D      = 0xBA, CP_E       = 0xBB, CP_H       = 0xBC, CP_L       = 0xBD, CP_IHL    = 0xBE, CP_A    = 0xBF,
    RET_NZ   = 0xC0, POP_BC   = 0xC1, JP_NZ_NN  = 0xC2, JP_NN      = 0xC3, CALL_NZ_NN = 0xC4, PUSH_BC  = 0xC5, ADD_N     = 0xC6, RST_00   = 0xC7, RET_Z        = 0xC8, RET       = 0xC9, JP_Z_NN   = 0xCA, PREFIX_CB  = 0xCB, CALL_Z_NN  = 0xCC, CALL_NN    = 0xCD, ADC_N     = 0xCE, RST_08  = 0xCF,
    RET_NC   = 0xD0, POP_DE   = 0xD1, JP_NC_NN  = 0xD2, /* No Opcode D3 */ CALL_NC_NN = 0xD4, PUSH_DE  = 0xD5, SUB_N     = 0xD6, RST_10   = 0xD7, RET_C        = 0xD8, RETI      = 0xD9, JP_C_NN   = 0xDA, /* No OpCode DB */ CALL_C_NN  = 0xDC, /* No OpCode DD */ SBC_N     = 0xDE, RST_18  = 0xDF,
    LDH_IN_A = 0xE0, POP_HL   = 0xE1, LD_IC_A   = 0xE2, /* No OpCode E3 */ /* No OpCode E4 */ PUSH_HL  = 0xE5, AND_N     = 0xE6, RST_20   = 0xE7, ADD_SP_N     = 0xE8, JP_IHL    = 0xE9, LD_INN_A  = 0xEA, /* No OpCode EB */ /* No OpCode EC */ /* No OpCode ED */ XOR_N     = 0xEE, RST_28  = 0xEF,
    LDH_A_IN = 0xF0, POP_AF   = 0xF1, LD_A_IC   = 0xF2, DI         = 0xF3, /* No OpCode F4 */ PUSH_AF  = 0xF5, OR_N      = 0xF6, RST_30   = 0xF7, LD_HL_SP_INP = 0xF8, LD_SP_HL  = 0xF9, LD_A_INN  = 0xFA, EI         = 0xFB, /* No OpCode FC */ /* No OpCode FD */ CP_N      = 0xFE, RST_38  = 0xFF
};

enum CB_MNEMONICS : uint8_t {
    RLC_B    = 0x00, RLC_C    = 0x01, RLC_D     = 0x02, RLC_E      = 0x03, RLC_H      = 0x04, RLC_L    = 0x05, RLC_IHL   = 0x06, RLC_A    = 0x07, RRC_B        = 0x08, RRC_C     = 0x09, RRC_D     = 0x0A, RRC_E      = 0x0B, RRC_H      = 0x0C, RRC_L      = 0x0D, RRC_IHL   = 0x0E, RRC_A   = 0x0F,
    RL_B     = 0x10, RL_C     = 0x11, RL_D      = 0x12, RL_E       = 0x13, RL_H       = 0x14, RL_L     = 0x15, RL_IHL    = 0x16, RL_A     = 0x17, RR_B         = 0x18, RR_C      = 0x19, RR_D      = 0x1A, RR_E       = 0x1B, RR_H       = 0x1C, RR_L       = 0x1D, RR_IHL    = 0x1E, RR_A    = 0x1F,
    SLA_B    = 0x20, SLA_C    = 0x21, SLA_D     = 0x22, SLA_E      = 0x23, SLA_H      = 0x24, SLA_L    = 0x25, SLA_IHL   = 0x26, SLA_A    = 0x27, SRA_B        = 0x28, SRA_C     = 0x29, SRA_D     = 0x2A, SRA_E      = 0x2B, SRA_H      = 0x2C, SRA_L      = 0x2D, SRA_IHL   = 0x2E, SRA_A   = 0x2F,
    SWAP_B   = 0x30, SWAP_C   = 0x31, SWAP_D    = 0x32, SWAP_E     = 0x33, SWAP_H     = 0x34, SWAP_L   = 0x35, SWAP_IHL  = 0x36, SWAP_A   = 0x37, SRL_B        = 0x38, SRL_C     = 0x39, SRL_D     = 0x3A, SRL_E      = 0x3B, SRL_H      = 0x3C, SRL_L      = 0x3D, SRL_IHL   = 0x3E, SRL_A   = 0x3F,
    BIT_0_B  = 0x40, BIT_0_C  = 0x41, BIT_0_D   = 0x42, BIT_0_E    = 0x43, BIT_0_H    = 0x44, BIT_0_L  = 0x45, BIT_0_IHL = 0x46, BIT_0_A  = 0x47, BIT_1_B      = 0x48, BIT_1_C   = 0x49, BIT_1_D   = 0x4A, BIT_1_E    = 0x4B, BIT_1_H    = 0x4C, BIT_1_L    = 0x4D, BIT_1_IHL = 0x4E, BIT_1_A = 0x4F,
    BIT_2_B  = 0x50, BIT_2_C  = 0x51, BIT_2_D   = 0x52, BIT_2_E    = 0x53, BIT_2_H    = 0x54, BIT_2_L  = 0x55, BIT_2_IHL = 0x56, BIT_2_A  = 0x57, BIT_3_B      = 0x58, BIT_3_C   = 0x59, BIT_3_D   = 0x5A, BIT_3_E    = 0x5B, BIT_3_H    = 0x5C, BIT_3_L    = 0x5D, BIT_3_IHL = 0x5E, BIT_3_A = 0x5F,
    BIT_4_B  = 0x60, BIT_4_C  = 0x61, BIT_4_D   = 0x62, BIT_4_E    = 0x63, BIT_4_H    = 0x64, BIT_4_L  = 0x65, BIT_4_IHL = 0x66, BIT_4_A  = 0x67, BIT_5_B      = 0x68, BIT_5_C   = 0x69, BIT_5_D   = 0x6A, BIT_5_E    = 0x6B, BIT_5_H    = 0x6C, BIT_5_L    = 0x6D, BIT_5_IHL = 0x6E, BIT_5_A = 0x6F,
    BIT_6_B  = 0x70, BIT_6_C  = 0x71, BIT_6_D   = 0x72, BIT_6_E    = 0x73, BIT_6_H    = 0x74, BIT_6_L  = 0x75, BIT_6_IHL = 0x76, BIT_6_A  = 0x77, BIT_7_B      = 0x78, BIT_7_C   = 0x79, BIT_7_D   = 0x7A, BIT_7_E    = 0x7B, BIT_7_H    = 0x7C, BIT_7_L    = 0x7D, BIT_7_IHL = 0x7E, BIT_7_A = 0x7F,
    RES_0_B  = 0x80, RES_0_C  = 0x81, RES_0_D   = 0x82, RES_0_E    = 0x83, RES_0_H    = 0x84, RES_0_L  = 0x85, RES_0_IHL = 0x86, RES_0_A  = 0x87, RES_1_B      = 0x88, RES_1_C   = 0x89, RES_1_D   = 0x8A, RES_1_E    = 0x8B, RES_1_H    = 0x8C, RES_1_L    = 0x8D, RES_1_IHL = 0x8E, RES_1_A = 0x8F,
    RES_2_B  = 0x90, RES_2_C  = 0x91, RES_2_D   = 0x92, RES_2_E    = 0x93, RES_2_H    = 0x94, RES_2_L  = 0x95, RES_2_IHL = 0x96, RES_2_A  = 0x97, RES_3_B      = 0x98, RES_3_C   = 0x99, RES_3_D   = 0x9A, RES_3_E    = 0x9B, RES_3_H    = 0x9C, RES_3_L    = 0x9D, RES_3_IHL = 0x9E, RES_3_A = 0x9F,
    RES_4_B  = 0xA0, RES_4_C  = 0xA1, RES_4_D   = 0xA2, RES_4_E    = 0xA3, RES_4_H    = 0xA4, RES_4_L  = 0xA5, RES_4_IHL = 0xA6, RES_4_A  = 0xA7, RES_5_B      = 0xA8, RES_5_C   = 0xA9, RES_5_D   = 0xAA, RES_5_E    = 0xAB, RES_5_H    = 0xAC, RES_5_L    = 0xAD, RES_5_IHL = 0xAE, RES_5_A = 0xAF,
    RES_6_B  = 0xB0, RES_6_C  = 0xB1, RES_6_D   = 0xB2, RES_6_E    = 0xB3, RES_6_H    = 0xB4, RES_6_L  = 0xB5, RES_6_IHL = 0xB6, RES_6_A  = 0xB7, RES_7_B      = 0xB8, RES_7_C   = 0xB9, RES_7_D   = 0xBA, RES_7_E    = 0xBB, RES_7_H    = 0xBC, RES_7_L    = 0xBD, RES_7_IHL = 0xBE, RES_7_A = 0xBF,
    SET_0_B  = 0xC0, SET_0_C  = 0xC1, SET_0_D   = 0xC2, SET_0_E    = 0xC3, SET_0_H    = 0xC4, SET_0_L  = 0xC5, SET_0_IHL = 0xC6, SET_0_A  = 0xC7, SET_1_B      = 0xC8, SET_1_C   = 0xC9, SET_1_D   = 0xCA, SET_1_E    = 0xCB, SET_1_H    = 0xCC, SET_1_L    = 0xCD, SET_1_IHL = 0xCE, SET_1_A = 0xCF,
    SET_2_B  = 0xD0, SET_2_C  = 0xD1, SET_2_D   = 0xD2, SET_2_E    = 0xD3, SET_2_H    = 0xD4, SET_2_L  = 0xD5, SET_2_IHL = 0xD6, SET_2_A  = 0xD7, SET_3_B      = 0xD8, SET_3_C   = 0xD9, SET_3_D   = 0xDA, SET_3_E    = 0xDB, SET_3_H    = 0xDC, SET_3_L    = 0xDD, SET_3_IHL = 0xDE, SET_3_A = 0xDF,
    SET_4_B  = 0xE0, SET_4_C  = 0xE1, SET_4_D   = 0xE2, SET_4_E    = 0xE3, SET_4_H    = 0xE4, SET_4_L  = 0xE5, SET_4_IHL = 0xE6, SET_4_A  = 0xE7, SET_5_B      = 0xE8, SET_5_C   = 0xE9, SET_5_D   = 0xEA, SET_5_E    = 0xEB, SET_5_H    = 0xEC, SET_5_L    = 0xED, SET_5_IHL = 0xEE, SET_5_A = 0xEF,
    SET_6_B  = 0xF0, SET_6_C  = 0xF1, SET_6_D   = 0xF2, SET_6_E    = 0xF3, SET_6_H    = 0xF4, SET_6_L  = 0xF5, SET_6_IHL = 0xF6, SET_6_A  = 0xF7, SET_7_B      = 0xF8, SET_7_C   = 0xF9, SET_7_D   = 0xFA, SET_7_E    = 0xFB, SET_7_H    = 0xFC, SET_7_L    = 0xFD, SET_7_IHL = 0xFE, SET_7_A = 0xFF
};

const char mnemonics[256][128] = {
  "NOP          "  , "LD BC,nn     ", "LD (BC),A    " , "INC BC       "  , "INC B        "  , "DEC B        ", "LD B,n       " , "RLCA         ", "LD (nn),SP   "    , "ADD HL,BC    " , "LD A,(BC)    " , "DEC BC       "  , "INC C        "  , "DEC C        "  , "LD C,n       " , "RRCA         ",
  "STOP         "  , "LD DE,nn     ", "LD (DE),A    " , "INC DE       "  , "INC D        "  , "DEC D        ", "LD D,n       " , "RLA          ", "JR e         "    , "ADD HL,DE    " , "LD A,(DE)    " , "DEC DE       "  , "INC E        "  , "DEC E        "  , "LD E,n       " , "RRA          ",
  "JR NZ,e      "  , "LD HL,nn     ", "LD (HL+),A   " , "INC HL       "  , "INC H        "  , "DEC H        ", "LD H,n       " , "DAA          ", "JR Z,e       "    , "ADD HL,HL    " , "LD A,(HL+)   " , "DEC HL       "  , "INC L        "  , "DEC L        "  , "LD L,n       " , "CPL          ",
  "JR NC,e      "  , "LD SP,nn     ", "LD (HL-),A   " , "INC SP       "  , "INC (HL)     "  , "DEC (HL)     ", "LD (HL),n    " , "SCF          ", "JR C,e       "    , "ADD HL,SP    " , "LD A,(HL-)   " , "DEC SP       "  , "INC A        "  , "DEC A        "  , "LD A,n       " , "CCF          ",
  "LD B,B       "  , "LD B,C       ", "LD B,D       " , "LD B,E       "  , "LD B,H       "  , "LD B,L       ", "LD B,(HL)    " , "LD B,A       ", "LD C,B       "    , "LD C,C       " , "LD C,D       " , "LD C,E       "  , "LD C,H       "  , "LD C,L       "  , "LD C,(HL)    " , "LD C,A       ",
  "LD D,B       "  , "LD D,C       ", "LD D,D       " , "LD D,E       "  , "LD D,H       "  , "LD D,L       ", "LD D,(HL)    " , "LD D,A       ", "LD E,B       "    , "LD E,C       " , "LD E,D       " , "LD E,E       "  , "LD E,H       "  , "LD E,L       "  , "LD E,(HL)    " , "LD E,A       ",
  "LD H,B       "  , "LD H,C       ", "LD H,D       " , "LD H,E       "  , "LD H,H       "  , "LD H,L       ", "LD H,(HL)    " , "LD H,A       ", "LD L,B       "    , "LD L,C       " , "LD L,D       " , "LD L,E       "  , "LD L,H       "  , "LD L,L       "  , "LD L,(HL)    " , "LD L,A       ",
  "LD (HL),B    "  , "LD (HL),C    ", "LD (HL),D    " , "LD (HL),E    "  , "LD (HL),H    "  , "LD (HL),L    ", "HALT         " , "LD (HL),A    ", "LD A,B       "    , "LD A,C       " , "LD A,D       " , "LD A,E       "  , "LD A,H       "  , "LD A,L       "  , "LD A,(HL)    " , "LD A,A       ",
  "ADD A,B      "  , "ADD A,C      ", "ADD A,D      " , "ADD A,E      "  , "ADD A,H      "  , "ADD A,L      ", "ADD A,(HL)   " , "ADD A,A      ", "ADC A,B      "    , "ADC A,C      " , "ADC A,D      " , "ADC A,E      "  , "ADC A,H      "  , "ADC A,L      "  , "ADC A,(HL)   " , "ADC A,A      ",
  "SUB A,B      "  , "SUB A,C      ", "SUB A,D      " , "SUB A,E      "  , "SUB A,H      "  , "SUB A,L      ", "SUB A,(HL)   " , "SUB A,A      ", "SBC A,B      "    , "SBC A,C      " , "SBC A,D      " , "SBC A,E      "  , "SBC A,H      "  , "SBC A,L      "  , "SBC A,(HL)   " , "SBC A,A      ",
  "AND A,B      "  , "AND A,C      ", "AND A,D      " , "AND A,E      "  , "AND A,H      "  , "AND A,L      ", "AND A,(HL)   " , "AND A,A      ", "XOR A,B      "    , "XOR A,C      " , "XOR A,D      " , "XOR A,E      "  , "XOR A,H      "  , "XOR A,L      "  , "XOR A,(HL)   " , "XOR A,A      ",
  "OR A,B       "  , "OR A,C       ", "OR A,D       " , "OR A,E       "  , "OR A,H       "  , "OR A,L       ", "OR A,(HL)    " , "OR A,A       ", "CP A,B       "    , "CP A,C       " , "CP A,D       " , "CP A,E       "  , "CP A,H       "  , "CP A,L       "  , "CP A,(HL)    " , "CP A,A       ",
  "RET NZ       "  , "POP BC       ", "JP NZ,nn     " , "JP nn        "  , "CALL NZ,nn   "  , "PUSH BC      ", "ADD A,n      " , "RST 0x00     ", "RET Z        "    , "RET          " , "JP Z,nn      " , "CB op        "  , "CALL Z,nn    "  , "CALL nn      "  , "ADC A,n      " , "RST 0x08     ",
  "RET NC       "  , "POP DE       ", "JP NC,nn     " , "INVALID OP D3"  , "CALL NC,nn   "  , "PUSH DE      ", "SUB A,n      " , "RST 0x10     ", "RET C        "    , "RETI         " , "JP C,nn      " , "INVALID OP DB"  , "CALL C,nn    "  , "INVALID OP DD"  , "SBC A,n      " , "RST 0x18     ",
  "LDH (n),A    "  , "POP HL       ", "LD (C),A     " , "INVALID OP E3"  , "INVALID OP E4"  , "PUSH HL      ", "AND A,n      " , "RST 0x20     ", "ADD SP,e     "    , "JP HL        " , "LD (nn),A    " , "INVALID OP EB"  , "INVALID OP EC"  , "INVALID OP ED"  , "XOR A,n      " , "RST 0x28     ",
  "LDH A,(n)    "  , "POP AF       ", "LD A,(C)     " , "DI           "  , "INVALID OP F4"  , "PUSH AF      ", "OR A,n       " , "RST 0x30     ", "LD HL,SP+e   "    , "LD SP,HL     " , "LD A,(nn)    " , "EI           "  , "INVALID OP FC"  , "INVALID OP FD"  , "CP A,n       " , "RST 0x38     ",
};

const char cb_mnemonics[256][128] = {
  "RLC B        "  , "RLC C        ", "RLC D        " , "RLC E        "  , "RLC H        "  , "RLC L        ", "RLC (HL)     " , "RLC A        ","RRC B        "     , "RRC C        " , "RRC D        " , "RRC E        "  , "RRC H        "  , "RRC L        "  , "RRC (HL)     " , "RRC A        ",
  "RL  B        "  , "RL  C        ", "RL  D        " , "RL  E        "  , "RL  H        "  , "RL  L        ", "RL  (HL)     " , "RL  A        ","RR  B        "     , "RR  C        " , "RR  D        " , "RR  E        "  , "RR  H        "  , "RR  L        "  , "RR  (HL)     " , "RR  A        ",
  "SLA B        "  , "SLA C        ", "SLA D        " , "SLA E        "  , "SLA H        "  , "SLA L        ", "SLA (HL)     " , "SLA A        ","SRA B        "     , "SRA C        " , "SRA D        " , "SRA E        "  , "SRA H        "  , "SRA L        "  , "SRA (HL)     " , "SRA A        ",
  "SWAP B       "  , "SWAP C       ", "SWAP D       " , "SWAP E       "  , "SWAP H       "  , "SWAP L       ", "SWAP (L)     " , "SWAP A       ","SRL B        "     , "SRL C        " , "SRL D        " , "SRL E        "  , "SRL H        "  , "SRL L        "  , "SRL (HL)     " , "SRL A        ",
  "BIT 0,B      "  , "BIT 0,C      ", "BIT 0,D      " , "BIT 0,E      "  , "BIT 0,H      "  , "BIT 0,L      ", "BIT 0,(HL)   " , "BIT 0,A      ","BIT 1,B      "     , "BIT 1,C      " , "BIT 1,D      " , "BIT 1,E      "  , "BIT 1,H      "  , "BIT 1,L      "  , "BIT 1,(HL)   " , "BIT 1,A      ",
  "BIT 2,B      "  , "BIT 2,C      ", "BIT 2,D      " , "BIT 2,E      "  , "BIT 2,H      "  , "BIT 2,L      ", "BIT 2,(HL)   " , "BIT 2,A      ","BIT 3,B      "     , "BIT 3,C      " , "BIT 3,D      " , "BIT 3,E      "  , "BIT 3,H      "  , "BIT 3,L      "  , "BIT 3,(HL)   " , "BIT 3,A      ",
  "BIT 4,B      "  , "BIT 4,C      ", "BIT 4,D      " , "BIT 4,E      "  , "BIT 4,H      "  , "BIT 4,L      ", "BIT 4,(HL)   " , "BIT 4,A      ","BIT 5,B      "     , "BIT 5,C      " , "BIT 5,D      " , "BIT 5,E      "  , "BIT 5,H      "  , "BIT 5,L      "  , "BIT 5,(HL)   " , "BIT 5,A      ",
  "BIT 6,B      "  , "BIT 6,C      ", "BIT 6,D      " , "BIT 6,E      "  , "BIT 6,H      "  , "BIT 6,L      ", "BIT 6,(HL)   " , "BIT 6,A      ","BIT 7,B      "     , "BIT 7,C      " , "BIT 7,D      " , "BIT 7,E      "  , "BIT 7,H      "  , "BIT 7,L      "  , "BIT 7,(HL)   " , "BIT 7,A      ",
  "RES 0,B      "  , "RES 0,C      ", "RES 0,D      " , "RES 0,E      "  , "RES 0,H      "  , "RES 0,L      ", "RES 0,(HL)   " , "RES 0,A      ","RES 1,B      "     , "RES 1,C      " , "RES 1,D      " , "RES 1,E      "  , "RES 1,H      "  , "RES 1,L      "  , "RES 1,(HL)   " , "RES 1,A      ",
  "RES 2,B      "  , "RES 2,C      ", "RES 2,D      " , "RES 2,E      "  , "RES 2,H      "  , "RES 2,L      ", "RES 2,(HL)   " , "RES 2,A      ","RES 3,B      "     , "RES 3,C      " , "RES 3,D      " , "RES 3,E      "  , "RES 3,H      "  , "RES 3,L      "  , "RES 3,(HL)   " , "RES 3,A      ",
  "RES 4,B      "  , "RES 4,C      ", "RES 4,D      " , "RES 4,E      "  , "RES 4,H      "  , "RES 4,L      ", "RES 4,(HL)   " , "RES 4,A      ","RES 5,B      "     , "RES 5,C      " , "RES 5,D      " , "RES 5,E      "  , "RES 5,H      "  , "RES 5,L      "  , "RES 5,(HL)   " , "RES 5,A      ",
  "RES 6,B      "  , "RES 6,C      ", "RES 6,D      " , "RES 6,E      "  , "RES 6,H      "  , "RES 6,L      ", "RES 6,(HL)   " , "RES 6,A      ","RES 7,B      "     , "RES 7,C      " , "RES 7,D      " , "RES 7,E      "  , "RES 7,H      "  , "RES 7,L      "  , "RES 7,(HL)   " , "RES 7,A      ",
  "SET 0,B      "  , "SET 0,C      ", "SET 0,D      " , "SET 0,E      "  , "SET 0,H      "  , "SET 0,L      ", "SET 0,(HL)   " , "SET 0,A      ","SET 1,B      "     , "SET 1,C      " , "SET 1,D      " , "SET 1,E      "  , "SET 1,H      "  , "SET 1,L      "  , "SET 1,(HL)   " , "SET 1,A      ",
  "SET 2,B      "  , "SET 2,C      ", "SET 2,D      " , "SET 2,E      "  , "SET 2,H      "  , "SET 2,L      ", "SET 2,(HL)   " , "SET 2,A      ","SET 3,B      "     , "SET 3,C      " , "SET 3,D      " , "SET 3,E      "  , "SET 3,H      "  , "SET 3,L      "  , "SET 3,(HL)   " , "SET 3,A      ",
  "SET 4,B      "  , "SET 4,C      ", "SET 4,D      " , "SET 4,E      "  , "SET 4,H      "  , "SET 4,L      ", "SET 4,(HL)   " , "SET 4,A      ","SET 5,B      "     , "SET 5,C      " , "SET 5,D      " , "SET 5,E      "  , "SET 5,H      "  , "SET 5,L      "  , "SET 5,(HL)   " , "SET 5,A      ",
  "SET 6,B      "  , "SET 6,C      ", "SET 6,D      " , "SET 6,E      "  , "SET 6,H      "  , "SET 6,L      ", "SET 6,(HL)   " , "SET 6,A      ","SET 7,B      "     , "SET 7,C      " , "SET 7,D      " , "SET 7,E      "  , "SET 7,H      "  , "SET 7,L      "  , "SET 7,(HL)   " , "SET 7,A      "
};

const uint8_t instr_lengths[256] = {
  1 /* 00 */       ,3 /* 01 */      ,1 /* 02 */       ,1 /* 03 */        ,1 /* 04 */        ,1 /* 05 */      ,2 /* 06 */       ,1 /* 07 */      ,3 /* 08 */          ,1 /* 09 */       ,1 /* 0A */       ,1 /* 0B */        ,1 /* 0C */        ,1 /* 0D */        ,2 /* 0E */       ,1 /* 0F */,
  2 /* 10 */       ,3 /* 11 */      ,1 /* 12 */       ,1 /* 13 */        ,1 /* 14 */        ,1 /* 15 */      ,2 /* 16 */       ,1 /* 17 */      ,2 /* 18 */          ,1 /* 19 */       ,1 /* 1A */       ,1 /* 1B */        ,1 /* 1C */        ,1 /* 1D */        ,2 /* 1E */       ,1 /* 1F */,
  2 /* 20 */       ,3 /* 21 */      ,1 /* 22 */       ,1 /* 23 */        ,1 /* 24 */        ,1 /* 25 */      ,2 /* 26 */       ,1 /* 27 */      ,2 /* 28 */          ,1 /* 29 */       ,1 /* 2A */       ,1 /* 2B */        ,1 /* 2C */        ,1 /* 2D */        ,2 /* 2E */       ,1 /* 2F */,
  2 /* 30 */       ,3 /* 31 */      ,1 /* 32 */       ,1 /* 33 */        ,1 /* 34 */        ,1 /* 35 */      ,2 /* 36 */       ,1 /* 37 */      ,2 /* 38 */          ,1 /* 39 */       ,1 /* 3A */       ,1 /* 3B */        ,1 /* 3C */        ,1 /* 3D */        ,2 /* 3E */       ,1 /* 3F */,
  1 /* 40 */       ,1 /* 41 */      ,1 /* 42 */       ,1 /* 43 */        ,1 /* 44 */        ,1 /* 45 */      ,1 /* 46 */       ,1 /* 47 */      ,1 /* 48 */          ,1 /* 49 */       ,1 /* 4A */       ,1 /* 4B */        ,1 /* 4C */        ,1 /* 4D */        ,1 /* 4E */       ,1 /* 4F */,
  1 /* 50 */       ,1 /* 51 */      ,1 /* 52 */       ,1 /* 53 */        ,1 /* 54 */        ,1 /* 55 */      ,1 /* 56 */       ,1 /* 57 */      ,1 /* 58 */          ,1 /* 59 */       ,1 /* 5A */       ,1 /* 5B */        ,1 /* 5C */        ,1 /* 5D */        ,1 /* 5E */       ,1 /* 5F */,
  1 /* 60 */       ,1 /* 61 */      ,1 /* 62 */       ,1 /* 63 */        ,1 /* 64 */        ,1 /* 65 */      ,1 /* 66 */       ,1 /* 67 */      ,1 /* 68 */          ,1 /* 69 */       ,1 /* 6A */       ,1 /* 6B */        ,1 /* 6C */        ,1 /* 6D */        ,1 /* 6E */       ,1 /* 6F */,
  1 /* 70 */       ,1 /* 71 */      ,1 /* 72 */       ,1 /* 73 */        ,1 /* 74 */        ,1 /* 75 */      ,1 /* 76 */       ,1 /* 77 */      ,1 /* 78 */          ,1 /* 79 */       ,1 /* 7A */       ,1 /* 7B */        ,1 /* 7C */        ,1 /* 7D */        ,1 /* 7E */       ,1 /* 7F */,
  1 /* 80 */       ,1 /* 81 */      ,1 /* 82 */       ,1 /* 83 */        ,1 /* 84 */        ,1 /* 85 */      ,1 /* 86 */       ,1 /* 87 */      ,1 /* 88 */          ,1 /* 89 */       ,1 /* 8A */       ,1 /* 8B */        ,1 /* 8C */        ,1 /* 8D */        ,1 /* 8E */       ,1 /* 8F */,
  1 /* 90 */       ,1 /* 91 */      ,1 /* 92 */       ,1 /* 93 */        ,1 /* 94 */        ,1 /* 95 */      ,1 /* 96 */       ,1 /* 97 */      ,1 /* 98 */          ,1 /* 99 */       ,1 /* 9A */       ,1 /* 9B */        ,1 /* 9C */        ,1 /* 9D */        ,1 /* 9E */       ,1 /* 9F */,
  1 /* A0 */       ,1 /* A1 */      ,1 /* A2 */       ,1 /* A3 */        ,1 /* A4 */        ,1 /* A5 */      ,1 /* A6 */       ,1 /* A7 */      ,1 /* A8 */          ,1 /* A9 */       ,1 /* AA */       ,1 /* AB */        ,1 /* AC */        ,1 /* AD */        ,1 /* AE */       ,1 /* AF */,
  1 /* B0 */       ,1 /* B1 */      ,1 /* B2 */       ,1 /* B3 */        ,1 /* B4 */        ,1 /* B5 */      ,1 /* B6 */       ,1 /* B7 */      ,1 /* B8 */          ,1 /* B9 */       ,1 /* BA */       ,1 /* BB */        ,1 /* BC */        ,1 /* BD */        ,1 /* BE */       ,1 /* BF */,
  1 /* C0 */       ,1 /* C1 */      ,3 /* C2 */       ,3 /* C3 */        ,3 /* C4 */        ,1 /* C5 */      ,2 /* C6 */       ,1 /* C7 */      ,1 /* C8 */          ,1 /* C9 */       ,3 /* CA */       ,1 /* CB */        ,3 /* CC */        ,3 /* CD */        ,2 /* CE */       ,1 /* CF */,
  1 /* D0 */       ,1 /* D1 */      ,3 /* D2 */       ,0 /* D3 */        ,3 /* D4 */        ,1 /* D5 */      ,2 /* D6 */       ,1 /* D7 */      ,1 /* D8 */          ,1 /* D9 */       ,3 /* DA */       ,0 /* DB */        ,3 /* DC */        ,0 /* DD */        ,2 /* DE */       ,1 /* DF */,
  2 /* E0 */       ,1 /* E1 */      ,1 /* E2 */       ,0 /* E3 */        ,0 /* E4 */        ,1 /* E5 */      ,2 /* E6 */       ,1 /* E7 */      ,2 /* E8 */          ,1 /* E9 */       ,3 /* EA */       ,0 /* EB */        ,0 /* EC */        ,0 /* ED */        ,2 /* EE */       ,1 /* EF */,
  2 /* F0 */       ,1 /* F1 */      ,1 /* F2 */       ,1 /* F3 */        ,0 /* F4 */        ,1 /* F5 */      ,2 /* F6 */       ,1 /* F7 */      ,2 /* F8 */          ,1 /* F9 */       ,3 /* FA */       ,1 /* FB */        ,0 /* FC */        ,0 /* FD */        ,2 /* FE */       ,1 /* FF */
};

const uint8_t instruction_timing[256] = {
  1 /* 00 */       ,3 /* 01 */      ,2 /* 02 */       ,2 /* 03 */        ,1 /* 04 */        ,1 /* 05 */      ,2 /* 06 */       ,1 /* 07 */      ,5 /* 08 */          ,2 /* 09 */       ,2 /* 0A */       ,2 /* 0B */        ,1 /* 0C */        ,1 /* 0D */        ,2 /* 0E */       ,1 /* 0F */,
  1 /* 10 */       ,3 /* 11 */      ,2 /* 12 */       ,2 /* 13 */        ,1 /* 14 */        ,1 /* 15 */      ,2 /* 16 */       ,1 /* 17 */      ,2 /* 18 */          ,2 /* 19 */       ,2 /* 1A */       ,2 /* 1B */        ,1 /* 1C */        ,1 /* 1D */        ,2 /* 1E */       ,1 /* 1F */,
  2 /* 20 */       ,3 /* 21 */      ,2 /* 22 */       ,2 /* 23 */        ,1 /* 24 */        ,1 /* 25 */      ,2 /* 26 */       ,1 /* 27 */      ,2 /* 28 */          ,2 /* 29 */       ,2 /* 2A */       ,2 /* 2B */        ,1 /* 2C */        ,1 /* 2D */        ,2 /* 2E */       ,1 /* 2F */,
  2 /* 30 */       ,3 /* 31 */      ,2 /* 32 */       ,2 /* 33 */        ,3 /* 34 */        ,3 /* 35 */      ,3 /* 36 */       ,1 /* 37 */      ,2 /* 38 */          ,2 /* 39 */       ,2 /* 3A */       ,2 /* 3B */        ,1 /* 3C */        ,1 /* 3D */        ,2 /* 3E */       ,1 /* 3F */,
  1 /* 40 */       ,1 /* 41 */      ,1 /* 42 */       ,1 /* 43 */        ,1 /* 44 */        ,1 /* 45 */      ,2 /* 46 */       ,1 /* 47 */      ,1 /* 48 */          ,1 /* 49 */       ,1 /* 4A */       ,1 /* 4B */        ,1 /* 4C */        ,1 /* 4D */        ,2 /* 4E */       ,1 /* 4F */,
  1 /* 50 */       ,1 /* 51 */      ,1 /* 52 */       ,1 /* 53 */        ,1 /* 54 */        ,1 /* 55 */      ,2 /* 56 */       ,1 /* 57 */      ,1 /* 58 */          ,1 /* 59 */       ,1 /* 5A */       ,1 /* 5B */        ,1 /* 5C */        ,1 /* 5D */        ,2 /* 5E */       ,1 /* 5F */,
  1 /* 60 */       ,1 /* 61 */      ,1 /* 62 */       ,1 /* 63 */        ,1 /* 64 */        ,1 /* 65 */      ,2 /* 66 */       ,1 /* 67 */      ,1 /* 68 */          ,1 /* 69 */       ,1 /* 6A */       ,1 /* 6B */        ,1 /* 6C */        ,1 /* 6D */        ,2 /* 6E */       ,1 /* 6F */,
  2 /* 70 */       ,2 /* 71 */      ,2 /* 72 */       ,2 /* 73 */        ,2 /* 74 */        ,2 /* 75 */      ,1 /* 76 */       ,2 /* 77 */      ,1 /* 78 */          ,1 /* 79 */       ,1 /* 7A */       ,1 /* 7B */        ,1 /* 7C */        ,1 /* 7D */        ,2 /* 7E */       ,1 /* 7F */,
  1 /* 80 */       ,1 /* 81 */      ,1 /* 82 */       ,1 /* 83 */        ,1 /* 84 */        ,1 /* 85 */      ,2 /* 86 */       ,1 /* 87 */      ,1 /* 88 */          ,1 /* 89 */       ,1 /* 8A */       ,1 /* 8B */        ,1 /* 8C */        ,1 /* 8D */        ,2 /* 8E */       ,1 /* 8F */,
  1 /* 90 */       ,1 /* 91 */      ,1 /* 92 */       ,1 /* 93 */        ,1 /* 94 */        ,1 /* 95 */      ,2 /* 96 */       ,1 /* 97 */      ,1 /* 98 */          ,1 /* 99 */       ,1 /* 9A */       ,1 /* 9B */        ,1 /* 9C */        ,1 /* 9D */        ,2 /* 9E */       ,1 /* 9F */,
  1 /* A0 */       ,1 /* A1 */      ,1 /* A2 */       ,1 /* A3 */        ,1 /* A4 */        ,1 /* A5 */      ,2 /* A6 */       ,1 /* A7 */      ,1 /* A8 */          ,1 /* A9 */       ,1 /* AA */       ,1 /* AB */        ,1 /* AC */        ,1 /* AD */        ,2 /* AE */       ,1 /* AF */,
  1 /* B0 */       ,1 /* B1 */      ,1 /* B2 */       ,1 /* B3 */        ,1 /* B4 */        ,1 /* B5 */      ,2 /* B6 */       ,1 /* B7 */      ,1 /* B8 */          ,1 /* B9 */       ,1 /* BA */       ,1 /* BB */        ,1 /* BC */        ,1 /* BD */        ,2 /* BE */       ,1 /* BF */,
  2 /* C0 */       ,3 /* C1 */      ,3 /* C2 */       ,3 /* C3 */        ,3 /* C4 */        ,4 /* C5 */      ,2 /* C6 */       ,4 /* C7 */      ,2 /* C8 */          ,1 /* C9 */       ,3 /* CA */       ,3 /* CB */        ,3 /* CC */        ,3 /* CD */        ,2 /* CE */       ,4 /* CF */,
  2 /* D0 */       ,3 /* D1 */      ,3 /* D2 */       ,0 /* D3 */        ,3 /* D4 */        ,4 /* D5 */      ,2 /* D6 */       ,4 /* D7 */      ,2 /* D8 */          ,1 /* D9 */       ,3 /* DA */       ,0 /* DB */        ,3 /* DC */        ,0 /* DD */        ,2 /* DE */       ,4 /* DF */,
  3 /* E0 */       ,3 /* E1 */      ,2 /* E2 */       ,0 /* E3 */        ,0 /* E4 */        ,4 /* E5 */      ,2 /* E6 */       ,4 /* E7 */      ,4 /* E8 */          ,1 /* E9 */       ,4 /* EA */       ,0 /* EB */        ,0 /* EC */        ,0 /* ED */        ,2 /* EE */       ,4 /* EF */,
  3 /* F0 */       ,3 /* F1 */      ,2 /* F2 */       ,1 /* F3 */        ,0 /* F4 */        ,4 /* F5 */      ,2 /* F6 */       ,4 /* F7 */      ,3 /* F8 */          ,2 /* F9 */       ,4 /* FA */       ,1 /* FB */        ,0 /* FC */        ,0 /* FD */        ,2 /* FE */       ,4 /* FF */
};

// clang-format on

}    // namespace LR35902

#endif
