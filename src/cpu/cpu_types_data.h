#ifndef GAMEBOY_CPU_TYPES_DATA_H
#define GAMEBOY_CPU_TYPES_DATA_H
#include <cpu_const.h>
#include <stdint.h>

namespace LR35902 {

struct CPU_state_flags_t {
    uint8_t paddinging : 4;
    bool    carry : 1;
    bool    half_carry : 1;
    bool    subtract : 1;
    bool    zero : 1;
};

struct Register_file_t {
    struct CPU_state_flags_t flags;
    uint8_t                  A;
    uint8_t                  B;
    uint8_t                  C;
    uint8_t                  D;
    uint8_t                  E;
    uint8_t                  H;
    uint8_t                  L;
    uint16_t                 stack_pointer;
    uint16_t                 program_counter;
    bool                     cb_mode; /* Not part of the normal register file */
    int8_t                   cycle_time;
};

}    // namespace LR35902

#endif
