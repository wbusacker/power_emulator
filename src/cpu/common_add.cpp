#include <cpu_instructions_groups.h>

namespace LR35902 {

uint8_t common_add(CPU_state_flags_t* flags, uint8_t left_arg, uint8_t right_arg) {

    uint16_t result = 0;

    /* Calculate if a half-carry will occur */
    if (((left_arg & LR35902::NIBBLE_MASK) + (right_arg & LR35902::NIBBLE_MASK)) > LR35902::NIBBLE_MASK) {
        flags->half_carry = true;
    } else {
        flags->half_carry = false;
    }

    result = left_arg + right_arg;

    /* Check if a carry out would have occured */
    if (result > LR35902::BYTE_MASK) {
        flags->carry = true;
    } else {
        flags->carry = false;
    }

    /* Mask down the result */
    result &= LR35902::BYTE_MASK;

    if (result == 0) {
        flags->zero = true;
    } else {
        flags->zero = false;
    }

    /* Always set subtract zero, let the higher functions correct that */
    flags->subtract = false;

    return static_cast<uint8_t>(result);
}

}    // namespace LR35902