#include <cpu_instructions_groups.h>
#include <stdio.h>

namespace LR35902 {

void subtract(Common_memory::Common_memory_bus* memory, Register_file_t* registers, uint8_t instruction) {

    /* Figure out where we are getting our second argument from */
    uint8_t* arg_source = get_register_pointer(registers, instruction & 0x7);

    uint8_t arg_variable = 0;
    /* If the arg source is nullptr, that means we need to do an HL indirect */
    if (arg_source == nullptr) {
        /* Figure out if this is the immediate instruction or not */
        if (instruction == LR35902::SUB_N) {
            arg_variable = get_next_pc(memory, registers);
        } else {
            arg_variable = get_hl_indirect(memory, registers);
        }
    } else {
        arg_variable = *arg_source;
    }

    /* Let common subtract assert CPU Flags and always assign the result back into A */
    registers->A = common_subtract(&registers->flags, registers->A, arg_variable);
}

}    // namespace LR35902