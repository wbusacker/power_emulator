#include <cpu.h>
#include <stdio.h>

namespace LR35902 {

void Gameboy_cpu::print_cpu_state(void) {

    /* Display current cycles */
    printf("%9ld ", (cycles - 1) * 4);

    /* Display current CPU Flag States */
    printf("%s%s%s%s ",
           (registers.flags.zero == true) ? "Z" : "-",
           (registers.flags.subtract == true) ? "N" : "-",
           (registers.flags.half_carry == true) ? "H" : "-",
           (registers.flags.carry == true) ? "C" : "-");

    /* Display all of the registers */
    printf("A: %02X BC: %02X%02X DE: %02X%02X HL: %02X%02X ",
           registers.A,
           registers.B,
           registers.C,
           registers.D,
           registers.E,
           registers.H,
           registers.L);

    /* Print Stack Pointer and program counter */
    printf("SP = %04X PC = %04X ", registers.stack_pointer, last_address);

    printf("%14s", LR35902::mnemonics[last_instruction]);

    /* If there are any extra bytes for this instruction, go grab them */
    for (uint16_t i = 1; i < LR35902::instr_lengths[last_instruction]; i++) {
        printf("%02X ", memory->read(last_address + i));
    }

    printf("\n");
    fflush(stdout);
}

}    // namespace LR35902