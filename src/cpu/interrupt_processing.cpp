#include <cpu.h>

namespace LR35902 {

void Gameboy_cpu::interrupt_processing(void) {

    /* If the CPU has disabled interrupts or is still executing another instruction, don't bother checking */
    if ((master_interrupt_flag == true) && (registers.cycle_time <= 0)) {

        /* Prioritize interrupts */
        uint8_t active_interrupts =
            interrupt_pending_flags & interrupt_enable_flags & LR35902::SERVICABLE_INTERRUPT_MASK;

        if (active_interrupts != 0) {

            /* Push back the current program counter */

            memory->write(--(registers.stack_pointer),
                          (registers.program_counter >> LR35902::BITS_PER_BYTE) & LR35902::BYTE_MASK);
            memory->write(--(registers.stack_pointer), (registers.program_counter) & LR35902::BYTE_MASK);

            /* Test the bits in the active interrupts flag to check
                to see if that is what caused the interrupt and
                jump to that address
            */
            if ((active_interrupts & LR35902::VBLANK_INTERRUPT) != 0) {
                registers.program_counter = LR35902::VBLANK_INTERRUPT_TARGET;
                interrupt_pending_flags &= ~(LR35902::VBLANK_INTERRUPT);

            } else if ((active_interrupts & LR35902::LCDC_INTERRUPT) != 0) {
                registers.program_counter = LR35902::LCDC_INTERRUPT_TARGET;
                interrupt_pending_flags &= ~(LR35902::LCDC_INTERRUPT);

            } else if ((active_interrupts & LR35902::TIMER_INTERRUPT) != 0) {
                registers.program_counter = LR35902::TIMER_INTERRUPT_TARGET;
                interrupt_pending_flags &= ~(LR35902::TIMER_INTERRUPT);

            } else if ((active_interrupts & LR35902::SIO_INTERRUPT) != 0) {
                registers.program_counter = LR35902::SIO_INTERRUPT_TARGET;
                interrupt_pending_flags &= ~(LR35902::SIO_INTERRUPT);

            } else if ((active_interrupts & LR35902::CONTROLLER_INTERRUPT) != 0) {
                registers.program_counter = LR35902::CONTROLLER_INTERRUPT_TARGET;
                interrupt_pending_flags &= ~(LR35902::CONTROLLER_INTERRUPT);
            }

            /* Disable interrupts from being processed at this point */
            master_interrupt_flag = false;

            /* Since we're processing an interrupt, turn the CPU back on */
            cpu_active_flag = true;
        }
    }
}

}    // namespace LR35902