#include <GLFW/glfw3.h>
#include <window_manager.h>

namespace Display {

void* Window_manager::render_thread(void* arg) {

    /* Cast the argument back to the local object */
    Display::Window_manager* gd = reinterpret_cast<Display::Window_manager*>(arg);

    gd->initialize_window();

    /* Check if initialization went ok */
    if (gd->close_thread == true) {
        return NULL;
    }

    /* Tell the main thread that we're entering periodic processing */
    gd->thread_active = true;

    while (gd->thread_active == true) {

        /* To prevent screen tearing, prevent this thread from drawing
            while we're in the middle of updating a frame
        */
        while (gd->draw_frame == false)
            ;
        gd->draw_frame = false;

        gd->render();
    }

    gd->thread_active = false;

    return NULL;
}

}    // namespace Display