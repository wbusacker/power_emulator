#include <stdio.h>
#include <window_manager.h>

namespace Display {

static Window_manager* win_manager;

void Window_manager::set_callback_global(void) {
    win_manager = this;
}

void Window_manager::keyboard_input_callback(GLFWwindow* win, int key, int scancode, int action, int mods) {

    /* Build a struct with all of the keyboard data */
    struct Keyboard_event input;
    input.key    = key;
    input.action = action;

    /* Pass the key to each of the interfaces to see what they want to do with it */
    for (uint16_t i = 0; i < win_manager->num_keyboard_interfaces; i++) {
        win_manager->keyboard_interfaces[i]->keyboard_input(input);
    }
}

}    // namespace Display