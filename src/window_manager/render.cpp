#include <GLFW/glfw3.h>
#include <window_manager.h>

namespace Display {

void Window_manager::render(void) {

    /* Ensure that we have the global window lock */
    pthread_mutex_lock(&global_window_lock);

    if ((! glfwWindowShouldClose(window)) && (close_thread == false)) {

        glfwMakeContextCurrent(window);

        glClear(GL_COLOR_BUFFER_BIT);

        /* Draw all of the pixels */
        for (uint32_t r = 0; r < pixel_rows; r++) {
            for (uint32_t c = 0; c < pixel_columns; c++) {
                struct Display::Pixel* pixel = &(frame_buffer[r][c]);

                glColor3f(pixel->color.red, pixel->color.green, pixel->color.blue);
                glRectf(pixel->lower_left_point.x,
                        pixel->lower_left_point.y,
                        pixel->upper_right_point.x,
                        pixel->upper_right_point.y);
            }
        }

        // glfwSwapBuffers(window);
        glFlush();

        if (poll_for_input == true) {
            glfwPollEvents();
        }
    } else {
        thread_active = false;
        close_thread  = true;
    }

    pthread_mutex_unlock(&global_window_lock);
}

}    // namespace Display