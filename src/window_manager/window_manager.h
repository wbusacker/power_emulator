#ifndef GAMEBOY_DISPLAY_H
#define GAMEBOY_DISPLAY_H
#include <GLFW/glfw3.h>
#include <common_memory_bus_types_data.h>
#include <window_manager_const.h>
#include <window_manager_types_data.h>

#ifdef __GNUC__
    #include <pthread.h>
#endif

namespace Display {

class Window_manager {
    public:
    Window_manager(uint64_t rows, uint64_t columns, uint8_t scaling, bool poll_input, const char* const window_name);
    virtual ~Window_manager(void);

    inline void set_pixel_color(uint32_t row, uint32_t col, double red, double green, double blue) {
        frame_buffer[row][col].color.red   = red;
        frame_buffer[row][col].color.green = green;
        frame_buffer[row][col].color.blue  = blue;
    }

    bool window_closed(void) { return thread_active; }

    void signal_frame(void);
    void render(void);

    void register_keyboard_interface(Keyboard_input_interface* interface);

    void disable_polling(void) { poll_for_input = false; }

    private:
    void initialize_window(void);

    void        set_callback_global(void);
    static void keyboard_input_callback(GLFWwindow* win, int key, int scancode, int action, int mods);

    volatile bool close_thread;
    volatile bool thread_active;
    volatile bool draw_frame;
    GLFWwindow*   window;

    static pthread_mutex_t global_window_lock;
    static bool            gwl_initialized;

    uint64_t pixel_columns;
    uint64_t pixel_rows;
    uint8_t  pixels_per_display;

    Keyboard_input_interface* keyboard_interfaces[NUM_KEYBOARD_INTERFACES];
    uint16_t                  num_keyboard_interfaces;

    static void* render_thread(void* arg);

    struct Pixel frame_buffer[1024][1024];

    bool poll_for_input;

    const char* const window_name;

    uint64_t successful_frames;
    uint64_t dropped_frames;

#ifdef __GNUC__
    pthread_t render_thread_handle;
#endif
};

}    // namespace Display

#endif
