#ifndef GAMEBOY_DISPLAY_TYPES_DATA_H
#define GAMEBOY_DISPLAY_TYPES_DATA_H
#include <window_manager_const.h>

namespace Display {

struct OpenGL_coordinate {
    double x;
    double y;
};

struct OpenGL_color {
    double red;
    double green;
    double blue;
};

struct Pixel {
    struct OpenGL_coordinate lower_left_point;
    struct OpenGL_coordinate upper_right_point;
    struct OpenGL_color      color;
};

struct Keyboard_event {
    int key;
    int action;
};

struct Keyboard_input_buffer {
    uint16_t              buffer_head;
    uint16_t              buffer_tail;
    struct Keyboard_event events[Display::NUM_KEYBOARD_INTERFACES];
};

class Keyboard_input_interface {
    public:
    Keyboard_input_interface() { }

    virtual void keyboard_input(const struct Keyboard_event event) { }
};

}    // namespace Display

#endif
