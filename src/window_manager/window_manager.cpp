#include <stdio.h>
#include <window_manager.h>
namespace Display {

pthread_mutex_t Window_manager::global_window_lock;
bool            Window_manager::gwl_initialized = false;

Window_manager::Window_manager(uint64_t          rows,
                               uint64_t          columns,
                               uint8_t           scaling,
                               bool              poll_input,
                               const char* const window_name) :
    window_name(window_name) {

    poll_for_input = poll_input;

    pixel_columns      = columns;
    pixel_rows         = rows;
    pixels_per_display = scaling;

    /* Initialize where all of the pixels go */
    double display_pixel_width  = 2.0 / pixel_columns;
    double display_pixel_height = 2.0 / pixel_rows;

    for (uint16_t r = 0; r < pixel_rows; r++) {
        for (uint16_t c = 0; c < pixel_columns; c++) {
            /* Get an easier access pointer to the pixel */
            struct Display::Pixel* pixel = &(frame_buffer[r][c]);

            /* OpenGL's coordinate frame is different from how the gameboy counts.
                OpenGL uses a normal cartesian with bottom left at -1, -1 but the
                Gameboy uses a y-inverted cartesian with top left as 0,0. So when
                we're calculating each individual pixel's actual location, we have
                to re-organize how things are shaped
            */

            double llx = (c * display_pixel_width) - 1;
            double urx = llx + display_pixel_width;

            double ury = ((r * display_pixel_height) - 1) * -1;
            double lly = ury - display_pixel_height;

            pixel->lower_left_point.x  = llx;
            pixel->lower_left_point.y  = lly;
            pixel->upper_right_point.x = urx;
            pixel->upper_right_point.y = ury;

            pixel->color.blue  = 1;
            pixel->color.green = 0;
            pixel->color.red   = 0;
        }
    }

    /* Open render window */

    close_thread  = false;
    thread_active = false;

    /* Do different things here depending on if we're on Windows vs Linux */

#ifdef __GNUC__

    /* GCC Linux Compiler */

    /* Launch the render thread */

    int ret_val = pthread_create(&render_thread_handle, NULL, Window_manager::render_thread, this);

    /* Check if the thread launched correctly */
    if (ret_val != 0) {
        perror("Failed to launch render thread\n");
    } else {

        /* Watch for either close thread or thread active change state */

        while ((close_thread == false) && (thread_active == false))
            ;

        /* If close thread is set true, then we know something went wrong */

        if (close_thread == true) {
            /* Something went wrong during window initialization, back out */
            pthread_join(render_thread_handle, NULL);
        }
    }

    if (gwl_initialized == false) {
        int ret_val     = pthread_mutex_init(&global_window_lock, NULL);
        gwl_initialized = true;
    }

#else

    /* MSVC Windows Compiler */
    initialize_window();

#endif

    for (uint16_t i = 0; i < NUM_KEYBOARD_INTERFACES; i++) {
        keyboard_interfaces[i] = nullptr;
    }

    num_keyboard_interfaces = 0;

    dropped_frames    = 0;
    successful_frames = 0;
}

Window_manager::~Window_manager(void) {

    /* Tell the thread to exit */

    close_thread = true;
    draw_frame   = true;
#ifdef __GNUC__

    pthread_join(render_thread_handle, NULL);

    printf("%s frame drop ratio %6.2f%%\n",
           window_name,
           (100.0 * dropped_frames) / (dropped_frames + successful_frames));

#endif
    pthread_mutex_lock(&global_window_lock);
    glfwTerminate();
    pthread_mutex_unlock(&global_window_lock);
}

void Window_manager::initialize_window(void) {
    pthread_mutex_lock(&global_window_lock);
    if (! glfwInit()) {
        close_thread = true;
        printf("Failed to init GLFW\n");
        return;
    }

    glfwWindowHint(GLFW_DOUBLEBUFFER, GL_FALSE);
    window =
        glfwCreateWindow(pixel_columns * pixels_per_display, pixel_rows * pixels_per_display, window_name, NULL, NULL);

    if (window == NULL) {
        glfwTerminate();
        close_thread = true;
        printf("Failed to create window\n");
        return;
    }

    thread_active = true;

    if (poll_for_input == true) {
        set_callback_global();
        glfwSetKeyCallback(window, Window_manager::keyboard_input_callback);
    }
    pthread_mutex_unlock(&global_window_lock);
}

void Window_manager::register_keyboard_interface(Keyboard_input_interface* interface) {
    if (num_keyboard_interfaces < NUM_KEYBOARD_INTERFACES) {
        keyboard_interfaces[num_keyboard_interfaces] = interface;
        num_keyboard_interfaces++;
    }
}

}    // namespace Display
