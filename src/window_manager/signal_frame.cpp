#include <GLFW/glfw3.h>
#include <window_manager.h>

namespace Display {

void Window_manager::signal_frame(void) {

#ifdef __GNUC__

    if (draw_frame == true) {
        dropped_frames++;
    } else {
        successful_frames++;
    }

    draw_frame = true;

#else

    render();

#endif
}

}    // namespace Display