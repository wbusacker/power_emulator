#ifndef GP_TIMERS_H
#define GP_TIMERS_H
#include <common_memory_bus.h>
#include <cpu.h>
#include <gp_timers_const.h>
#include <gp_timers_types_data.h>

namespace Time {

class GP_timers : public Common_memory::CMB_interface {
    public:
    GP_timers(LR35902::Gameboy_cpu* cpu_core);

    void cycle(void);

    uint8_t read(uint16_t addr);
    void    write(uint16_t addr, uint8_t data);

    private:
    void                  buffer_write(void);
    LR35902::Gameboy_cpu* cpu;

    uint16_t master_counter;

    uint16_t timer_period;
    uint8_t  timer_count;
    uint8_t  timer_reload;
    uint8_t  timer_active;
    uint8_t  divider_count;

    uint16_t buffer_addr;
    uint16_t buffer_data;
    int      buffer_cycles;

    bool rollover;
};

}    // namespace Time

#endif
