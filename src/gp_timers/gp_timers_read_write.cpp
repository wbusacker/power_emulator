#include <gp_timers.h>

namespace Time {

uint8_t GP_timers::read(uint16_t addr) {
    /*
        Due to how the CPU core is implemented, reads and writes
        are instantaneous which on actual GB hardware, they aren't
        This causes some funiness with the timers which is taken into
        account here as the simpler solution. Reads should be delayed
        by effectively two clock cycles
    */

    uint16_t time_corrected_timer_modulo   = (master_counter % timer_period) + Time::READ_DELAY;
    uint16_t time_corrected_divider_modulo = (master_counter % Time::DIVIDER_PERIOD) + Time::READ_DELAY;

    uint8_t time_corrected_timer_count   = timer_count;
    uint8_t time_corrected_divider_count = divider_count;

    if (time_corrected_timer_modulo > timer_period) {
        time_corrected_timer_count++;
    }
    if (time_corrected_divider_modulo > Time::DIVIDER_PERIOD) {
        time_corrected_divider_count++;
    }

    uint8_t data = 0;

    switch (addr) {
        case Time::TIMER_DIVIDER_ADDRESS:
            data = time_corrected_divider_count;
            break;
        case Time::TIMER_COUNTER_ADDRESS:

            data = time_corrected_timer_count;
            break;
        case Time::TIMER_RELOAD_ADDRESS:
            data = timer_reload;
            break;
        case Time::TIMER_CONTROL_ADDRESS:
            data |= timer_active << Time::TIMER_CONTROL_BIT;
            switch (timer_period) {
                case Time::TIMER_PERIOD_0:
                    data |= Time::TIMER_PERIOD_0_ID;
                    break;
                case Time::TIMER_PERIOD_1:
                    data |= Time::TIMER_PERIOD_1_ID;
                    break;
                case Time::TIMER_PERIOD_2:
                    data |= Time::TIMER_PERIOD_2_ID;
                    break;
                case Time::TIMER_PERIOD_3:
                    data |= Time::TIMER_PERIOD_3_ID;
                    break;
            }
            break;
    }

    return data;
}

void GP_timers::write(uint16_t addr, uint8_t data) {

    buffer_addr   = addr;
    buffer_data   = data;
    buffer_cycles = 1;
}

void GP_timers::buffer_write(void) {

    switch (buffer_addr) {
        case Time::TIMER_DIVIDER_ADDRESS:
            master_counter = 0;
            break;
        case Time::TIMER_COUNTER_ADDRESS:
            timer_count = buffer_data;
            break;
        case Time::TIMER_RELOAD_ADDRESS:
            timer_reload = buffer_data;
            break;
        case Time::TIMER_CONTROL_ADDRESS:
            timer_active = (buffer_data >> Time::TIMER_CONTROL_BIT) & 1;
            switch (buffer_data & Time::TIMER_FREQUENCY_MASK) {
                case Time::TIMER_PERIOD_0_ID:
                    timer_period = Time::TIMER_PERIOD_0;
                    break;
                case Time::TIMER_PERIOD_1_ID:
                    timer_period = Time::TIMER_PERIOD_1;
                    break;
                case Time::TIMER_PERIOD_2_ID:
                    timer_period = Time::TIMER_PERIOD_2;
                    break;
                case Time::TIMER_PERIOD_3_ID:
                    timer_period = Time::TIMER_PERIOD_3;
                    break;
            }
            break;
    }
}

}    // namespace Time