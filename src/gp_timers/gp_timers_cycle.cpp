#include <gp_timers.h>

namespace Time {

void GP_timers::cycle(void) {

    if (buffer_cycles == 0) {
        buffer_write();
    }

    if (buffer_cycles >= 0) {
        buffer_cycles--;
    }

    master_counter++;

    if ((master_counter % Time::DIVIDER_PERIOD) == 0) {
        divider_count++;
    }

    if (timer_active == 1) {

        if ((master_counter % timer_period) == 0) {
            timer_count++;

            /* If we just increased into zero, we overflowed */
            if (timer_count == 0) {

                /* Trigger the interrupt */
                cpu->raise_interrupt(LR35902::TIMER_INTERRUPT);

                /* Set the rollover flag */
                rollover = true;
            }

            if ((rollover == true) && (timer_count == 1)) {
                timer_count = timer_reload;
                rollover    = false;
            }
        }
    }
}

}    // namespace Time