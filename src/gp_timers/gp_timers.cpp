#include <gp_timers.h>

namespace Time {

GP_timers::GP_timers(LR35902::Gameboy_cpu* cpu_core) {

    cpu = cpu_core;

    master_counter = 0;

    timer_period = Time::TIMER_PERIOD_0;
    timer_count  = 0;
    timer_reload = 0;
    timer_active = 0;

    buffer_addr   = 0;
    buffer_data   = 0;
    buffer_cycles = -1;

    rollover = false;
}

}    // namespace Time
