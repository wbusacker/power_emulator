#ifndef POWER_EMULATOR_H
#define POWER_EMULATOR_H
#include <common_memory_bus.h>
#include <debugger.h>
#include <function_tracer.h>
#include <power_emulator_const.h>
#include <power_emulator_types_data.h>
#include <vector>
#include <window_manager.h>

namespace Power {

class Power_emulator {
    public:
    Power_emulator(Power::Emulation_modes mode, char* game_file_name);
    ~Power_emulator();

    void execute(void);

    private:
    bool stable;

    void                    configure_powerboy(char* filename);
    Debug::Function_tracer* tracer;
    Debug::Debugger*        debugger;

    Common_memory::Common_memory_bus* main_memory_bus;

    std::vector<Common_memory::CMB_interface*> bus_interfaces;

    Display::Window_manager* main_display;
};

}    // namespace Power

#endif
