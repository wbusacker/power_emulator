#include <power_emulator.h>
#include <stdio.h>

namespace Power {

void Power_emulator::execute(void) {

    /* Cycle the memory bus forever */

    while (main_display->window_closed()) {
        main_memory_bus->process_clock_cycle();
        // printf("\n");
    }
}

}    // namespace Power