#include <cartridge.h>
#include <com_port.h>
#include <controller.h>
#include <cpu.h>
#include <gp_timers.h>
#include <picture_processing_unit.h>
#include <power_emulator.h>
#include <ram_block.h>
#include <sound_controller.h>
#include <stdio.h>

namespace Power {

void Power_emulator::configure_powerboy(char* filename) {

    /* Create the main memory bus */
    main_memory_bus = new Common_memory::Common_memory_bus(Power::POWERBOY_CLOCK_SPEED);
    main_display    = new Display::Window_manager(Display::GAMEBOY_SCREEN_ROW_COUNT,
                                               Display::GAMEBOY_SCREEN_COL_COUNT,
                                               3,
                                               true,
                                               Display::MAIN_DISPLAY_WINDOW_NAME);
    // debugger_display = new Display::Window_manager(Display::GAMEBOY_SCREEN_ROW_COUNT,
    // Display::GAMEBOY_SCREEN_COL_COUNT);

    /* Start creating all of the individual objects needed */
    Cart::Gameboy_cartridge* cart         = new Cart::Gameboy_cartridge(filename);
    LR35902::Gameboy_cpu*    cpu          = new LR35902::Gameboy_cpu(main_memory_bus);
    Serial::Com_port*        com          = new Serial::Com_port(stdout, stdin);
    RAM::Ram_block*          vram         = new RAM::Ram_block(RAM::VRAM_START, RAM::VRAM_SIZE);
    RAM::Ram_block*          internal_ram = new RAM::Ram_block(RAM::INTERNAL_RAM_START, RAM::INTERNAL_RAM_SIZE);
    RAM::Ram_block*          high_ram     = new RAM::Ram_block(RAM::HIGH_RAM_START, RAM::HIGH_RAM_SIZE);
    Time::GP_timers*         timer        = new Time::GP_timers(cpu);
    Input::Controller*       controller   = new Input::Controller(cpu);
    Display::Gameboy_picture_processing_unit* ppu =
        new Display::Gameboy_picture_processing_unit(main_memory_bus, main_display, cpu);
    Audio::Sound_controller* sound = new Audio::Sound_controller();

    /* Only do further stuff if the cartridge loaded correctly */
    if (cart->get_load_status()) {
        /* Place all of the pointers onto the vector list */
        bus_interfaces.push_back(cart);
        bus_interfaces.push_back(cpu);
        bus_interfaces.push_back(com);
        bus_interfaces.push_back(vram);
        bus_interfaces.push_back(internal_ram);
        bus_interfaces.push_back(high_ram);
        bus_interfaces.push_back(ppu);
        bus_interfaces.push_back(timer);
        bus_interfaces.push_back(controller);
        bus_interfaces.push_back(sound);

        /* Attach all of the memory spaces

            Memory Map

            Interrupt Enable Register       FFFF    65535
            Internal RAM                    FF80    65408
            Unused                          FF4C
            IO Ports                        FF00
            Unused                          FEA0
            OAM                             FE00
            ECHO Ram                        E000
            Internal Ram                    C000
            Swap Ram                        A000
            VRAM                            8000
            Swap Rom                        4000
            Rom Bank 0                      0000
         */

        main_memory_bus->register_memory_module(cpu, LR35902::INTERRUPT_ENABLE_ADDR);
        main_memory_bus->register_memory_module(high_ram, RAM::HIGH_RAM_START, RAM::HIGH_RAM_END);
        main_memory_bus->register_memory_module(ppu,
                                                Display::CONTROL_REGISTER_START_ADDR,
                                                Display::CONTROL_REGISTER_END_ADDR);
        main_memory_bus->register_memory_module(sound, 0xFF10, 0xFF40);
        main_memory_bus->register_memory_module(cpu, LR35902::INTERRUPT_FLAG_ADDR);
        main_memory_bus->register_memory_module(timer,
                                                Time::CONTROL_REGISTER_START_ADDR,
                                                Time::CONTROL_REGISTER_END_ADDR);

        main_memory_bus->register_memory_module(com, Serial::COM_MEMORY_START, Serial::COM_MEMORY_END);
        main_memory_bus->register_memory_module(controller, Input::JOYPAD_REGISTER_ADDR);

        main_memory_bus->register_memory_module(ppu, Display::OAM_START_ADDR, Display::OAM_END_ADDR);
        main_memory_bus->register_memory_module(internal_ram, RAM::INTERNAL_RAM_START, RAM::INTERNAL_MIRROR_RAM_END);
        main_memory_bus->register_memory_module(cart,
                                                Cart::Gameboy::CARTRIDGE_RAM_START,
                                                Cart::Gameboy::CARTRIDGE_RAM_END);
        main_memory_bus->register_memory_module(ppu, RAM::VRAM_START, RAM::VRAM_END);
        main_memory_bus->register_memory_module(cart,
                                                Cart::Gameboy::CARTRIDGE_ROM_START,
                                                Cart::Gameboy::CARTRIDGE_ROM_END);

        /* Attach all modules that need to be cycled during execution

            We need to ensure that the timer system cycles first
         */
        main_memory_bus->register_cycle_module(timer);
        main_memory_bus->register_cycle_module(cpu);
        main_memory_bus->register_cycle_module(ppu);
        main_memory_bus->register_cycle_module(sound);

        main_display->register_keyboard_interface(controller);
        stable = true;

        // tracer = new Debug::Function_tracer(cart->get_controller(), main_memory_bus);
        // cpu->install_function_tracer(tracer);

        // debugger = new Debug::Debugger(cart->get_controller(), main_memory_bus, cpu);
        // main_memory_bus->register_cycle_module(debugger);

    } else {
        printf("Failed to initialize Power Emulator\n");
        fflush(stdout);
        stable = false;
    }
}

}    // namespace Power