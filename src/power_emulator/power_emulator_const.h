#ifndef POWER_EMULATOR_CONST_H
#define POWER_EMULATOR_CONST_H

namespace Power {

const uint64_t POWERBOY_CLOCK_SPEED = 1048576;

}

#endif
