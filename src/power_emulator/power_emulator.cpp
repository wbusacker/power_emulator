#include <power_emulator.h>
#include <stdio.h>

namespace Power {

Power_emulator::Power_emulator(Power::Emulation_modes mode, char* game_file_name) {

    switch (mode) {
        case Power::POWERBOY:
            configure_powerboy(game_file_name);
            break;
    }
    tracer   = nullptr;
    debugger = nullptr;
}

Power_emulator::~Power_emulator(void) {
    if (tracer != nullptr) {
        printf("Tracer points to %lX\n", reinterpret_cast<uint64_t>(tracer));
        delete tracer;
    }
    if (debugger != nullptr) {
        delete debugger;
    }
    /* Iterate through all of the modules and delete them all */
    for (uint8_t i = 0; i < bus_interfaces.size(); i++) {
        delete bus_interfaces[i];
    }

    /* Lastely delete the memory bus itself */
    delete main_memory_bus;

    delete main_display;
}

}    // namespace Power
