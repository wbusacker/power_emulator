#include <ram_block.h>
#include <string.h>

namespace RAM {

Ram_block::Ram_block(uint16_t base_addr, uint16_t size) {

    ram_bank = new uint8_t[size];
    length   = size;

    start_addr = base_addr;

    memset(ram_bank, 0, size);
}

Ram_block::~Ram_block(void) {
    delete ram_bank;
}

}    // namespace RAM
