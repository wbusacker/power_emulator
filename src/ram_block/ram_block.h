#ifndef RAM_BLOCK_H
#define RAM_BLOCK_H
#include <common_memory_bus_types_data.h>
#include <ram_block_const.h>
#include <ram_block_types_data.h>

namespace RAM {

class Ram_block : public Common_memory::CMB_interface {
    public:
    Ram_block(uint16_t base_addr, uint16_t size);
    virtual ~Ram_block(void);

    uint8_t read(uint16_t addr);
    void    write(uint16_t addr, uint8_t data);

    private:
    uint8_t* ram_bank;

    uint16_t start_addr;
    uint16_t length;
};

}    // namespace RAM

#endif
