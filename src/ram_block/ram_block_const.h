#ifndef RAM_BLOCK_CONST_H
#define RAM_BLOCK_CONST_H

#include <stdint.h>

namespace RAM {

const uint16_t VRAM_START                = 0x8000;
const uint16_t VRAM_END                  = 0xA000;
const uint16_t INTERNAL_RAM_START        = 0xC000;
const uint16_t INTERNAL_RAM_END          = 0xE000;
const uint16_t INTERNAL_MIRROR_RAM_START = 0xE000;
const uint16_t INTERNAL_MIRROR_RAM_END   = 0xFE00;
const uint16_t SPRITE_RAM_START          = 0xFE00;
const uint16_t SPRITE_RAM_END            = 0xFEA0;
const uint16_t HIGH_RAM_START            = 0xFF80;
const uint16_t HIGH_RAM_END              = 0xFFFF;

const uint16_t VRAM_SIZE                = 0x2000;
const uint16_t INTERNAL_RAM_SIZE        = 0x2000;
const uint16_t INTERNAL_MIRROR_RAM_SIZE = 0x1E00;
const uint16_t SPRITE_RAM_SIZE          = 0xA0;
const uint16_t HIGH_RAM_SIZE            = 0x80;

}    // namespace RAM

#endif
