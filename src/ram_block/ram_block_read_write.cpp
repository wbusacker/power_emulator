#include <ram_block.h>

namespace RAM {

uint8_t Ram_block::read(uint16_t addr) {
    uint32_t access_index = (addr - start_addr) % length;
    uint8_t  return_data  = ram_bank[access_index];
    return return_data;
}

void Ram_block::write(uint16_t addr, uint8_t data) {
    uint32_t access_index  = (addr - start_addr) % length;
    ram_bank[access_index] = data;
}

}    // namespace RAM